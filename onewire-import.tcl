# check if the environment variable is set
if [info exists cce_repo_root] {
    puts "cce_repo_root is set to $cce_repo_root"
} else {
    puts "cce_repo_root is not set"
    exit 1
}

# import source files
add_files "${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_regs.vhd\
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_regs_const_pkg.vhd\
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_regs_axi.vhd\
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/axi_onewire.vhd\ 
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire.vhd\
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_idtemp.vhd\
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_interface.vhd\
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_discover.vhd\
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_idtemp_pkg.vhd\
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_control.vhd\
 ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_crc.vhd\
 ${cce_repo_root}/common/vhd/general-cores/wishbone/wishbone_pkg.vhd\
 ${cce_repo_root}/common/vhd/packages/lfsr/src/rtl/lfsr_pkg.vhd\
 ${cce_repo_root}/common/vhd/memory/two_port_ram/src/rtl/two_port_ram_tmr.vhd\
 ${cce_repo_root}/common/vhd/generic/mem_data_triplicator/src/rtl/mem_data_triplicator_rd_only.vhd\
 ${cce_repo_root}/common/vhd/generic/mem_data_triplicator/src/rtl/mem_data_triplicator_wr_only.vhd\
 ${cce_repo_root}/common/vhd/memory/two_port_ram/src/rtl/two_port_ram.vhd\
 ${cce_repo_root}/common/vhd/generic/mem_data_triplicator/src/rtl/mem_data_triplicator/mem_data_triplicator_addr.vhd\
 ${cce_repo_root}/common/vhd/generic/mem_data_triplicator/src/rtl/mem_data_triplicator/mem_data_triplicator_wr.vhd\
 ${cce_repo_root}/common/vhd/generic/mem_data_triplicator/src/rtl/mem_data_triplicator/mem_data_triplicator_rd.vhd"

 set_property file_type {VHDL 2008} [get_files  ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire_interface.vhd]
 set_property file_type {VHDL 2008} [get_files  ${cce_repo_root}/common/vhd/interfaces/onewire/rtl/onewire.vhd]

 set_property -dict [list CONFIG.clk_frequency_g {100000000} CONFIG.max_devices_g {32}] [get_bd_cells axi_onewire]