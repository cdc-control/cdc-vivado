# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "IOSTANDARD_G" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SS_SIZE_G" -parent ${Page_0}


}

proc update_PARAM_VALUE.IOSTANDARD_G { PARAM_VALUE.IOSTANDARD_G } {
	# Procedure called to update IOSTANDARD_G when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IOSTANDARD_G { PARAM_VALUE.IOSTANDARD_G } {
	# Procedure called to validate IOSTANDARD_G
	return true
}

proc update_PARAM_VALUE.SS_SIZE_G { PARAM_VALUE.SS_SIZE_G } {
	# Procedure called to update SS_SIZE_G when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SS_SIZE_G { PARAM_VALUE.SS_SIZE_G } {
	# Procedure called to validate SS_SIZE_G
	return true
}


proc update_MODELPARAM_VALUE.SS_SIZE_G { MODELPARAM_VALUE.SS_SIZE_G PARAM_VALUE.SS_SIZE_G } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SS_SIZE_G}] ${MODELPARAM_VALUE.SS_SIZE_G}
}

proc update_MODELPARAM_VALUE.IOSTANDARD_G { MODELPARAM_VALUE.IOSTANDARD_G PARAM_VALUE.IOSTANDARD_G } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IOSTANDARD_G}] ${MODELPARAM_VALUE.IOSTANDARD_G}
}

