package onewire_regs_Consts is
  constant ONEWIRE_REGS_SIZE : Natural := 2048;
  constant ADDR_ONEWIRE_REGS_MUX : Natural := 16#0#;
  constant ONEWIRE_REGS_MUX_EN_OFFSET : Natural := 0;
  constant ONEWIRE_REGS_MUX_CTRL_OFFSET : Natural := 1;
  constant ADDR_ONEWIRE_REGS_CONTROL : Natural := 16#4#;
  constant ONEWIRE_REGS_CONTROL_TRIG_OFFSET : Natural := 0;
  constant ONEWIRE_REGS_CONTROL_RST_OFFSET : Natural := 1;
  constant ADDR_ONEWIRE_REGS_STATUS : Natural := 16#8#;
  constant ONEWIRE_REGS_STATUS_BUSY_OFFSET : Natural := 0;
  constant ONEWIRE_REGS_STATUS_DONE_OFFSET : Natural := 1;
  constant ONEWIRE_REGS_STATUS_COUNT_OFFSET : Natural := 2;
  constant ONEWIRE_REGS_STATUS_DEV_TOO_MANY_OFFSET : Natural := 8;
  constant ADDR_ONEWIRE_REGS_MEM_READOUT : Natural := 16#400#;
  constant ONEWIRE_REGS_MEM_READOUT_SIZE : Natural := 4;
  constant ADDR_ONEWIRE_REGS_MEM_READOUT_DATA : Natural := 16#0#;
  constant ONEWIRE_REGS_MEM_READOUT_DATA_LOW_OFFSET : Natural := 0;
  constant ONEWIRE_REGS_MEM_READOUT_DATA_HIGH_OFFSET : Natural := 16;
end package onewire_regs_Consts;
