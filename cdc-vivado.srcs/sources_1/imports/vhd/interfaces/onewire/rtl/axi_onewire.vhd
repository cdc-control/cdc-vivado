-------------------------------------------------------------------------------
--! @file   axi_onewire.vhd
--! @author Pawel Wozny, Aleksi Korsman, ttsiakir <theodoros.tsiakiris@cern.ch>
--! @brief  One Wire module with Axi-Lite interface.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

--! @brief Entity declaration of  axi_onewire
--! @section regmap Register memory map
--! @htmlinclude [axi_onewire_regs.html]
entity  axi_onewire is
  generic (
    --! Clock frequency (Hz)
    clk_frequency_g : natural := 40e6;
    --! Max devices on the bus
    max_devices_g   : positive := 16;
    --! Invert the transmit signal polarity (ow_o)
    invert_tx_g     : boolean  := true;
    --! Invert the receive signal polarity (ow_i)
    invert_rx_g     : boolean  := false;
    --! Invert the pullup signal polarity (ow_pullup_o)
    invert_pullup_g : boolean  := false;
    --! Use triple modular redundant BRAM or not
    use_tmr_bram_g  : boolean := false
  );
  port (
    -----------------------------------------------------------------------------
    --! @name AXI ports
    -----------------------------------------------------------------------------
    --! @{

    --! AXI interface clock and reset
    axi_aresetn_i : in std_logic; --! axi reset
    axi_clk_i : in std_logic; --! axi clock

    --! AXI write address (write channel)
    axi_awvalid_i : in  std_logic; --! axi write address valid
    axi_awready_o : out std_logic; --! axi wrire address ready
    axi_awaddr_i  : in  std_logic_vector(10 downto 0); --! axi write address
    axi_awprot_i  : in  std_logic_vector(2 downto 0); --! axi write protection encoding (not used)

    --! AXI write data (write channel)
    axi_wvalid_i : in  std_logic; --! axi write valid
    axi_wready_o : out std_logic; --! axi write ready
    axi_wdata_i  : in  std_logic_vector(31 downto 0); --! axi write data
    axi_wstrb_i  : in  std_logic_vector(3 downto 0); --!  axi write strobe (not used)
    --
    --! AXI address write response (not used)
    axi_bvalid_o : out std_logic; --! axi write response valid (not used)
    axi_bready_i : in  std_logic; --! axi write response ready (not used)
    axi_bresp_o  : out std_logic_vector(1 downto 0); --! axi read response (not used)

    --! AXI address read (write channel)
    axi_arvalid_i : in  std_logic; --! axi read address valid
    axi_arready_o : out std_logic; --! axi read address ready
    axi_araddr_i  : in  std_logic_vector(10 downto 0); --! axi read address
    axi_arprot_i  : in  std_logic_vector(2 downto 0); --! axi read address protection encoding (not used)

    --! AXI data read (read channel)
    axi_rvalid_o : out std_logic; --! axi read data valid
    axi_rready_i : in  std_logic; --! axi read data ready
    axi_rdata_o  : out std_logic_vector(31 downto 0); --! axi read data
    axi_rresp_o  : out std_logic_vector(1 downto 0); --! axi read response (not used)
    --! @}

    -----------------------------------------------------------------------------
    --! @name One Wire ports
    --! The input and output are seperated here, but in reality Onewire
    --! is only one bus, thus these signals need to be merged on the PCB
    -----------------------------------------------------------------------------

    --! Onewire input
    ow_i        : in std_logic;
    --! Onewire output
    ow_o        : out std_logic;
    --! Onewire pullup enable
    ow_pullup_o : out std_logic;
    --! Onewire control bits (for selecting output for a multiplexer)
    ow_ctrl_o   : out std_logic_vector(2 downto 0);
    --! Onewire output enable
    ow_en_o     : out std_logic

  );
end entity  axi_onewire;

--! @brief Architecture definition of axi_onewire
architecture rtl of  axi_onewire is

  signal ow_ctrl           : std_logic_vector(2 downto 0);
  signal ow_en             : std_ulogic;
  signal ow_trig           : std_ulogic;
  signal ow_reg_rst        : std_ulogic;
  signal ow_rst            : std_ulogic;
  signal ow_busy           : std_ulogic;
  signal ow_done           : std_ulogic;
  signal ow_dev_count      : std_logic_vector(5 downto 0);
  signal ow_too_many       : std_ulogic;
  signal ow_rd_addr        : std_logic_vector(7 downto 0);
  signal ow_rd_en          : std_ulogic;
  signal ow_rd_data        : std_logic_vector(63 downto 0);
  signal ow_rd_rdy         : std_ulogic;
  signal ow_rd_busy        : std_ulogic;
  signal ow_rd_data_chunk  : std_logic_vector(31 downto 0);
  signal ground            : std_ulogic;

  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_INFO of axi_awaddr_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  ATTRIBUTE X_INTERFACE_INFO of axi_awprot_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI AWPROT";
  ATTRIBUTE X_INTERFACE_INFO of axi_awvalid_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  ATTRIBUTE X_INTERFACE_INFO of axi_awready_o: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  ATTRIBUTE X_INTERFACE_INFO of axi_wdata_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  ATTRIBUTE X_INTERFACE_INFO of axi_wstrb_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI WSTRB";
  ATTRIBUTE X_INTERFACE_INFO of axi_wvalid_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  ATTRIBUTE X_INTERFACE_INFO of axi_wready_o: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  ATTRIBUTE X_INTERFACE_INFO of axi_bresp_o: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  ATTRIBUTE X_INTERFACE_INFO of axi_bvalid_o: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  ATTRIBUTE X_INTERFACE_INFO of axi_bready_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  ATTRIBUTE X_INTERFACE_INFO of axi_araddr_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  ATTRIBUTE X_INTERFACE_INFO of axi_arprot_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI ARPROT";
  ATTRIBUTE X_INTERFACE_INFO of axi_arvalid_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  ATTRIBUTE X_INTERFACE_INFO of axi_arready_o: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  ATTRIBUTE X_INTERFACE_INFO of axi_rdata_o: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  ATTRIBUTE X_INTERFACE_INFO of axi_rresp_o: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI RRESP";
  ATTRIBUTE X_INTERFACE_INFO of axi_rvalid_o: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  ATTRIBUTE X_INTERFACE_INFO of axi_rready_i: SIGNAL is "xilinx.com:interface:aximm:1.0 S_AXI RREADY";
begin

  -- Memory in the Onewire module is 64-bit, whereas AXI bus is 32-bit
  -- Therefore, the last bit in the address is used to select the 32-bit chunk
  ow_rd_data_chunk <= ow_rd_data(31 downto 0) when ow_rd_addr(0) = '0' else
                      ow_rd_data(63 downto 32);

  ow_rst <= ow_reg_rst or (not axi_aresetn_i);

  --! Onewire registers

  --! Generated with Cheby.
  --! The interface for internal two port TMR RAM is implemented with AXI-Lite
  --! due to Cheby limitations. See Cheby issue #91 for further info:
  --! https://gitlab.cern.ch/cohtdrivers/cheby/-/issues/91
  axi_onewire_regs : entity work.onewire_regs_axi
  port map (
    aclk     => axi_clk_i,
    areset_n => axi_aresetn_i,
    awvalid  => axi_awvalid_i,
    awready  => axi_awready_o,
    awaddr   => axi_awaddr_i,
    awprot   => axi_awprot_i,
    wvalid   => axi_wvalid_i,
    wready   => axi_wready_o,
    wdata    => axi_wdata_i,
    wstrb    => axi_wstrb_i,
    bvalid   => axi_bvalid_o,
    bready   => axi_bready_i,
    bresp    => axi_bresp_o,
    arvalid  => axi_arvalid_i,
    arready  => axi_arready_o,
    araddr   => axi_araddr_i,
    arprot   => axi_arprot_i,
    rvalid   => axi_rvalid_o,
    rready   => axi_rready_i,
    rdata    => axi_rdata_o,
    rresp    => axi_rresp_o,

    mux_en_o              => ow_en,
    mux_ctrl_o            => ow_ctrl,
    control_trig_o        => ow_trig,
    control_rst_o         => ow_reg_rst,
    status_busy_i         => ow_busy,
    status_done_i         => ow_done,
    status_count_i        => ow_dev_count,
    status_dev_too_many_i => ow_too_many,

    mem_readout_cyc_o   => open,
    mem_readout_stb_o   => ow_rd_en,
    mem_readout_adr_o   => ow_rd_addr,
    mem_readout_sel_o   => open,
    mem_readout_we_o    => open,
    mem_readout_dat_o   => open,
    mem_readout_ack_i   => ow_rd_rdy,
    mem_readout_err_i   => '0',
    mem_readout_rty_i   => '0',
    mem_readout_stall_i => '0',
    mem_readout_dat_i   => ow_rd_data_chunk
  );

  --! Onewire interface
  onewire_if : entity work.onewire
  generic map(
    clk_frequency_g => clk_frequency_g,
    max_devices_g   => max_devices_g,
    invert_tx_g     => invert_tx_g,
    invert_rx_g     => invert_rx_g,
    invert_pullup_g => invert_pullup_g,
    use_tmr_bram_g  => use_tmr_bram_g
  )
  port map(
    clk_i       => axi_clk_i,
    rst_asy_n_i => '1',
    rst_syn_i   => ow_rst,

    ow_trig_i     => ow_trig,
    ow_busy_o     => ow_busy,
    ow_done_o     => ow_done,
    std_logic_vector(ow_dev_cnt_o) => ow_dev_count,
    ow_too_many_o => ow_too_many,
    ow_ctrl_i     => std_ulogic_vector(ow_ctrl),
    ow_en_i       => ow_en,
    ow_rd_addr_i  => std_ulogic_vector(ow_rd_addr(6 downto 1)),
    ow_rd_en_i    => ow_rd_en,
    std_logic_vector(ow_rd_data_o) => ow_rd_data,
    ow_rd_rdy_o   => ow_rd_rdy,
    ow_rd_busy_o  => ow_rd_busy,

    ow_i        => ow_i,
    ow_o        => ow_o,
    ow_pullup_o => ow_pullup_o,
    ow_ctrl_o   => ow_ctrl_o,
    ow_en_o     => ow_en_o
  );

end architecture;
