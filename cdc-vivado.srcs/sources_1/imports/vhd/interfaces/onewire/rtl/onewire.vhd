
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.math_real.all;

entity onewire is
  generic (
    clk_frequency_g : natural  := 40e6;
    max_devices_g   : positive := 16;
    invert_tx_g     : boolean  := true;
    invert_rx_g     : boolean  := false;
    invert_pullup_g : boolean  := false;
    use_tmr_bram_g  : boolean  := false
  );
  port (
    clk_i         : in std_ulogic;
    rst_asy_n_i   : in std_ulogic;
    rst_syn_i     : in std_ulogic;
        --
    ow_trig_i     : in std_ulogic;
    ow_busy_o     : out std_ulogic;
    ow_done_o     : out std_ulogic;
    ow_dev_cnt_o  : out std_ulogic_vector(natural(ceil(log2(real(max_devices_g + 1)))) - 1 downto 0);
    ow_too_many_o : out std_ulogic;
    ow_ctrl_i     : in std_ulogic_vector(2 downto 0);
    ow_en_i       : in std_ulogic;
        --
    ow_rd_addr_i  : in std_ulogic_vector(natural(ceil(log2(real(max_devices_g * 2)))) - 1 downto 0);
    ow_rd_en_i    : in std_ulogic;
    ow_rd_data_o  : out std_ulogic_vector(63 downto 0);
    ow_rd_rdy_o   : out std_ulogic;
    ow_rd_busy_o  : out std_ulogic;
        --
    ow_i          : in std_logic;
    ow_o          : out std_logic;
    ow_pullup_o   : out std_logic;
    ow_ctrl_o     : out std_logic_vector(2 downto 0);
    ow_en_o       : out std_logic
  );
end entity onewire;

architecture arch of onewire is

  signal ow_disc_trig : std_ulogic;
  signal ow_temp_trig : std_ulogic;
  signal ow_busy      : std_ulogic;
  signal ow_done      : std_ulogic;
  signal ow_rx        : std_ulogic;
  signal ow_tx        : std_ulogic;
  type ow_st is (IDLE, DISCOVERY, TEMPERATURE_READ);
  signal ow_state     : ow_st;

begin
  -- passthrough outputs
  ow_en_o   <= ow_en_i;
  ow_ctrl_o <= ow_ctrl_i;

  -- invert port levels
  ow_rx <= not(ow_i)  when invert_rx_g = true else ow_i;
  ow_o  <= not(ow_tx) when invert_tx_g = true else ow_tx;

  --  assert generic values
  assert max_devices_g < 33 report "Up to 32 devices are supported on one Onewire bus" severity failure;

  -- main one wire instance
  x_bus_master : entity work.onewire_idtemp
  generic map(
    clk_frequency_g  => clk_frequency_g,
    max_devices_g    => max_devices_g,
    invert_bus_g     => false,
    invert_pullup_g  => invert_pullup_g,
    use_tmr_bram_g   => use_tmr_bram_g
  )
  port map(
    clk_i            => clk_i,
    rst_asy_n_i      => rst_asy_n_i,
    rst_syn_i        => rst_syn_i,

    discover_i       => ow_disc_trig,
    get_temp_i       => ow_temp_trig,
    busy_o           => ow_busy,
    done_o           => ow_done,
    device_count_o   => ow_dev_cnt_o,
    error_too_many_o => ow_too_many_o,

    rd_addr_i        => ow_rd_addr_i,
    rd_en_i          => ow_rd_en_i,
    rd_data_o        => ow_rd_data_o,
    rd_data_en_o     => ow_rd_rdy_o,
    rd_busy_o        => ow_rd_busy_o,

    strong_pullup_o  => ow_pullup_o,
    rx_i             => ow_rx,
    tx_o             => ow_tx
  );

  onewire_state : process (all)
    procedure reset is
    begin
      ow_state     <= IDLE;
      ow_disc_trig <= '0';
      ow_temp_trig <= '0';
      ow_busy_o    <= '0';
      ow_done_o    <= '0';
    end procedure;
  begin
    if rst_asy_n_i = '0' then
      reset;
    elsif rising_edge(clk_i) then
      if rst_syn_i = '1' then
        reset;
      else
        case ow_state is

          when IDLE =>
            if ow_trig_i = '1' and ow_busy = '0' then
              ow_disc_trig <= '1';
              ow_busy_o    <= '1';
              ow_done_o    <= '0';
              ow_state     <= DISCOVERY;
            end if;

          when DISCOVERY =>
            ow_disc_trig <= '0';
            if ow_done = '1' then
              if ow_too_many_o = '1' then
                -- the only way to get out of this latch is to
                -- reset entire design, as onewire_idtemp
                -- do not clears this flag
                ow_busy_o <= '0';
                ow_done_o <= '1';
                ow_state <= IDLE;
              else
                ow_temp_trig <= '1';
                ow_state     <= TEMPERATURE_READ;
              end if;
            end if;

          when TEMPERATURE_READ =>
            ow_temp_trig <= '0';
            if ow_done = '1' then
              ow_busy_o <= '0';
              ow_done_o <= '1';
              ow_state  <= IDLE;
            end if;

          when others =>
            reset;

        end case;
      end if;
    end if;
  end process;

end arch;
