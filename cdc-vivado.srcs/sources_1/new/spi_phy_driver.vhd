----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/25/2023 12:25:03 PM
-- Design Name: 
-- Module Name: spi_phy_driver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity spi_phy_driver is
    Generic (
        SS_SIZE_G : integer := 1;
        IOSTANDARD_G : string := "LVDS_25"
    );
    Port ( mosi_i : in STD_LOGIC;
           miso_i_p : in STD_LOGIC;
           miso_i_n : in STD_LOGIC;
           sck_i : in STD_LOGIC;
           ss_i : in STD_LOGIC_VECTOR (SS_SIZE_G-1 downto 0);
           mosi_o_p : out STD_LOGIC;
           mosi_o_n : out STD_LOGIC;
           miso_o : out STD_LOGIC;
           sck_o_p : out STD_LOGIC;
           sck_o_n : out STD_LOGIC;
           ss_o_p : out STD_LOGIC_VECTOR (SS_SIZE_G-1 downto 0);
           ss_o_n : out STD_LOGIC_VECTOR (SS_SIZE_G-1 downto 0));
end spi_phy_driver;

architecture Behavioral of spi_phy_driver is

begin
mosi_obufds : OBUFDS
    generic map (
        IOSTANDARD => IOSTANDARD_G
    )
    port map (
        O => mosi_o_p,
        OB => mosi_o_n,
        I => mosi_i
    );
miso_ibufds : IBUFDS
    generic map (
        IOSTANDARD => IOSTANDARD_G,
        DIFF_TERM => TRUE
    )
    port map (
        I => miso_i_p,
        IB => miso_i_n,
        O => miso_o
    );

sck_obufds : OBUFDS
    generic map (
        IOSTANDARD => IOSTANDARD_G
    )
    port map (
        O => sck_o_p,
        OB => sck_o_n,
        I => sck_i
    );

ss_forgen : for i in SS_SIZE_G-1 downto 0 generate
    ss_obufds : OBUFDS
        generic map (
            IOSTANDARD => IOSTANDARD_G
        )
        port map (
            O => ss_o_p(i),
            OB => ss_o_n(i),
            I => ss_i(i)
        );    
end generate;

end Behavioral;
