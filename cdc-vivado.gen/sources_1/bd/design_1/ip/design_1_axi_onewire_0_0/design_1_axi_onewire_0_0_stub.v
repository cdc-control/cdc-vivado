// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Thu Mar  2 11:27:44 2023
// Host        : PCSY219 running 64-bit unknown
// Command     : write_verilog -force -mode synth_stub -rename_top design_1_axi_onewire_0_0 -prefix
//               design_1_axi_onewire_0_0_ design_1_axi_onewire_0_0_stub.v
// Design      : design_1_axi_onewire_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z007sclg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "axi_onewire,Vivado 2022.2" *)
module design_1_axi_onewire_0_0(axi_aresetn_i, axi_clk_i, axi_awvalid_i, 
  axi_awready_o, axi_awaddr_i, axi_awprot_i, axi_wvalid_i, axi_wready_o, axi_wdata_i, 
  axi_wstrb_i, axi_bvalid_o, axi_bready_i, axi_bresp_o, axi_arvalid_i, axi_arready_o, 
  axi_araddr_i, axi_arprot_i, axi_rvalid_o, axi_rready_i, axi_rdata_o, axi_rresp_o, ow_i, ow_o, 
  ow_pullup_o, ow_ctrl_o, ow_en_o)
/* synthesis syn_black_box black_box_pad_pin="axi_aresetn_i,axi_clk_i,axi_awvalid_i,axi_awready_o,axi_awaddr_i[10:0],axi_awprot_i[2:0],axi_wvalid_i,axi_wready_o,axi_wdata_i[31:0],axi_wstrb_i[3:0],axi_bvalid_o,axi_bready_i,axi_bresp_o[1:0],axi_arvalid_i,axi_arready_o,axi_araddr_i[10:0],axi_arprot_i[2:0],axi_rvalid_o,axi_rready_i,axi_rdata_o[31:0],axi_rresp_o[1:0],ow_i,ow_o,ow_pullup_o,ow_ctrl_o[2:0],ow_en_o" */;
  input axi_aresetn_i;
  input axi_clk_i;
  input axi_awvalid_i;
  output axi_awready_o;
  input [10:0]axi_awaddr_i;
  input [2:0]axi_awprot_i;
  input axi_wvalid_i;
  output axi_wready_o;
  input [31:0]axi_wdata_i;
  input [3:0]axi_wstrb_i;
  output axi_bvalid_o;
  input axi_bready_i;
  output [1:0]axi_bresp_o;
  input axi_arvalid_i;
  output axi_arready_o;
  input [10:0]axi_araddr_i;
  input [2:0]axi_arprot_i;
  output axi_rvalid_o;
  input axi_rready_i;
  output [31:0]axi_rdata_o;
  output [1:0]axi_rresp_o;
  input ow_i;
  output ow_o;
  output ow_pullup_o;
  output [2:0]ow_ctrl_o;
  output ow_en_o;
endmodule
