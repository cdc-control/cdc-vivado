-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Thu Mar  2 11:27:44 2023
-- Host        : PCSY219 running 64-bit unknown
-- Command     : write_vhdl -force -mode funcsim -rename_top design_1_axi_onewire_0_0 -prefix
--               design_1_axi_onewire_0_0_ design_1_axi_onewire_0_0_sim_netlist.vhdl
-- Design      : design_1_axi_onewire_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z007sclg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_onewire_crc is
  port (
    \reg_reg[id_bit_number][2]\ : out STD_LOGIC;
    \reg_reg[id][63]\ : out STD_LOGIC;
    \reg_reg[id][62]\ : out STD_LOGIC;
    \reg_reg[id][61]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_0\ : out STD_LOGIC;
    \reg_reg[id][59]\ : out STD_LOGIC;
    \reg_reg[id][58]\ : out STD_LOGIC;
    \reg_reg[id][57]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_1\ : out STD_LOGIC;
    \reg_reg[id][55]\ : out STD_LOGIC;
    \reg_reg[id][54]\ : out STD_LOGIC;
    \reg_reg[id][53]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_2\ : out STD_LOGIC;
    \reg_reg[id][51]\ : out STD_LOGIC;
    \reg_reg[id][50]\ : out STD_LOGIC;
    \reg_reg[id][49]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_3\ : out STD_LOGIC;
    \reg_reg[id][47]\ : out STD_LOGIC;
    \reg_reg[id][46]\ : out STD_LOGIC;
    \reg_reg[id][45]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_4\ : out STD_LOGIC;
    \reg_reg[id][43]\ : out STD_LOGIC;
    \reg_reg[id][42]\ : out STD_LOGIC;
    \reg_reg[id][41]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_5\ : out STD_LOGIC;
    \reg_reg[id][39]\ : out STD_LOGIC;
    \reg_reg[id][38]\ : out STD_LOGIC;
    \reg_reg[id][37]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_6\ : out STD_LOGIC;
    \reg_reg[id][35]\ : out STD_LOGIC;
    \reg_reg[id][34]\ : out STD_LOGIC;
    \reg_reg[id][33]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_7\ : out STD_LOGIC;
    \reg_reg[id][31]\ : out STD_LOGIC;
    \reg_reg[id][30]\ : out STD_LOGIC;
    \reg_reg[id][29]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_8\ : out STD_LOGIC;
    \reg_reg[id][27]\ : out STD_LOGIC;
    \reg_reg[id][26]\ : out STD_LOGIC;
    \reg_reg[id][25]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_9\ : out STD_LOGIC;
    \reg_reg[id][23]\ : out STD_LOGIC;
    \reg_reg[id][22]\ : out STD_LOGIC;
    \reg_reg[id][21]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_10\ : out STD_LOGIC;
    \reg_reg[id][19]\ : out STD_LOGIC;
    \reg_reg[id][18]\ : out STD_LOGIC;
    \reg_reg[id][17]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_11\ : out STD_LOGIC;
    \reg_reg[id][15]\ : out STD_LOGIC;
    \reg_reg[id][14]\ : out STD_LOGIC;
    \reg_reg[id][13]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_12\ : out STD_LOGIC;
    \reg_reg[id][11]\ : out STD_LOGIC;
    \reg_reg[id][10]\ : out STD_LOGIC;
    \reg_reg[id][9]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_13\ : out STD_LOGIC;
    \reg_reg[id][7]\ : out STD_LOGIC;
    \reg_reg[id][6]\ : out STD_LOGIC;
    \reg_reg[id][5]\ : out STD_LOGIC;
    \reg_reg[id_bit_number][2]_14\ : out STD_LOGIC;
    \reg_reg[id][3]\ : out STD_LOGIC;
    \reg_reg[id][2]\ : out STD_LOGIC;
    \reg_reg[id][1]\ : out STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    \reg_reg[id][1]_0\ : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_reg[id][1]_1\ : in STD_LOGIC;
    \reg_reg[id][1]_2\ : in STD_LOGIC;
    \reg_reg[id][1]_3\ : in STD_LOGIC;
    \reg_reg[id][9]_0\ : in STD_LOGIC;
    \reg_reg[id][17]_0\ : in STD_LOGIC;
    \reg[state]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \reg_reg[id][25]_0\ : in STD_LOGIC;
    \reg_reg[id][57]_0\ : in STD_LOGIC;
    ow_reg_rst : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    pristine_reg_0 : in STD_LOGIC;
    dcvr_bit_send : in STD_LOGIC;
    \crc_reg[2]_0\ : in STD_LOGIC;
    \reg_reg[id][1]_4\ : in STD_LOGIC;
    if_done : in STD_LOGIC;
    \reg_reg[id][1]_5\ : in STD_LOGIC;
    ow_rst : in STD_LOGIC;
    \reg_reg[id][64]\ : in STD_LOGIC;
    \reg_reg[id][64]_0\ : in STD_LOGIC;
    \reg_reg[id][64]_1\ : in STD_LOGIC;
    dcvr_id : in STD_LOGIC_VECTOR ( 63 downto 0 );
    \reg_reg[id][63]_0\ : in STD_LOGIC;
    \reg_reg[id][62]_0\ : in STD_LOGIC;
    \reg_reg[id][61]_0\ : in STD_LOGIC;
    \reg_reg[id][59]_0\ : in STD_LOGIC;
    \reg_reg[id][58]_0\ : in STD_LOGIC;
    \reg_reg[id][57]_1\ : in STD_LOGIC
  );
end design_1_axi_onewire_0_0_onewire_crc;

architecture STRUCTURE of design_1_axi_onewire_0_0_onewire_crc is
  signal \crc[0]_i_1_n_0\ : STD_LOGIC;
  signal \crc[1]_i_1_n_0\ : STD_LOGIC;
  signal \crc[2]_i_1_n_0\ : STD_LOGIC;
  signal \crc[3]_i_1_n_0\ : STD_LOGIC;
  signal \crc[4]_i_1_n_0\ : STD_LOGIC;
  signal \crc[5]_i_1_n_0\ : STD_LOGIC;
  signal \crc[6]_i_1_n_0\ : STD_LOGIC;
  signal \crc[7]_i_1_n_0\ : STD_LOGIC;
  signal \crc[7]_i_2_n_0\ : STD_LOGIC;
  signal \crc_reg_n_0_[0]\ : STD_LOGIC;
  signal \crc_reg_n_0_[7]\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal p_3_in : STD_LOGIC;
  signal p_4_in : STD_LOGIC;
  signal p_5_in : STD_LOGIC;
  signal p_6_in : STD_LOGIC;
  signal pristine : STD_LOGIC;
  signal pristine_i_1_n_0 : STD_LOGIC;
  signal \reg[id][16]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][24]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][32]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][40]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][48]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][56]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_11_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_12_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_4_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_5_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_7_n_0\ : STD_LOGIC;
  signal \reg[id][8]_i_2_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \crc[1]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \crc[4]_i_1\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \crc[5]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \crc[6]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \crc[7]_i_2\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of pristine_i_1 : label is "soft_lutpair41";
begin
\crc[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => p_3_in,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \crc[0]_i_1_n_0\
    );
\crc[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => p_4_in,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \crc[1]_i_1_n_0\
    );
\crc[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400000400040400"
    )
        port map (
      I0 => pristine_reg_0,
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => p_1_in,
      I4 => \crc_reg[2]_0\,
      I5 => \crc_reg_n_0_[0]\,
      O => \crc[2]_i_1_n_0\
    );
\crc[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400000400040400"
    )
        port map (
      I0 => pristine_reg_0,
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => p_2_in,
      I4 => \crc_reg[2]_0\,
      I5 => \crc_reg_n_0_[0]\,
      O => \crc[3]_i_1_n_0\
    );
\crc[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => p_5_in,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \crc[4]_i_1_n_0\
    );
\crc[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => p_6_in,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \crc[5]_i_1_n_0\
    );
\crc[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \crc_reg_n_0_[7]\,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \crc[6]_i_1_n_0\
    );
\crc[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFB"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => pristine_reg_0,
      I3 => dcvr_bit_send,
      O => \crc[7]_i_1_n_0\
    );
\crc[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000600"
    )
        port map (
      I0 => \crc_reg[2]_0\,
      I1 => \crc_reg_n_0_[0]\,
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => pristine_reg_0,
      O => \crc[7]_i_2_n_0\
    );
\crc_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1_n_0\,
      D => \crc[0]_i_1_n_0\,
      Q => \crc_reg_n_0_[0]\,
      R => '0'
    );
\crc_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1_n_0\,
      D => \crc[1]_i_1_n_0\,
      Q => p_3_in,
      R => '0'
    );
\crc_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1_n_0\,
      D => \crc[2]_i_1_n_0\,
      Q => p_4_in,
      R => '0'
    );
\crc_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1_n_0\,
      D => \crc[3]_i_1_n_0\,
      Q => p_1_in,
      R => '0'
    );
\crc_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1_n_0\,
      D => \crc[4]_i_1_n_0\,
      Q => p_2_in,
      R => '0'
    );
\crc_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1_n_0\,
      D => \crc[5]_i_1_n_0\,
      Q => p_5_in,
      R => '0'
    );
\crc_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1_n_0\,
      D => \crc[6]_i_1_n_0\,
      Q => p_6_in,
      R => '0'
    );
\crc_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1_n_0\,
      D => \crc[7]_i_2_n_0\,
      Q => \crc_reg_n_0_[7]\,
      R => '0'
    );
pristine_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => pristine_reg_0,
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => pristine_i_1_n_0
    );
pristine_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1_n_0\,
      D => pristine_i_1_n_0,
      Q => pristine,
      R => '0'
    );
\reg[id][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][58]_0\,
      I2 => \reg[id][16]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(9),
      O => \reg_reg[id][10]\
    );
\reg[id][11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][59]_0\,
      I2 => \reg[id][16]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(10),
      O => \reg_reg[id][11]\
    );
\reg[id][12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFBFFAAAA0800"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][16]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(11),
      O => \reg_reg[id_bit_number][2]_12\
    );
\reg[id][13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][61]_0\,
      I2 => \reg[id][16]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(12),
      O => \reg_reg[id][13]\
    );
\reg[id][14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][62]_0\,
      I2 => \reg[id][16]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(13),
      O => \reg_reg[id][14]\
    );
\reg[id][15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][63]_0\,
      I2 => \reg[id][16]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(14),
      O => \reg_reg[id][15]\
    );
\reg[id][16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFEFFAAAA0200"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][16]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(15),
      O => \reg_reg[id_bit_number][2]_11\
    );
\reg[id][16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000E00000"
    )
        port map (
      I0 => \reg_reg[id][1]_0\,
      I1 => CO(0),
      I2 => \reg[id][64]_i_7_n_0\,
      I3 => \reg_reg[id][1]_1\,
      I4 => \reg_reg[id][1]_2\,
      I5 => \reg_reg[id][9]_0\,
      O => \reg[id][16]_i_2_n_0\
    );
\reg[id][17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][57]_1\,
      I2 => \reg[id][24]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(16),
      O => \reg_reg[id][17]\
    );
\reg[id][18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][58]_0\,
      I2 => \reg[id][24]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(17),
      O => \reg_reg[id][18]\
    );
\reg[id][19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][59]_0\,
      I2 => \reg[id][24]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(18),
      O => \reg_reg[id][19]\
    );
\reg[id][1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][57]_1\,
      I2 => \reg[id][8]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(0),
      O => \reg_reg[id][1]\
    );
\reg[id][20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFBFFAAAA0800"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][24]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(19),
      O => \reg_reg[id_bit_number][2]_10\
    );
\reg[id][21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][61]_0\,
      I2 => \reg[id][24]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(20),
      O => \reg_reg[id][21]\
    );
\reg[id][22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][62]_0\,
      I2 => \reg[id][24]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(21),
      O => \reg_reg[id][22]\
    );
\reg[id][23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][63]_0\,
      I2 => \reg[id][24]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(22),
      O => \reg_reg[id][23]\
    );
\reg[id][24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFEFFAAAA0200"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][24]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(23),
      O => \reg_reg[id_bit_number][2]_9\
    );
\reg[id][24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000E00000"
    )
        port map (
      I0 => \reg_reg[id][1]_0\,
      I1 => CO(0),
      I2 => \reg[id][64]_i_7_n_0\,
      I3 => \reg_reg[id][1]_1\,
      I4 => \reg_reg[id][1]_2\,
      I5 => \reg_reg[id][17]_0\,
      O => \reg[id][24]_i_2_n_0\
    );
\reg[id][25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][57]_1\,
      I2 => \reg[id][32]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(24),
      O => \reg_reg[id][25]\
    );
\reg[id][26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][58]_0\,
      I2 => \reg[id][32]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(25),
      O => \reg_reg[id][26]\
    );
\reg[id][27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][59]_0\,
      I2 => \reg[id][32]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(26),
      O => \reg_reg[id][27]\
    );
\reg[id][28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFBFFAAAA0800"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][32]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(27),
      O => \reg_reg[id_bit_number][2]_8\
    );
\reg[id][29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][61]_0\,
      I2 => \reg[id][32]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(28),
      O => \reg_reg[id][29]\
    );
\reg[id][2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][58]_0\,
      I2 => \reg[id][8]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(1),
      O => \reg_reg[id][2]\
    );
\reg[id][30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][62]_0\,
      I2 => \reg[id][32]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(29),
      O => \reg_reg[id][30]\
    );
\reg[id][31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][63]_0\,
      I2 => \reg[id][32]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(30),
      O => \reg_reg[id][31]\
    );
\reg[id][32]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFEFFAAAA0200"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][32]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(31),
      O => \reg_reg[id_bit_number][2]_7\
    );
\reg[id][32]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000E000"
    )
        port map (
      I0 => \reg_reg[id][1]_0\,
      I1 => CO(0),
      I2 => \reg[id][64]_i_7_n_0\,
      I3 => \reg[state]\(2),
      I4 => \reg[state]\(1),
      I5 => \reg_reg[id][25]_0\,
      O => \reg[id][32]_i_2_n_0\
    );
\reg[id][33]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][57]_1\,
      I2 => \reg[id][40]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(32),
      O => \reg_reg[id][33]\
    );
\reg[id][34]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][58]_0\,
      I2 => \reg[id][40]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(33),
      O => \reg_reg[id][34]\
    );
\reg[id][35]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][59]_0\,
      I2 => \reg[id][40]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(34),
      O => \reg_reg[id][35]\
    );
\reg[id][36]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFBFFAAAA0800"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][40]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(35),
      O => \reg_reg[id_bit_number][2]_6\
    );
\reg[id][37]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][61]_0\,
      I2 => \reg[id][40]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(36),
      O => \reg_reg[id][37]\
    );
\reg[id][38]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][62]_0\,
      I2 => \reg[id][40]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(37),
      O => \reg_reg[id][38]\
    );
\reg[id][39]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][63]_0\,
      I2 => \reg[id][40]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(38),
      O => \reg_reg[id][39]\
    );
\reg[id][3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][59]_0\,
      I2 => \reg[id][8]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(2),
      O => \reg_reg[id][3]\
    );
\reg[id][40]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFEFFAAAA0200"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][40]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(39),
      O => \reg_reg[id_bit_number][2]_5\
    );
\reg[id][40]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000E0"
    )
        port map (
      I0 => \reg_reg[id][1]_0\,
      I1 => CO(0),
      I2 => \reg[id][64]_i_7_n_0\,
      I3 => \reg_reg[id][1]_1\,
      I4 => \reg_reg[id][1]_2\,
      I5 => \reg_reg[id][1]_3\,
      O => \reg[id][40]_i_2_n_0\
    );
\reg[id][41]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][57]_1\,
      I2 => \reg[id][48]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(40),
      O => \reg_reg[id][41]\
    );
\reg[id][42]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][58]_0\,
      I2 => \reg[id][48]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(41),
      O => \reg_reg[id][42]\
    );
\reg[id][43]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][59]_0\,
      I2 => \reg[id][48]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(42),
      O => \reg_reg[id][43]\
    );
\reg[id][44]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFBFFAAAA0800"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][48]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(43),
      O => \reg_reg[id_bit_number][2]_4\
    );
\reg[id][45]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][61]_0\,
      I2 => \reg[id][48]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(44),
      O => \reg_reg[id][45]\
    );
\reg[id][46]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][62]_0\,
      I2 => \reg[id][48]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(45),
      O => \reg_reg[id][46]\
    );
\reg[id][47]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][63]_0\,
      I2 => \reg[id][48]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(46),
      O => \reg_reg[id][47]\
    );
\reg[id][48]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFEFFAAAA0200"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][48]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(47),
      O => \reg_reg[id_bit_number][2]_3\
    );
\reg[id][48]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000E0"
    )
        port map (
      I0 => \reg_reg[id][1]_0\,
      I1 => CO(0),
      I2 => \reg[id][64]_i_7_n_0\,
      I3 => \reg_reg[id][1]_1\,
      I4 => \reg_reg[id][1]_2\,
      I5 => \reg_reg[id][9]_0\,
      O => \reg[id][48]_i_2_n_0\
    );
\reg[id][49]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][57]_1\,
      I2 => \reg[id][56]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(48),
      O => \reg_reg[id][49]\
    );
\reg[id][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFBFFAAAA0800"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][8]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(3),
      O => \reg_reg[id_bit_number][2]_14\
    );
\reg[id][50]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][58]_0\,
      I2 => \reg[id][56]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(49),
      O => \reg_reg[id][50]\
    );
\reg[id][51]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][59]_0\,
      I2 => \reg[id][56]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(50),
      O => \reg_reg[id][51]\
    );
\reg[id][52]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFBFFAAAA0800"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][56]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(51),
      O => \reg_reg[id_bit_number][2]_2\
    );
\reg[id][53]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][61]_0\,
      I2 => \reg[id][56]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(52),
      O => \reg_reg[id][53]\
    );
\reg[id][54]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][62]_0\,
      I2 => \reg[id][56]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(53),
      O => \reg_reg[id][54]\
    );
\reg[id][55]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][63]_0\,
      I2 => \reg[id][56]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(54),
      O => \reg_reg[id][55]\
    );
\reg[id][56]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFEFFAAAA0200"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][56]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(55),
      O => \reg_reg[id_bit_number][2]_1\
    );
\reg[id][56]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000E0"
    )
        port map (
      I0 => \reg_reg[id][1]_0\,
      I1 => CO(0),
      I2 => \reg[id][64]_i_7_n_0\,
      I3 => \reg_reg[id][1]_1\,
      I4 => \reg_reg[id][1]_2\,
      I5 => \reg_reg[id][17]_0\,
      O => \reg[id][56]_i_2_n_0\
    );
\reg[id][57]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][57]_1\,
      I2 => \reg[id][64]_i_4_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(56),
      O => \reg_reg[id][57]\
    );
\reg[id][58]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][58]_0\,
      I2 => \reg[id][64]_i_4_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(57),
      O => \reg_reg[id][58]\
    );
\reg[id][59]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][59]_0\,
      I2 => \reg[id][64]_i_4_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(58),
      O => \reg_reg[id][59]\
    );
\reg[id][5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][61]_0\,
      I2 => \reg[id][8]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(4),
      O => \reg_reg[id][5]\
    );
\reg[id][60]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFBFFAAAA0800"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][64]_i_4_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(59),
      O => \reg_reg[id_bit_number][2]_0\
    );
\reg[id][61]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][61]_0\,
      I2 => \reg[id][64]_i_4_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(60),
      O => \reg_reg[id][61]\
    );
\reg[id][62]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][62]_0\,
      I2 => \reg[id][64]_i_4_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(61),
      O => \reg_reg[id][62]\
    );
\reg[id][63]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][63]_0\,
      I2 => \reg[id][64]_i_4_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(62),
      O => \reg_reg[id][63]\
    );
\reg[id][64]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFEFFAAAA0200"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][64]_i_4_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(63),
      O => \reg_reg[id_bit_number][2]\
    );
\reg[id][64]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0FF00EF0"
    )
        port map (
      I0 => p_6_in,
      I1 => p_2_in,
      I2 => \reg[state]\(0),
      I3 => \reg[state]\(1),
      I4 => \crc_reg_n_0_[7]\,
      O => \reg[id][64]_i_11_n_0\
    );
\reg[id][64]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F000F000F000E00"
    )
        port map (
      I0 => p_1_in,
      I1 => \crc_reg_n_0_[0]\,
      I2 => \reg[state]\(0),
      I3 => \reg[state]\(1),
      I4 => p_5_in,
      I5 => p_4_in,
      O => \reg[id][64]_i_12_n_0\
    );
\reg[id][64]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000E000"
    )
        port map (
      I0 => \reg_reg[id][1]_0\,
      I1 => CO(0),
      I2 => \reg[id][64]_i_7_n_0\,
      I3 => \reg[state]\(2),
      I4 => \reg[state]\(1),
      I5 => \reg_reg[id][57]_0\,
      O => \reg[id][64]_i_4_n_0\
    );
\reg[id][64]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF80808A80"
    )
        port map (
      I0 => \reg[id][64]_i_7_n_0\,
      I1 => \reg_reg[id][1]_4\,
      I2 => \reg[state]\(2),
      I3 => if_done,
      I4 => \reg_reg[id][1]_5\,
      I5 => ow_rst,
      O => \reg[id][64]_i_5_n_0\
    );
\reg[id][64]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF00E0"
    )
        port map (
      I0 => pristine,
      I1 => p_3_in,
      I2 => \reg[state]\(1),
      I3 => \reg[state]\(0),
      I4 => \reg[id][64]_i_11_n_0\,
      I5 => \reg[id][64]_i_12_n_0\,
      O => \reg[id][64]_i_7_n_0\
    );
\reg[id][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][62]_0\,
      I2 => \reg[id][8]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(5),
      O => \reg_reg[id][6]\
    );
\reg[id][7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][63]_0\,
      I2 => \reg[id][8]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(6),
      O => \reg_reg[id][7]\
    );
\reg[id][8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFEFFAAAA0200"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][64]_0\,
      I2 => \reg_reg[id][64]_1\,
      I3 => \reg[id][8]_i_2_n_0\,
      I4 => \reg[id][64]_i_5_n_0\,
      I5 => dcvr_id(7),
      O => \reg_reg[id_bit_number][2]_13\
    );
\reg[id][8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000E00000"
    )
        port map (
      I0 => \reg_reg[id][1]_0\,
      I1 => CO(0),
      I2 => \reg[id][64]_i_7_n_0\,
      I3 => \reg_reg[id][1]_1\,
      I4 => \reg_reg[id][1]_2\,
      I5 => \reg_reg[id][1]_3\,
      O => \reg[id][8]_i_2_n_0\
    );
\reg[id][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEFAA20"
    )
        port map (
      I0 => \reg_reg[id][64]\,
      I1 => \reg_reg[id][57]_1\,
      I2 => \reg[id][16]_i_2_n_0\,
      I3 => \reg[id][64]_i_5_n_0\,
      I4 => dcvr_id(8),
      O => \reg_reg[id][9]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_onewire_crc_0 is
  port (
    \FSM_sequential_reg_reg[state][0]\ : out STD_LOGIC;
    \FSM_sequential_reg_reg[state][3]\ : out STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    ow_reg_rst : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    pristine_reg_0 : in STD_LOGIC;
    if_rx_data_en : in STD_LOGIC;
    if_rx_data : in STD_LOGIC;
    \reg_reg[data][0]\ : in STD_LOGIC;
    \reg_reg[data][0]_0\ : in STD_LOGIC;
    \reg_reg[data][0]_1\ : in STD_LOGIC;
    \reg_reg[data][0]_2\ : in STD_LOGIC;
    \reg_reg[data][0]_3\ : in STD_LOGIC;
    \reg_reg[data][30]\ : in STD_LOGIC;
    \reg_reg[data][30]_0\ : in STD_LOGIC;
    \reg_reg[data][30]_1\ : in STD_LOGIC;
    \reg_reg[data][30]_2\ : in STD_LOGIC;
    \reg_reg[data][30]_3\ : in STD_LOGIC;
    \reg_reg[data][30]_4\ : in STD_LOGIC;
    \reg_reg[data][30]_5\ : in STD_LOGIC;
    \reg_reg[data][30]_6\ : in STD_LOGIC;
    \reg[data][63]_i_5_0\ : in STD_LOGIC;
    \reg[state]\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_axi_onewire_0_0_onewire_crc_0 : entity is "onewire_crc";
end design_1_axi_onewire_0_0_onewire_crc_0;

architecture STRUCTURE of design_1_axi_onewire_0_0_onewire_crc_0 is
  signal \crc[7]_i_1__0_n_0\ : STD_LOGIC;
  signal \crc_reg_n_0_[0]\ : STD_LOGIC;
  signal \crc_reg_n_0_[7]\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal \p_1_in__0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_2_in : STD_LOGIC;
  signal p_3_in : STD_LOGIC;
  signal p_4_in : STD_LOGIC;
  signal p_5_in : STD_LOGIC;
  signal p_6_in : STD_LOGIC;
  signal pristine : STD_LOGIC;
  signal \pristine_i_1__0_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_12_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_13_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_14_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_5_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \crc[1]_i_1__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \crc[4]_i_1__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \crc[5]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \crc[6]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \crc[7]_i_2__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \pristine_i_1__0\ : label is "soft_lutpair7";
begin
\crc[0]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => p_3_in,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \p_1_in__0\(0)
    );
\crc[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => p_4_in,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \p_1_in__0\(1)
    );
\crc[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400000400040400"
    )
        port map (
      I0 => pristine_reg_0,
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => p_1_in,
      I4 => if_rx_data,
      I5 => \crc_reg_n_0_[0]\,
      O => \p_1_in__0\(2)
    );
\crc[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400000400040400"
    )
        port map (
      I0 => pristine_reg_0,
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => p_2_in,
      I4 => if_rx_data,
      I5 => \crc_reg_n_0_[0]\,
      O => \p_1_in__0\(3)
    );
\crc[4]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => p_5_in,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \p_1_in__0\(4)
    );
\crc[5]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => p_6_in,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \p_1_in__0\(5)
    );
\crc[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \crc_reg_n_0_[7]\,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => pristine_reg_0,
      O => \p_1_in__0\(6)
    );
\crc[7]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFB"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => pristine_reg_0,
      I3 => if_rx_data_en,
      O => \crc[7]_i_1__0_n_0\
    );
\crc[7]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000600"
    )
        port map (
      I0 => if_rx_data,
      I1 => \crc_reg_n_0_[0]\,
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => pristine_reg_0,
      O => \p_1_in__0\(7)
    );
\crc_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1__0_n_0\,
      D => \p_1_in__0\(0),
      Q => \crc_reg_n_0_[0]\,
      R => '0'
    );
\crc_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1__0_n_0\,
      D => \p_1_in__0\(1),
      Q => p_3_in,
      R => '0'
    );
\crc_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1__0_n_0\,
      D => \p_1_in__0\(2),
      Q => p_4_in,
      R => '0'
    );
\crc_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1__0_n_0\,
      D => \p_1_in__0\(3),
      Q => p_1_in,
      R => '0'
    );
\crc_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1__0_n_0\,
      D => \p_1_in__0\(4),
      Q => p_2_in,
      R => '0'
    );
\crc_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1__0_n_0\,
      D => \p_1_in__0\(5),
      Q => p_5_in,
      R => '0'
    );
\crc_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1__0_n_0\,
      D => \p_1_in__0\(6),
      Q => p_6_in,
      R => '0'
    );
\crc_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1__0_n_0\,
      D => \p_1_in__0\(7),
      Q => \crc_reg_n_0_[7]\,
      R => '0'
    );
\pristine_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => pristine_reg_0,
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \pristine_i_1__0_n_0\
    );
pristine_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \crc[7]_i_1__0_n_0\,
      D => \pristine_i_1__0_n_0\,
      Q => pristine,
      R => '0'
    );
\reg[data][63]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFCFFFC"
    )
        port map (
      I0 => \reg_reg[data][30]\,
      I1 => \reg_reg[data][30]_0\,
      I2 => \reg_reg[data][30]_1\,
      I3 => \reg[data][63]_i_5_n_0\,
      I4 => \reg_reg[data][0]_1\,
      I5 => \reg_reg[data][0]_3\,
      O => \FSM_sequential_reg_reg[state][3]\
    );
\reg[data][63]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000C80800000000"
    )
        port map (
      I0 => if_rx_data_en,
      I1 => \reg[data][63]_i_5_0\,
      I2 => \reg[state]\(0),
      I3 => \reg[data][63]_i_13_n_0\,
      I4 => \reg_reg[data][0]_2\,
      I5 => \reg[state]\(1),
      O => \reg[data][63]_i_12_n_0\
    );
\reg[data][63]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \reg[data][63]_i_14_n_0\,
      I1 => p_5_in,
      I2 => p_2_in,
      I3 => p_1_in,
      I4 => p_4_in,
      O => \reg[data][63]_i_13_n_0\
    );
\reg[data][63]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pristine,
      I1 => p_6_in,
      I2 => \crc_reg_n_0_[7]\,
      I3 => \crc_reg_n_0_[0]\,
      I4 => p_3_in,
      O => \reg[data][63]_i_14_n_0\
    );
\reg[data][63]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEAA0000"
    )
        port map (
      I0 => \reg_reg[data][30]_2\,
      I1 => \reg_reg[data][30]_3\,
      I2 => \reg_reg[data][30]_4\,
      I3 => \reg_reg[data][30]_5\,
      I4 => \reg_reg[data][30]_6\,
      I5 => \reg[data][63]_i_12_n_0\,
      O => \reg[data][63]_i_5_n_0\
    );
\reg[data][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFEFCFCFFFE"
    )
        port map (
      I0 => \reg_reg[data][0]\,
      I1 => \reg_reg[data][0]_0\,
      I2 => \reg[data][63]_i_5_n_0\,
      I3 => \reg_reg[data][0]_1\,
      I4 => \reg_reg[data][0]_2\,
      I5 => \reg_reg[data][0]_3\,
      O => \FSM_sequential_reg_reg[state][0]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_onewire_interface is
  port (
    if_rx_data : out STD_LOGIC;
    if_rx_data_en : out STD_LOGIC;
    if_done : out STD_LOGIC;
    ow_o : out STD_LOGIC;
    \reg_reg[done]_0\ : out STD_LOGIC;
    \reg_reg[done]_1\ : out STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    ctrl_bit_tx : in STD_LOGIC;
    dcvr_bit_tx : in STD_LOGIC;
    ow_rst : in STD_LOGIC;
    ow_i : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    ow_reg_rst : in STD_LOGIC;
    \FSM_sequential_reg_reg[state][1]_0\ : in STD_LOGIC;
    ctrl_bus_rst : in STD_LOGIC;
    dcvr_bus_rst : in STD_LOGIC;
    \reg_reg[data]_0\ : in STD_LOGIC;
    \reg_reg[data]_1\ : in STD_LOGIC;
    \reg_reg[bit_send]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_sequential_reg_reg[state][0]_0\ : in STD_LOGIC
  );
end design_1_axi_onewire_0_0_onewire_interface;

architecture STRUCTURE of design_1_axi_onewire_0_0_onewire_interface is
  signal \FSM_sequential_reg[state][0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_4_n_0\ : STD_LOGIC;
  signal \^if_done\ : STD_LOGIC;
  signal \^if_rx_data\ : STD_LOGIC;
  signal lfsr_shift : STD_LOGIC_VECTOR ( 16 downto 1 );
  signal \nxt_reg[tx]__0\ : STD_LOGIC;
  signal \^ow_o\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \reg[data]\ : STD_LOGIC;
  signal \reg[data]_i_11_n_0\ : STD_LOGIC;
  signal \reg[data]_i_12_n_0\ : STD_LOGIC;
  signal \reg[data]_i_13_n_0\ : STD_LOGIC;
  signal \reg[data]_i_14_n_0\ : STD_LOGIC;
  signal \reg[data]_i_15_n_0\ : STD_LOGIC;
  signal \reg[data]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data]_i_3_n_0\ : STD_LOGIC;
  signal \reg[data]_i_4_n_0\ : STD_LOGIC;
  signal \reg[data]_i_5_n_0\ : STD_LOGIC;
  signal \reg[data]_i_6_n_0\ : STD_LOGIC;
  signal \reg[data]_i_7_n_0\ : STD_LOGIC;
  signal \reg[data]_i_8_n_0\ : STD_LOGIC;
  signal \reg[data_en]\ : STD_LOGIC;
  signal \reg[done]\ : STD_LOGIC;
  signal \reg[done]_i_2_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_3_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_4_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_5__0_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_6__0_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_7_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_8_n_0\ : STD_LOGIC;
  signal \reg[lfsr][16]_i_1_n_0\ : STD_LOGIC;
  signal \reg[lfsr][16]_i_3_n_0\ : STD_LOGIC;
  signal \reg[lfsr][16]_i_4_n_0\ : STD_LOGIC;
  signal \reg[lfsr][16]_i_5_n_0\ : STD_LOGIC;
  signal \reg[lfsr][16]_i_6_n_0\ : STD_LOGIC;
  signal \reg[lfsr][16]_i_7_n_0\ : STD_LOGIC;
  signal \reg[state]\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \reg[tx]_i_10_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_11_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_12_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_13_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_14_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_1_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_2_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_3_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_4_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_5_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_7_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_8_n_0\ : STD_LOGIC;
  signal \reg[tx]_i_9_n_0\ : STD_LOGIC;
  signal \reg_reg[lfsr_n_0_][16]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][1]_i_3\ : label is "soft_lutpair66";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_reg_reg[state][0]\ : label is "reset:01,send:10,idle:00,receive:11";
  attribute FSM_ENCODED_STATES of \FSM_sequential_reg_reg[state][1]\ : label is "reset:01,send:10,idle:00,receive:11";
  attribute SOFT_HLUTNM of \reg[bit_send]_i_3__0\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \reg[data][63]_i_10\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \reg[data]_i_14\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \reg[lfsr][0]_i_5__0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \reg[lfsr][0]_i_7\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \reg[lfsr][0]_i_8\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \reg[lfsr][10]_i_1__0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \reg[lfsr][11]_i_1__0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \reg[lfsr][12]_i_1__0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \reg[lfsr][13]_i_1__0\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \reg[lfsr][14]_i_1__0\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \reg[lfsr][15]_i_1__0\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \reg[lfsr][16]_i_2\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \reg[lfsr][16]_i_4\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \reg[lfsr][16]_i_5\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \reg[lfsr][16]_i_6\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \reg[lfsr][1]_i_1__1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \reg[lfsr][2]_i_1__1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \reg[lfsr][3]_i_1__1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \reg[lfsr][4]_i_1__0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \reg[lfsr][5]_i_1__0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \reg[lfsr][6]_i_1__0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \reg[lfsr][7]_i_1__0\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \reg[lfsr][8]_i_1__0\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \reg[lfsr][9]_i_1__0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \reg[tx]_i_12\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \reg[tx]_i_13\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \reg[tx]_i_8\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \reg[tx]_i_9\ : label is "soft_lutpair71";
begin
  if_done <= \^if_done\;
  if_rx_data <= \^if_rx_data\;
  ow_o <= \^ow_o\;
\FSM_sequential_reg[state][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3332FFFF33320000"
    )
        port map (
      I0 => \FSM_sequential_reg_reg[state][0]_0\,
      I1 => \reg[lfsr][16]_i_1_n_0\,
      I2 => dcvr_bus_rst,
      I3 => ctrl_bus_rst,
      I4 => \FSM_sequential_reg[state][1]_i_3_n_0\,
      I5 => \reg[state]\(0),
      O => \FSM_sequential_reg[state][0]_i_1_n_0\
    );
\FSM_sequential_reg[state][1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100FFFF01000000"
    )
        port map (
      I0 => dcvr_bus_rst,
      I1 => ctrl_bus_rst,
      I2 => \reg[lfsr][16]_i_1_n_0\,
      I3 => \FSM_sequential_reg_reg[state][1]_0\,
      I4 => \FSM_sequential_reg[state][1]_i_3_n_0\,
      I5 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][1]_i_1_n_0\
    );
\FSM_sequential_reg[state][1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFAEAA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_4_n_0\,
      I1 => \reg[done]_i_2_n_0\,
      I2 => \reg[state]\(1),
      I3 => \reg[state]\(0),
      I4 => \reg[data]_i_3_n_0\,
      O => \FSM_sequential_reg[state][1]_i_3_n_0\
    );
\FSM_sequential_reg[state][1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF11111110"
    )
        port map (
      I0 => \reg[state]\(0),
      I1 => \reg[state]\(1),
      I2 => \FSM_sequential_reg_reg[state][1]_0\,
      I3 => ctrl_bus_rst,
      I4 => dcvr_bus_rst,
      I5 => ow_rst,
      O => \FSM_sequential_reg[state][1]_i_4_n_0\
    );
\FSM_sequential_reg_reg[state][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => '1',
      D => \FSM_sequential_reg[state][0]_i_1_n_0\,
      Q => \reg[state]\(0),
      R => '0'
    );
\FSM_sequential_reg_reg[state][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => '1',
      D => \FSM_sequential_reg[state][1]_i_1_n_0\,
      Q => \reg[state]\(1),
      R => '0'
    );
\reg[bit_send]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^if_done\,
      I1 => \reg_reg[bit_send]\(1),
      O => \reg_reg[done]_0\
    );
\reg[data][63]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^if_done\,
      I1 => \reg_reg[bit_send]\(0),
      O => \reg_reg[done]_1\
    );
\reg[data]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAFBAAAAAA08"
    )
        port map (
      I0 => \reg[data]\,
      I1 => \reg[data]_i_3_n_0\,
      I2 => \reg[state]\(0),
      I3 => \reg[data]_i_4_n_0\,
      I4 => \reg[data]_i_5_n_0\,
      I5 => \^if_rx_data\,
      O => \reg[data]_i_1_n_0\
    );
\reg[data]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0444000000000000"
    )
        port map (
      I0 => \reg[lfsr][16]_i_7_n_0\,
      I1 => \reg[data]_i_12_n_0\,
      I2 => lfsr_shift(10),
      I3 => lfsr_shift(11),
      I4 => lfsr_shift(14),
      I5 => \reg[data]_i_13_n_0\,
      O => \reg[data]_i_11_n_0\
    );
\reg[data]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040000000000000"
    )
        port map (
      I0 => lfsr_shift(1),
      I1 => lfsr_shift(8),
      I2 => \reg[data]_i_14_n_0\,
      I3 => lfsr_shift(13),
      I4 => lfsr_shift(16),
      I5 => \reg[data]_i_15_n_0\,
      O => \reg[data]_i_12_n_0\
    );
\reg[data]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000D5000000D5D5"
    )
        port map (
      I0 => lfsr_shift(6),
      I1 => lfsr_shift(4),
      I2 => lfsr_shift(3),
      I3 => lfsr_shift(1),
      I4 => lfsr_shift(2),
      I5 => lfsr_shift(15),
      O => \reg[data]_i_13_n_0\
    );
\reg[data]_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \reg[state]\(0),
      I1 => \reg[state]\(1),
      O => \reg[data]_i_14_n_0\
    );
\reg[data]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001500"
    )
        port map (
      I0 => lfsr_shift(9),
      I1 => lfsr_shift(7),
      I2 => lfsr_shift(8),
      I3 => lfsr_shift(4),
      I4 => lfsr_shift(5),
      I5 => lfsr_shift(12),
      O => \reg[data]_i_15_n_0\
    );
\reg[data]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FF000000320032"
    )
        port map (
      I0 => ctrl_bit_tx,
      I1 => \reg[state]\(1),
      I2 => dcvr_bit_tx,
      I3 => ow_rst,
      I4 => ow_i,
      I5 => \reg[state]\(0),
      O => \reg[data]\
    );
\reg[data]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[lfsr][0]_i_3_n_0\,
      O => \reg[data]_i_3_n_0\
    );
\reg[data]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000888800000000"
    )
        port map (
      I0 => \reg[data]_i_6_n_0\,
      I1 => \reg[data]_i_7_n_0\,
      I2 => lfsr_shift(12),
      I3 => lfsr_shift(14),
      I4 => lfsr_shift(15),
      I5 => \reg[data]_i_8_n_0\,
      O => \reg[data]_i_4_n_0\
    );
\reg[data]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAAABAAAA"
    )
        port map (
      I0 => ow_rst,
      I1 => \reg[state]\(0),
      I2 => \reg[state]\(1),
      I3 => \reg_reg[data]_0\,
      I4 => \reg_reg[data]_1\,
      I5 => \reg[data]_i_11_n_0\,
      O => \reg[data]_i_5_n_0\
    );
\reg[data]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => \reg_reg[lfsr_n_0_][16]\,
      I1 => lfsr_shift(14),
      I2 => lfsr_shift(13),
      I3 => lfsr_shift(15),
      I4 => lfsr_shift(2),
      I5 => lfsr_shift(9),
      O => \reg[data]_i_6_n_0\
    );
\reg[data]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \reg[state]\(0),
      I1 => \reg[state]\(1),
      I2 => lfsr_shift(8),
      I3 => lfsr_shift(7),
      I4 => lfsr_shift(6),
      I5 => lfsr_shift(16),
      O => \reg[data]_i_7_n_0\
    );
\reg[data]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => lfsr_shift(11),
      I1 => lfsr_shift(10),
      I2 => lfsr_shift(1),
      I3 => lfsr_shift(5),
      I4 => lfsr_shift(3),
      I5 => lfsr_shift(4),
      O => \reg[data]_i_8_n_0\
    );
\reg[data_en]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(0),
      I3 => \reg[data]_i_3_n_0\,
      O => \reg[data_en]\
    );
\reg[done]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AEAA0000"
    )
        port map (
      I0 => \reg[data]_i_3_n_0\,
      I1 => \reg[done]_i_2_n_0\,
      I2 => \reg[state]\(1),
      I3 => \reg[state]\(0),
      I4 => axi_aresetn_i,
      I5 => ow_reg_rst,
      O => \reg[done]\
    );
\reg[done]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000015"
    )
        port map (
      I0 => \reg[lfsr][16]_i_5_n_0\,
      I1 => lfsr_shift(8),
      I2 => lfsr_shift(7),
      I3 => lfsr_shift(9),
      I4 => \reg[lfsr][16]_i_4_n_0\,
      O => \reg[done]_i_2_n_0\
    );
\reg[lfsr][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEFFEAFFA"
    )
        port map (
      I0 => ow_rst,
      I1 => \reg[lfsr][0]_i_3_n_0\,
      I2 => lfsr_shift(14),
      I3 => \reg_reg[lfsr_n_0_][16]\,
      I4 => \reg[state]\(1),
      I5 => \reg[lfsr][0]_i_4_n_0\,
      O => p_1_in(0)
    );
\reg[lfsr][0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000000000000"
    )
        port map (
      I0 => \reg[lfsr][0]_i_5__0_n_0\,
      I1 => \reg_reg[lfsr_n_0_][16]\,
      I2 => \reg[lfsr][0]_i_6__0_n_0\,
      I3 => \reg[lfsr][0]_i_7_n_0\,
      I4 => lfsr_shift(3),
      I5 => lfsr_shift(4),
      O => \reg[lfsr][0]_i_3_n_0\
    );
\reg[lfsr][0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001000000"
    )
        port map (
      I0 => \reg[lfsr][0]_i_8_n_0\,
      I1 => lfsr_shift(7),
      I2 => \reg[state]\(1),
      I3 => lfsr_shift(15),
      I4 => lfsr_shift(16),
      I5 => \reg[lfsr][16]_i_4_n_0\,
      O => \reg[lfsr][0]_i_4_n_0\
    );
\reg[lfsr][0]_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => lfsr_shift(16),
      I1 => lfsr_shift(6),
      I2 => lfsr_shift(7),
      O => \reg[lfsr][0]_i_5__0_n_0\
    );
\reg[lfsr][0]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => lfsr_shift(9),
      I1 => lfsr_shift(2),
      I2 => lfsr_shift(11),
      I3 => lfsr_shift(10),
      I4 => lfsr_shift(14),
      I5 => lfsr_shift(12),
      O => \reg[lfsr][0]_i_6__0_n_0\
    );
\reg[lfsr][0]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFBFFF"
    )
        port map (
      I0 => lfsr_shift(1),
      I1 => lfsr_shift(8),
      I2 => lfsr_shift(13),
      I3 => lfsr_shift(5),
      I4 => lfsr_shift(15),
      O => \reg[lfsr][0]_i_7_n_0\
    );
\reg[lfsr][0]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => lfsr_shift(4),
      I1 => lfsr_shift(3),
      O => \reg[lfsr][0]_i_8_n_0\
    );
\reg[lfsr][10]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(10),
      O => p_1_in(10)
    );
\reg[lfsr][11]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(11),
      O => p_1_in(11)
    );
\reg[lfsr][12]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(12),
      O => p_1_in(12)
    );
\reg[lfsr][13]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(13),
      O => p_1_in(13)
    );
\reg[lfsr][14]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(14),
      O => p_1_in(14)
    );
\reg[lfsr][15]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(15),
      O => p_1_in(15)
    );
\reg[lfsr][16]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(0),
      I2 => axi_aresetn_i,
      I3 => ow_reg_rst,
      O => \reg[lfsr][16]_i_1_n_0\
    );
\reg[lfsr][16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(16),
      O => p_1_in(16)
    );
\reg[lfsr][16]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000FEFFFE"
    )
        port map (
      I0 => \reg[lfsr][16]_i_4_n_0\,
      I1 => lfsr_shift(7),
      I2 => \reg[lfsr][16]_i_5_n_0\,
      I3 => \reg[state]\(1),
      I4 => \reg[lfsr][0]_i_3_n_0\,
      I5 => ow_rst,
      O => \reg[lfsr][16]_i_3_n_0\
    );
\reg[lfsr][16]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => lfsr_shift(12),
      I1 => lfsr_shift(10),
      I2 => \reg[lfsr][16]_i_6_n_0\,
      I3 => \reg[lfsr][16]_i_7_n_0\,
      O => \reg[lfsr][16]_i_4_n_0\
    );
\reg[lfsr][16]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => lfsr_shift(15),
      I1 => lfsr_shift(16),
      I2 => lfsr_shift(3),
      I3 => lfsr_shift(4),
      O => \reg[lfsr][16]_i_5_n_0\
    );
\reg[lfsr][16]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => lfsr_shift(5),
      I1 => lfsr_shift(13),
      I2 => lfsr_shift(8),
      I3 => lfsr_shift(1),
      O => \reg[lfsr][16]_i_6_n_0\
    );
\reg[lfsr][16]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFFFFFFFFFFFF"
    )
        port map (
      I0 => lfsr_shift(2),
      I1 => \reg_reg[lfsr_n_0_][16]\,
      I2 => lfsr_shift(14),
      I3 => lfsr_shift(9),
      I4 => lfsr_shift(6),
      I5 => lfsr_shift(11),
      O => \reg[lfsr][16]_i_7_n_0\
    );
\reg[lfsr][1]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(1),
      O => p_1_in(1)
    );
\reg[lfsr][2]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(2),
      O => p_1_in(2)
    );
\reg[lfsr][3]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(3),
      O => p_1_in(3)
    );
\reg[lfsr][4]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(4),
      O => p_1_in(4)
    );
\reg[lfsr][5]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(5),
      O => p_1_in(5)
    );
\reg[lfsr][6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(6),
      O => p_1_in(6)
    );
\reg[lfsr][7]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(7),
      O => p_1_in(7)
    );
\reg[lfsr][8]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(8),
      O => p_1_in(8)
    );
\reg[lfsr][9]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg[lfsr][16]_i_3_n_0\,
      I1 => lfsr_shift(9),
      O => p_1_in(9)
    );
\reg[tx]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \reg[tx]_i_2_n_0\,
      I1 => \reg[tx]_i_3_n_0\,
      I2 => \^ow_o\,
      O => \reg[tx]_i_1_n_0\
    );
\reg[tx]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFBE"
    )
        port map (
      I0 => lfsr_shift(7),
      I1 => lfsr_shift(13),
      I2 => lfsr_shift(12),
      I3 => \reg[tx]_i_13_n_0\,
      I4 => lfsr_shift(1),
      O => \reg[tx]_i_10_n_0\
    );
\reg[tx]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000020000"
    )
        port map (
      I0 => \reg[tx]_i_14_n_0\,
      I1 => lfsr_shift(4),
      I2 => lfsr_shift(5),
      I3 => lfsr_shift(8),
      I4 => lfsr_shift(10),
      I5 => lfsr_shift(12),
      O => \reg[tx]_i_11_n_0\
    );
\reg[tx]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => lfsr_shift(6),
      I1 => lfsr_shift(7),
      I2 => lfsr_shift(5),
      I3 => lfsr_shift(13),
      O => \reg[tx]_i_12_n_0\
    );
\reg[tx]_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F7"
    )
        port map (
      I0 => lfsr_shift(11),
      I1 => lfsr_shift(6),
      I2 => lfsr_shift(9),
      O => \reg[tx]_i_13_n_0\
    );
\reg[tx]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000800000"
    )
        port map (
      I0 => lfsr_shift(16),
      I1 => lfsr_shift(15),
      I2 => \reg[state]\(0),
      I3 => \reg[state]\(1),
      I4 => lfsr_shift(14),
      I5 => \reg_reg[lfsr_n_0_][16]\,
      O => \reg[tx]_i_14_n_0\
    );
\reg[tx]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFA8FFFFFFFF"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[tx]_i_4_n_0\,
      I2 => \^if_rx_data\,
      I3 => \reg[state]\(0),
      I4 => ow_reg_rst,
      I5 => axi_aresetn_i,
      O => \reg[tx]_i_2_n_0\
    );
\reg[tx]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFCCFFCCEFCCECCC"
    )
        port map (
      I0 => \reg[lfsr][0]_i_3_n_0\,
      I1 => \reg[tx]_i_5_n_0\,
      I2 => \reg[state]\(0),
      I3 => \reg[state]\(1),
      I4 => \reg[tx]_i_4_n_0\,
      I5 => \nxt_reg[tx]__0\,
      O => \reg[tx]_i_3_n_0\
    );
\reg[tx]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000A800000008000"
    )
        port map (
      I0 => \reg[tx]_i_7_n_0\,
      I1 => \reg[tx]_i_8_n_0\,
      I2 => lfsr_shift(4),
      I3 => lfsr_shift(7),
      I4 => lfsr_shift(6),
      I5 => \reg[tx]_i_9_n_0\,
      O => \reg[tx]_i_4_n_0\
    );
\reg[tx]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF01400000"
    )
        port map (
      I0 => \reg[tx]_i_10_n_0\,
      I1 => lfsr_shift(3),
      I2 => lfsr_shift(4),
      I3 => lfsr_shift(2),
      I4 => \reg[tx]_i_11_n_0\,
      I5 => \FSM_sequential_reg[state][1]_i_4_n_0\,
      O => \reg[tx]_i_5_n_0\
    );
\reg[tx]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000040"
    )
        port map (
      I0 => \reg[lfsr][16]_i_5_n_0\,
      I1 => \reg[tx]_i_12_n_0\,
      I2 => lfsr_shift(1),
      I3 => lfsr_shift(8),
      I4 => \reg_reg[lfsr_n_0_][16]\,
      I5 => \reg[lfsr][0]_i_6__0_n_0\,
      O => \nxt_reg[tx]__0\
    );
\reg[tx]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000020000000"
    )
        port map (
      I0 => lfsr_shift(10),
      I1 => lfsr_shift(11),
      I2 => lfsr_shift(2),
      I3 => lfsr_shift(9),
      I4 => lfsr_shift(3),
      I5 => \reg[lfsr][0]_i_7_n_0\,
      O => \reg[tx]_i_7_n_0\
    );
\reg[tx]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => lfsr_shift(16),
      I1 => \reg_reg[lfsr_n_0_][16]\,
      I2 => lfsr_shift(14),
      I3 => lfsr_shift(12),
      O => \reg[tx]_i_8_n_0\
    );
\reg[tx]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \reg_reg[lfsr_n_0_][16]\,
      I1 => lfsr_shift(16),
      I2 => lfsr_shift(14),
      I3 => lfsr_shift(12),
      O => \reg[tx]_i_9_n_0\
    );
\reg_reg[data]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[data]_i_1_n_0\,
      Q => \^if_rx_data\,
      R => '0'
    );
\reg_reg[data_en]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[data_en]\,
      Q => if_rx_data_en,
      R => '0'
    );
\reg_reg[done]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[done]\,
      Q => \^if_done\,
      R => '0'
    );
\reg_reg[lfsr][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(0),
      Q => lfsr_shift(1),
      R => '0'
    );
\reg_reg[lfsr][10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(10),
      Q => lfsr_shift(11),
      R => '0'
    );
\reg_reg[lfsr][11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(11),
      Q => lfsr_shift(12),
      R => '0'
    );
\reg_reg[lfsr][12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(12),
      Q => lfsr_shift(13),
      R => '0'
    );
\reg_reg[lfsr][13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(13),
      Q => lfsr_shift(14),
      R => '0'
    );
\reg_reg[lfsr][14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(14),
      Q => lfsr_shift(15),
      R => '0'
    );
\reg_reg[lfsr][15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(15),
      Q => lfsr_shift(16),
      R => '0'
    );
\reg_reg[lfsr][16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(16),
      Q => \reg_reg[lfsr_n_0_][16]\,
      R => '0'
    );
\reg_reg[lfsr][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(1),
      Q => lfsr_shift(2),
      R => '0'
    );
\reg_reg[lfsr][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(2),
      Q => lfsr_shift(3),
      R => '0'
    );
\reg_reg[lfsr][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(3),
      Q => lfsr_shift(4),
      R => '0'
    );
\reg_reg[lfsr][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(4),
      Q => lfsr_shift(5),
      R => '0'
    );
\reg_reg[lfsr][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(5),
      Q => lfsr_shift(6),
      R => '0'
    );
\reg_reg[lfsr][6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(6),
      Q => lfsr_shift(7),
      R => '0'
    );
\reg_reg[lfsr][7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(7),
      Q => lfsr_shift(8),
      R => '0'
    );
\reg_reg[lfsr][8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(8),
      Q => lfsr_shift(9),
      R => '0'
    );
\reg_reg[lfsr][9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][16]_i_1_n_0\,
      D => p_1_in(9),
      Q => lfsr_shift(10),
      R => '0'
    );
\reg_reg[tx]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[tx]_i_1_n_0\,
      Q => \^ow_o\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_onewire_regs_axi is
  port (
    ow_en_o : out STD_LOGIC;
    axi_rvalid_o : out STD_LOGIC;
    axi_wdone_reg_0 : out STD_LOGIC;
    ow_trig : out STD_LOGIC;
    ow_reg_rst : out STD_LOGIC;
    mem_readout_rt_reg_0 : out STD_LOGIC;
    rd_data_en : out STD_LOGIC;
    ow_rst : out STD_LOGIC;
    ADDRA : out STD_LOGIC_VECTOR ( 5 downto 0 );
    axi_awready_o : out STD_LOGIC;
    axi_wready_o : out STD_LOGIC;
    axi_arready_o : out STD_LOGIC;
    ow_ctrl_o : out STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_rdata_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_clk_i : in STD_LOGIC;
    axi_awvalid_i : in STD_LOGIC;
    axi_wvalid_i : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    ow_busy : in STD_LOGIC;
    mem_rd_data_en : in STD_LOGIC;
    axi_arvalid_i : in STD_LOGIC;
    axi_rready_i : in STD_LOGIC;
    ctrl_rd_en : in STD_LOGIC;
    ctrl_rd_addr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    ow_busy_o : in STD_LOGIC;
    ow_rd_data : in STD_LOGIC_VECTOR ( 63 downto 0 );
    ow_done_o : in STD_LOGIC;
    ow_dev_count : in STD_LOGIC_VECTOR ( 5 downto 0 );
    ow_too_many : in STD_LOGIC;
    axi_bready_i : in STD_LOGIC;
    axi_awaddr_i : in STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_araddr_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    axi_wdata_i : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end design_1_axi_onewire_0_0_onewire_regs_axi;

architecture STRUCTURE of design_1_axi_onewire_0_0_onewire_regs_axi is
  signal axi_arset : STD_LOGIC;
  signal axi_arset_i_1_n_0 : STD_LOGIC;
  signal axi_awset : STD_LOGIC;
  signal axi_awset_i_1_n_0 : STD_LOGIC;
  signal axi_rdone_i_1_n_0 : STD_LOGIC;
  signal \^axi_rvalid_o\ : STD_LOGIC;
  signal axi_wdone_i_1_n_0 : STD_LOGIC;
  signal axi_wdone_i_2_n_0 : STD_LOGIC;
  signal axi_wdone_i_4_n_0 : STD_LOGIC;
  signal \^axi_wdone_reg_0\ : STD_LOGIC;
  signal axi_wset_i_1_n_0 : STD_LOGIC;
  signal axi_wset_reg_n_0 : STD_LOGIC;
  signal control_wack : STD_LOGIC;
  signal control_wreq : STD_LOGIC;
  signal mem_readout_rr : STD_LOGIC;
  signal mem_readout_rr0 : STD_LOGIC;
  signal mem_readout_rt : STD_LOGIC;
  signal mem_readout_rt0 : STD_LOGIC;
  signal mem_readout_wr : STD_LOGIC;
  signal mem_readout_wr0 : STD_LOGIC;
  signal mem_readout_wt0 : STD_LOGIC;
  signal mem_readout_wt_reg_n_0 : STD_LOGIC;
  signal mux_wack : STD_LOGIC;
  signal mux_wreq : STD_LOGIC;
  signal \^ow_ctrl_o\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^ow_en_o\ : STD_LOGIC;
  signal \^ow_reg_rst\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal rd_ack : STD_LOGIC;
  signal rd_ack_d0 : STD_LOGIC;
  signal rd_addr : STD_LOGIC;
  signal \rd_addr_reg_n_0_[10]\ : STD_LOGIC;
  signal \rd_addr_reg_n_0_[2]\ : STD_LOGIC;
  signal \rd_addr_reg_n_0_[3]\ : STD_LOGIC;
  signal \rd_addr_reg_n_0_[4]\ : STD_LOGIC;
  signal \rd_addr_reg_n_0_[5]\ : STD_LOGIC;
  signal \rd_addr_reg_n_0_[6]\ : STD_LOGIC;
  signal \rd_addr_reg_n_0_[7]\ : STD_LOGIC;
  signal \rd_addr_reg_n_0_[8]\ : STD_LOGIC;
  signal rd_dat_d0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rd_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \rd_data[0]_i_2_n_0\ : STD_LOGIC;
  signal \rd_data[1]_i_2_n_0\ : STD_LOGIC;
  signal \rd_data[2]_i_2_n_0\ : STD_LOGIC;
  signal \rd_data[3]_i_2_n_0\ : STD_LOGIC;
  signal \rd_data[8]_i_2_n_0\ : STD_LOGIC;
  signal \rd_data[8]_i_3_n_0\ : STD_LOGIC;
  signal \rd_data[8]_i_4_n_0\ : STD_LOGIC;
  signal rd_req : STD_LOGIC;
  signal rd_req0 : STD_LOGIC;
  signal regs_control_rst_reg_i_1_n_0 : STD_LOGIC;
  signal regs_control_trig_reg_i_1_n_0 : STD_LOGIC;
  signal \regs_mux_ctrl_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \regs_mux_ctrl_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal wr_ack : STD_LOGIC;
  signal wr_addr : STD_LOGIC_VECTOR ( 10 downto 2 );
  signal wr_addr_0 : STD_LOGIC;
  signal \wr_adr_d0_reg_n_0_[2]\ : STD_LOGIC;
  signal \wr_adr_d0_reg_n_0_[3]\ : STD_LOGIC;
  signal \wr_adr_d0_reg_n_0_[4]\ : STD_LOGIC;
  signal \wr_adr_d0_reg_n_0_[5]\ : STD_LOGIC;
  signal \wr_adr_d0_reg_n_0_[6]\ : STD_LOGIC;
  signal \wr_adr_d0_reg_n_0_[7]\ : STD_LOGIC;
  signal \wr_adr_d0_reg_n_0_[8]\ : STD_LOGIC;
  signal \wr_adr_d0_reg_n_0_[9]\ : STD_LOGIC;
  signal \wr_dat_d0_reg_n_0_[0]\ : STD_LOGIC;
  signal wr_data : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal wr_data_1 : STD_LOGIC;
  signal wr_req : STD_LOGIC;
  signal wr_req_d0 : STD_LOGIC;
  signal wr_req_i_1_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_o_INST_0 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_arset_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_awready_o_INST_0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of axi_wready_o_INST_0 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of axi_wset_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \rd_data[0]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \rd_data[63]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \rd_data[8]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \rd_data[8]_i_3\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \rd_data[8]_i_4\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of rd_data_en_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of regs_control_rst_reg_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of regs_control_trig_reg_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of wr_req_i_1 : label is "soft_lutpair0";
begin
  axi_rvalid_o <= \^axi_rvalid_o\;
  axi_wdone_reg_0 <= \^axi_wdone_reg_0\;
  ow_ctrl_o(2 downto 0) <= \^ow_ctrl_o\(2 downto 0);
  ow_en_o <= \^ow_en_o\;
  ow_reg_rst <= \^ow_reg_rst\;
axi_arready_o_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_arset,
      O => axi_arready_o
    );
axi_arset_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00A8A8A8"
    )
        port map (
      I0 => axi_aresetn_i,
      I1 => axi_arvalid_i,
      I2 => axi_arset,
      I3 => axi_rready_i,
      I4 => \^axi_rvalid_o\,
      O => axi_arset_i_1_n_0
    );
axi_arset_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => axi_arset_i_1_n_0,
      Q => axi_arset,
      R => '0'
    );
axi_awready_o_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_awset,
      O => axi_awready_o
    );
axi_awset_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00E0E0E0"
    )
        port map (
      I0 => axi_awset,
      I1 => axi_awvalid_i,
      I2 => axi_aresetn_i,
      I3 => \^axi_wdone_reg_0\,
      I4 => axi_bready_i,
      O => axi_awset_i_1_n_0
    );
axi_awset_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => axi_awset_i_1_n_0,
      Q => axi_awset,
      R => '0'
    );
axi_rdone_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => rd_ack,
      I1 => axi_rready_i,
      I2 => \^axi_rvalid_o\,
      O => axi_rdone_i_1_n_0
    );
axi_rdone_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => axi_rdone_i_1_n_0,
      Q => \^axi_rvalid_o\,
      R => axi_wdone_i_1_n_0
    );
axi_wdone_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_aresetn_i,
      O => axi_wdone_i_1_n_0
    );
axi_wdone_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => axi_bready_i,
      I1 => \^axi_wdone_reg_0\,
      I2 => wr_ack,
      O => axi_wdone_i_2_n_0
    );
axi_wdone_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20FF2000"
    )
        port map (
      I0 => mem_readout_wt_reg_n_0,
      I1 => ow_busy,
      I2 => mem_rd_data_en,
      I3 => p_0_in,
      I4 => axi_wdone_i_4_n_0,
      O => wr_ack
    );
axi_wdone_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABA8ABABABA8A8A8"
    )
        port map (
      I0 => wr_req_d0,
      I1 => \regs_mux_ctrl_reg[2]_i_3_n_0\,
      I2 => \wr_adr_d0_reg_n_0_[3]\,
      I3 => control_wack,
      I4 => \wr_adr_d0_reg_n_0_[2]\,
      I5 => mux_wack,
      O => axi_wdone_i_4_n_0
    );
axi_wdone_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => axi_wdone_i_2_n_0,
      Q => \^axi_wdone_reg_0\,
      R => axi_wdone_i_1_n_0
    );
axi_wready_o_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_wset_reg_n_0,
      O => axi_wready_o
    );
axi_wset_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00E0E0E0"
    )
        port map (
      I0 => axi_wset_reg_n_0,
      I1 => axi_wvalid_i,
      I2 => axi_aresetn_i,
      I3 => \^axi_wdone_reg_0\,
      I4 => axi_bready_i,
      O => axi_wset_i_1_n_0
    );
axi_wset_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => axi_wset_i_1_n_0,
      Q => axi_wset_reg_n_0,
      R => '0'
    );
control_wack_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \wr_adr_d0_reg_n_0_[2]\,
      I1 => \regs_mux_ctrl_reg[2]_i_2_n_0\,
      O => control_wreq
    );
control_wack_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => control_wreq,
      Q => control_wack,
      R => axi_wdone_i_1_n_0
    );
mem_readout_rr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8F800F8F8F8F8F8"
    )
        port map (
      I0 => rd_req,
      I1 => \rd_addr_reg_n_0_[10]\,
      I2 => mem_readout_rr,
      I3 => mem_rd_data_en,
      I4 => ow_busy,
      I5 => mem_readout_rt,
      O => mem_readout_rr0
    );
mem_readout_rr_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => mem_readout_rr0,
      Q => mem_readout_rr,
      R => axi_wdone_i_1_n_0
    );
mem_readout_rt_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBBBB000F0000"
    )
        port map (
      I0 => ow_busy,
      I1 => mem_rd_data_en,
      I2 => mem_readout_wr,
      I3 => mem_readout_wt_reg_n_0,
      I4 => mem_readout_rr,
      I5 => mem_readout_rt,
      O => mem_readout_rt0
    );
mem_readout_rt_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => mem_readout_rt0,
      Q => mem_readout_rt,
      R => axi_wdone_i_1_n_0
    );
mem_readout_wr_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8F800F8F8F8F8F8"
    )
        port map (
      I0 => p_0_in,
      I1 => wr_req_d0,
      I2 => mem_readout_wr,
      I3 => mem_rd_data_en,
      I4 => ow_busy,
      I5 => mem_readout_wt_reg_n_0,
      O => mem_readout_wr0
    );
mem_readout_wr_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => mem_readout_wr0,
      Q => mem_readout_wr,
      R => axi_wdone_i_1_n_0
    );
mem_readout_wt_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B0B0BFB0"
    )
        port map (
      I0 => ow_busy,
      I1 => mem_rd_data_en,
      I2 => mem_readout_wt_reg_n_0,
      I3 => mem_readout_wr,
      I4 => mem_readout_rt,
      O => mem_readout_wt0
    );
mem_readout_wt_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => mem_readout_wt0,
      Q => mem_readout_wt_reg_n_0,
      R => axi_wdone_i_1_n_0
    );
mem_reg_0_63_0_2_i_10: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E2E2"
    )
        port map (
      I0 => \rd_addr_reg_n_0_[3]\,
      I1 => mem_readout_wt_reg_n_0,
      I2 => \wr_adr_d0_reg_n_0_[3]\,
      I3 => ctrl_rd_addr(0),
      I4 => ctrl_rd_en,
      O => ADDRA(0)
    );
mem_reg_0_63_0_2_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E2E2"
    )
        port map (
      I0 => \rd_addr_reg_n_0_[8]\,
      I1 => mem_readout_wt_reg_n_0,
      I2 => \wr_adr_d0_reg_n_0_[8]\,
      I3 => ctrl_rd_addr(5),
      I4 => ctrl_rd_en,
      O => ADDRA(5)
    );
mem_reg_0_63_0_2_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E2E2"
    )
        port map (
      I0 => \rd_addr_reg_n_0_[7]\,
      I1 => mem_readout_wt_reg_n_0,
      I2 => \wr_adr_d0_reg_n_0_[7]\,
      I3 => ctrl_rd_addr(4),
      I4 => ctrl_rd_en,
      O => ADDRA(4)
    );
mem_reg_0_63_0_2_i_7: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E2E2"
    )
        port map (
      I0 => \rd_addr_reg_n_0_[6]\,
      I1 => mem_readout_wt_reg_n_0,
      I2 => \wr_adr_d0_reg_n_0_[6]\,
      I3 => ctrl_rd_addr(3),
      I4 => ctrl_rd_en,
      O => ADDRA(3)
    );
mem_reg_0_63_0_2_i_8: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E2E2"
    )
        port map (
      I0 => \rd_addr_reg_n_0_[5]\,
      I1 => mem_readout_wt_reg_n_0,
      I2 => \wr_adr_d0_reg_n_0_[5]\,
      I3 => ctrl_rd_addr(2),
      I4 => ctrl_rd_en,
      O => ADDRA(2)
    );
mem_reg_0_63_0_2_i_9: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00E2E2"
    )
        port map (
      I0 => \rd_addr_reg_n_0_[4]\,
      I1 => mem_readout_wt_reg_n_0,
      I2 => \wr_adr_d0_reg_n_0_[4]\,
      I3 => ctrl_rd_addr(1),
      I4 => ctrl_rd_en,
      O => ADDRA(1)
    );
mux_wack_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => mux_wreq,
      Q => mux_wack,
      R => axi_wdone_i_1_n_0
    );
rd_ack_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20FF2000"
    )
        port map (
      I0 => mem_readout_rt,
      I1 => ow_busy,
      I2 => mem_rd_data_en,
      I3 => \rd_addr_reg_n_0_[10]\,
      I4 => rd_req,
      O => rd_ack_d0
    );
rd_ack_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => rd_ack_d0,
      Q => rd_ack,
      R => axi_wdone_i_1_n_0
    );
\rd_addr[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => axi_arset,
      I1 => axi_aresetn_i,
      I2 => axi_arvalid_i,
      O => rd_addr
    );
\rd_addr_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_addr,
      D => axi_araddr_i(7),
      Q => \rd_addr_reg_n_0_[10]\,
      R => '0'
    );
\rd_addr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_addr,
      D => axi_araddr_i(0),
      Q => \rd_addr_reg_n_0_[2]\,
      R => '0'
    );
\rd_addr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_addr,
      D => axi_araddr_i(1),
      Q => \rd_addr_reg_n_0_[3]\,
      R => '0'
    );
\rd_addr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_addr,
      D => axi_araddr_i(2),
      Q => \rd_addr_reg_n_0_[4]\,
      R => '0'
    );
\rd_addr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_addr,
      D => axi_araddr_i(3),
      Q => \rd_addr_reg_n_0_[5]\,
      R => '0'
    );
\rd_addr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_addr,
      D => axi_araddr_i(4),
      Q => \rd_addr_reg_n_0_[6]\,
      R => '0'
    );
\rd_addr_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_addr,
      D => axi_araddr_i(5),
      Q => \rd_addr_reg_n_0_[7]\,
      R => '0'
    );
\rd_addr_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_addr,
      D => axi_araddr_i(6),
      Q => \rd_addr_reg_n_0_[8]\,
      R => '0'
    );
\rd_data[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBBAABAA"
    )
        port map (
      I0 => \rd_data[0]_i_2_n_0\,
      I1 => \rd_addr_reg_n_0_[10]\,
      I2 => \rd_addr_reg_n_0_[3]\,
      I3 => \^ow_en_o\,
      I4 => ow_busy_o,
      O => rd_dat_d0(0)
    );
\rd_data[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(32),
      I1 => ow_rd_data(0),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => \rd_data[0]_i_2_n_0\
    );
\rd_data[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(42),
      I1 => ow_rd_data(10),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(10)
    );
\rd_data[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(43),
      I1 => ow_rd_data(11),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(11)
    );
\rd_data[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(44),
      I1 => ow_rd_data(12),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(12)
    );
\rd_data[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(45),
      I1 => ow_rd_data(13),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(13)
    );
\rd_data[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(46),
      I1 => ow_rd_data(14),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(14)
    );
\rd_data[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(47),
      I1 => ow_rd_data(15),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(15)
    );
\rd_data[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(48),
      I1 => ow_rd_data(16),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(16)
    );
\rd_data[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(49),
      I1 => ow_rd_data(17),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(17)
    );
\rd_data[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(50),
      I1 => ow_rd_data(18),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(18)
    );
\rd_data[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(51),
      I1 => ow_rd_data(19),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(19)
    );
\rd_data[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBBAABAA"
    )
        port map (
      I0 => \rd_data[1]_i_2_n_0\,
      I1 => \rd_addr_reg_n_0_[10]\,
      I2 => \rd_addr_reg_n_0_[3]\,
      I3 => \^ow_ctrl_o\(0),
      I4 => ow_done_o,
      O => rd_dat_d0(1)
    );
\rd_data[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(33),
      I1 => ow_rd_data(1),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => \rd_data[1]_i_2_n_0\
    );
\rd_data[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(52),
      I1 => ow_rd_data(20),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(20)
    );
\rd_data[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(53),
      I1 => ow_rd_data(21),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(21)
    );
\rd_data[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(54),
      I1 => ow_rd_data(22),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(22)
    );
\rd_data[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(55),
      I1 => ow_rd_data(23),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(23)
    );
\rd_data[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(56),
      I1 => ow_rd_data(24),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(24)
    );
\rd_data[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(57),
      I1 => ow_rd_data(25),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(25)
    );
\rd_data[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(58),
      I1 => ow_rd_data(26),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(26)
    );
\rd_data[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(59),
      I1 => ow_rd_data(27),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(27)
    );
\rd_data[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(60),
      I1 => ow_rd_data(28),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(28)
    );
\rd_data[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(61),
      I1 => ow_rd_data(29),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(29)
    );
\rd_data[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBBAABAA"
    )
        port map (
      I0 => \rd_data[2]_i_2_n_0\,
      I1 => \rd_addr_reg_n_0_[10]\,
      I2 => \rd_addr_reg_n_0_[3]\,
      I3 => \^ow_ctrl_o\(1),
      I4 => ow_dev_count(0),
      O => rd_dat_d0(2)
    );
\rd_data[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(34),
      I1 => ow_rd_data(2),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => \rd_data[2]_i_2_n_0\
    );
\rd_data[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(62),
      I1 => ow_rd_data(30),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(30)
    );
\rd_data[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(63),
      I1 => ow_rd_data(31),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(31)
    );
\rd_data[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBBAABAA"
    )
        port map (
      I0 => \rd_data[3]_i_2_n_0\,
      I1 => \rd_addr_reg_n_0_[10]\,
      I2 => \rd_addr_reg_n_0_[3]\,
      I3 => \^ow_ctrl_o\(2),
      I4 => ow_dev_count(1),
      O => rd_dat_d0(3)
    );
\rd_data[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(35),
      I1 => ow_rd_data(3),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => \rd_data[3]_i_2_n_0\
    );
\rd_data[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rd_data[8]_i_2_n_0\,
      I1 => ow_dev_count(2),
      I2 => \rd_data[8]_i_3_n_0\,
      I3 => ow_rd_data(4),
      I4 => ow_rd_data(36),
      I5 => \rd_data[8]_i_4_n_0\,
      O => rd_dat_d0(4)
    );
\rd_data[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rd_data[8]_i_2_n_0\,
      I1 => ow_dev_count(3),
      I2 => \rd_data[8]_i_3_n_0\,
      I3 => ow_rd_data(5),
      I4 => ow_rd_data(37),
      I5 => \rd_data[8]_i_4_n_0\,
      O => rd_dat_d0(5)
    );
\rd_data[63]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => mem_readout_rt,
      I1 => ctrl_rd_en,
      I2 => mem_readout_wt_reg_n_0,
      I3 => axi_aresetn_i,
      I4 => \^ow_reg_rst\,
      O => mem_readout_rt_reg_0
    );
\rd_data[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rd_data[8]_i_2_n_0\,
      I1 => ow_dev_count(4),
      I2 => \rd_data[8]_i_3_n_0\,
      I3 => ow_rd_data(6),
      I4 => ow_rd_data(38),
      I5 => \rd_data[8]_i_4_n_0\,
      O => rd_dat_d0(6)
    );
\rd_data[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rd_data[8]_i_2_n_0\,
      I1 => ow_dev_count(5),
      I2 => \rd_data[8]_i_3_n_0\,
      I3 => ow_rd_data(7),
      I4 => ow_rd_data(39),
      I5 => \rd_data[8]_i_4_n_0\,
      O => rd_dat_d0(7)
    );
\rd_data[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \rd_data[8]_i_2_n_0\,
      I1 => ow_too_many,
      I2 => \rd_data[8]_i_3_n_0\,
      I3 => ow_rd_data(8),
      I4 => ow_rd_data(40),
      I5 => \rd_data[8]_i_4_n_0\,
      O => rd_dat_d0(8)
    );
\rd_data[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \rd_addr_reg_n_0_[3]\,
      I1 => \rd_addr_reg_n_0_[10]\,
      O => \rd_data[8]_i_2_n_0\
    );
\rd_data[8]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \rd_addr_reg_n_0_[10]\,
      I1 => \rd_addr_reg_n_0_[2]\,
      I2 => mem_readout_wt_reg_n_0,
      I3 => \wr_adr_d0_reg_n_0_[2]\,
      O => \rd_data[8]_i_3_n_0\
    );
\rd_data[8]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E200"
    )
        port map (
      I0 => \rd_addr_reg_n_0_[2]\,
      I1 => mem_readout_wt_reg_n_0,
      I2 => \wr_adr_d0_reg_n_0_[2]\,
      I3 => \rd_addr_reg_n_0_[10]\,
      O => \rd_data[8]_i_4_n_0\
    );
\rd_data[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A0C0C0C0A0C0"
    )
        port map (
      I0 => ow_rd_data(41),
      I1 => ow_rd_data(9),
      I2 => \rd_addr_reg_n_0_[10]\,
      I3 => \rd_addr_reg_n_0_[2]\,
      I4 => mem_readout_wt_reg_n_0,
      I5 => \wr_adr_d0_reg_n_0_[2]\,
      O => rd_dat_d0(9)
    );
rd_data_en_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FE00"
    )
        port map (
      I0 => mem_readout_rt,
      I1 => ctrl_rd_en,
      I2 => mem_readout_wt_reg_n_0,
      I3 => axi_aresetn_i,
      I4 => \^ow_reg_rst\,
      O => rd_data_en
    );
\rd_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(0),
      Q => rd_data(0),
      R => '0'
    );
\rd_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(10),
      Q => rd_data(10),
      R => '0'
    );
\rd_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(11),
      Q => rd_data(11),
      R => '0'
    );
\rd_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(12),
      Q => rd_data(12),
      R => '0'
    );
\rd_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(13),
      Q => rd_data(13),
      R => '0'
    );
\rd_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(14),
      Q => rd_data(14),
      R => '0'
    );
\rd_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(15),
      Q => rd_data(15),
      R => '0'
    );
\rd_data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(16),
      Q => rd_data(16),
      R => '0'
    );
\rd_data_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(17),
      Q => rd_data(17),
      R => '0'
    );
\rd_data_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(18),
      Q => rd_data(18),
      R => '0'
    );
\rd_data_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(19),
      Q => rd_data(19),
      R => '0'
    );
\rd_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(1),
      Q => rd_data(1),
      R => '0'
    );
\rd_data_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(20),
      Q => rd_data(20),
      R => '0'
    );
\rd_data_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(21),
      Q => rd_data(21),
      R => '0'
    );
\rd_data_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(22),
      Q => rd_data(22),
      R => '0'
    );
\rd_data_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(23),
      Q => rd_data(23),
      R => '0'
    );
\rd_data_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(24),
      Q => rd_data(24),
      R => '0'
    );
\rd_data_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(25),
      Q => rd_data(25),
      R => '0'
    );
\rd_data_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(26),
      Q => rd_data(26),
      R => '0'
    );
\rd_data_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(27),
      Q => rd_data(27),
      R => '0'
    );
\rd_data_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(28),
      Q => rd_data(28),
      R => '0'
    );
\rd_data_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(29),
      Q => rd_data(29),
      R => '0'
    );
\rd_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(2),
      Q => rd_data(2),
      R => '0'
    );
\rd_data_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(30),
      Q => rd_data(30),
      R => '0'
    );
\rd_data_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(31),
      Q => rd_data(31),
      R => '0'
    );
\rd_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(3),
      Q => rd_data(3),
      R => '0'
    );
\rd_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(4),
      Q => rd_data(4),
      R => '0'
    );
\rd_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(5),
      Q => rd_data(5),
      R => '0'
    );
\rd_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(6),
      Q => rd_data(6),
      R => '0'
    );
\rd_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(7),
      Q => rd_data(7),
      R => '0'
    );
\rd_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(8),
      Q => rd_data(8),
      R => '0'
    );
\rd_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => rd_dat_d0(9),
      Q => rd_data(9),
      R => '0'
    );
rd_req_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => axi_arvalid_i,
      I1 => axi_arset,
      O => rd_req0
    );
rd_req_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => rd_req0,
      Q => rd_req,
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(0),
      Q => axi_rdata_o(0),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(10),
      Q => axi_rdata_o(10),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(11),
      Q => axi_rdata_o(11),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(12),
      Q => axi_rdata_o(12),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(13),
      Q => axi_rdata_o(13),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(14),
      Q => axi_rdata_o(14),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(15),
      Q => axi_rdata_o(15),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(16),
      Q => axi_rdata_o(16),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(17),
      Q => axi_rdata_o(17),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(18),
      Q => axi_rdata_o(18),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(19),
      Q => axi_rdata_o(19),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(1),
      Q => axi_rdata_o(1),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(20),
      Q => axi_rdata_o(20),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(21),
      Q => axi_rdata_o(21),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(22),
      Q => axi_rdata_o(22),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(23),
      Q => axi_rdata_o(23),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(24),
      Q => axi_rdata_o(24),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(25),
      Q => axi_rdata_o(25),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(26),
      Q => axi_rdata_o(26),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(27),
      Q => axi_rdata_o(27),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(28),
      Q => axi_rdata_o(28),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(29),
      Q => axi_rdata_o(29),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(2),
      Q => axi_rdata_o(2),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(30),
      Q => axi_rdata_o(30),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(31),
      Q => axi_rdata_o(31),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(3),
      Q => axi_rdata_o(3),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(4),
      Q => axi_rdata_o(4),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(5),
      Q => axi_rdata_o(5),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(6),
      Q => axi_rdata_o(6),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(7),
      Q => axi_rdata_o(7),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(8),
      Q => axi_rdata_o(8),
      R => axi_wdone_i_1_n_0
    );
\rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => rd_ack,
      D => rd_data(9),
      Q => axi_rdata_o(9),
      R => axi_wdone_i_1_n_0
    );
\reg[lfsr][0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^ow_reg_rst\,
      I1 => axi_aresetn_i,
      O => ow_rst
    );
regs_control_rst_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \regs_mux_ctrl_reg[2]_i_2_n_0\,
      I2 => axi_aresetn_i,
      I3 => \wr_adr_d0_reg_n_0_[2]\,
      O => regs_control_rst_reg_i_1_n_0
    );
regs_control_rst_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => regs_control_rst_reg_i_1_n_0,
      Q => \^ow_reg_rst\,
      R => '0'
    );
regs_control_trig_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \wr_dat_d0_reg_n_0_[0]\,
      I1 => \regs_mux_ctrl_reg[2]_i_2_n_0\,
      I2 => axi_aresetn_i,
      I3 => \wr_adr_d0_reg_n_0_[2]\,
      O => regs_control_trig_reg_i_1_n_0
    );
regs_control_trig_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => regs_control_trig_reg_i_1_n_0,
      Q => ow_trig,
      R => '0'
    );
\regs_mux_ctrl_reg[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \regs_mux_ctrl_reg[2]_i_2_n_0\,
      I1 => \wr_adr_d0_reg_n_0_[2]\,
      O => mux_wreq
    );
\regs_mux_ctrl_reg[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => wr_req_d0,
      I1 => p_0_in,
      I2 => \wr_adr_d0_reg_n_0_[3]\,
      I3 => \regs_mux_ctrl_reg[2]_i_3_n_0\,
      O => \regs_mux_ctrl_reg[2]_i_2_n_0\
    );
\regs_mux_ctrl_reg[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \wr_adr_d0_reg_n_0_[9]\,
      I1 => \wr_adr_d0_reg_n_0_[8]\,
      I2 => \wr_adr_d0_reg_n_0_[7]\,
      I3 => \wr_adr_d0_reg_n_0_[6]\,
      I4 => \wr_adr_d0_reg_n_0_[4]\,
      I5 => \wr_adr_d0_reg_n_0_[5]\,
      O => \regs_mux_ctrl_reg[2]_i_3_n_0\
    );
\regs_mux_ctrl_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => mux_wreq,
      D => p_1_in(0),
      Q => \^ow_ctrl_o\(0),
      R => axi_wdone_i_1_n_0
    );
\regs_mux_ctrl_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => mux_wreq,
      D => p_1_in(1),
      Q => \^ow_ctrl_o\(1),
      R => axi_wdone_i_1_n_0
    );
\regs_mux_ctrl_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => mux_wreq,
      D => p_1_in(2),
      Q => \^ow_ctrl_o\(2),
      R => axi_wdone_i_1_n_0
    );
regs_mux_en_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => mux_wreq,
      D => \wr_dat_d0_reg_n_0_[0]\,
      Q => \^ow_en_o\,
      R => axi_wdone_i_1_n_0
    );
\wr_addr[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => axi_awset,
      I1 => axi_awvalid_i,
      I2 => axi_aresetn_i,
      O => wr_addr_0
    );
\wr_addr_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_addr_0,
      D => axi_awaddr_i(8),
      Q => wr_addr(10),
      R => '0'
    );
\wr_addr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_addr_0,
      D => axi_awaddr_i(0),
      Q => wr_addr(2),
      R => '0'
    );
\wr_addr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_addr_0,
      D => axi_awaddr_i(1),
      Q => wr_addr(3),
      R => '0'
    );
\wr_addr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_addr_0,
      D => axi_awaddr_i(2),
      Q => wr_addr(4),
      R => '0'
    );
\wr_addr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_addr_0,
      D => axi_awaddr_i(3),
      Q => wr_addr(5),
      R => '0'
    );
\wr_addr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_addr_0,
      D => axi_awaddr_i(4),
      Q => wr_addr(6),
      R => '0'
    );
\wr_addr_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_addr_0,
      D => axi_awaddr_i(5),
      Q => wr_addr(7),
      R => '0'
    );
\wr_addr_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_addr_0,
      D => axi_awaddr_i(6),
      Q => wr_addr(8),
      R => '0'
    );
\wr_addr_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_addr_0,
      D => axi_awaddr_i(7),
      Q => wr_addr(9),
      R => '0'
    );
\wr_adr_d0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_addr(10),
      Q => p_0_in,
      R => '0'
    );
\wr_adr_d0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_addr(2),
      Q => \wr_adr_d0_reg_n_0_[2]\,
      R => '0'
    );
\wr_adr_d0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_addr(3),
      Q => \wr_adr_d0_reg_n_0_[3]\,
      R => '0'
    );
\wr_adr_d0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_addr(4),
      Q => \wr_adr_d0_reg_n_0_[4]\,
      R => '0'
    );
\wr_adr_d0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_addr(5),
      Q => \wr_adr_d0_reg_n_0_[5]\,
      R => '0'
    );
\wr_adr_d0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_addr(6),
      Q => \wr_adr_d0_reg_n_0_[6]\,
      R => '0'
    );
\wr_adr_d0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_addr(7),
      Q => \wr_adr_d0_reg_n_0_[7]\,
      R => '0'
    );
\wr_adr_d0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_addr(8),
      Q => \wr_adr_d0_reg_n_0_[8]\,
      R => '0'
    );
\wr_adr_d0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_addr(9),
      Q => \wr_adr_d0_reg_n_0_[9]\,
      R => '0'
    );
\wr_dat_d0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_data(0),
      Q => \wr_dat_d0_reg_n_0_[0]\,
      R => '0'
    );
\wr_dat_d0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_data(1),
      Q => p_1_in(0),
      R => '0'
    );
\wr_dat_d0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_data(2),
      Q => p_1_in(1),
      R => '0'
    );
\wr_dat_d0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => axi_aresetn_i,
      D => wr_data(3),
      Q => p_1_in(2),
      R => '0'
    );
\wr_data[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => axi_wset_reg_n_0,
      I1 => axi_aresetn_i,
      I2 => axi_wvalid_i,
      O => wr_data_1
    );
\wr_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_data_1,
      D => axi_wdata_i(0),
      Q => wr_data(0),
      R => '0'
    );
\wr_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_data_1,
      D => axi_wdata_i(1),
      Q => wr_data(1),
      R => '0'
    );
\wr_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_data_1,
      D => axi_wdata_i(2),
      Q => wr_data(2),
      R => '0'
    );
\wr_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => wr_data_1,
      D => axi_wdata_i(3),
      Q => wr_data(3),
      R => '0'
    );
wr_req_d0_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => wr_req,
      Q => wr_req_d0,
      R => axi_wdone_i_1_n_0
    );
wr_req_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2200E000"
    )
        port map (
      I0 => axi_awvalid_i,
      I1 => axi_awset,
      I2 => axi_wvalid_i,
      I3 => axi_aresetn_i,
      I4 => axi_wset_reg_n_0,
      O => wr_req_i_1_n_0
    );
wr_req_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => wr_req_i_1_n_0,
      Q => wr_req,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_two_port_ram is
  port (
    mem_wr_done : out STD_LOGIC;
    rd_data_en_reg_0 : out STD_LOGIC;
    \rd_data_reg[3]_0\ : out STD_LOGIC;
    ow_rd_data : out STD_LOGIC_VECTOR ( 63 downto 0 );
    rd_data_en_reg_1 : out STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    wr_data_i : in STD_LOGIC_VECTOR ( 63 downto 0 );
    wr_done : in STD_LOGIC;
    ADDRD : in STD_LOGIC_VECTOR ( 5 downto 0 );
    ADDRA : in STD_LOGIC_VECTOR ( 5 downto 0 );
    rd_data_en : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    ow_reg_rst : in STD_LOGIC;
    \reg[mem_addr][5]_i_4\ : in STD_LOGIC;
    \rd_data_reg[63]_0\ : in STD_LOGIC
  );
end design_1_axi_onewire_0_0_two_port_ram;

architecture STRUCTURE of design_1_axi_onewire_0_0_two_port_ram is
  signal \FSM_sequential_reg[state][2]_i_8__0_n_0\ : STD_LOGIC;
  signal \^ow_rd_data\ : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal rd_data0 : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal \rd_data[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[10]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[11]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[13]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[14]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[15]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[16]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[17]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[18]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[19]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[20]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[21]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[22]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[23]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[24]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[25]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[26]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[27]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[28]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[29]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[30]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[31]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[32]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[33]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[34]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[35]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[36]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[37]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[38]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[39]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[40]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[41]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[42]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[43]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[44]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[45]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[46]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[47]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[48]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[49]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[50]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[51]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[52]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[53]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[54]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[55]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[56]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[57]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[58]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[59]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[60]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[61]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[62]_i_1_n_0\ : STD_LOGIC;
  signal \rd_data[63]_i_2_n_0\ : STD_LOGIC;
  signal \rd_data[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[7]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \rd_data[9]_i_1__0_n_0\ : STD_LOGIC;
  signal \^rd_data_en_reg_0\ : STD_LOGIC;
  signal NLW_mem_reg_0_63_0_2_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_12_14_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_15_17_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_18_20_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_21_23_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_24_26_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_27_29_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_30_32_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_33_35_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_36_38_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_39_41_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_3_5_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_42_44_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_45_47_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_48_50_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_51_53_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_54_56_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_57_59_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_60_62_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_63_63_SPO_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_6_8_DOD_UNCONNECTED : STD_LOGIC;
  signal NLW_mem_reg_0_63_9_11_DOD_UNCONNECTED : STD_LOGIC;
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_0_2 : label is "";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of mem_reg_0_63_0_2 : label is 4096;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of mem_reg_0_63_0_2 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_0_2";
  attribute RTL_RAM_TYPE : string;
  attribute RTL_RAM_TYPE of mem_reg_0_63_0_2 : label is "RAM_SDP";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of mem_reg_0_63_0_2 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of mem_reg_0_63_0_2 : label is 63;
  attribute ram_offset : integer;
  attribute ram_offset of mem_reg_0_63_0_2 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of mem_reg_0_63_0_2 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of mem_reg_0_63_0_2 : label is 2;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_12_14 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_12_14 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_12_14 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_12_14";
  attribute RTL_RAM_TYPE of mem_reg_0_63_12_14 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_12_14 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_12_14 : label is 63;
  attribute ram_offset of mem_reg_0_63_12_14 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_12_14 : label is 12;
  attribute ram_slice_end of mem_reg_0_63_12_14 : label is 14;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_15_17 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_15_17 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_15_17 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_15_17";
  attribute RTL_RAM_TYPE of mem_reg_0_63_15_17 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_15_17 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_15_17 : label is 63;
  attribute ram_offset of mem_reg_0_63_15_17 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_15_17 : label is 15;
  attribute ram_slice_end of mem_reg_0_63_15_17 : label is 17;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_18_20 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_18_20 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_18_20 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_18_20";
  attribute RTL_RAM_TYPE of mem_reg_0_63_18_20 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_18_20 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_18_20 : label is 63;
  attribute ram_offset of mem_reg_0_63_18_20 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_18_20 : label is 18;
  attribute ram_slice_end of mem_reg_0_63_18_20 : label is 20;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_21_23 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_21_23 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_21_23 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_21_23";
  attribute RTL_RAM_TYPE of mem_reg_0_63_21_23 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_21_23 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_21_23 : label is 63;
  attribute ram_offset of mem_reg_0_63_21_23 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_21_23 : label is 21;
  attribute ram_slice_end of mem_reg_0_63_21_23 : label is 23;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_24_26 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_24_26 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_24_26 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_24_26";
  attribute RTL_RAM_TYPE of mem_reg_0_63_24_26 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_24_26 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_24_26 : label is 63;
  attribute ram_offset of mem_reg_0_63_24_26 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_24_26 : label is 24;
  attribute ram_slice_end of mem_reg_0_63_24_26 : label is 26;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_27_29 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_27_29 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_27_29 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_27_29";
  attribute RTL_RAM_TYPE of mem_reg_0_63_27_29 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_27_29 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_27_29 : label is 63;
  attribute ram_offset of mem_reg_0_63_27_29 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_27_29 : label is 27;
  attribute ram_slice_end of mem_reg_0_63_27_29 : label is 29;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_30_32 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_30_32 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_30_32 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_30_32";
  attribute RTL_RAM_TYPE of mem_reg_0_63_30_32 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_30_32 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_30_32 : label is 63;
  attribute ram_offset of mem_reg_0_63_30_32 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_30_32 : label is 30;
  attribute ram_slice_end of mem_reg_0_63_30_32 : label is 32;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_33_35 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_33_35 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_33_35 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_33_35";
  attribute RTL_RAM_TYPE of mem_reg_0_63_33_35 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_33_35 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_33_35 : label is 63;
  attribute ram_offset of mem_reg_0_63_33_35 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_33_35 : label is 33;
  attribute ram_slice_end of mem_reg_0_63_33_35 : label is 35;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_36_38 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_36_38 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_36_38 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_36_38";
  attribute RTL_RAM_TYPE of mem_reg_0_63_36_38 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_36_38 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_36_38 : label is 63;
  attribute ram_offset of mem_reg_0_63_36_38 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_36_38 : label is 36;
  attribute ram_slice_end of mem_reg_0_63_36_38 : label is 38;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_39_41 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_39_41 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_39_41 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_39_41";
  attribute RTL_RAM_TYPE of mem_reg_0_63_39_41 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_39_41 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_39_41 : label is 63;
  attribute ram_offset of mem_reg_0_63_39_41 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_39_41 : label is 39;
  attribute ram_slice_end of mem_reg_0_63_39_41 : label is 41;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_3_5 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_3_5 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_3_5 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_3_5";
  attribute RTL_RAM_TYPE of mem_reg_0_63_3_5 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_3_5 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_3_5 : label is 63;
  attribute ram_offset of mem_reg_0_63_3_5 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_3_5 : label is 3;
  attribute ram_slice_end of mem_reg_0_63_3_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_42_44 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_42_44 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_42_44 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_42_44";
  attribute RTL_RAM_TYPE of mem_reg_0_63_42_44 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_42_44 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_42_44 : label is 63;
  attribute ram_offset of mem_reg_0_63_42_44 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_42_44 : label is 42;
  attribute ram_slice_end of mem_reg_0_63_42_44 : label is 44;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_45_47 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_45_47 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_45_47 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_45_47";
  attribute RTL_RAM_TYPE of mem_reg_0_63_45_47 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_45_47 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_45_47 : label is 63;
  attribute ram_offset of mem_reg_0_63_45_47 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_45_47 : label is 45;
  attribute ram_slice_end of mem_reg_0_63_45_47 : label is 47;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_48_50 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_48_50 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_48_50 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_48_50";
  attribute RTL_RAM_TYPE of mem_reg_0_63_48_50 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_48_50 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_48_50 : label is 63;
  attribute ram_offset of mem_reg_0_63_48_50 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_48_50 : label is 48;
  attribute ram_slice_end of mem_reg_0_63_48_50 : label is 50;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_51_53 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_51_53 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_51_53 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_51_53";
  attribute RTL_RAM_TYPE of mem_reg_0_63_51_53 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_51_53 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_51_53 : label is 63;
  attribute ram_offset of mem_reg_0_63_51_53 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_51_53 : label is 51;
  attribute ram_slice_end of mem_reg_0_63_51_53 : label is 53;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_54_56 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_54_56 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_54_56 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_54_56";
  attribute RTL_RAM_TYPE of mem_reg_0_63_54_56 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_54_56 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_54_56 : label is 63;
  attribute ram_offset of mem_reg_0_63_54_56 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_54_56 : label is 54;
  attribute ram_slice_end of mem_reg_0_63_54_56 : label is 56;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_57_59 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_57_59 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_57_59 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_57_59";
  attribute RTL_RAM_TYPE of mem_reg_0_63_57_59 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_57_59 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_57_59 : label is 63;
  attribute ram_offset of mem_reg_0_63_57_59 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_57_59 : label is 57;
  attribute ram_slice_end of mem_reg_0_63_57_59 : label is 59;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_60_62 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_60_62 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_60_62 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_60_62";
  attribute RTL_RAM_TYPE of mem_reg_0_63_60_62 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_60_62 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_60_62 : label is 63;
  attribute ram_offset of mem_reg_0_63_60_62 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_60_62 : label is 60;
  attribute ram_slice_end of mem_reg_0_63_60_62 : label is 62;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_63_63 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_63_63 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_63_63 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_63_63";
  attribute RTL_RAM_TYPE of mem_reg_0_63_63_63 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_63_63 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_63_63 : label is 63;
  attribute ram_offset of mem_reg_0_63_63_63 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_63_63 : label is 63;
  attribute ram_slice_end of mem_reg_0_63_63_63 : label is 63;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_6_8 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_6_8 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_6_8 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_6_8";
  attribute RTL_RAM_TYPE of mem_reg_0_63_6_8 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_6_8 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_6_8 : label is 63;
  attribute ram_offset of mem_reg_0_63_6_8 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_6_8 : label is 6;
  attribute ram_slice_end of mem_reg_0_63_6_8 : label is 8;
  attribute METHODOLOGY_DRC_VIOS of mem_reg_0_63_9_11 : label is "";
  attribute RTL_RAM_BITS of mem_reg_0_63_9_11 : label is 4096;
  attribute RTL_RAM_NAME of mem_reg_0_63_9_11 : label is "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_9_11";
  attribute RTL_RAM_TYPE of mem_reg_0_63_9_11 : label is "RAM_SDP";
  attribute ram_addr_begin of mem_reg_0_63_9_11 : label is 0;
  attribute ram_addr_end of mem_reg_0_63_9_11 : label is 63;
  attribute ram_offset of mem_reg_0_63_9_11 : label is 0;
  attribute ram_slice_begin of mem_reg_0_63_9_11 : label is 9;
  attribute ram_slice_end of mem_reg_0_63_9_11 : label is 11;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \rd_data[0]_i_1__0\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \rd_data[10]_i_1__0\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \rd_data[11]_i_1__0\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \rd_data[12]_i_1__0\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \rd_data[13]_i_1__0\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \rd_data[14]_i_1__0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \rd_data[15]_i_1__0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \rd_data[16]_i_1__0\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \rd_data[17]_i_1__0\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \rd_data[18]_i_1__0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \rd_data[19]_i_1__0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \rd_data[1]_i_1__0\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \rd_data[20]_i_1__0\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \rd_data[21]_i_1__0\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \rd_data[22]_i_1__0\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \rd_data[23]_i_1__0\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \rd_data[24]_i_1__0\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \rd_data[25]_i_1__0\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \rd_data[26]_i_1__0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \rd_data[27]_i_1__0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \rd_data[28]_i_1__0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \rd_data[29]_i_1__0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \rd_data[2]_i_1__0\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \rd_data[30]_i_1__0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \rd_data[31]_i_1__0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \rd_data[32]_i_1\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \rd_data[33]_i_1\ : label is "soft_lutpair97";
  attribute SOFT_HLUTNM of \rd_data[34]_i_1\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \rd_data[35]_i_1\ : label is "soft_lutpair98";
  attribute SOFT_HLUTNM of \rd_data[36]_i_1\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \rd_data[37]_i_1\ : label is "soft_lutpair99";
  attribute SOFT_HLUTNM of \rd_data[38]_i_1\ : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \rd_data[39]_i_1\ : label is "soft_lutpair100";
  attribute SOFT_HLUTNM of \rd_data[3]_i_1__0\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \rd_data[40]_i_1\ : label is "soft_lutpair101";
  attribute SOFT_HLUTNM of \rd_data[41]_i_1\ : label is "soft_lutpair101";
  attribute SOFT_HLUTNM of \rd_data[42]_i_1\ : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of \rd_data[43]_i_1\ : label is "soft_lutpair102";
  attribute SOFT_HLUTNM of \rd_data[44]_i_1\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \rd_data[45]_i_1\ : label is "soft_lutpair103";
  attribute SOFT_HLUTNM of \rd_data[46]_i_1\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \rd_data[47]_i_1\ : label is "soft_lutpair104";
  attribute SOFT_HLUTNM of \rd_data[48]_i_1\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \rd_data[49]_i_1\ : label is "soft_lutpair105";
  attribute SOFT_HLUTNM of \rd_data[4]_i_1__0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \rd_data[50]_i_1\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \rd_data[51]_i_1\ : label is "soft_lutpair106";
  attribute SOFT_HLUTNM of \rd_data[52]_i_1\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \rd_data[53]_i_1\ : label is "soft_lutpair107";
  attribute SOFT_HLUTNM of \rd_data[54]_i_1\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \rd_data[55]_i_1\ : label is "soft_lutpair108";
  attribute SOFT_HLUTNM of \rd_data[56]_i_1\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \rd_data[57]_i_1\ : label is "soft_lutpair109";
  attribute SOFT_HLUTNM of \rd_data[58]_i_1\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \rd_data[59]_i_1\ : label is "soft_lutpair110";
  attribute SOFT_HLUTNM of \rd_data[5]_i_1__0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \rd_data[60]_i_1\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \rd_data[61]_i_1\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \rd_data[62]_i_1\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \rd_data[63]_i_2\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \rd_data[6]_i_1__0\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \rd_data[7]_i_1__0\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \rd_data[8]_i_1__0\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \rd_data[9]_i_1__0\ : label is "soft_lutpair85";
begin
  ow_rd_data(63 downto 0) <= \^ow_rd_data\(63 downto 0);
  rd_data_en_reg_0 <= \^rd_data_en_reg_0\;
\FSM_sequential_reg[state][2]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFF7"
    )
        port map (
      I0 => \^ow_rd_data\(3),
      I1 => \^ow_rd_data\(5),
      I2 => \^ow_rd_data\(6),
      I3 => \^ow_rd_data\(4),
      I4 => \FSM_sequential_reg[state][2]_i_8__0_n_0\,
      O => \rd_data_reg[3]_0\
    );
\FSM_sequential_reg[state][2]_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^ow_rd_data\(0),
      I1 => \^ow_rd_data\(1),
      I2 => \^ow_rd_data\(2),
      I3 => \^ow_rd_data\(7),
      O => \FSM_sequential_reg[state][2]_i_8__0_n_0\
    );
mem_reg_0_63_0_2: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(0),
      DIB => wr_data_i(1),
      DIC => wr_data_i(2),
      DID => '0',
      DOA => rd_data0(0),
      DOB => rd_data0(1),
      DOC => rd_data0(2),
      DOD => NLW_mem_reg_0_63_0_2_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_12_14: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(12),
      DIB => wr_data_i(13),
      DIC => wr_data_i(14),
      DID => '0',
      DOA => rd_data0(12),
      DOB => rd_data0(13),
      DOC => rd_data0(14),
      DOD => NLW_mem_reg_0_63_12_14_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_15_17: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(15),
      DIB => wr_data_i(16),
      DIC => wr_data_i(17),
      DID => '0',
      DOA => rd_data0(15),
      DOB => rd_data0(16),
      DOC => rd_data0(17),
      DOD => NLW_mem_reg_0_63_15_17_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_18_20: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(18),
      DIB => wr_data_i(19),
      DIC => wr_data_i(20),
      DID => '0',
      DOA => rd_data0(18),
      DOB => rd_data0(19),
      DOC => rd_data0(20),
      DOD => NLW_mem_reg_0_63_18_20_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_21_23: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(21),
      DIB => wr_data_i(22),
      DIC => wr_data_i(23),
      DID => '0',
      DOA => rd_data0(21),
      DOB => rd_data0(22),
      DOC => rd_data0(23),
      DOD => NLW_mem_reg_0_63_21_23_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_24_26: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(24),
      DIB => wr_data_i(25),
      DIC => wr_data_i(26),
      DID => '0',
      DOA => rd_data0(24),
      DOB => rd_data0(25),
      DOC => rd_data0(26),
      DOD => NLW_mem_reg_0_63_24_26_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_27_29: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(27),
      DIB => wr_data_i(28),
      DIC => wr_data_i(29),
      DID => '0',
      DOA => rd_data0(27),
      DOB => rd_data0(28),
      DOC => rd_data0(29),
      DOD => NLW_mem_reg_0_63_27_29_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_30_32: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(30),
      DIB => wr_data_i(31),
      DIC => wr_data_i(32),
      DID => '0',
      DOA => rd_data0(30),
      DOB => rd_data0(31),
      DOC => rd_data0(32),
      DOD => NLW_mem_reg_0_63_30_32_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_33_35: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(33),
      DIB => wr_data_i(34),
      DIC => wr_data_i(35),
      DID => '0',
      DOA => rd_data0(33),
      DOB => rd_data0(34),
      DOC => rd_data0(35),
      DOD => NLW_mem_reg_0_63_33_35_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_36_38: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(36),
      DIB => wr_data_i(37),
      DIC => wr_data_i(38),
      DID => '0',
      DOA => rd_data0(36),
      DOB => rd_data0(37),
      DOC => rd_data0(38),
      DOD => NLW_mem_reg_0_63_36_38_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_39_41: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(39),
      DIB => wr_data_i(40),
      DIC => wr_data_i(41),
      DID => '0',
      DOA => rd_data0(39),
      DOB => rd_data0(40),
      DOC => rd_data0(41),
      DOD => NLW_mem_reg_0_63_39_41_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_3_5: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(3),
      DIB => wr_data_i(4),
      DIC => wr_data_i(5),
      DID => '0',
      DOA => rd_data0(3),
      DOB => rd_data0(4),
      DOC => rd_data0(5),
      DOD => NLW_mem_reg_0_63_3_5_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_42_44: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(42),
      DIB => wr_data_i(43),
      DIC => wr_data_i(44),
      DID => '0',
      DOA => rd_data0(42),
      DOB => rd_data0(43),
      DOC => rd_data0(44),
      DOD => NLW_mem_reg_0_63_42_44_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_45_47: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(45),
      DIB => wr_data_i(46),
      DIC => wr_data_i(47),
      DID => '0',
      DOA => rd_data0(45),
      DOB => rd_data0(46),
      DOC => rd_data0(47),
      DOD => NLW_mem_reg_0_63_45_47_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_48_50: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(48),
      DIB => wr_data_i(49),
      DIC => wr_data_i(50),
      DID => '0',
      DOA => rd_data0(48),
      DOB => rd_data0(49),
      DOC => rd_data0(50),
      DOD => NLW_mem_reg_0_63_48_50_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_51_53: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(51),
      DIB => wr_data_i(52),
      DIC => wr_data_i(53),
      DID => '0',
      DOA => rd_data0(51),
      DOB => rd_data0(52),
      DOC => rd_data0(53),
      DOD => NLW_mem_reg_0_63_51_53_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_54_56: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(54),
      DIB => wr_data_i(55),
      DIC => wr_data_i(56),
      DID => '0',
      DOA => rd_data0(54),
      DOB => rd_data0(55),
      DOC => rd_data0(56),
      DOD => NLW_mem_reg_0_63_54_56_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_57_59: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(57),
      DIB => wr_data_i(58),
      DIC => wr_data_i(59),
      DID => '0',
      DOA => rd_data0(57),
      DOB => rd_data0(58),
      DOC => rd_data0(59),
      DOD => NLW_mem_reg_0_63_57_59_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_60_62: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(60),
      DIB => wr_data_i(61),
      DIC => wr_data_i(62),
      DID => '0',
      DOA => rd_data0(60),
      DOB => rd_data0(61),
      DOC => rd_data0(62),
      DOD => NLW_mem_reg_0_63_60_62_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_63_63: unisim.vcomponents.RAM64X1D
     port map (
      A0 => ADDRD(0),
      A1 => ADDRD(1),
      A2 => ADDRD(2),
      A3 => ADDRD(3),
      A4 => ADDRD(4),
      A5 => ADDRD(5),
      D => wr_data_i(63),
      DPO => rd_data0(63),
      DPRA0 => ADDRA(0),
      DPRA1 => ADDRA(1),
      DPRA2 => ADDRA(2),
      DPRA3 => ADDRA(3),
      DPRA4 => ADDRA(4),
      DPRA5 => ADDRA(5),
      SPO => NLW_mem_reg_0_63_63_63_SPO_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_6_8: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(6),
      DIB => wr_data_i(7),
      DIC => wr_data_i(8),
      DID => '0',
      DOA => rd_data0(6),
      DOB => rd_data0(7),
      DOC => rd_data0(8),
      DOD => NLW_mem_reg_0_63_6_8_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
mem_reg_0_63_9_11: unisim.vcomponents.RAM64M
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRB(5 downto 0) => ADDRA(5 downto 0),
      ADDRC(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ADDRD(5 downto 0),
      DIA => wr_data_i(9),
      DIB => wr_data_i(10),
      DIC => wr_data_i(11),
      DID => '0',
      DOA => rd_data0(9),
      DOB => rd_data0(10),
      DOC => rd_data0(11),
      DOD => NLW_mem_reg_0_63_9_11_DOD_UNCONNECTED,
      WCLK => axi_clk_i,
      WE => wr_done
    );
\rd_data[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(0),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[0]_i_1__0_n_0\
    );
\rd_data[10]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(10),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[10]_i_1__0_n_0\
    );
\rd_data[11]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(11),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[11]_i_1__0_n_0\
    );
\rd_data[12]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(12),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[12]_i_1__0_n_0\
    );
\rd_data[13]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(13),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[13]_i_1__0_n_0\
    );
\rd_data[14]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(14),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[14]_i_1__0_n_0\
    );
\rd_data[15]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(15),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[15]_i_1__0_n_0\
    );
\rd_data[16]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(16),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[16]_i_1__0_n_0\
    );
\rd_data[17]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(17),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[17]_i_1__0_n_0\
    );
\rd_data[18]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(18),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[18]_i_1__0_n_0\
    );
\rd_data[19]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(19),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[19]_i_1__0_n_0\
    );
\rd_data[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(1),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[1]_i_1__0_n_0\
    );
\rd_data[20]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(20),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[20]_i_1__0_n_0\
    );
\rd_data[21]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(21),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[21]_i_1__0_n_0\
    );
\rd_data[22]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(22),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[22]_i_1__0_n_0\
    );
\rd_data[23]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(23),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[23]_i_1__0_n_0\
    );
\rd_data[24]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(24),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[24]_i_1__0_n_0\
    );
\rd_data[25]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(25),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[25]_i_1__0_n_0\
    );
\rd_data[26]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(26),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[26]_i_1__0_n_0\
    );
\rd_data[27]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(27),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[27]_i_1__0_n_0\
    );
\rd_data[28]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(28),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[28]_i_1__0_n_0\
    );
\rd_data[29]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(29),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[29]_i_1__0_n_0\
    );
\rd_data[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(2),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[2]_i_1__0_n_0\
    );
\rd_data[30]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(30),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[30]_i_1__0_n_0\
    );
\rd_data[31]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(31),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[31]_i_1__0_n_0\
    );
\rd_data[32]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(32),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[32]_i_1_n_0\
    );
\rd_data[33]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(33),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[33]_i_1_n_0\
    );
\rd_data[34]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(34),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[34]_i_1_n_0\
    );
\rd_data[35]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(35),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[35]_i_1_n_0\
    );
\rd_data[36]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(36),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[36]_i_1_n_0\
    );
\rd_data[37]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(37),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[37]_i_1_n_0\
    );
\rd_data[38]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(38),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[38]_i_1_n_0\
    );
\rd_data[39]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(39),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[39]_i_1_n_0\
    );
\rd_data[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(3),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[3]_i_1__0_n_0\
    );
\rd_data[40]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(40),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[40]_i_1_n_0\
    );
\rd_data[41]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(41),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[41]_i_1_n_0\
    );
\rd_data[42]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(42),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[42]_i_1_n_0\
    );
\rd_data[43]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(43),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[43]_i_1_n_0\
    );
\rd_data[44]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(44),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[44]_i_1_n_0\
    );
\rd_data[45]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(45),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[45]_i_1_n_0\
    );
\rd_data[46]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(46),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[46]_i_1_n_0\
    );
\rd_data[47]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(47),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[47]_i_1_n_0\
    );
\rd_data[48]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(48),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[48]_i_1_n_0\
    );
\rd_data[49]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(49),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[49]_i_1_n_0\
    );
\rd_data[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(4),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[4]_i_1__0_n_0\
    );
\rd_data[50]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(50),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[50]_i_1_n_0\
    );
\rd_data[51]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(51),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[51]_i_1_n_0\
    );
\rd_data[52]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(52),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[52]_i_1_n_0\
    );
\rd_data[53]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(53),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[53]_i_1_n_0\
    );
\rd_data[54]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(54),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[54]_i_1_n_0\
    );
\rd_data[55]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(55),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[55]_i_1_n_0\
    );
\rd_data[56]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(56),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[56]_i_1_n_0\
    );
\rd_data[57]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(57),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[57]_i_1_n_0\
    );
\rd_data[58]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(58),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[58]_i_1_n_0\
    );
\rd_data[59]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(59),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[59]_i_1_n_0\
    );
\rd_data[5]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(5),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[5]_i_1__0_n_0\
    );
\rd_data[60]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(60),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[60]_i_1_n_0\
    );
\rd_data[61]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(61),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[61]_i_1_n_0\
    );
\rd_data[62]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(62),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[62]_i_1_n_0\
    );
\rd_data[63]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(63),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[63]_i_2_n_0\
    );
\rd_data[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(6),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[6]_i_1__0_n_0\
    );
\rd_data[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(7),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[7]_i_1__0_n_0\
    );
\rd_data[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(8),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[8]_i_1__0_n_0\
    );
\rd_data[9]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => rd_data0(9),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \rd_data[9]_i_1__0_n_0\
    );
rd_data_en_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => rd_data_en,
      Q => \^rd_data_en_reg_0\,
      R => '0'
    );
\rd_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[0]_i_1__0_n_0\,
      Q => \^ow_rd_data\(0),
      R => '0'
    );
\rd_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[10]_i_1__0_n_0\,
      Q => \^ow_rd_data\(10),
      R => '0'
    );
\rd_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[11]_i_1__0_n_0\,
      Q => \^ow_rd_data\(11),
      R => '0'
    );
\rd_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[12]_i_1__0_n_0\,
      Q => \^ow_rd_data\(12),
      R => '0'
    );
\rd_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[13]_i_1__0_n_0\,
      Q => \^ow_rd_data\(13),
      R => '0'
    );
\rd_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[14]_i_1__0_n_0\,
      Q => \^ow_rd_data\(14),
      R => '0'
    );
\rd_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[15]_i_1__0_n_0\,
      Q => \^ow_rd_data\(15),
      R => '0'
    );
\rd_data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[16]_i_1__0_n_0\,
      Q => \^ow_rd_data\(16),
      R => '0'
    );
\rd_data_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[17]_i_1__0_n_0\,
      Q => \^ow_rd_data\(17),
      R => '0'
    );
\rd_data_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[18]_i_1__0_n_0\,
      Q => \^ow_rd_data\(18),
      R => '0'
    );
\rd_data_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[19]_i_1__0_n_0\,
      Q => \^ow_rd_data\(19),
      R => '0'
    );
\rd_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[1]_i_1__0_n_0\,
      Q => \^ow_rd_data\(1),
      R => '0'
    );
\rd_data_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[20]_i_1__0_n_0\,
      Q => \^ow_rd_data\(20),
      R => '0'
    );
\rd_data_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[21]_i_1__0_n_0\,
      Q => \^ow_rd_data\(21),
      R => '0'
    );
\rd_data_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[22]_i_1__0_n_0\,
      Q => \^ow_rd_data\(22),
      R => '0'
    );
\rd_data_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[23]_i_1__0_n_0\,
      Q => \^ow_rd_data\(23),
      R => '0'
    );
\rd_data_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[24]_i_1__0_n_0\,
      Q => \^ow_rd_data\(24),
      R => '0'
    );
\rd_data_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[25]_i_1__0_n_0\,
      Q => \^ow_rd_data\(25),
      R => '0'
    );
\rd_data_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[26]_i_1__0_n_0\,
      Q => \^ow_rd_data\(26),
      R => '0'
    );
\rd_data_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[27]_i_1__0_n_0\,
      Q => \^ow_rd_data\(27),
      R => '0'
    );
\rd_data_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[28]_i_1__0_n_0\,
      Q => \^ow_rd_data\(28),
      R => '0'
    );
\rd_data_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[29]_i_1__0_n_0\,
      Q => \^ow_rd_data\(29),
      R => '0'
    );
\rd_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[2]_i_1__0_n_0\,
      Q => \^ow_rd_data\(2),
      R => '0'
    );
\rd_data_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[30]_i_1__0_n_0\,
      Q => \^ow_rd_data\(30),
      R => '0'
    );
\rd_data_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[31]_i_1__0_n_0\,
      Q => \^ow_rd_data\(31),
      R => '0'
    );
\rd_data_reg[32]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[32]_i_1_n_0\,
      Q => \^ow_rd_data\(32),
      R => '0'
    );
\rd_data_reg[33]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[33]_i_1_n_0\,
      Q => \^ow_rd_data\(33),
      R => '0'
    );
\rd_data_reg[34]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[34]_i_1_n_0\,
      Q => \^ow_rd_data\(34),
      R => '0'
    );
\rd_data_reg[35]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[35]_i_1_n_0\,
      Q => \^ow_rd_data\(35),
      R => '0'
    );
\rd_data_reg[36]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[36]_i_1_n_0\,
      Q => \^ow_rd_data\(36),
      R => '0'
    );
\rd_data_reg[37]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[37]_i_1_n_0\,
      Q => \^ow_rd_data\(37),
      R => '0'
    );
\rd_data_reg[38]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[38]_i_1_n_0\,
      Q => \^ow_rd_data\(38),
      R => '0'
    );
\rd_data_reg[39]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[39]_i_1_n_0\,
      Q => \^ow_rd_data\(39),
      R => '0'
    );
\rd_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[3]_i_1__0_n_0\,
      Q => \^ow_rd_data\(3),
      R => '0'
    );
\rd_data_reg[40]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[40]_i_1_n_0\,
      Q => \^ow_rd_data\(40),
      R => '0'
    );
\rd_data_reg[41]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[41]_i_1_n_0\,
      Q => \^ow_rd_data\(41),
      R => '0'
    );
\rd_data_reg[42]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[42]_i_1_n_0\,
      Q => \^ow_rd_data\(42),
      R => '0'
    );
\rd_data_reg[43]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[43]_i_1_n_0\,
      Q => \^ow_rd_data\(43),
      R => '0'
    );
\rd_data_reg[44]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[44]_i_1_n_0\,
      Q => \^ow_rd_data\(44),
      R => '0'
    );
\rd_data_reg[45]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[45]_i_1_n_0\,
      Q => \^ow_rd_data\(45),
      R => '0'
    );
\rd_data_reg[46]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[46]_i_1_n_0\,
      Q => \^ow_rd_data\(46),
      R => '0'
    );
\rd_data_reg[47]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[47]_i_1_n_0\,
      Q => \^ow_rd_data\(47),
      R => '0'
    );
\rd_data_reg[48]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[48]_i_1_n_0\,
      Q => \^ow_rd_data\(48),
      R => '0'
    );
\rd_data_reg[49]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[49]_i_1_n_0\,
      Q => \^ow_rd_data\(49),
      R => '0'
    );
\rd_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[4]_i_1__0_n_0\,
      Q => \^ow_rd_data\(4),
      R => '0'
    );
\rd_data_reg[50]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[50]_i_1_n_0\,
      Q => \^ow_rd_data\(50),
      R => '0'
    );
\rd_data_reg[51]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[51]_i_1_n_0\,
      Q => \^ow_rd_data\(51),
      R => '0'
    );
\rd_data_reg[52]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[52]_i_1_n_0\,
      Q => \^ow_rd_data\(52),
      R => '0'
    );
\rd_data_reg[53]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[53]_i_1_n_0\,
      Q => \^ow_rd_data\(53),
      R => '0'
    );
\rd_data_reg[54]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[54]_i_1_n_0\,
      Q => \^ow_rd_data\(54),
      R => '0'
    );
\rd_data_reg[55]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[55]_i_1_n_0\,
      Q => \^ow_rd_data\(55),
      R => '0'
    );
\rd_data_reg[56]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[56]_i_1_n_0\,
      Q => \^ow_rd_data\(56),
      R => '0'
    );
\rd_data_reg[57]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[57]_i_1_n_0\,
      Q => \^ow_rd_data\(57),
      R => '0'
    );
\rd_data_reg[58]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[58]_i_1_n_0\,
      Q => \^ow_rd_data\(58),
      R => '0'
    );
\rd_data_reg[59]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[59]_i_1_n_0\,
      Q => \^ow_rd_data\(59),
      R => '0'
    );
\rd_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[5]_i_1__0_n_0\,
      Q => \^ow_rd_data\(5),
      R => '0'
    );
\rd_data_reg[60]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[60]_i_1_n_0\,
      Q => \^ow_rd_data\(60),
      R => '0'
    );
\rd_data_reg[61]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[61]_i_1_n_0\,
      Q => \^ow_rd_data\(61),
      R => '0'
    );
\rd_data_reg[62]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[62]_i_1_n_0\,
      Q => \^ow_rd_data\(62),
      R => '0'
    );
\rd_data_reg[63]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[63]_i_2_n_0\,
      Q => \^ow_rd_data\(63),
      R => '0'
    );
\rd_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[6]_i_1__0_n_0\,
      Q => \^ow_rd_data\(6),
      R => '0'
    );
\rd_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[7]_i_1__0_n_0\,
      Q => \^ow_rd_data\(7),
      R => '0'
    );
\rd_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[8]_i_1__0_n_0\,
      Q => \^ow_rd_data\(8),
      R => '0'
    );
\rd_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \rd_data_reg[63]_0\,
      D => \rd_data[9]_i_1__0_n_0\,
      Q => \^ow_rd_data\(9),
      R => '0'
    );
\reg[mem_addr][5]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^rd_data_en_reg_0\,
      I1 => \reg[mem_addr][5]_i_4\,
      O => rd_data_en_reg_1
    );
wr_done_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => wr_done,
      Q => mem_wr_done,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_onewire_control is
  port (
    \reg_reg[busy]_0\ : out STD_LOGIC;
    \reg_reg[too_many]_0\ : out STD_LOGIC;
    dcvr_start : out STD_LOGIC;
    ctrl_rd_en : out STD_LOGIC;
    ctrl_bus_rst : out STD_LOGIC;
    ctrl_bit_send : out STD_LOGIC;
    \FSM_sequential_reg_reg[state][2]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_reg[mem_addr][0]_0\ : out STD_LOGIC;
    \reg_reg[mem_addr][1]_0\ : out STD_LOGIC;
    \reg_reg[mem_addr][2]_0\ : out STD_LOGIC;
    \reg_reg[mem_addr][3]_0\ : out STD_LOGIC;
    \reg_reg[mem_addr][4]_0\ : out STD_LOGIC;
    \reg_reg[mem_addr][5]_0\ : out STD_LOGIC;
    ctrl_wr_data : out STD_LOGIC_VECTOR ( 62 downto 0 );
    \reg_reg[device_count][0]_0\ : out STD_LOGIC;
    \reg_reg[device_count][1]_0\ : out STD_LOGIC;
    \reg_reg[device_count][2]_0\ : out STD_LOGIC;
    \reg_reg[device_count][3]_0\ : out STD_LOGIC;
    \reg_reg[device_count][4]_0\ : out STD_LOGIC;
    ow_dev_count : out STD_LOGIC_VECTOR ( 0 to 0 );
    wr_done : out STD_LOGIC;
    \reg_reg[bit_recv]_0\ : out STD_LOGIC;
    \reg_reg[bit_recv]_1\ : out STD_LOGIC;
    ow_pullup_o : out STD_LOGIC;
    \FSM_sequential_ow_state_reg[0]\ : out STD_LOGIC;
    \FSM_sequential_ow_state_reg[0]_0\ : out STD_LOGIC;
    ow_done_o_reg : out STD_LOGIC;
    \FSM_sequential_ow_state_reg[1]\ : out STD_LOGIC;
    \reg_reg[too_many]_1\ : out STD_LOGIC;
    \reg_reg[done]_0\ : out STD_LOGIC;
    ctrl_bit_tx : out STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    ow_reg_rst : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    if_rx_data_en : in STD_LOGIC;
    if_rx_data : in STD_LOGIC;
    if_done : in STD_LOGIC;
    dcvr_id_en : in STD_LOGIC;
    dcvr_done : in STD_LOGIC;
    ow_temp_trig_reg : in STD_LOGIC;
    ow_disc_trig_reg : in STD_LOGIC;
    ow_rst : in STD_LOGIC;
    ow_rd_data : in STD_LOGIC_VECTOR ( 63 downto 0 );
    \FSM_sequential_reg_reg[state][2]_1\ : in STD_LOGIC;
    \reg_reg[bit_send]_0\ : in STD_LOGIC;
    mem_wr_done : in STD_LOGIC;
    \ow_state__0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    dcvr_bit_recv : in STD_LOGIC;
    dcvr_bit_send : in STD_LOGIC;
    \reg_reg[data][30]_0\ : in STD_LOGIC;
    \reg_reg[mem_addr][0]_1\ : in STD_LOGIC;
    \FSM_sequential_reg[state][3]_i_6_0\ : in STD_LOGIC;
    ow_trig : in STD_LOGIC;
    ow_done_o : in STD_LOGIC;
    ow_busy_o_reg : in STD_LOGIC;
    ow_busy_o : in STD_LOGIC
  );
end design_1_axi_onewire_0_0_onewire_control;

architecture STRUCTURE of design_1_axi_onewire_0_0_onewire_control is
  signal \FSM_sequential_ow_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][0]_i_1__1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][0]_i_2__1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_1__1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_2__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_4__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_2__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_3__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_7__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_11_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_12_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_13_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_14_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_15_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_16_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_17_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_18_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_19_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_20_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_21_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_22_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_23_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][3]_i_9_n_0\ : STD_LOGIC;
  signal \^fsm_sequential_reg_reg[state][2]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal crc_inst_n_0 : STD_LOGIC;
  signal crc_inst_n_1 : STD_LOGIC;
  signal ctrl_bit_recv : STD_LOGIC;
  signal \^ctrl_bit_send\ : STD_LOGIC;
  signal \^ctrl_wr_data\ : STD_LOGIC_VECTOR ( 62 downto 0 );
  signal ctrl_wr_en : STD_LOGIC;
  signal lfsr_shift : STD_LOGIC_VECTOR ( 26 downto 1 );
  signal \lfsr_shift__0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ow_busy_o_i_3_n_0 : STD_LOGIC;
  signal \^ow_dev_count\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ow_done : STD_LOGIC;
  signal ow_done_o1_out : STD_LOGIC;
  signal p_0_in8_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 26 downto 0 );
  signal \reg[bit_recv]\ : STD_LOGIC;
  signal \reg[bit_send]\ : STD_LOGIC;
  signal \reg[bit_send]_i_2__0_n_0\ : STD_LOGIC;
  signal \reg[bit_send]_i_4_n_0\ : STD_LOGIC;
  signal \reg[bit_send]_i_5_n_0\ : STD_LOGIC;
  signal \reg[bit_send]_i_6_n_0\ : STD_LOGIC;
  signal \reg[bit_send]_i_7_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]\ : STD_LOGIC;
  signal \reg[bus_rst]_i_10_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_11_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_12_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_13_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_2__0_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_3_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_4_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_5_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_6_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_7_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_8_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]_i_9_n_0\ : STD_LOGIC;
  signal \reg[busy]_i_1_n_0\ : STD_LOGIC;
  signal \reg[busy]_i_2_n_0\ : STD_LOGIC;
  signal \reg[busy]_i_3_n_0\ : STD_LOGIC;
  signal \reg[crc_reset]\ : STD_LOGIC;
  signal \reg[data][0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][0]_i_2_n_0\ : STD_LOGIC;
  signal \reg[data][0]_i_3_n_0\ : STD_LOGIC;
  signal \reg[data][10]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][11]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][12]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][13]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][14]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][15]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][16]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][17]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][18]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][19]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][1]_i_2_n_0\ : STD_LOGIC;
  signal \reg[data][20]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][21]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][22]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][23]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][24]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][25]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][26]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][27]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][28]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][29]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][2]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][2]_i_2_n_0\ : STD_LOGIC;
  signal \reg[data][2]_i_3_n_0\ : STD_LOGIC;
  signal \reg[data][30]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][31]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][32]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][33]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][34]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][35]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][36]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][37]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][38]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][39]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][3]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][40]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][41]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][42]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][43]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][44]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][45]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][46]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][47]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][48]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][49]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][4]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][4]_i_2_n_0\ : STD_LOGIC;
  signal \reg[data][4]_i_3_n_0\ : STD_LOGIC;
  signal \reg[data][50]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][51]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][52]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][53]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][54]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][55]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][56]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][57]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][58]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][59]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][5]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][5]_i_2_n_0\ : STD_LOGIC;
  signal \reg[data][5]_i_3_n_0\ : STD_LOGIC;
  signal \reg[data][5]_i_4_n_0\ : STD_LOGIC;
  signal \reg[data][5]_i_5_n_0\ : STD_LOGIC;
  signal \reg[data][5]_i_6_n_0\ : STD_LOGIC;
  signal \reg[data][60]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][61]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][62]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][62]_i_2_n_0\ : STD_LOGIC;
  signal \reg[data][62]_i_3_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_11_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_2_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_3_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_4_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_6_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_7_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_8_n_0\ : STD_LOGIC;
  signal \reg[data][63]_i_9_n_0\ : STD_LOGIC;
  signal \reg[data][6]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][6]_i_2_n_0\ : STD_LOGIC;
  signal \reg[data][6]_i_3_n_0\ : STD_LOGIC;
  signal \reg[data][6]_i_4_n_0\ : STD_LOGIC;
  signal \reg[data][6]_i_5_n_0\ : STD_LOGIC;
  signal \reg[data][6]_i_6_n_0\ : STD_LOGIC;
  signal \reg[data][6]_i_7_n_0\ : STD_LOGIC;
  signal \reg[data][7]_i_10_n_0\ : STD_LOGIC;
  signal \reg[data][7]_i_2_n_0\ : STD_LOGIC;
  signal \reg[data][7]_i_3_n_0\ : STD_LOGIC;
  signal \reg[data][7]_i_4_n_0\ : STD_LOGIC;
  signal \reg[data][7]_i_5_n_0\ : STD_LOGIC;
  signal \reg[data][7]_i_6_n_0\ : STD_LOGIC;
  signal \reg[data][7]_i_7_n_0\ : STD_LOGIC;
  signal \reg[data][7]_i_8_n_0\ : STD_LOGIC;
  signal \reg[data][7]_i_9_n_0\ : STD_LOGIC;
  signal \reg[data][8]_i_1_n_0\ : STD_LOGIC;
  signal \reg[data][9]_i_1_n_0\ : STD_LOGIC;
  signal \reg[device_count][0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[device_count][1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[device_count][2]_i_1_n_0\ : STD_LOGIC;
  signal \reg[device_count][3]_i_1_n_0\ : STD_LOGIC;
  signal \reg[device_count][4]_i_1_n_0\ : STD_LOGIC;
  signal \reg[device_count][5]_i_1_n_0\ : STD_LOGIC;
  signal \reg[device_count][5]_i_2_n_0\ : STD_LOGIC;
  signal \reg[device_count][5]_i_3_n_0\ : STD_LOGIC;
  signal \reg[discover]\ : STD_LOGIC;
  signal \reg[discover]_i_2_n_0\ : STD_LOGIC;
  signal \reg[done]\ : STD_LOGIC;
  signal \reg[done]_i_2__1_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_10_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_2_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_3__0_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_4__0_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_5_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_6_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_7__0_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_9_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_10_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_11_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_12_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_13_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_14_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_15_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_16_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_17_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_18_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_19_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_1_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_20_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_21_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_22_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_23_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_24_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_25_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_26_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_27_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_3_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_4_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_5_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_6_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_7_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_8_n_0\ : STD_LOGIC;
  signal \reg[lfsr][26]_i_9_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][1]_i_2_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][1]_i_3_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][2]_i_1_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][2]_i_2_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][2]_i_3_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][3]_i_1_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][3]_i_2_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][4]_i_1_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][4]_i_2_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_11_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_1_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_2_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_3_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_4_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_5_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_6_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_7_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_8_n_0\ : STD_LOGIC;
  signal \reg[mem_addr][5]_i_9_n_0\ : STD_LOGIC;
  signal \reg[mem_rd_en]\ : STD_LOGIC;
  signal \reg[mem_rd_en]_i_2_n_0\ : STD_LOGIC;
  signal \reg[mem_rd_en]_i_3_n_0\ : STD_LOGIC;
  signal \reg[mem_rd_en]_i_4_n_0\ : STD_LOGIC;
  signal \reg[mem_rd_en]_i_5_n_0\ : STD_LOGIC;
  signal \reg[mem_rd_en]_i_6_n_0\ : STD_LOGIC;
  signal \reg[mem_wr_en]\ : STD_LOGIC;
  signal \reg[mem_wr_en]_i_2_n_0\ : STD_LOGIC;
  signal \reg[state]\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \reg[strong_pullup]_i_10_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_11_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_1_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_2_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_3_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_4_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_5_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_6_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_7_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_8_n_0\ : STD_LOGIC;
  signal \reg[strong_pullup]_i_9_n_0\ : STD_LOGIC;
  signal \reg[too_many]_i_1_n_0\ : STD_LOGIC;
  signal \reg[too_many]_i_2_n_0\ : STD_LOGIC;
  signal \reg[too_many]_i_3_n_0\ : STD_LOGIC;
  signal \^reg_reg[busy]_0\ : STD_LOGIC;
  signal \reg_reg[crc_reset_n_0_]\ : STD_LOGIC;
  signal \^reg_reg[device_count][0]_0\ : STD_LOGIC;
  signal \^reg_reg[device_count][1]_0\ : STD_LOGIC;
  signal \^reg_reg[device_count][2]_0\ : STD_LOGIC;
  signal \^reg_reg[device_count][3]_0\ : STD_LOGIC;
  signal \^reg_reg[device_count][4]_0\ : STD_LOGIC;
  signal \reg_reg[lfsr_n_0_][26]\ : STD_LOGIC;
  signal \^reg_reg[mem_addr][0]_0\ : STD_LOGIC;
  signal \^reg_reg[mem_addr][1]_0\ : STD_LOGIC;
  signal \^reg_reg[mem_addr][2]_0\ : STD_LOGIC;
  signal \^reg_reg[mem_addr][3]_0\ : STD_LOGIC;
  signal \^reg_reg[mem_addr][4]_0\ : STD_LOGIC;
  signal \^reg_reg[mem_addr][5]_0\ : STD_LOGIC;
  signal \^reg_reg[too_many]_0\ : STD_LOGIC;
  signal strong_pullup : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][0]_i_2\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][1]_i_2__1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][2]_i_3__0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][2]_i_6\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][2]_i_7__0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_10\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_11\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_13\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_14\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_15\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_16\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_17\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_19\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_5\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][3]_i_8\ : label is "soft_lutpair39";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_reg_reg[state][0]\ : label is "skip_command:0100,convert:0011,scratchpad_crc:1101,discover:0010,read_command:1011,scratchpad:1100,send_id:1010,erase:0001,idle:0000,get_id:1001,wait_reset:0111,match_command:1000,wait_conversion:0110,check_num:1111,save_data:1110,convert_command:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_reg_reg[state][1]\ : label is "skip_command:0100,convert:0011,scratchpad_crc:1101,discover:0010,read_command:1011,scratchpad:1100,send_id:1010,erase:0001,idle:0000,get_id:1001,wait_reset:0111,match_command:1000,wait_conversion:0110,check_num:1111,save_data:1110,convert_command:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_reg_reg[state][2]\ : label is "skip_command:0100,convert:0011,scratchpad_crc:1101,discover:0010,read_command:1011,scratchpad:1100,send_id:1010,erase:0001,idle:0000,get_id:1001,wait_reset:0111,match_command:1000,wait_conversion:0110,check_num:1111,save_data:1110,convert_command:0101";
  attribute FSM_ENCODED_STATES of \FSM_sequential_reg_reg[state][3]\ : label is "skip_command:0100,convert:0011,scratchpad_crc:1101,discover:0010,read_command:1011,scratchpad:1100,send_id:1010,erase:0001,idle:0000,get_id:1001,wait_reset:0111,match_command:1000,wait_conversion:0110,check_num:1111,save_data:1110,convert_command:0101";
  attribute SOFT_HLUTNM of \reg[bit_send]_i_2__0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \reg[bit_send]_i_7\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \reg[bus_rst]_i_10\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \reg[bus_rst]_i_12\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \reg[data][0]_i_3\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \reg[data][5]_i_4\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \reg[data][5]_i_6\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \reg[data][63]_i_11\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \reg[data][63]_i_3\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \reg[data][63]_i_6\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \reg[data][63]_i_8\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \reg[data][63]_i_9\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \reg[data][6]_i_2\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \reg[data][6]_i_5\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \reg[data][7]_i_10\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \reg[data][7]_i_3\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \reg[data][7]_i_4\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \reg[data][7]_i_5\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \reg[data][7]_i_7\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \reg[device_count][0]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \reg[device_count][1]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \reg[discover]_i_2\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \reg[done]_i_2__1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \reg[lfsr][0]_i_3__0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \reg[lfsr][0]_i_4__0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \reg[lfsr][0]_i_6\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \reg[lfsr][0]_i_7__0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \reg[lfsr][0]_i_8__0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_13\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_18\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_19\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_21\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_22\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_24\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_25\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_27\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_3\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_5\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \reg[lfsr][26]_i_7\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \reg[mem_addr][1]_i_2\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \reg[mem_addr][1]_i_3\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \reg[mem_addr][3]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \reg[mem_addr][5]_i_11\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \reg[mem_addr][5]_i_5\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \reg[mem_addr][5]_i_8\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \reg[mem_addr][5]_i_9\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \reg[mem_rd_en]_i_3\ : label is "soft_lutpair26";
begin
  \FSM_sequential_reg_reg[state][2]_0\(1 downto 0) <= \^fsm_sequential_reg_reg[state][2]_0\(1 downto 0);
  ctrl_bit_send <= \^ctrl_bit_send\;
  ctrl_wr_data(62 downto 0) <= \^ctrl_wr_data\(62 downto 0);
  ow_dev_count(0) <= \^ow_dev_count\(0);
  \reg_reg[busy]_0\ <= \^reg_reg[busy]_0\;
  \reg_reg[device_count][0]_0\ <= \^reg_reg[device_count][0]_0\;
  \reg_reg[device_count][1]_0\ <= \^reg_reg[device_count][1]_0\;
  \reg_reg[device_count][2]_0\ <= \^reg_reg[device_count][2]_0\;
  \reg_reg[device_count][3]_0\ <= \^reg_reg[device_count][3]_0\;
  \reg_reg[device_count][4]_0\ <= \^reg_reg[device_count][4]_0\;
  \reg_reg[mem_addr][0]_0\ <= \^reg_reg[mem_addr][0]_0\;
  \reg_reg[mem_addr][1]_0\ <= \^reg_reg[mem_addr][1]_0\;
  \reg_reg[mem_addr][2]_0\ <= \^reg_reg[mem_addr][2]_0\;
  \reg_reg[mem_addr][3]_0\ <= \^reg_reg[mem_addr][3]_0\;
  \reg_reg[mem_addr][4]_0\ <= \^reg_reg[mem_addr][4]_0\;
  \reg_reg[mem_addr][5]_0\ <= \^reg_reg[mem_addr][5]_0\;
  \reg_reg[too_many]_0\ <= \^reg_reg[too_many]_0\;
\FSM_sequential_ow_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5151515100050000"
    )
        port map (
      I0 => ow_rst,
      I1 => ow_done,
      I2 => \ow_state__0\(1),
      I3 => \^reg_reg[busy]_0\,
      I4 => ow_trig,
      I5 => \ow_state__0\(0),
      O => \reg_reg[done]_0\
    );
\FSM_sequential_ow_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3310331000000300"
    )
        port map (
      I0 => \^reg_reg[too_many]_0\,
      I1 => ow_rst,
      I2 => ow_done,
      I3 => \ow_state__0\(1),
      I4 => \FSM_sequential_ow_state[1]_i_2_n_0\,
      I5 => \ow_state__0\(0),
      O => \reg_reg[too_many]_1\
    );
\FSM_sequential_ow_state[1]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^reg_reg[busy]_0\,
      I1 => ow_trig,
      I2 => \ow_state__0\(1),
      O => \FSM_sequential_ow_state[1]_i_2_n_0\
    );
\FSM_sequential_reg[state][0]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF005100"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[state]\(1),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \FSM_sequential_reg[state][0]_i_2__1_n_0\,
      I4 => \reg[state]\(3),
      I5 => ow_rst,
      O => \FSM_sequential_reg[state][0]_i_1__1_n_0\
    );
\FSM_sequential_reg[state][0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"000E"
    )
        port map (
      I0 => ctrl_bit_recv,
      I1 => dcvr_bit_recv,
      I2 => \^ctrl_bit_send\,
      I3 => dcvr_bit_send,
      O => \reg_reg[bit_recv]_0\
    );
\FSM_sequential_reg[state][0]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFF00FF00F0AAFF"
    )
        port map (
      I0 => \FSM_sequential_reg_reg[state][2]_1\,
      I1 => p_0_in8_in,
      I2 => \reg[state]\(3),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => \reg[state]\(1),
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \FSM_sequential_reg[state][0]_i_2__1_n_0\
    );
\FSM_sequential_reg[state][1]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF8"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => p_0_in8_in,
      I2 => \FSM_sequential_reg[state][1]_i_4__0_n_0\,
      I3 => \FSM_sequential_reg[state][1]_i_5_n_0\,
      O => \FSM_sequential_reg[state][1]_i_1__1_n_0\
    );
\FSM_sequential_reg[state][1]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40000000"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(3),
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \FSM_sequential_reg[state][1]_i_2__0_n_0\
    );
\FSM_sequential_reg[state][1]_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ctrl_bit_recv,
      I1 => dcvr_bit_recv,
      I2 => \^ctrl_bit_send\,
      I3 => dcvr_bit_send,
      O => \reg_reg[bit_recv]_1\
    );
\FSM_sequential_reg[state][1]_i_3__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \^reg_reg[mem_addr][0]_0\,
      I1 => \^reg_reg[mem_addr][5]_0\,
      I2 => \^reg_reg[mem_addr][3]_0\,
      I3 => \^reg_reg[mem_addr][4]_0\,
      I4 => \^reg_reg[mem_addr][1]_0\,
      I5 => \^reg_reg[mem_addr][2]_0\,
      O => p_0_in8_in
    );
\FSM_sequential_reg[state][1]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000FF0001"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => ow_disc_trig_reg,
      I2 => \reg[state]\(3),
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I5 => ow_rst,
      O => \FSM_sequential_reg[state][1]_i_4__0_n_0\
    );
\FSM_sequential_reg[state][1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000C000800"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => \reg[state]\(1),
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \reg[state]\(3),
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \FSM_sequential_reg[state][1]_i_5_n_0\
    );
\FSM_sequential_reg[state][2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAAAEAAAFFFFEAAA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][2]_i_2__0_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_3__0_n_0\,
      I2 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I3 => \FSM_sequential_reg_reg[state][2]_1\,
      I4 => \FSM_sequential_reg[state][2]_i_6_n_0\,
      I5 => \FSM_sequential_reg[state][2]_i_7__0_n_0\,
      O => \FSM_sequential_reg[state][2]_i_1__0_n_0\
    );
\FSM_sequential_reg[state][2]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C000A0A0A0A0A0A"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => p_0_in8_in,
      I2 => ow_rst,
      I3 => \reg[state]\(3),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I5 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][2]_i_2__0_n_0\
    );
\FSM_sequential_reg[state][2]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][2]_i_3__0_n_0\
    );
\FSM_sequential_reg[state][2]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \FSM_sequential_reg[state][2]_i_4_n_0\
    );
\FSM_sequential_reg[state][2]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \FSM_sequential_reg[state][2]_i_6_n_0\
    );
\FSM_sequential_reg[state][2]_i_7__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][2]_i_7__0_n_0\
    );
\FSM_sequential_reg[state][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEEEEEEE"
    )
        port map (
      I0 => \FSM_sequential_reg[state][3]_i_3_n_0\,
      I1 => \FSM_sequential_reg[state][3]_i_4_n_0\,
      I2 => \reg[state]\(3),
      I3 => \FSM_sequential_reg[state][3]_i_5_n_0\,
      I4 => \FSM_sequential_reg[state][3]_i_6_n_0\,
      I5 => \FSM_sequential_reg[state][3]_i_7_n_0\,
      O => \FSM_sequential_reg[state][3]_i_1_n_0\
    );
\FSM_sequential_reg[state][3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \FSM_sequential_reg[state][3]_i_10_n_0\
    );
\FSM_sequential_reg[state][3]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00004000"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[state]\(1),
      I2 => if_done,
      I3 => \FSM_sequential_reg[state][3]_i_15_n_0\,
      I4 => \FSM_sequential_reg[state][3]_i_16_n_0\,
      O => \FSM_sequential_reg[state][3]_i_11_n_0\
    );
\FSM_sequential_reg[state][3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => \FSM_sequential_reg[state][3]_i_18_n_0\,
      I1 => \FSM_sequential_reg[state][3]_i_19_n_0\,
      I2 => lfsr_shift(17),
      I3 => lfsr_shift(18),
      I4 => \FSM_sequential_reg[state][3]_i_20_n_0\,
      I5 => \FSM_sequential_reg[state][3]_i_21_n_0\,
      O => \FSM_sequential_reg[state][3]_i_12_n_0\
    );
\FSM_sequential_reg[state][3]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F088F0"
    )
        port map (
      I0 => \FSM_sequential_reg[state][3]_i_6_0\,
      I1 => \^reg_reg[busy]_0\,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][3]_i_13_n_0\
    );
\FSM_sequential_reg[state][3]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \FSM_sequential_reg[state][3]_i_14_n_0\
    );
\FSM_sequential_reg[state][3]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => lfsr_shift(24),
      I1 => \reg_reg[lfsr_n_0_][26]\,
      I2 => lfsr_shift(26),
      I3 => lfsr_shift(23),
      O => \FSM_sequential_reg[state][3]_i_15_n_0\
    );
\FSM_sequential_reg[state][3]_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => lfsr_shift(26),
      I1 => lfsr_shift(25),
      I2 => \reg_reg[lfsr_n_0_][26]\,
      O => \FSM_sequential_reg[state][3]_i_16_n_0\
    );
\FSM_sequential_reg[state][3]_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00AA00FC"
    )
        port map (
      I0 => dcvr_done,
      I1 => ow_temp_trig_reg,
      I2 => ow_disc_trig_reg,
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][3]_i_17_n_0\
    );
\FSM_sequential_reg[state][3]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3100000000000000"
    )
        port map (
      I0 => lfsr_shift(22),
      I1 => lfsr_shift(24),
      I2 => lfsr_shift(23),
      I3 => \FSM_sequential_reg[state][3]_i_22_n_0\,
      I4 => \FSM_sequential_reg[state][3]_i_23_n_0\,
      I5 => \reg[bus_rst]_i_11_n_0\,
      O => \FSM_sequential_reg[state][3]_i_18_n_0\
    );
\FSM_sequential_reg[state][3]_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => lfsr_shift(8),
      I1 => lfsr_shift(6),
      I2 => lfsr_shift(5),
      O => \FSM_sequential_reg[state][3]_i_19_n_0\
    );
\FSM_sequential_reg[state][3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"006000C000C000C0"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[state]\(3),
      I2 => axi_aresetn_i,
      I3 => ow_reg_rst,
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][3]_i_2_n_0\
    );
\FSM_sequential_reg[state][3]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010000000000000"
    )
        port map (
      I0 => lfsr_shift(11),
      I1 => lfsr_shift(15),
      I2 => lfsr_shift(20),
      I3 => lfsr_shift(21),
      I4 => lfsr_shift(14),
      I5 => lfsr_shift(12),
      O => \FSM_sequential_reg[state][3]_i_20_n_0\
    );
\FSM_sequential_reg[state][3]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100010001"
    )
        port map (
      I0 => lfsr_shift(2),
      I1 => lfsr_shift(3),
      I2 => lfsr_shift(9),
      I3 => lfsr_shift(15),
      I4 => lfsr_shift(13),
      I5 => lfsr_shift(14),
      O => \FSM_sequential_reg[state][3]_i_21_n_0\
    );
\FSM_sequential_reg[state][3]_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000232300000023"
    )
        port map (
      I0 => lfsr_shift(17),
      I1 => lfsr_shift(18),
      I2 => lfsr_shift(16),
      I3 => lfsr_shift(7),
      I4 => lfsr_shift(9),
      I5 => lfsr_shift(8),
      O => \FSM_sequential_reg[state][3]_i_22_n_0\
    );
\FSM_sequential_reg[state][3]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000003100003131"
    )
        port map (
      I0 => lfsr_shift(1),
      I1 => lfsr_shift(3),
      I2 => lfsr_shift(2),
      I3 => lfsr_shift(19),
      I4 => lfsr_shift(21),
      I5 => lfsr_shift(20),
      O => \FSM_sequential_reg[state][3]_i_23_n_0\
    );
\FSM_sequential_reg[state][3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000088000000F0"
    )
        port map (
      I0 => \reg[mem_rd_en]_i_2_n_0\,
      I1 => if_done,
      I2 => \reg[bus_rst]_i_3_n_0\,
      I3 => \reg[state]\(3),
      I4 => \FSM_sequential_reg[state][3]_i_8_n_0\,
      I5 => \reg[mem_addr][5]_i_5_n_0\,
      O => \FSM_sequential_reg[state][3]_i_3_n_0\
    );
\FSM_sequential_reg[state][3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFAAAAAAAEEAAAA"
    )
        port map (
      I0 => ow_rst,
      I1 => if_done,
      I2 => \FSM_sequential_reg[state][3]_i_9_n_0\,
      I3 => \reg[state]\(3),
      I4 => \FSM_sequential_reg[state][3]_i_8_n_0\,
      I5 => \reg[mem_addr][5]_i_5_n_0\,
      O => \FSM_sequential_reg[state][3]_i_4_n_0\
    );
\FSM_sequential_reg[state][3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \FSM_sequential_reg[state][3]_i_5_n_0\
    );
\FSM_sequential_reg[state][3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFF808080"
    )
        port map (
      I0 => if_done,
      I1 => \FSM_sequential_reg[state][3]_i_10_n_0\,
      I2 => \reg[mem_rd_en]_i_2_n_0\,
      I3 => \FSM_sequential_reg[state][3]_i_11_n_0\,
      I4 => \FSM_sequential_reg[state][3]_i_12_n_0\,
      I5 => \FSM_sequential_reg[state][3]_i_13_n_0\,
      O => \FSM_sequential_reg[state][3]_i_6_n_0\
    );
\FSM_sequential_reg[state][3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8080A28080808080"
    )
        port map (
      I0 => \reg[lfsr][26]_i_3_n_0\,
      I1 => \FSM_sequential_reg[state][3]_i_14_n_0\,
      I2 => \reg[mem_rd_en]_i_2_n_0\,
      I3 => \FSM_sequential_reg[state][3]_i_15_n_0\,
      I4 => \FSM_sequential_reg[state][3]_i_16_n_0\,
      I5 => \FSM_sequential_reg[state][3]_i_12_n_0\,
      O => \FSM_sequential_reg[state][3]_i_7_n_0\
    );
\FSM_sequential_reg[state][3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[state]\(1),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \FSM_sequential_reg[state][3]_i_8_n_0\
    );
\FSM_sequential_reg[state][3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFC0C80008"
    )
        port map (
      I0 => mem_wr_done,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => \reg[state]\(1),
      I3 => p_0_in8_in,
      I4 => if_done,
      I5 => \FSM_sequential_reg[state][3]_i_17_n_0\,
      O => \FSM_sequential_reg[state][3]_i_9_n_0\
    );
\FSM_sequential_reg_reg[state][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => \FSM_sequential_reg[state][3]_i_1_n_0\,
      D => \FSM_sequential_reg[state][0]_i_1__1_n_0\,
      Q => \^fsm_sequential_reg_reg[state][2]_0\(0),
      R => '0'
    );
\FSM_sequential_reg_reg[state][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => \FSM_sequential_reg[state][3]_i_1_n_0\,
      D => \FSM_sequential_reg[state][1]_i_1__1_n_0\,
      Q => \reg[state]\(1),
      R => '0'
    );
\FSM_sequential_reg_reg[state][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => \FSM_sequential_reg[state][3]_i_1_n_0\,
      D => \FSM_sequential_reg[state][2]_i_1__0_n_0\,
      Q => \^fsm_sequential_reg_reg[state][2]_0\(1),
      R => '0'
    );
\FSM_sequential_reg_reg[state][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => \FSM_sequential_reg[state][3]_i_1_n_0\,
      D => \FSM_sequential_reg[state][3]_i_2_n_0\,
      Q => \reg[state]\(3),
      R => '0'
    );
crc_inst: entity work.design_1_axi_onewire_0_0_onewire_crc_0
     port map (
      \FSM_sequential_reg_reg[state][0]\ => crc_inst_n_0,
      \FSM_sequential_reg_reg[state][3]\ => crc_inst_n_1,
      axi_aresetn_i => axi_aresetn_i,
      axi_clk_i => axi_clk_i,
      if_rx_data => if_rx_data,
      if_rx_data_en => if_rx_data_en,
      ow_reg_rst => ow_reg_rst,
      pristine_reg_0 => \reg_reg[crc_reset_n_0_]\,
      \reg[data][63]_i_5_0\ => \^fsm_sequential_reg_reg[state][2]_0\(1),
      \reg[state]\(1) => \reg[state]\(3),
      \reg[state]\(0) => \reg[state]\(1),
      \reg_reg[data][0]\ => \reg[data][7]_i_3_n_0\,
      \reg_reg[data][0]_0\ => \reg[data][7]_i_4_n_0\,
      \reg_reg[data][0]_1\ => \reg[data][63]_i_6_n_0\,
      \reg_reg[data][0]_2\ => \^fsm_sequential_reg_reg[state][2]_0\(0),
      \reg_reg[data][0]_3\ => \reg[lfsr][26]_i_4_n_0\,
      \reg_reg[data][30]\ => \reg[data][63]_i_3_n_0\,
      \reg_reg[data][30]_0\ => \reg[data][63]_i_4_n_0\,
      \reg_reg[data][30]_1\ => \reg[lfsr][26]_i_6_n_0\,
      \reg_reg[data][30]_2\ => \reg[data][63]_i_9_n_0\,
      \reg_reg[data][30]_3\ => \reg[lfsr][26]_i_12_n_0\,
      \reg_reg[data][30]_4\ => \reg[lfsr][26]_i_15_n_0\,
      \reg_reg[data][30]_5\ => \reg_reg[data][30]_0\,
      \reg_reg[data][30]_6\ => \reg[data][63]_i_11_n_0\
    );
mem_reg_0_63_0_2_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3020"
    )
        port map (
      I0 => ctrl_wr_en,
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => dcvr_id_en,
      O => wr_done
    );
ow_busy_o_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400FFFF04000000"
    )
        port map (
      I0 => \ow_state__0\(1),
      I1 => ow_trig,
      I2 => \^reg_reg[busy]_0\,
      I3 => ow_busy_o_reg,
      I4 => ow_busy_o_i_3_n_0,
      I5 => ow_busy_o,
      O => \FSM_sequential_ow_state_reg[1]\
    );
ow_busy_o_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCECCCECFFFFFCCC"
    )
        port map (
      I0 => \^reg_reg[too_many]_0\,
      I1 => ow_rst,
      I2 => ow_done,
      I3 => \ow_state__0\(1),
      I4 => \FSM_sequential_ow_state[1]_i_2_n_0\,
      I5 => \ow_state__0\(0),
      O => ow_busy_o_i_3_n_0
    );
ow_disc_trig_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5151515100000100"
    )
        port map (
      I0 => ow_rst,
      I1 => \ow_state__0\(0),
      I2 => \ow_state__0\(1),
      I3 => ow_trig,
      I4 => \^reg_reg[busy]_0\,
      I5 => ow_disc_trig_reg,
      O => \FSM_sequential_ow_state_reg[0]\
    );
ow_done_o_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ow_done_o1_out,
      I1 => ow_busy_o_i_3_n_0,
      I2 => ow_done_o,
      O => ow_done_o_reg
    );
ow_done_o_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000002C200000"
    )
        port map (
      I0 => ow_done,
      I1 => \ow_state__0\(0),
      I2 => \ow_state__0\(1),
      I3 => \^reg_reg[too_many]_0\,
      I4 => axi_aresetn_i,
      I5 => ow_reg_rst,
      O => ow_done_o1_out
    );
ow_pullup_o_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => strong_pullup,
      O => ow_pullup_o
    );
ow_temp_trig_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4545454500000400"
    )
        port map (
      I0 => ow_rst,
      I1 => \ow_state__0\(0),
      I2 => \ow_state__0\(1),
      I3 => ow_done,
      I4 => \^reg_reg[too_many]_0\,
      I5 => ow_temp_trig_reg,
      O => \FSM_sequential_ow_state_reg[0]_0\
    );
\reg[bit_recv]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAEFAAAAAAAAAAAA"
    )
        port map (
      I0 => \reg[crc_reset]\,
      I1 => \reg[lfsr][26]_i_4_n_0\,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \FSM_sequential_reg[state][3]_i_5_n_0\,
      I4 => if_rx_data_en,
      I5 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      O => \reg[bit_recv]\
    );
\reg[bit_send]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF80FF80FF80"
    )
        port map (
      I0 => \reg[bit_send]_i_2__0_n_0\,
      I1 => \reg_reg[bit_send]_0\,
      I2 => \reg[lfsr][26]_i_4_n_0\,
      I3 => \reg[bit_send]_i_4_n_0\,
      I4 => \reg[bit_send]_i_5_n_0\,
      I5 => \FSM_sequential_reg[state][2]_i_6_n_0\,
      O => \reg[bit_send]\
    );
\reg[bit_send]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(3),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[bit_send]_i_2__0_n_0\
    );
\reg[bit_send]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000020000020000"
    )
        port map (
      I0 => if_done,
      I1 => ow_rst,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \reg[state]\(3),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => \reg[state]\(1),
      O => \reg[bit_send]_i_4_n_0\
    );
\reg[bit_send]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFEFEAA"
    )
        port map (
      I0 => \reg[bit_send]_i_6_n_0\,
      I1 => \reg[data][63]_i_8_n_0\,
      I2 => \reg[lfsr][26]_i_5_n_0\,
      I3 => \reg[lfsr][26]_i_15_n_0\,
      I4 => \reg[lfsr][26]_i_12_n_0\,
      I5 => \reg[bit_send]_i_7_n_0\,
      O => \reg[bit_send]_i_5_n_0\
    );
\reg[bit_send]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000080000"
    )
        port map (
      I0 => \FSM_sequential_reg[state][3]_i_6_0\,
      I1 => \^reg_reg[busy]_0\,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \reg[state]\(1),
      I4 => \reg[state]\(3),
      I5 => \FSM_sequential_reg_reg[state][2]_1\,
      O => \reg[bit_send]_i_6_n_0\
    );
\reg[bit_send]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => if_done,
      I2 => \reg[state]\(1),
      O => \reg[bit_send]_i_7_n_0\
    );
\reg[bus_rst]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => lfsr_shift(14),
      I1 => lfsr_shift(13),
      I2 => lfsr_shift(15),
      O => \reg[bus_rst]_i_10_n_0\
    );
\reg[bus_rst]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"005D005D0000005D"
    )
        port map (
      I0 => lfsr_shift(12),
      I1 => lfsr_shift(10),
      I2 => lfsr_shift(11),
      I3 => lfsr_shift(6),
      I4 => lfsr_shift(4),
      I5 => lfsr_shift(5),
      O => \reg[bus_rst]_i_11_n_0\
    );
\reg[bus_rst]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => lfsr_shift(12),
      I1 => lfsr_shift(14),
      O => \reg[bus_rst]_i_12_n_0\
    );
\reg[bus_rst]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => lfsr_shift(11),
      I1 => lfsr_shift(15),
      O => \reg[bus_rst]_i_13_n_0\
    );
\reg[bus_rst]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEAAAEAAAEAAA"
    )
        port map (
      I0 => \reg[bus_rst]_i_2__0_n_0\,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => p_0_in8_in,
      I3 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I4 => \reg[bus_rst]_i_3_n_0\,
      I5 => \reg[bus_rst]_i_4_n_0\,
      O => \reg[bus_rst]\
    );
\reg[bus_rst]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000020000"
    )
        port map (
      I0 => \reg[discover]_i_2_n_0\,
      I1 => \reg[state]\(3),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => ow_rst,
      I4 => ow_temp_trig_reg,
      I5 => ow_disc_trig_reg,
      O => \reg[bus_rst]_i_2__0_n_0\
    );
\reg[bus_rst]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \reg[bus_rst]_i_5_n_0\,
      I1 => \reg[bus_rst]_i_6_n_0\,
      I2 => \reg[bus_rst]_i_7_n_0\,
      I3 => \reg[bus_rst]_i_8_n_0\,
      I4 => \reg[bus_rst]_i_9_n_0\,
      O => \reg[bus_rst]_i_3_n_0\
    );
\reg[bus_rst]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I5 => \reg[state]\(3),
      O => \reg[bus_rst]_i_4_n_0\
    );
\reg[bus_rst]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000080000"
    )
        port map (
      I0 => lfsr_shift(24),
      I1 => lfsr_shift(23),
      I2 => lfsr_shift(22),
      I3 => \reg_reg[lfsr_n_0_][26]\,
      I4 => lfsr_shift(25),
      I5 => lfsr_shift(26),
      O => \reg[bus_rst]_i_5_n_0\
    );
\reg[bus_rst]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000EA"
    )
        port map (
      I0 => lfsr_shift(18),
      I1 => lfsr_shift(16),
      I2 => lfsr_shift(17),
      I3 => lfsr_shift(5),
      I4 => lfsr_shift(6),
      I5 => lfsr_shift(8),
      O => \reg[bus_rst]_i_6_n_0\
    );
\reg[bus_rst]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000005D00005D5D"
    )
        port map (
      I0 => lfsr_shift(9),
      I1 => lfsr_shift(7),
      I2 => lfsr_shift(8),
      I3 => lfsr_shift(19),
      I4 => lfsr_shift(21),
      I5 => lfsr_shift(20),
      O => \reg[bus_rst]_i_7_n_0\
    );
\reg[bus_rst]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => lfsr_shift(2),
      I1 => lfsr_shift(1),
      I2 => lfsr_shift(9),
      I3 => lfsr_shift(3),
      I4 => \reg[bus_rst]_i_10_n_0\,
      I5 => \reg[bus_rst]_i_11_n_0\,
      O => \reg[bus_rst]_i_8_n_0\
    );
\reg[bus_rst]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000040000"
    )
        port map (
      I0 => \reg[bus_rst]_i_12_n_0\,
      I1 => lfsr_shift(20),
      I2 => lfsr_shift(21),
      I3 => \reg[bus_rst]_i_13_n_0\,
      I4 => lfsr_shift(17),
      I5 => lfsr_shift(18),
      O => \reg[bus_rst]_i_9_n_0\
    );
\reg[busy]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1110FFFF11100000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => ow_rst,
      I2 => ow_disc_trig_reg,
      I3 => ow_temp_trig_reg,
      I4 => \reg[busy]_i_2_n_0\,
      I5 => \^reg_reg[busy]_0\,
      O => \reg[busy]_i_1_n_0\
    );
\reg[busy]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \reg[busy]_i_3_n_0\,
      I1 => p_0_in8_in,
      I2 => \reg[state]\(3),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => \reg[state]\(1),
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[busy]_i_2_n_0\
    );
\reg[busy]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCEECCCCCCFCCC"
    )
        port map (
      I0 => dcvr_done,
      I1 => \reg[lfsr][26]_i_6_n_0\,
      I2 => ow_temp_trig_reg,
      I3 => \reg[mem_addr][5]_i_9_n_0\,
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => \reg[state]\(1),
      O => \reg[busy]_i_3_n_0\
    );
\reg[crc_reset]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I2 => \reg[state]\(1),
      I3 => if_done,
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I5 => \reg[mem_rd_en]_i_2_n_0\,
      O => \reg[crc_reset]\
    );
\reg[data][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEAAAEAAAEAAA"
    )
        port map (
      I0 => \reg[data][0]_i_2_n_0\,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I2 => \^ctrl_wr_data\(0),
      I3 => \reg[data][5]_i_3_n_0\,
      I4 => \reg[data][0]_i_3_n_0\,
      I5 => \reg[data][6]_i_6_n_0\,
      O => \reg[data][0]_i_1_n_0\
    );
\reg[data][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FAAAEAFAAAAAEAAA"
    )
        port map (
      I0 => \reg[data][6]_i_2_n_0\,
      I1 => ow_rd_data(0),
      I2 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => \reg[state]\(1),
      I5 => \^ctrl_wr_data\(0),
      O => \reg[data][0]_i_2_n_0\
    );
\reg[data][0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04000000"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \^ctrl_wr_data\(0),
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \reg[state]\(1),
      O => \reg[data][0]_i_3_n_0\
    );
\reg[data][10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(10),
      I3 => \^ctrl_wr_data\(10),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][10]_i_1_n_0\
    );
\reg[data][11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(11),
      I3 => \^ctrl_wr_data\(11),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][11]_i_1_n_0\
    );
\reg[data][12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(12),
      I3 => \^ctrl_wr_data\(12),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][12]_i_1_n_0\
    );
\reg[data][13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(13),
      I3 => \^ctrl_wr_data\(13),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][13]_i_1_n_0\
    );
\reg[data][14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(14),
      I3 => \^ctrl_wr_data\(14),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][14]_i_1_n_0\
    );
\reg[data][15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(15),
      I3 => \^ctrl_wr_data\(15),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][15]_i_1_n_0\
    );
\reg[data][16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(16),
      I3 => \^ctrl_wr_data\(16),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][16]_i_1_n_0\
    );
\reg[data][17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(17),
      I3 => \^ctrl_wr_data\(17),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][17]_i_1_n_0\
    );
\reg[data][18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(18),
      I3 => \^ctrl_wr_data\(18),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][18]_i_1_n_0\
    );
\reg[data][19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(19),
      I3 => \^ctrl_wr_data\(19),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][19]_i_1_n_0\
    );
\reg[data][1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFEFEFEEEFEEEEE"
    )
        port map (
      I0 => \reg[data][7]_i_6_n_0\,
      I1 => \reg[data][1]_i_2_n_0\,
      I2 => \^ctrl_wr_data\(1),
      I3 => \FSM_sequential_reg[state][3]_i_5_n_0\,
      I4 => \reg[data][5]_i_3_n_0\,
      I5 => \reg[data][5]_i_4_n_0\,
      O => \reg[data][1]_i_1_n_0\
    );
\reg[data][1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => ow_rd_data(1),
      O => \reg[data][1]_i_2_n_0\
    );
\reg[data][20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(20),
      I3 => \^ctrl_wr_data\(20),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][20]_i_1_n_0\
    );
\reg[data][21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(21),
      I3 => \^ctrl_wr_data\(21),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][21]_i_1_n_0\
    );
\reg[data][22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(22),
      I3 => \^ctrl_wr_data\(22),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][22]_i_1_n_0\
    );
\reg[data][23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(23),
      I3 => \^ctrl_wr_data\(23),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][23]_i_1_n_0\
    );
\reg[data][24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(24),
      I3 => \^ctrl_wr_data\(24),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][24]_i_1_n_0\
    );
\reg[data][25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(25),
      I3 => \^ctrl_wr_data\(25),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][25]_i_1_n_0\
    );
\reg[data][26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(26),
      I3 => \^ctrl_wr_data\(26),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][26]_i_1_n_0\
    );
\reg[data][27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(27),
      I3 => \^ctrl_wr_data\(27),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][27]_i_1_n_0\
    );
\reg[data][28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(28),
      I3 => \^ctrl_wr_data\(28),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][28]_i_1_n_0\
    );
\reg[data][29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(29),
      I3 => \^ctrl_wr_data\(29),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][29]_i_1_n_0\
    );
\reg[data][2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \reg[data][2]_i_2_n_0\,
      I1 => \reg[data][6]_i_3_n_0\,
      I2 => \reg[data][4]_i_2_n_0\,
      O => \reg[data][2]_i_1_n_0\
    );
\reg[data][2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEAC0C0EAEAC0C0"
    )
        port map (
      I0 => \reg[data][2]_i_3_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(2),
      I3 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I4 => \^ctrl_wr_data\(2),
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[data][2]_i_2_n_0\
    );
\reg[data][2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3030303030002020"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[data][2]_i_3_n_0\
    );
\reg[data][30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(30),
      I3 => \^ctrl_wr_data\(30),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][30]_i_1_n_0\
    );
\reg[data][31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(31),
      I3 => \^ctrl_wr_data\(31),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][31]_i_1_n_0\
    );
\reg[data][32]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(32),
      I3 => \^ctrl_wr_data\(32),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][32]_i_1_n_0\
    );
\reg[data][33]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(33),
      I3 => \^ctrl_wr_data\(33),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][33]_i_1_n_0\
    );
\reg[data][34]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(34),
      I3 => \^ctrl_wr_data\(34),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][34]_i_1_n_0\
    );
\reg[data][35]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(35),
      I3 => \^ctrl_wr_data\(35),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][35]_i_1_n_0\
    );
\reg[data][36]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(36),
      I3 => \^ctrl_wr_data\(36),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][36]_i_1_n_0\
    );
\reg[data][37]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(37),
      I3 => \^ctrl_wr_data\(37),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][37]_i_1_n_0\
    );
\reg[data][38]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(38),
      I3 => \^ctrl_wr_data\(38),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][38]_i_1_n_0\
    );
\reg[data][39]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(39),
      I3 => \^ctrl_wr_data\(39),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][39]_i_1_n_0\
    );
\reg[data][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \reg[data][7]_i_5_n_0\,
      I1 => \reg[data][7]_i_6_n_0\,
      I2 => \reg[data][7]_i_7_n_0\,
      I3 => ow_rd_data(3),
      I4 => \^ctrl_wr_data\(3),
      I5 => \reg[data][7]_i_8_n_0\,
      O => \reg[data][3]_i_1_n_0\
    );
\reg[data][40]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(40),
      I3 => \^ctrl_wr_data\(40),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][40]_i_1_n_0\
    );
\reg[data][41]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(41),
      I3 => \^ctrl_wr_data\(41),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][41]_i_1_n_0\
    );
\reg[data][42]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(42),
      I3 => \^ctrl_wr_data\(42),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][42]_i_1_n_0\
    );
\reg[data][43]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(43),
      I3 => \^ctrl_wr_data\(43),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][43]_i_1_n_0\
    );
\reg[data][44]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(44),
      I3 => \^ctrl_wr_data\(44),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][44]_i_1_n_0\
    );
\reg[data][45]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(45),
      I3 => \^ctrl_wr_data\(45),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][45]_i_1_n_0\
    );
\reg[data][46]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(46),
      I3 => \^ctrl_wr_data\(46),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][46]_i_1_n_0\
    );
\reg[data][47]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(47),
      I3 => \^ctrl_wr_data\(47),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][47]_i_1_n_0\
    );
\reg[data][48]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(48),
      I3 => \^ctrl_wr_data\(48),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][48]_i_1_n_0\
    );
\reg[data][49]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(49),
      I3 => \^ctrl_wr_data\(49),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][49]_i_1_n_0\
    );
\reg[data][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFCC80"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => \^ctrl_wr_data\(4),
      I2 => \reg[data][5]_i_3_n_0\,
      I3 => \reg[data][5]_i_4_n_0\,
      I4 => \reg[data][4]_i_2_n_0\,
      I5 => \reg[data][4]_i_3_n_0\,
      O => \reg[data][4]_i_1_n_0\
    );
\reg[data][4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F00020000000000"
    )
        port map (
      I0 => \reg[lfsr][0]_i_9_n_0\,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => \reg[state]\(1),
      O => \reg[data][4]_i_2_n_0\
    );
\reg[data][4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000000000000"
    )
        port map (
      I0 => ow_rd_data(4),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => \reg[state]\(1),
      I3 => ow_reg_rst,
      I4 => axi_aresetn_i,
      I5 => \reg[state]\(3),
      O => \reg[data][4]_i_3_n_0\
    );
\reg[data][50]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(50),
      I3 => \^ctrl_wr_data\(50),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][50]_i_1_n_0\
    );
\reg[data][51]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(51),
      I3 => \^ctrl_wr_data\(51),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][51]_i_1_n_0\
    );
\reg[data][52]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(52),
      I3 => \^ctrl_wr_data\(52),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][52]_i_1_n_0\
    );
\reg[data][53]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(53),
      I3 => \^ctrl_wr_data\(53),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][53]_i_1_n_0\
    );
\reg[data][54]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(54),
      I3 => \^ctrl_wr_data\(54),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][54]_i_1_n_0\
    );
\reg[data][55]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(55),
      I3 => \^ctrl_wr_data\(55),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][55]_i_1_n_0\
    );
\reg[data][56]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(56),
      I3 => \^ctrl_wr_data\(56),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][56]_i_1_n_0\
    );
\reg[data][57]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(57),
      I3 => \^ctrl_wr_data\(57),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][57]_i_1_n_0\
    );
\reg[data][58]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(58),
      I3 => \^ctrl_wr_data\(58),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][58]_i_1_n_0\
    );
\reg[data][59]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(59),
      I3 => \^ctrl_wr_data\(59),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][59]_i_1_n_0\
    );
\reg[data][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFEFEFEEEFEEEEE"
    )
        port map (
      I0 => \reg[data][7]_i_6_n_0\,
      I1 => \reg[data][5]_i_2_n_0\,
      I2 => \^ctrl_wr_data\(5),
      I3 => \FSM_sequential_reg[state][3]_i_5_n_0\,
      I4 => \reg[data][5]_i_3_n_0\,
      I5 => \reg[data][5]_i_4_n_0\,
      O => \reg[data][5]_i_1_n_0\
    );
\reg[data][5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => ow_rd_data(5),
      O => \reg[data][5]_i_2_n_0\
    );
\reg[data][5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF0000FFFE"
    )
        port map (
      I0 => \reg[lfsr][26]_i_12_n_0\,
      I1 => \reg[data][5]_i_5_n_0\,
      I2 => lfsr_shift(14),
      I3 => lfsr_shift(4),
      I4 => ow_rst,
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[data][5]_i_3_n_0\
    );
\reg[data][5]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0D000400"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[state]\(1),
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \reg[state]\(3),
      O => \reg[data][5]_i_4_n_0\
    );
\reg[data][5]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFBFF"
    )
        port map (
      I0 => lfsr_shift(25),
      I1 => lfsr_shift(8),
      I2 => lfsr_shift(20),
      I3 => \reg[data][5]_i_6_n_0\,
      I4 => \reg[lfsr][26]_i_14_n_0\,
      I5 => lfsr_shift(24),
      O => \reg[data][5]_i_5_n_0\
    );
\reg[data][5]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => lfsr_shift(18),
      I1 => lfsr_shift(17),
      O => \reg[data][5]_i_6_n_0\
    );
\reg[data][60]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(60),
      I3 => \^ctrl_wr_data\(60),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][60]_i_1_n_0\
    );
\reg[data][61]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(61),
      I3 => \^ctrl_wr_data\(61),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][61]_i_1_n_0\
    );
\reg[data][62]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(62),
      I3 => \^ctrl_wr_data\(62),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][62]_i_1_n_0\
    );
\reg[data][62]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000040"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I4 => \reg[state]\(1),
      O => \reg[data][62]_i_2_n_0\
    );
\reg[data][62]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444444004440"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(3),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I5 => \reg[state]\(1),
      O => \reg[data][62]_i_3_n_0\
    );
\reg[data][63]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => \reg[state]\(1),
      I2 => \reg[state]\(3),
      O => \reg[data][63]_i_11_n_0\
    );
\reg[data][63]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA0088F0AA008800"
    )
        port map (
      I0 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I1 => if_rx_data,
      I2 => ow_rd_data(63),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I4 => \reg[state]\(1),
      I5 => \FSM_sequential_reg[state][2]_i_6_n_0\,
      O => \reg[data][63]_i_2_n_0\
    );
\reg[data][63]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \reg[state]\(1),
      I4 => if_done,
      O => \reg[data][63]_i_3_n_0\
    );
\reg[data][63]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888880"
    )
        port map (
      I0 => \reg[data][63]_i_7_n_0\,
      I1 => \reg[data][63]_i_8_n_0\,
      I2 => \reg[lfsr][26]_i_17_n_0\,
      I3 => \reg[lfsr][26]_i_22_n_0\,
      I4 => lfsr_shift(7),
      I5 => \reg[lfsr][26]_i_23_n_0\,
      O => \reg[data][63]_i_4_n_0\
    );
\reg[data][63]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => if_done,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \reg[state]\(1),
      O => \reg[data][63]_i_6_n_0\
    );
\reg[data][63]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[data][63]_i_7_n_0\
    );
\reg[data][63]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => if_done,
      I1 => \reg[state]\(1),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[data][63]_i_8_n_0\
    );
\reg[data][63]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \FSM_sequential_reg[state][3]_i_6_0\,
      I2 => \^reg_reg[busy]_0\,
      I3 => \FSM_sequential_reg_reg[state][2]_1\,
      O => \reg[data][63]_i_9_n_0\
    );
\reg[data][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEFE"
    )
        port map (
      I0 => \reg[data][6]_i_2_n_0\,
      I1 => \reg[data][6]_i_3_n_0\,
      I2 => \reg[data][6]_i_4_n_0\,
      I3 => \reg[data][6]_i_5_n_0\,
      I4 => \reg[data][6]_i_6_n_0\,
      I5 => \reg[data][6]_i_7_n_0\,
      O => \reg[data][6]_i_1_n_0\
    );
\reg[data][6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I2 => axi_aresetn_i,
      I3 => ow_reg_rst,
      O => \reg[data][6]_i_2_n_0\
    );
\reg[data][6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000FF0008"
    )
        port map (
      I0 => \reg[mem_rd_en]_i_2_n_0\,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \reg[state]\(3),
      I4 => \reg[state]\(1),
      I5 => ow_rst,
      O => \reg[data][6]_i_3_n_0\
    );
\reg[data][6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF200020002000"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => ow_rst,
      I2 => \^ctrl_wr_data\(6),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I4 => ow_rd_data(6),
      I5 => \reg[data][62]_i_2_n_0\,
      O => \reg[data][6]_i_4_n_0\
    );
\reg[data][6]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04000000"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \^ctrl_wr_data\(6),
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \reg[state]\(1),
      O => \reg[data][6]_i_5_n_0\
    );
\reg[data][6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \reg[lfsr][26]_i_23_n_0\,
      I1 => lfsr_shift(2),
      I2 => lfsr_shift(1),
      I3 => lfsr_shift(3),
      I4 => lfsr_shift(7),
      I5 => \reg[lfsr][26]_i_17_n_0\,
      O => \reg[data][6]_i_6_n_0\
    );
\reg[data][6]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFC20000"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => \reg[state]\(1),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I4 => \^ctrl_wr_data\(6),
      I5 => ow_rst,
      O => \reg[data][6]_i_7_n_0\
    );
\reg[data][7]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I2 => axi_aresetn_i,
      I3 => ow_reg_rst,
      O => \reg[data][7]_i_10_n_0\
    );
\reg[data][7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFEEEFEEE"
    )
        port map (
      I0 => \reg[data][7]_i_5_n_0\,
      I1 => \reg[data][7]_i_6_n_0\,
      I2 => \reg[data][7]_i_7_n_0\,
      I3 => ow_rd_data(7),
      I4 => \^ctrl_wr_data\(7),
      I5 => \reg[data][7]_i_8_n_0\,
      O => \reg[data][7]_i_2_n_0\
    );
\reg[data][7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => \reg[state]\(1),
      I2 => if_done,
      I3 => \reg[state]\(3),
      O => \reg[data][7]_i_3_n_0\
    );
\reg[data][7]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF2000"
    )
        port map (
      I0 => if_done,
      I1 => \reg[state]\(3),
      I2 => \reg[state]\(1),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => \reg[lfsr][26]_i_6_n_0\,
      O => \reg[data][7]_i_4_n_0\
    );
\reg[data][7]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \reg[state]\(1),
      O => \reg[data][7]_i_5_n_0\
    );
\reg[data][7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F2002200"
    )
        port map (
      I0 => \reg[lfsr][0]_i_9_n_0\,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \reg[state]\(1),
      I4 => \reg[state]\(3),
      I5 => ow_rst,
      O => \reg[data][7]_i_6_n_0\
    );
\reg[data][7]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => \reg[state]\(1),
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[data][7]_i_7_n_0\
    );
\reg[data][7]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAEFFAEFFAEAEAE"
    )
        port map (
      I0 => \reg[data][7]_i_9_n_0\,
      I1 => \reg[mem_addr][1]_i_3_n_0\,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \reg[data][7]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_15_n_0\,
      I5 => \reg[lfsr][26]_i_12_n_0\,
      O => \reg[data][7]_i_8_n_0\
    );
\reg[data][7]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"003B000000380000"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => \reg[state]\(1),
      I3 => ow_reg_rst,
      I4 => axi_aresetn_i,
      I5 => \reg[state]\(3),
      O => \reg[data][7]_i_9_n_0\
    );
\reg[data][8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(8),
      I3 => \^ctrl_wr_data\(8),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][8]_i_1_n_0\
    );
\reg[data][9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \reg[data][62]_i_2_n_0\,
      I2 => ow_rd_data(9),
      I3 => \^ctrl_wr_data\(9),
      I4 => \reg[data][62]_i_3_n_0\,
      O => \reg[data][9]_i_1_n_0\
    );
\reg[device_count][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(1),
      I3 => \^reg_reg[device_count][0]_0\,
      O => \reg[device_count][0]_i_1_n_0\
    );
\reg[device_count][1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00080800"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => \^reg_reg[device_count][0]_0\,
      I4 => \^reg_reg[device_count][1]_0\,
      O => \reg[device_count][1]_i_1_n_0\
    );
\reg[device_count][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008080808000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => \^reg_reg[device_count][0]_0\,
      I4 => \^reg_reg[device_count][1]_0\,
      I5 => \^reg_reg[device_count][2]_0\,
      O => \reg[device_count][2]_i_1_n_0\
    );
\reg[device_count][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"007F000000800000"
    )
        port map (
      I0 => \^reg_reg[device_count][0]_0\,
      I1 => \^reg_reg[device_count][1]_0\,
      I2 => \^reg_reg[device_count][2]_0\,
      I3 => ow_rst,
      I4 => \reg[state]\(1),
      I5 => \^reg_reg[device_count][3]_0\,
      O => \reg[device_count][3]_i_1_n_0\
    );
\reg[device_count][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFF000080000000"
    )
        port map (
      I0 => \^reg_reg[device_count][2]_0\,
      I1 => \^reg_reg[device_count][1]_0\,
      I2 => \^reg_reg[device_count][0]_0\,
      I3 => \^reg_reg[device_count][3]_0\,
      I4 => \reg[mem_addr][1]_i_3_n_0\,
      I5 => \^reg_reg[device_count][4]_0\,
      O => \reg[device_count][4]_i_1_n_0\
    );
\reg[device_count][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000200"
    )
        port map (
      I0 => dcvr_id_en,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => \reg[state]\(3),
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => \reg[lfsr][26]_i_6_n_0\,
      O => \reg[device_count][5]_i_1_n_0\
    );
\reg[device_count][5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0B00000004000000"
    )
        port map (
      I0 => \reg[device_count][5]_i_3_n_0\,
      I1 => \^reg_reg[device_count][4]_0\,
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \reg[state]\(1),
      I5 => \^ow_dev_count\(0),
      O => \reg[device_count][5]_i_2_n_0\
    );
\reg[device_count][5]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \^reg_reg[device_count][2]_0\,
      I1 => \^reg_reg[device_count][1]_0\,
      I2 => \^reg_reg[device_count][0]_0\,
      I3 => \^reg_reg[device_count][3]_0\,
      O => \reg[device_count][5]_i_3_n_0\
    );
\reg[discover]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000040000000000"
    )
        port map (
      I0 => p_0_in8_in,
      I1 => mem_wr_done,
      I2 => \reg[state]\(3),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => ow_rst,
      I5 => \reg[discover]_i_2_n_0\,
      O => \reg[discover]\
    );
\reg[discover]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[discover]_i_2_n_0\
    );
\reg[done]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0808FF0808080808"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2__0_n_0\,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => p_0_in8_in,
      I3 => \reg[state]\(1),
      I4 => ow_rst,
      I5 => \reg[done]_i_2__1_n_0\,
      O => \reg[done]\
    );
\reg[done]_i_2__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => dcvr_done,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \reg[state]\(3),
      O => \reg[done]_i_2__1_n_0\
    );
\reg[lfsr][0]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => lfsr_shift(5),
      I1 => \reg[mem_rd_en]_i_6_n_0\,
      I2 => lfsr_shift(6),
      I3 => lfsr_shift(8),
      I4 => lfsr_shift(9),
      I5 => lfsr_shift(7),
      O => \reg[lfsr][0]_i_10_n_0\
    );
\reg[lfsr][0]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFEEFFEE"
    )
        port map (
      I0 => \reg[lfsr][0]_i_2_n_0\,
      I1 => \reg[lfsr][0]_i_3__0_n_0\,
      I2 => \reg[lfsr][0]_i_4__0_n_0\,
      I3 => \reg[lfsr][0]_i_5_n_0\,
      I4 => \reg[lfsr][0]_i_6_n_0\,
      I5 => \reg[lfsr][0]_i_7__0_n_0\,
      O => p_1_in(0)
    );
\reg[lfsr][0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAEEFEFEFE"
    )
        port map (
      I0 => ow_rst,
      I1 => \lfsr_shift__0\(0),
      I2 => \reg[mem_rd_en]_i_2_n_0\,
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => \reg[state]\(3),
      O => \reg[lfsr][0]_i_2_n_0\
    );
\reg[lfsr][0]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B0A03333"
    )
        port map (
      I0 => \lfsr_shift__0\(0),
      I1 => \reg[state]\(3),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[lfsr][0]_i_3__0_n_0\
    );
\reg[lfsr][0]_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[lfsr][0]_i_4__0_n_0\
    );
\reg[lfsr][0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5151510051005100"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => \reg[state]\(1),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \lfsr_shift__0\(0),
      I4 => \reg[mem_rd_en]_i_2_n_0\,
      I5 => \reg[state]\(3),
      O => \reg[lfsr][0]_i_5_n_0\
    );
\reg[lfsr][0]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I2 => \reg[state]\(1),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[lfsr][0]_i_6_n_0\
    );
\reg[lfsr][0]_i_7__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BEEBEBBE"
    )
        port map (
      I0 => \reg[lfsr][0]_i_9_n_0\,
      I1 => lfsr_shift(22),
      I2 => \reg_reg[lfsr_n_0_][26]\,
      I3 => lfsr_shift(26),
      I4 => lfsr_shift(25),
      O => \reg[lfsr][0]_i_7__0_n_0\
    );
\reg[lfsr][0]_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => lfsr_shift(25),
      I1 => lfsr_shift(26),
      I2 => \reg_reg[lfsr_n_0_][26]\,
      I3 => lfsr_shift(22),
      O => \lfsr_shift__0\(0)
    );
\reg[lfsr][0]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => \reg[lfsr][26]_i_26_n_0\,
      I1 => \reg[mem_rd_en]_i_3_n_0\,
      I2 => lfsr_shift(17),
      I3 => \reg[lfsr][0]_i_10_n_0\,
      O => \reg[lfsr][0]_i_9_n_0\
    );
\reg[lfsr][10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(10),
      O => p_1_in(10)
    );
\reg[lfsr][11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(11),
      O => p_1_in(11)
    );
\reg[lfsr][12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(12),
      O => p_1_in(12)
    );
\reg[lfsr][13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(13),
      O => p_1_in(13)
    );
\reg[lfsr][14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(14),
      O => p_1_in(14)
    );
\reg[lfsr][15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(15),
      O => p_1_in(15)
    );
\reg[lfsr][16]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(16),
      O => p_1_in(16)
    );
\reg[lfsr][17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(17),
      O => p_1_in(17)
    );
\reg[lfsr][18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(18),
      O => p_1_in(18)
    );
\reg[lfsr][19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(19),
      O => p_1_in(19)
    );
\reg[lfsr][1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(1),
      O => p_1_in(1)
    );
\reg[lfsr][20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(20),
      O => p_1_in(20)
    );
\reg[lfsr][21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(21),
      O => p_1_in(21)
    );
\reg[lfsr][22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(22),
      O => p_1_in(22)
    );
\reg[lfsr][23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(23),
      O => p_1_in(23)
    );
\reg[lfsr][24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(24),
      O => p_1_in(24)
    );
\reg[lfsr][25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(25),
      O => p_1_in(25)
    );
\reg[lfsr][26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFA2"
    )
        port map (
      I0 => \reg[lfsr][26]_i_3_n_0\,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I2 => \reg[lfsr][26]_i_4_n_0\,
      I3 => \reg[lfsr][26]_i_5_n_0\,
      I4 => \reg[lfsr][26]_i_6_n_0\,
      I5 => \reg[lfsr][26]_i_7_n_0\,
      O => \reg[lfsr][26]_i_1_n_0\
    );
\reg[lfsr][26]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA8"
    )
        port map (
      I0 => \reg[lfsr][26]_i_20_n_0\,
      I1 => \reg[lfsr][26]_i_15_n_0\,
      I2 => \reg[lfsr][26]_i_21_n_0\,
      I3 => lfsr_shift(5),
      I4 => lfsr_shift(6),
      I5 => \reg_reg[lfsr_n_0_][26]\,
      O => \reg[lfsr][26]_i_10_n_0\
    );
\reg[lfsr][26]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888880"
    )
        port map (
      I0 => \reg[lfsr][26]_i_19_n_0\,
      I1 => \reg[bit_send]_i_2__0_n_0\,
      I2 => \reg[lfsr][26]_i_17_n_0\,
      I3 => \reg[lfsr][26]_i_22_n_0\,
      I4 => lfsr_shift(7),
      I5 => \reg[lfsr][26]_i_23_n_0\,
      O => \reg[lfsr][26]_i_11_n_0\
    );
\reg[lfsr][26]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => lfsr_shift(12),
      I1 => \reg[mem_rd_en]_i_3_n_0\,
      I2 => lfsr_shift(10),
      I3 => \reg[lfsr][26]_i_24_n_0\,
      I4 => \reg[lfsr][26]_i_25_n_0\,
      I5 => \reg_reg[lfsr_n_0_][26]\,
      O => \reg[lfsr][26]_i_12_n_0\
    );
\reg[lfsr][26]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => lfsr_shift(18),
      I1 => lfsr_shift(17),
      I2 => lfsr_shift(20),
      I3 => lfsr_shift(8),
      I4 => lfsr_shift(25),
      O => \reg[lfsr][26]_i_13_n_0\
    );
\reg[lfsr][26]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => lfsr_shift(22),
      I1 => lfsr_shift(19),
      I2 => lfsr_shift(21),
      I3 => lfsr_shift(26),
      I4 => lfsr_shift(23),
      O => \reg[lfsr][26]_i_14_n_0\
    );
\reg[lfsr][26]_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => lfsr_shift(4),
      I1 => lfsr_shift(14),
      I2 => lfsr_shift(24),
      I3 => \reg[lfsr][26]_i_14_n_0\,
      I4 => \reg[lfsr][26]_i_13_n_0\,
      O => \reg[lfsr][26]_i_15_n_0\
    );
\reg[lfsr][26]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004000000000000"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => \reg[state]\(3),
      I4 => \reg[state]\(1),
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[lfsr][26]_i_16_n_0\
    );
\reg[lfsr][26]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => lfsr_shift(18),
      I1 => lfsr_shift(17),
      I2 => lfsr_shift(8),
      I3 => lfsr_shift(6),
      I4 => lfsr_shift(5),
      I5 => \reg[lfsr][26]_i_26_n_0\,
      O => \reg[lfsr][26]_i_17_n_0\
    );
\reg[lfsr][26]_i_18\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => lfsr_shift(3),
      I1 => lfsr_shift(1),
      I2 => lfsr_shift(2),
      I3 => lfsr_shift(7),
      I4 => \reg[lfsr][26]_i_23_n_0\,
      O => \reg[lfsr][26]_i_18_n_0\
    );
\reg[lfsr][26]_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[state]\(1),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[lfsr][26]_i_19_n_0\
    );
\reg[lfsr][26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(26),
      O => p_1_in(26)
    );
\reg[lfsr][26]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000100010001000"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => ow_reg_rst,
      I2 => axi_aresetn_i,
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I4 => \reg[state]\(1),
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[lfsr][26]_i_20_n_0\
    );
\reg[lfsr][26]_i_21\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => lfsr_shift(7),
      I1 => lfsr_shift(9),
      I2 => lfsr_shift(10),
      I3 => \reg[mem_rd_en]_i_3_n_0\,
      I4 => lfsr_shift(12),
      O => \reg[lfsr][26]_i_21_n_0\
    );
\reg[lfsr][26]_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => lfsr_shift(3),
      I1 => lfsr_shift(1),
      I2 => lfsr_shift(2),
      O => \reg[lfsr][26]_i_22_n_0\
    );
\reg[lfsr][26]_i_23\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \reg[lfsr][26]_i_27_n_0\,
      I1 => lfsr_shift(24),
      I2 => \reg_reg[lfsr_n_0_][26]\,
      I3 => lfsr_shift(25),
      I4 => lfsr_shift(9),
      O => \reg[lfsr][26]_i_23_n_0\
    );
\reg[lfsr][26]_i_24\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => lfsr_shift(7),
      I1 => lfsr_shift(9),
      O => \reg[lfsr][26]_i_24_n_0\
    );
\reg[lfsr][26]_i_25\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => lfsr_shift(5),
      I1 => lfsr_shift(6),
      O => \reg[lfsr][26]_i_25_n_0\
    );
\reg[lfsr][26]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFBFFFFFFF"
    )
        port map (
      I0 => lfsr_shift(4),
      I1 => lfsr_shift(20),
      I2 => lfsr_shift(14),
      I3 => lfsr_shift(12),
      I4 => lfsr_shift(10),
      I5 => \reg[lfsr][26]_i_14_n_0\,
      O => \reg[lfsr][26]_i_26_n_0\
    );
\reg[lfsr][26]_i_27\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => lfsr_shift(16),
      I1 => lfsr_shift(13),
      I2 => lfsr_shift(15),
      I3 => lfsr_shift(11),
      O => \reg[lfsr][26]_i_27_n_0\
    );
\reg[lfsr][26]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => if_rx_data_en,
      I1 => \reg[state]\(3),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \reg[state]\(1),
      O => \reg[lfsr][26]_i_3_n_0\
    );
\reg[lfsr][26]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \reg[lfsr][26]_i_12_n_0\,
      I1 => \reg[lfsr][26]_i_13_n_0\,
      I2 => \reg[lfsr][26]_i_14_n_0\,
      I3 => lfsr_shift(24),
      I4 => lfsr_shift(14),
      I5 => lfsr_shift(4),
      O => \reg[lfsr][26]_i_4_n_0\
    );
\reg[lfsr][26]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => if_done,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[lfsr][26]_i_5_n_0\
    );
\reg[lfsr][26]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000002"
    )
        port map (
      I0 => ow_disc_trig_reg,
      I1 => \reg[state]\(3),
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I5 => ow_rst,
      O => \reg[lfsr][26]_i_6_n_0\
    );
\reg[lfsr][26]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1100C040"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[state]\(3),
      I2 => if_done,
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      O => \reg[lfsr][26]_i_7_n_0\
    );
\reg[lfsr][26]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF0E000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_12_n_0\,
      I1 => \reg[lfsr][26]_i_15_n_0\,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I5 => \reg[lfsr][26]_i_16_n_0\,
      O => \reg[lfsr][26]_i_8_n_0\
    );
\reg[lfsr][26]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0EFF0EFF0EFF0E0"
    )
        port map (
      I0 => \reg[lfsr][26]_i_17_n_0\,
      I1 => \reg[lfsr][26]_i_18_n_0\,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I3 => \reg[lfsr][26]_i_19_n_0\,
      I4 => \reg[lfsr][26]_i_15_n_0\,
      I5 => \reg[lfsr][26]_i_12_n_0\,
      O => \reg[lfsr][26]_i_9_n_0\
    );
\reg[lfsr][2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(2),
      O => p_1_in(2)
    );
\reg[lfsr][3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(3),
      O => p_1_in(3)
    );
\reg[lfsr][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(4),
      O => p_1_in(4)
    );
\reg[lfsr][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(5),
      O => p_1_in(5)
    );
\reg[lfsr][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(6),
      O => p_1_in(6)
    );
\reg[lfsr][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(7),
      O => p_1_in(7)
    );
\reg[lfsr][8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(8),
      O => p_1_in(8)
    );
\reg[lfsr][9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEA00000000"
    )
        port map (
      I0 => \reg[lfsr][26]_i_8_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I2 => \reg[lfsr][26]_i_9_n_0\,
      I3 => \reg[lfsr][26]_i_10_n_0\,
      I4 => \reg[lfsr][26]_i_11_n_0\,
      I5 => lfsr_shift(9),
      O => p_1_in(9)
    );
\reg[mem_addr][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000C000C0F020C00"
    )
        port map (
      I0 => p_0_in8_in,
      I1 => \reg[state]\(1),
      I2 => ow_rst,
      I3 => \reg[state]\(3),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I5 => \^reg_reg[mem_addr][0]_0\,
      O => \reg[mem_addr][0]_i_1_n_0\
    );
\reg[mem_addr][1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000444400008F88"
    )
        port map (
      I0 => \^reg_reg[mem_addr][0]_0\,
      I1 => \reg[mem_addr][1]_i_2_n_0\,
      I2 => \reg[state]\(3),
      I3 => \reg[mem_addr][1]_i_3_n_0\,
      I4 => \reg[mem_addr][5]_i_8_n_0\,
      I5 => \^reg_reg[mem_addr][1]_0\,
      O => \reg[mem_addr][1]_i_1_n_0\
    );
\reg[mem_addr][1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0F000200"
    )
        port map (
      I0 => p_0_in8_in,
      I1 => \reg[state]\(1),
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \reg[state]\(3),
      O => \reg[mem_addr][1]_i_2_n_0\
    );
\reg[mem_addr][1]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \reg[mem_addr][1]_i_3_n_0\
    );
\reg[mem_addr][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACA0ACA0ACA00000"
    )
        port map (
      I0 => \reg[mem_addr][2]_i_2_n_0\,
      I1 => \reg[mem_addr][2]_i_3_n_0\,
      I2 => \^reg_reg[mem_addr][2]_0\,
      I3 => \^reg_reg[mem_addr][1]_0\,
      I4 => \reg[state]\(1),
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[mem_addr][2]_i_1_n_0\
    );
\reg[mem_addr][2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000773300007770"
    )
        port map (
      I0 => \^reg_reg[mem_addr][0]_0\,
      I1 => \^reg_reg[mem_addr][1]_0\,
      I2 => p_0_in8_in,
      I3 => \reg[state]\(3),
      I4 => ow_rst,
      I5 => \reg[state]\(1),
      O => \reg[mem_addr][2]_i_2_n_0\
    );
\reg[mem_addr][2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AFA80000"
    )
        port map (
      I0 => \^reg_reg[mem_addr][0]_0\,
      I1 => p_0_in8_in,
      I2 => \reg[state]\(3),
      I3 => \reg[state]\(1),
      I4 => axi_aresetn_i,
      I5 => ow_reg_rst,
      O => \reg[mem_addr][2]_i_3_n_0\
    );
\reg[mem_addr][3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA0CCC0"
    )
        port map (
      I0 => \reg[mem_addr][3]_i_2_n_0\,
      I1 => \reg[mem_addr][5]_i_7_n_0\,
      I2 => \reg[state]\(1),
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => \^reg_reg[mem_addr][3]_0\,
      O => \reg[mem_addr][3]_i_1_n_0\
    );
\reg[mem_addr][3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FFFFFFF00222222"
    )
        port map (
      I0 => \reg[mem_addr][1]_i_3_n_0\,
      I1 => \reg[state]\(3),
      I2 => \^reg_reg[mem_addr][0]_0\,
      I3 => \^reg_reg[mem_addr][2]_0\,
      I4 => \^reg_reg[mem_addr][1]_0\,
      I5 => \reg[mem_addr][1]_i_2_n_0\,
      O => \reg[mem_addr][3]_i_2_n_0\
    );
\reg[mem_addr][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACA0ACA0ACA00000"
    )
        port map (
      I0 => \reg[mem_addr][4]_i_2_n_0\,
      I1 => \reg[mem_addr][5]_i_7_n_0\,
      I2 => \^reg_reg[mem_addr][4]_0\,
      I3 => \^reg_reg[mem_addr][3]_0\,
      I4 => \reg[state]\(1),
      I5 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[mem_addr][4]_i_1_n_0\
    );
\reg[mem_addr][4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00550054"
    )
        port map (
      I0 => \^reg_reg[mem_addr][3]_0\,
      I1 => p_0_in8_in,
      I2 => \reg[state]\(3),
      I3 => ow_rst,
      I4 => \reg[state]\(1),
      I5 => \reg[mem_addr][3]_i_2_n_0\,
      O => \reg[mem_addr][4]_i_2_n_0\
    );
\reg[mem_addr][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCFCFCFCFEFFFCFC"
    )
        port map (
      I0 => p_0_in8_in,
      I1 => \reg[mem_addr][5]_i_3_n_0\,
      I2 => \reg[mem_addr][5]_i_4_n_0\,
      I3 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I4 => \reg[state]\(3),
      I5 => \reg[mem_addr][5]_i_5_n_0\,
      O => \reg[mem_addr][5]_i_1_n_0\
    );
\reg[mem_addr][5]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00F000E0"
    )
        port map (
      I0 => p_0_in8_in,
      I1 => \reg[state]\(3),
      I2 => axi_aresetn_i,
      I3 => ow_reg_rst,
      I4 => \reg[state]\(1),
      O => \reg[mem_addr][5]_i_11_n_0\
    );
\reg[mem_addr][5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAEAAAAAA"
    )
        port map (
      I0 => \reg[mem_addr][5]_i_6_n_0\,
      I1 => \reg[mem_addr][5]_i_7_n_0\,
      I2 => \^reg_reg[mem_addr][5]_0\,
      I3 => \^reg_reg[mem_addr][3]_0\,
      I4 => \^reg_reg[mem_addr][4]_0\,
      I5 => \reg[mem_addr][5]_i_8_n_0\,
      O => \reg[mem_addr][5]_i_2_n_0\
    );
\reg[mem_addr][5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCEECCCCCCFCCC"
    )
        port map (
      I0 => dcvr_id_en,
      I1 => \reg[lfsr][26]_i_6_n_0\,
      I2 => ow_temp_trig_reg,
      I3 => \reg[mem_addr][5]_i_9_n_0\,
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => \reg[state]\(1),
      O => \reg[mem_addr][5]_i_3_n_0\
    );
\reg[mem_addr][5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8880008000800080"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[discover]_i_2_n_0\,
      I2 => mem_wr_done,
      I3 => \reg[state]\(3),
      I4 => \reg_reg[mem_addr][0]_1\,
      I5 => \FSM_sequential_reg_reg[state][2]_1\,
      O => \reg[mem_addr][5]_i_4_n_0\
    );
\reg[mem_addr][5]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I1 => \reg[state]\(1),
      O => \reg[mem_addr][5]_i_5_n_0\
    );
\reg[mem_addr][5]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444040044444400"
    )
        port map (
      I0 => \reg[mem_addr][5]_i_8_n_0\,
      I1 => \^reg_reg[mem_addr][5]_0\,
      I2 => \^reg_reg[mem_addr][3]_0\,
      I3 => \reg[mem_addr][5]_i_11_n_0\,
      I4 => \reg[mem_addr][3]_i_2_n_0\,
      I5 => \^reg_reg[mem_addr][4]_0\,
      O => \reg[mem_addr][5]_i_6_n_0\
    );
\reg[mem_addr][5]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F200000022000000"
    )
        port map (
      I0 => \reg[mem_addr][1]_i_3_n_0\,
      I1 => \reg[state]\(3),
      I2 => \^reg_reg[mem_addr][0]_0\,
      I3 => \^reg_reg[mem_addr][2]_0\,
      I4 => \^reg_reg[mem_addr][1]_0\,
      I5 => \reg[mem_addr][1]_i_2_n_0\,
      O => \reg[mem_addr][5]_i_7_n_0\
    );
\reg[mem_addr][5]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I1 => \reg[state]\(1),
      O => \reg[mem_addr][5]_i_8_n_0\
    );
\reg[mem_addr][5]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \reg[state]\(3),
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      O => \reg[mem_addr][5]_i_9_n_0\
    );
\reg[mem_rd_en]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => \FSM_sequential_reg[state][2]_i_4_n_0\,
      I1 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I2 => if_done,
      I3 => \reg[state]\(1),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I5 => \reg[mem_rd_en]_i_2_n_0\,
      O => \reg[mem_rd_en]\
    );
\reg[mem_rd_en]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000004"
    )
        port map (
      I0 => \reg[mem_rd_en]_i_3_n_0\,
      I1 => \reg[mem_rd_en]_i_4_n_0\,
      I2 => lfsr_shift(4),
      I3 => lfsr_shift(5),
      I4 => lfsr_shift(6),
      I5 => \reg[mem_rd_en]_i_5_n_0\,
      O => \reg[mem_rd_en]_i_2_n_0\
    );
\reg[mem_rd_en]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => lfsr_shift(11),
      I1 => lfsr_shift(15),
      I2 => lfsr_shift(13),
      I3 => lfsr_shift(16),
      I4 => \reg[lfsr][26]_i_22_n_0\,
      O => \reg[mem_rd_en]_i_3_n_0\
    );
\reg[mem_rd_en]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => lfsr_shift(10),
      I1 => lfsr_shift(12),
      I2 => lfsr_shift(14),
      I3 => lfsr_shift(17),
      I4 => lfsr_shift(9),
      I5 => lfsr_shift(7),
      O => \reg[mem_rd_en]_i_4_n_0\
    );
\reg[mem_rd_en]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => lfsr_shift(20),
      I1 => \reg[mem_rd_en]_i_6_n_0\,
      I2 => lfsr_shift(8),
      I3 => \reg[lfsr][26]_i_14_n_0\,
      O => \reg[mem_rd_en]_i_5_n_0\
    );
\reg[mem_rd_en]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => lfsr_shift(25),
      I1 => \reg_reg[lfsr_n_0_][26]\,
      I2 => lfsr_shift(24),
      I3 => lfsr_shift(18),
      O => \reg[mem_rd_en]_i_6_n_0\
    );
\reg[mem_wr_en]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEAAAAAA"
    )
        port map (
      I0 => \reg[mem_wr_en]_i_2_n_0\,
      I1 => \reg[data][7]_i_7_n_0\,
      I2 => \reg[state]\(3),
      I3 => mem_wr_done,
      I4 => p_0_in8_in,
      O => \reg[mem_wr_en]\
    );
\reg[mem_wr_en]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0300000000000002"
    )
        port map (
      I0 => ow_disc_trig_reg,
      I1 => ow_rst,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \reg[state]\(3),
      I4 => \^fsm_sequential_reg_reg[state][2]_0\(1),
      I5 => \reg[state]\(1),
      O => \reg[mem_wr_en]_i_2_n_0\
    );
\reg[strong_pullup]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"040404FF04040400"
    )
        port map (
      I0 => ow_rst,
      I1 => if_done,
      I2 => \FSM_sequential_reg[state][3]_i_5_n_0\,
      I3 => \reg[strong_pullup]_i_2_n_0\,
      I4 => \reg[strong_pullup]_i_3_n_0\,
      I5 => strong_pullup,
      O => \reg[strong_pullup]_i_1_n_0\
    );
\reg[strong_pullup]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3131003100310031"
    )
        port map (
      I0 => lfsr_shift(19),
      I1 => lfsr_shift(21),
      I2 => lfsr_shift(20),
      I3 => lfsr_shift(18),
      I4 => lfsr_shift(16),
      I5 => lfsr_shift(17),
      O => \reg[strong_pullup]_i_10_n_0\
    );
\reg[strong_pullup]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => lfsr_shift(5),
      I1 => lfsr_shift(4),
      I2 => lfsr_shift(6),
      O => \reg[strong_pullup]_i_11_n_0\
    );
\reg[strong_pullup]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00200000"
    )
        port map (
      I0 => \reg[mem_rd_en]_i_2_n_0\,
      I1 => \FSM_sequential_reg[state][3]_i_5_n_0\,
      I2 => \^fsm_sequential_reg_reg[state][2]_0\(0),
      I3 => \reg[state]\(3),
      I4 => if_done,
      I5 => \reg[lfsr][26]_i_6_n_0\,
      O => \reg[strong_pullup]_i_2_n_0\
    );
\reg[strong_pullup]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100000000000000"
    )
        port map (
      I0 => lfsr_shift(21),
      I1 => lfsr_shift(20),
      I2 => lfsr_shift(22),
      I3 => lfsr_shift(23),
      I4 => \reg[strong_pullup]_i_4_n_0\,
      I5 => \reg[strong_pullup]_i_5_n_0\,
      O => \reg[strong_pullup]_i_3_n_0\
    );
\reg[strong_pullup]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => \reg[mem_addr][5]_i_5_n_0\,
      I1 => \reg[mem_addr][5]_i_9_n_0\,
      I2 => lfsr_shift(24),
      I3 => lfsr_shift(25),
      I4 => lfsr_shift(26),
      I5 => \reg_reg[lfsr_n_0_][26]\,
      O => \reg[strong_pullup]_i_4_n_0\
    );
\reg[strong_pullup]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000000000000"
    )
        port map (
      I0 => \reg[strong_pullup]_i_6_n_0\,
      I1 => \reg[strong_pullup]_i_7_n_0\,
      I2 => \reg[strong_pullup]_i_8_n_0\,
      I3 => \FSM_sequential_reg[state][3]_i_19_n_0\,
      I4 => \reg[strong_pullup]_i_9_n_0\,
      I5 => \reg[strong_pullup]_i_10_n_0\,
      O => \reg[strong_pullup]_i_5_n_0\
    );
\reg[strong_pullup]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => lfsr_shift(11),
      I1 => lfsr_shift(12),
      I2 => lfsr_shift(15),
      I3 => lfsr_shift(14),
      I4 => lfsr_shift(18),
      I5 => lfsr_shift(17),
      O => \reg[strong_pullup]_i_6_n_0\
    );
\reg[strong_pullup]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000080"
    )
        port map (
      I0 => \reg[strong_pullup]_i_11_n_0\,
      I1 => lfsr_shift(3),
      I2 => lfsr_shift(9),
      I3 => lfsr_shift(1),
      I4 => lfsr_shift(2),
      O => \reg[strong_pullup]_i_7_n_0\
    );
\reg[strong_pullup]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => lfsr_shift(8),
      I1 => lfsr_shift(7),
      I2 => lfsr_shift(9),
      O => \reg[strong_pullup]_i_8_n_0\
    );
\reg[strong_pullup]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00005D005D005D00"
    )
        port map (
      I0 => lfsr_shift(15),
      I1 => lfsr_shift(13),
      I2 => lfsr_shift(14),
      I3 => lfsr_shift(12),
      I4 => lfsr_shift(11),
      I5 => lfsr_shift(10),
      O => \reg[strong_pullup]_i_9_n_0\
    );
\reg[too_many]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"404040FF40404000"
    )
        port map (
      I0 => ow_rst,
      I1 => \reg[state]\(1),
      I2 => dcvr_id_en,
      I3 => \reg[too_many]_i_2_n_0\,
      I4 => \reg[lfsr][26]_i_6_n_0\,
      I5 => \^reg_reg[too_many]_0\,
      O => \reg[too_many]_i_1_n_0\
    );
\reg[too_many]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000010000000000"
    )
        port map (
      I0 => \^reg_reg[device_count][2]_0\,
      I1 => \^reg_reg[device_count][1]_0\,
      I2 => \^reg_reg[device_count][0]_0\,
      I3 => \reg[too_many]_i_3_n_0\,
      I4 => \FSM_sequential_reg[state][2]_i_7__0_n_0\,
      I5 => \reg[mem_addr][5]_i_9_n_0\,
      O => \reg[too_many]_i_2_n_0\
    );
\reg[too_many]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => dcvr_id_en,
      I1 => \^ow_dev_count\(0),
      I2 => \^reg_reg[device_count][4]_0\,
      I3 => \^reg_reg[device_count][3]_0\,
      O => \reg[too_many]_i_3_n_0\
    );
\reg_reg[bit_recv]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[bit_recv]\,
      Q => ctrl_bit_recv,
      R => '0'
    );
\reg_reg[bit_send]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[bit_send]\,
      Q => \^ctrl_bit_send\,
      R => '0'
    );
\reg_reg[bus_rst]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[bus_rst]\,
      Q => ctrl_bus_rst,
      R => '0'
    );
\reg_reg[busy]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[busy]_i_1_n_0\,
      Q => \^reg_reg[busy]_0\,
      R => '0'
    );
\reg_reg[crc_reset]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[crc_reset]\,
      Q => \reg_reg[crc_reset_n_0_]\,
      R => '0'
    );
\reg_reg[data][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_0,
      D => \reg[data][0]_i_1_n_0\,
      Q => ctrl_bit_tx,
      R => '0'
    );
\reg_reg[data][10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][10]_i_1_n_0\,
      Q => \^ctrl_wr_data\(9),
      R => '0'
    );
\reg_reg[data][11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][11]_i_1_n_0\,
      Q => \^ctrl_wr_data\(10),
      R => '0'
    );
\reg_reg[data][12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][12]_i_1_n_0\,
      Q => \^ctrl_wr_data\(11),
      R => '0'
    );
\reg_reg[data][13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][13]_i_1_n_0\,
      Q => \^ctrl_wr_data\(12),
      R => '0'
    );
\reg_reg[data][14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][14]_i_1_n_0\,
      Q => \^ctrl_wr_data\(13),
      R => '0'
    );
\reg_reg[data][15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][15]_i_1_n_0\,
      Q => \^ctrl_wr_data\(14),
      R => '0'
    );
\reg_reg[data][16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][16]_i_1_n_0\,
      Q => \^ctrl_wr_data\(15),
      R => '0'
    );
\reg_reg[data][17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][17]_i_1_n_0\,
      Q => \^ctrl_wr_data\(16),
      R => '0'
    );
\reg_reg[data][18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][18]_i_1_n_0\,
      Q => \^ctrl_wr_data\(17),
      R => '0'
    );
\reg_reg[data][19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][19]_i_1_n_0\,
      Q => \^ctrl_wr_data\(18),
      R => '0'
    );
\reg_reg[data][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_0,
      D => \reg[data][1]_i_1_n_0\,
      Q => \^ctrl_wr_data\(0),
      R => '0'
    );
\reg_reg[data][20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][20]_i_1_n_0\,
      Q => \^ctrl_wr_data\(19),
      R => '0'
    );
\reg_reg[data][21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][21]_i_1_n_0\,
      Q => \^ctrl_wr_data\(20),
      R => '0'
    );
\reg_reg[data][22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][22]_i_1_n_0\,
      Q => \^ctrl_wr_data\(21),
      R => '0'
    );
\reg_reg[data][23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][23]_i_1_n_0\,
      Q => \^ctrl_wr_data\(22),
      R => '0'
    );
\reg_reg[data][24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][24]_i_1_n_0\,
      Q => \^ctrl_wr_data\(23),
      R => '0'
    );
\reg_reg[data][25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][25]_i_1_n_0\,
      Q => \^ctrl_wr_data\(24),
      R => '0'
    );
\reg_reg[data][26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][26]_i_1_n_0\,
      Q => \^ctrl_wr_data\(25),
      R => '0'
    );
\reg_reg[data][27]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][27]_i_1_n_0\,
      Q => \^ctrl_wr_data\(26),
      R => '0'
    );
\reg_reg[data][28]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][28]_i_1_n_0\,
      Q => \^ctrl_wr_data\(27),
      R => '0'
    );
\reg_reg[data][29]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][29]_i_1_n_0\,
      Q => \^ctrl_wr_data\(28),
      R => '0'
    );
\reg_reg[data][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_0,
      D => \reg[data][2]_i_1_n_0\,
      Q => \^ctrl_wr_data\(1),
      R => '0'
    );
\reg_reg[data][30]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][30]_i_1_n_0\,
      Q => \^ctrl_wr_data\(29),
      R => '0'
    );
\reg_reg[data][31]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][31]_i_1_n_0\,
      Q => \^ctrl_wr_data\(30),
      R => '0'
    );
\reg_reg[data][32]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][32]_i_1_n_0\,
      Q => \^ctrl_wr_data\(31),
      R => '0'
    );
\reg_reg[data][33]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][33]_i_1_n_0\,
      Q => \^ctrl_wr_data\(32),
      R => '0'
    );
\reg_reg[data][34]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][34]_i_1_n_0\,
      Q => \^ctrl_wr_data\(33),
      R => '0'
    );
\reg_reg[data][35]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][35]_i_1_n_0\,
      Q => \^ctrl_wr_data\(34),
      R => '0'
    );
\reg_reg[data][36]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][36]_i_1_n_0\,
      Q => \^ctrl_wr_data\(35),
      R => '0'
    );
\reg_reg[data][37]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][37]_i_1_n_0\,
      Q => \^ctrl_wr_data\(36),
      R => '0'
    );
\reg_reg[data][38]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][38]_i_1_n_0\,
      Q => \^ctrl_wr_data\(37),
      R => '0'
    );
\reg_reg[data][39]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][39]_i_1_n_0\,
      Q => \^ctrl_wr_data\(38),
      R => '0'
    );
\reg_reg[data][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_0,
      D => \reg[data][3]_i_1_n_0\,
      Q => \^ctrl_wr_data\(2),
      R => '0'
    );
\reg_reg[data][40]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][40]_i_1_n_0\,
      Q => \^ctrl_wr_data\(39),
      R => '0'
    );
\reg_reg[data][41]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][41]_i_1_n_0\,
      Q => \^ctrl_wr_data\(40),
      R => '0'
    );
\reg_reg[data][42]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][42]_i_1_n_0\,
      Q => \^ctrl_wr_data\(41),
      R => '0'
    );
\reg_reg[data][43]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][43]_i_1_n_0\,
      Q => \^ctrl_wr_data\(42),
      R => '0'
    );
\reg_reg[data][44]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][44]_i_1_n_0\,
      Q => \^ctrl_wr_data\(43),
      R => '0'
    );
\reg_reg[data][45]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][45]_i_1_n_0\,
      Q => \^ctrl_wr_data\(44),
      R => '0'
    );
\reg_reg[data][46]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][46]_i_1_n_0\,
      Q => \^ctrl_wr_data\(45),
      R => '0'
    );
\reg_reg[data][47]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][47]_i_1_n_0\,
      Q => \^ctrl_wr_data\(46),
      R => '0'
    );
\reg_reg[data][48]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][48]_i_1_n_0\,
      Q => \^ctrl_wr_data\(47),
      R => '0'
    );
\reg_reg[data][49]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][49]_i_1_n_0\,
      Q => \^ctrl_wr_data\(48),
      R => '0'
    );
\reg_reg[data][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_0,
      D => \reg[data][4]_i_1_n_0\,
      Q => \^ctrl_wr_data\(3),
      R => '0'
    );
\reg_reg[data][50]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][50]_i_1_n_0\,
      Q => \^ctrl_wr_data\(49),
      R => '0'
    );
\reg_reg[data][51]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][51]_i_1_n_0\,
      Q => \^ctrl_wr_data\(50),
      R => '0'
    );
\reg_reg[data][52]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][52]_i_1_n_0\,
      Q => \^ctrl_wr_data\(51),
      R => '0'
    );
\reg_reg[data][53]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][53]_i_1_n_0\,
      Q => \^ctrl_wr_data\(52),
      R => '0'
    );
\reg_reg[data][54]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][54]_i_1_n_0\,
      Q => \^ctrl_wr_data\(53),
      R => '0'
    );
\reg_reg[data][55]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][55]_i_1_n_0\,
      Q => \^ctrl_wr_data\(54),
      R => '0'
    );
\reg_reg[data][56]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][56]_i_1_n_0\,
      Q => \^ctrl_wr_data\(55),
      R => '0'
    );
\reg_reg[data][57]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][57]_i_1_n_0\,
      Q => \^ctrl_wr_data\(56),
      R => '0'
    );
\reg_reg[data][58]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][58]_i_1_n_0\,
      Q => \^ctrl_wr_data\(57),
      R => '0'
    );
\reg_reg[data][59]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][59]_i_1_n_0\,
      Q => \^ctrl_wr_data\(58),
      R => '0'
    );
\reg_reg[data][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_0,
      D => \reg[data][5]_i_1_n_0\,
      Q => \^ctrl_wr_data\(4),
      R => '0'
    );
\reg_reg[data][60]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][60]_i_1_n_0\,
      Q => \^ctrl_wr_data\(59),
      R => '0'
    );
\reg_reg[data][61]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][61]_i_1_n_0\,
      Q => \^ctrl_wr_data\(60),
      R => '0'
    );
\reg_reg[data][62]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][62]_i_1_n_0\,
      Q => \^ctrl_wr_data\(61),
      R => '0'
    );
\reg_reg[data][63]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][63]_i_2_n_0\,
      Q => \^ctrl_wr_data\(62),
      R => '0'
    );
\reg_reg[data][6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_0,
      D => \reg[data][6]_i_1_n_0\,
      Q => \^ctrl_wr_data\(5),
      R => '0'
    );
\reg_reg[data][7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_0,
      D => \reg[data][7]_i_2_n_0\,
      Q => \^ctrl_wr_data\(6),
      R => '0'
    );
\reg_reg[data][8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][8]_i_1_n_0\,
      Q => \^ctrl_wr_data\(7),
      R => '0'
    );
\reg_reg[data][9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => crc_inst_n_1,
      D => \reg[data][9]_i_1_n_0\,
      Q => \^ctrl_wr_data\(8),
      R => '0'
    );
\reg_reg[device_count][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[device_count][5]_i_1_n_0\,
      D => \reg[device_count][0]_i_1_n_0\,
      Q => \^reg_reg[device_count][0]_0\,
      R => '0'
    );
\reg_reg[device_count][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[device_count][5]_i_1_n_0\,
      D => \reg[device_count][1]_i_1_n_0\,
      Q => \^reg_reg[device_count][1]_0\,
      R => '0'
    );
\reg_reg[device_count][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[device_count][5]_i_1_n_0\,
      D => \reg[device_count][2]_i_1_n_0\,
      Q => \^reg_reg[device_count][2]_0\,
      R => '0'
    );
\reg_reg[device_count][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[device_count][5]_i_1_n_0\,
      D => \reg[device_count][3]_i_1_n_0\,
      Q => \^reg_reg[device_count][3]_0\,
      R => '0'
    );
\reg_reg[device_count][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[device_count][5]_i_1_n_0\,
      D => \reg[device_count][4]_i_1_n_0\,
      Q => \^reg_reg[device_count][4]_0\,
      R => '0'
    );
\reg_reg[device_count][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[device_count][5]_i_1_n_0\,
      D => \reg[device_count][5]_i_2_n_0\,
      Q => \^ow_dev_count\(0),
      R => '0'
    );
\reg_reg[discover]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[discover]\,
      Q => dcvr_start,
      R => '0'
    );
\reg_reg[done]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[done]\,
      Q => ow_done,
      R => '0'
    );
\reg_reg[lfsr][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(0),
      Q => lfsr_shift(1),
      R => '0'
    );
\reg_reg[lfsr][10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(10),
      Q => lfsr_shift(11),
      R => '0'
    );
\reg_reg[lfsr][11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(11),
      Q => lfsr_shift(12),
      R => '0'
    );
\reg_reg[lfsr][12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(12),
      Q => lfsr_shift(13),
      R => '0'
    );
\reg_reg[lfsr][13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(13),
      Q => lfsr_shift(14),
      R => '0'
    );
\reg_reg[lfsr][14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(14),
      Q => lfsr_shift(15),
      R => '0'
    );
\reg_reg[lfsr][15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(15),
      Q => lfsr_shift(16),
      R => '0'
    );
\reg_reg[lfsr][16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(16),
      Q => lfsr_shift(17),
      R => '0'
    );
\reg_reg[lfsr][17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(17),
      Q => lfsr_shift(18),
      R => '0'
    );
\reg_reg[lfsr][18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(18),
      Q => lfsr_shift(19),
      R => '0'
    );
\reg_reg[lfsr][19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(19),
      Q => lfsr_shift(20),
      R => '0'
    );
\reg_reg[lfsr][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(1),
      Q => lfsr_shift(2),
      R => '0'
    );
\reg_reg[lfsr][20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(20),
      Q => lfsr_shift(21),
      R => '0'
    );
\reg_reg[lfsr][21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(21),
      Q => lfsr_shift(22),
      R => '0'
    );
\reg_reg[lfsr][22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(22),
      Q => lfsr_shift(23),
      R => '0'
    );
\reg_reg[lfsr][23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(23),
      Q => lfsr_shift(24),
      R => '0'
    );
\reg_reg[lfsr][24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(24),
      Q => lfsr_shift(25),
      R => '0'
    );
\reg_reg[lfsr][25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(25),
      Q => lfsr_shift(26),
      R => '0'
    );
\reg_reg[lfsr][26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(26),
      Q => \reg_reg[lfsr_n_0_][26]\,
      R => '0'
    );
\reg_reg[lfsr][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(2),
      Q => lfsr_shift(3),
      R => '0'
    );
\reg_reg[lfsr][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(3),
      Q => lfsr_shift(4),
      R => '0'
    );
\reg_reg[lfsr][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(4),
      Q => lfsr_shift(5),
      R => '0'
    );
\reg_reg[lfsr][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(5),
      Q => lfsr_shift(6),
      R => '0'
    );
\reg_reg[lfsr][6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(6),
      Q => lfsr_shift(7),
      R => '0'
    );
\reg_reg[lfsr][7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(7),
      Q => lfsr_shift(8),
      R => '0'
    );
\reg_reg[lfsr][8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(8),
      Q => lfsr_shift(9),
      R => '0'
    );
\reg_reg[lfsr][9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][26]_i_1_n_0\,
      D => p_1_in(9),
      Q => lfsr_shift(10),
      R => '0'
    );
\reg_reg[mem_addr][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[mem_addr][5]_i_1_n_0\,
      D => \reg[mem_addr][0]_i_1_n_0\,
      Q => \^reg_reg[mem_addr][0]_0\,
      R => '0'
    );
\reg_reg[mem_addr][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[mem_addr][5]_i_1_n_0\,
      D => \reg[mem_addr][1]_i_1_n_0\,
      Q => \^reg_reg[mem_addr][1]_0\,
      R => '0'
    );
\reg_reg[mem_addr][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[mem_addr][5]_i_1_n_0\,
      D => \reg[mem_addr][2]_i_1_n_0\,
      Q => \^reg_reg[mem_addr][2]_0\,
      R => '0'
    );
\reg_reg[mem_addr][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[mem_addr][5]_i_1_n_0\,
      D => \reg[mem_addr][3]_i_1_n_0\,
      Q => \^reg_reg[mem_addr][3]_0\,
      R => '0'
    );
\reg_reg[mem_addr][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[mem_addr][5]_i_1_n_0\,
      D => \reg[mem_addr][4]_i_1_n_0\,
      Q => \^reg_reg[mem_addr][4]_0\,
      R => '0'
    );
\reg_reg[mem_addr][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[mem_addr][5]_i_1_n_0\,
      D => \reg[mem_addr][5]_i_2_n_0\,
      Q => \^reg_reg[mem_addr][5]_0\,
      R => '0'
    );
\reg_reg[mem_rd_en]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[mem_rd_en]\,
      Q => ctrl_rd_en,
      R => '0'
    );
\reg_reg[mem_wr_en]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[mem_wr_en]\,
      Q => ctrl_wr_en,
      R => '0'
    );
\reg_reg[strong_pullup]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[strong_pullup]_i_1_n_0\,
      Q => strong_pullup,
      R => '0'
    );
\reg_reg[too_many]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[too_many]_i_1_n_0\,
      Q => \^reg_reg[too_many]_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_onewire_discover is
  port (
    dcvr_id_en : out STD_LOGIC;
    dcvr_done : out STD_LOGIC;
    dcvr_bus_rst : out STD_LOGIC;
    dcvr_bit_send : out STD_LOGIC;
    dcvr_bit_tx : out STD_LOGIC;
    dcvr_bit_recv : out STD_LOGIC;
    \reg_reg[bus_rst]_0\ : out STD_LOGIC;
    \reg_reg[bit_send]_0\ : out STD_LOGIC;
    wr_data_i : out STD_LOGIC_VECTOR ( 63 downto 0 );
    axi_clk_i : in STD_LOGIC;
    if_done : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    ow_reg_rst : in STD_LOGIC;
    ow_rst : in STD_LOGIC;
    if_rx_data : in STD_LOGIC;
    if_rx_data_en : in STD_LOGIC;
    dcvr_start : in STD_LOGIC;
    ctrl_bus_rst : in STD_LOGIC;
    ctrl_bit_send : in STD_LOGIC;
    ctrl_wr_data : in STD_LOGIC_VECTOR ( 62 downto 0 );
    ctrl_bit_tx : in STD_LOGIC
  );
end design_1_axi_onewire_0_0_onewire_discover;

architecture STRUCTURE of design_1_axi_onewire_0_0_onewire_discover is
  signal \FSM_sequential_reg[state][0]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][0]_i_2__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][0]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][1]_i_3__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_4__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_6__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_reg[state][2]_i_8_n_0\ : STD_LOGIC;
  signal crc_inst_n_0 : STD_LOGIC;
  signal crc_inst_n_1 : STD_LOGIC;
  signal crc_inst_n_10 : STD_LOGIC;
  signal crc_inst_n_11 : STD_LOGIC;
  signal crc_inst_n_12 : STD_LOGIC;
  signal crc_inst_n_13 : STD_LOGIC;
  signal crc_inst_n_14 : STD_LOGIC;
  signal crc_inst_n_15 : STD_LOGIC;
  signal crc_inst_n_16 : STD_LOGIC;
  signal crc_inst_n_17 : STD_LOGIC;
  signal crc_inst_n_18 : STD_LOGIC;
  signal crc_inst_n_19 : STD_LOGIC;
  signal crc_inst_n_2 : STD_LOGIC;
  signal crc_inst_n_20 : STD_LOGIC;
  signal crc_inst_n_21 : STD_LOGIC;
  signal crc_inst_n_22 : STD_LOGIC;
  signal crc_inst_n_23 : STD_LOGIC;
  signal crc_inst_n_24 : STD_LOGIC;
  signal crc_inst_n_25 : STD_LOGIC;
  signal crc_inst_n_26 : STD_LOGIC;
  signal crc_inst_n_27 : STD_LOGIC;
  signal crc_inst_n_28 : STD_LOGIC;
  signal crc_inst_n_29 : STD_LOGIC;
  signal crc_inst_n_3 : STD_LOGIC;
  signal crc_inst_n_30 : STD_LOGIC;
  signal crc_inst_n_31 : STD_LOGIC;
  signal crc_inst_n_32 : STD_LOGIC;
  signal crc_inst_n_33 : STD_LOGIC;
  signal crc_inst_n_34 : STD_LOGIC;
  signal crc_inst_n_35 : STD_LOGIC;
  signal crc_inst_n_36 : STD_LOGIC;
  signal crc_inst_n_37 : STD_LOGIC;
  signal crc_inst_n_38 : STD_LOGIC;
  signal crc_inst_n_39 : STD_LOGIC;
  signal crc_inst_n_4 : STD_LOGIC;
  signal crc_inst_n_40 : STD_LOGIC;
  signal crc_inst_n_41 : STD_LOGIC;
  signal crc_inst_n_42 : STD_LOGIC;
  signal crc_inst_n_43 : STD_LOGIC;
  signal crc_inst_n_44 : STD_LOGIC;
  signal crc_inst_n_45 : STD_LOGIC;
  signal crc_inst_n_46 : STD_LOGIC;
  signal crc_inst_n_47 : STD_LOGIC;
  signal crc_inst_n_48 : STD_LOGIC;
  signal crc_inst_n_49 : STD_LOGIC;
  signal crc_inst_n_5 : STD_LOGIC;
  signal crc_inst_n_50 : STD_LOGIC;
  signal crc_inst_n_51 : STD_LOGIC;
  signal crc_inst_n_52 : STD_LOGIC;
  signal crc_inst_n_53 : STD_LOGIC;
  signal crc_inst_n_54 : STD_LOGIC;
  signal crc_inst_n_55 : STD_LOGIC;
  signal crc_inst_n_56 : STD_LOGIC;
  signal crc_inst_n_57 : STD_LOGIC;
  signal crc_inst_n_58 : STD_LOGIC;
  signal crc_inst_n_59 : STD_LOGIC;
  signal crc_inst_n_6 : STD_LOGIC;
  signal crc_inst_n_60 : STD_LOGIC;
  signal crc_inst_n_61 : STD_LOGIC;
  signal crc_inst_n_62 : STD_LOGIC;
  signal crc_inst_n_63 : STD_LOGIC;
  signal crc_inst_n_7 : STD_LOGIC;
  signal crc_inst_n_8 : STD_LOGIC;
  signal crc_inst_n_9 : STD_LOGIC;
  signal \^dcvr_bit_send\ : STD_LOGIC;
  signal \^dcvr_bit_tx\ : STD_LOGIC;
  signal \^dcvr_bus_rst\ : STD_LOGIC;
  signal dcvr_id : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal \^dcvr_id_en\ : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_6_n_0\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal in30 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal lfsr : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \nxt_reg[bit_send]19_out\ : STD_LOGIC;
  signal \nxt_reg[id]1__4\ : STD_LOGIC;
  signal \nxt_reg[id]1_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \nxt_reg[id]1_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \nxt_reg[id]1_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \nxt_reg[id]1_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \nxt_reg[state]125_out\ : STD_LOGIC;
  signal \reg[bit_recv]\ : STD_LOGIC;
  signal \reg[bit_recv]_i_2_n_0\ : STD_LOGIC;
  signal \reg[bit_send]\ : STD_LOGIC;
  signal \reg[bit_send]_i_2_n_0\ : STD_LOGIC;
  signal \reg[bit_send]_i_3_n_0\ : STD_LOGIC;
  signal \reg[bus_rst]\ : STD_LOGIC;
  signal \reg[bus_rst]_i_2_n_0\ : STD_LOGIC;
  signal \reg[cmd][0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[cmd][1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[cmd][2]_i_1_n_0\ : STD_LOGIC;
  signal \reg[cmd][3]_i_1_n_0\ : STD_LOGIC;
  signal \reg[cmd][4]_i_1_n_0\ : STD_LOGIC;
  signal \reg[cmd][5]_i_1_n_0\ : STD_LOGIC;
  signal \reg[cmd][6]_i_1_n_0\ : STD_LOGIC;
  signal \reg[cmd][7]_i_1_n_0\ : STD_LOGIC;
  signal \reg[cmd][7]_i_2_n_0\ : STD_LOGIC;
  signal \reg[cmd][7]_i_3_n_0\ : STD_LOGIC;
  signal \reg[cmp_id_bit]_i_1_n_0\ : STD_LOGIC;
  signal \reg[cmp_id_bit]_i_2_n_0\ : STD_LOGIC;
  signal \reg[cmp_id_bit]_i_3_n_0\ : STD_LOGIC;
  signal \reg[crc_reset]\ : STD_LOGIC;
  signal \reg[crc_reset]_i_2_n_0\ : STD_LOGIC;
  signal \reg[done]\ : STD_LOGIC;
  signal \reg[done]_i_2__0_n_0\ : STD_LOGIC;
  signal \reg[id][32]_i_3_n_0\ : STD_LOGIC;
  signal \reg[id][40]_i_3_n_0\ : STD_LOGIC;
  signal \reg[id][48]_i_3_n_0\ : STD_LOGIC;
  signal \reg[id][56]_i_3_n_0\ : STD_LOGIC;
  signal \reg[id][56]_i_4_n_0\ : STD_LOGIC;
  signal \reg[id][57]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][58]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][59]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][61]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][62]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][63]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_10_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_3_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_6_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_8_n_0\ : STD_LOGIC;
  signal \reg[id][64]_i_9_n_0\ : STD_LOGIC;
  signal \reg[id_bit]_i_1_n_0\ : STD_LOGIC;
  signal \reg[id_bit]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][2]_i_1_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][3]_i_1_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][3]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][4]_i_1_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][5]_i_1_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][6]_i_1_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][6]_i_2_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][6]_i_3_n_0\ : STD_LOGIC;
  signal \reg[id_bit_number][6]_i_4_n_0\ : STD_LOGIC;
  signal \reg[id_en]\ : STD_LOGIC;
  signal \reg[last_discrepancy]\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \reg[last_discrepancy][0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[last_discrepancy][1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[last_discrepancy][2]_i_1_n_0\ : STD_LOGIC;
  signal \reg[last_discrepancy][3]_i_1_n_0\ : STD_LOGIC;
  signal \reg[last_discrepancy][4]_i_1_n_0\ : STD_LOGIC;
  signal \reg[last_discrepancy][5]_i_1_n_0\ : STD_LOGIC;
  signal \reg[last_discrepancy][6]_i_1_n_0\ : STD_LOGIC;
  signal \reg[last_discrepancy][6]_i_2_n_0\ : STD_LOGIC;
  signal \reg[last_discrepancy][6]_i_3_n_0\ : STD_LOGIC;
  signal \reg[lfsr][0]_i_1__0_n_0\ : STD_LOGIC;
  signal \reg[lfsr][1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[lfsr][2]_i_1_n_0\ : STD_LOGIC;
  signal \reg[lfsr][3]_i_1_n_0\ : STD_LOGIC;
  signal \reg[lfsr][3]_i_2_n_0\ : STD_LOGIC;
  signal \reg[lfsr][3]_i_3_n_0\ : STD_LOGIC;
  signal \reg[lfsr][3]_i_5_n_0\ : STD_LOGIC;
  signal \reg[marker][0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[marker][1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[marker][2]_i_1_n_0\ : STD_LOGIC;
  signal \reg[marker][3]_i_1_n_0\ : STD_LOGIC;
  signal \reg[marker][4]_i_1_n_0\ : STD_LOGIC;
  signal \reg[marker][5]_i_1_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_10_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_11_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_12_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_13_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_14_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_15_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_16_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_17_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_18_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_19_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_1_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_20_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_21_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_22_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_23_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_24_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_25_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_26_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_27_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_28_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_29_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_2_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_30_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_31_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_32_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_33_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_34_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_35_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_36_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_37_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_38_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_39_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_3_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_40_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_41_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_42_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_43_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_44_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_45_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_4_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_5_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_6_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_7_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_8_n_0\ : STD_LOGIC;
  signal \reg[marker][6]_i_9_n_0\ : STD_LOGIC;
  signal \reg[search]_i_1_n_0\ : STD_LOGIC;
  signal \reg[search]_i_2_n_0\ : STD_LOGIC;
  signal \reg[search]_i_4_n_0\ : STD_LOGIC;
  signal \reg[search]_i_5_n_0\ : STD_LOGIC;
  signal \reg[state]\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \reg_reg[cmd_n_0_][0]\ : STD_LOGIC;
  signal \reg_reg[cmp_id_bit_n_0_]\ : STD_LOGIC;
  signal \reg_reg[crc_reset_n_0_]\ : STD_LOGIC;
  signal \reg_reg[id_bit_n_0_]\ : STD_LOGIC;
  signal \reg_reg[id_bit_number_n_0_][0]\ : STD_LOGIC;
  signal \reg_reg[id_bit_number_n_0_][1]\ : STD_LOGIC;
  signal \reg_reg[id_bit_number_n_0_][2]\ : STD_LOGIC;
  signal \reg_reg[id_bit_number_n_0_][3]\ : STD_LOGIC;
  signal \reg_reg[id_bit_number_n_0_][4]\ : STD_LOGIC;
  signal \reg_reg[id_bit_number_n_0_][5]\ : STD_LOGIC;
  signal \reg_reg[id_bit_number_n_0_][6]\ : STD_LOGIC;
  signal \reg_reg[marker_n_0_][0]\ : STD_LOGIC;
  signal \reg_reg[marker_n_0_][1]\ : STD_LOGIC;
  signal \reg_reg[marker_n_0_][2]\ : STD_LOGIC;
  signal \reg_reg[marker_n_0_][3]\ : STD_LOGIC;
  signal \reg_reg[marker_n_0_][4]\ : STD_LOGIC;
  signal \reg_reg[marker_n_0_][5]\ : STD_LOGIC;
  signal \reg_reg[marker_n_0_][6]\ : STD_LOGIC;
  signal \NLW_nxt_reg[id]1_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][0]_i_2__0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][1]_i_2\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][2]_i_5\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][2]_i_6__0\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \FSM_sequential_reg[state][2]_i_7\ : label is "soft_lutpair48";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_reg_reg[state][0]\ : label is "read_cmp_id_bit:100,compare:101,search_command:010,check:110,read_id_bit:011,reset_done:001,idle:000";
  attribute FSM_ENCODED_STATES of \FSM_sequential_reg_reg[state][1]\ : label is "read_cmp_id_bit:100,compare:101,search_command:010,check:110,read_id_bit:011,reset_done:001,idle:000";
  attribute FSM_ENCODED_STATES of \FSM_sequential_reg_reg[state][2]\ : label is "read_cmp_id_bit:100,compare:101,search_command:010,check:110,read_id_bit:011,reset_done:001,idle:000";
  attribute COMPARATOR_THRESHOLD : integer;
  attribute COMPARATOR_THRESHOLD of \nxt_reg[id]1_inferred__0/i__carry\ : label is 11;
  attribute SOFT_HLUTNM of \reg[bit_send]_i_3\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \reg[cmd][7]_i_3\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \reg[crc_reset]_i_2\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \reg[done]_i_2__0\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \reg[id][40]_i_3\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \reg[id][48]_i_3\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \reg[id][56]_i_4\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \reg[id][57]_i_2\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \reg[id][58]_i_2\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \reg[id][59]_i_2\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \reg[id][61]_i_2\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \reg[id][62]_i_2\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \reg[id][63]_i_2\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \reg[id][64]_i_10\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \reg[id][64]_i_2\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \reg[id][64]_i_3\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \reg[id][64]_i_9\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \reg[id_bit_number][3]_i_2\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \reg[id_bit_number][6]_i_5\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \reg[last_discrepancy][0]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \reg[last_discrepancy][1]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \reg[last_discrepancy][2]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \reg[last_discrepancy][3]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \reg[last_discrepancy][4]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \reg[last_discrepancy][5]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \reg[last_discrepancy][6]_i_2\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \reg[last_discrepancy][6]_i_3\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \reg[lfsr][1]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \reg[lfsr][2]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \reg[lfsr][3]_i_2\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \reg[lfsr][3]_i_4\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \reg[marker][6]_i_10\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \reg[marker][6]_i_14\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \reg[marker][6]_i_18\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \reg[marker][6]_i_22\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \reg[marker][6]_i_30\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \reg[marker][6]_i_31\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \reg[marker][6]_i_4\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \reg[search]_i_3\ : label is "soft_lutpair61";
begin
  dcvr_bit_send <= \^dcvr_bit_send\;
  dcvr_bit_tx <= \^dcvr_bit_tx\;
  dcvr_bus_rst <= \^dcvr_bus_rst\;
  dcvr_id_en <= \^dcvr_id_en\;
\FSM_sequential_reg[state][0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF0F0D0000"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][6]\,
      I1 => \FSM_sequential_reg[state][0]_i_2__0_n_0\,
      I2 => ow_rst,
      I3 => \FSM_sequential_reg[state][0]_i_3_n_0\,
      I4 => \FSM_sequential_reg[state][2]_i_3_n_0\,
      I5 => \reg[state]\(0),
      O => \FSM_sequential_reg[state][0]_i_1__0_n_0\
    );
\FSM_sequential_reg[state][0]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \reg[state]\(2),
      I1 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][0]_i_2__0_n_0\
    );
\FSM_sequential_reg[state][0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \FSM_sequential_reg[state][0]_i_4_n_0\,
      I1 => \reg_reg[marker_n_0_][5]\,
      I2 => \reg_reg[marker_n_0_][3]\,
      I3 => \reg_reg[marker_n_0_][2]\,
      O => \FSM_sequential_reg[state][0]_i_3_n_0\
    );
\FSM_sequential_reg[state][0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \reg_reg[marker_n_0_][4]\,
      I1 => \reg_reg[marker_n_0_][6]\,
      I2 => \reg_reg[marker_n_0_][0]\,
      I3 => \reg_reg[marker_n_0_][1]\,
      O => \FSM_sequential_reg[state][0]_i_4_n_0\
    );
\FSM_sequential_reg[state][1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8FFFFFF880000"
    )
        port map (
      I0 => \FSM_sequential_reg[state][2]_i_2_n_0\,
      I1 => \reg[state]\(0),
      I2 => \FSM_sequential_reg[state][1]_i_2_n_0\,
      I3 => \FSM_sequential_reg[state][1]_i_3__0_n_0\,
      I4 => \FSM_sequential_reg[state][2]_i_3_n_0\,
      I5 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][1]_i_1__0_n_0\
    );
\FSM_sequential_reg[state][1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(0),
      I3 => \reg_reg[id_bit_number_n_0_][6]\,
      O => \FSM_sequential_reg[state][1]_i_2_n_0\
    );
\FSM_sequential_reg[state][1]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000040000040400"
    )
        port map (
      I0 => \reg[state]\(2),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      I3 => \reg[state]\(1),
      I4 => \reg[state]\(0),
      I5 => if_rx_data,
      O => \FSM_sequential_reg[state][1]_i_3__0_n_0\
    );
\FSM_sequential_reg[state][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAABFFFFBAAA0000"
    )
        port map (
      I0 => \FSM_sequential_reg[state][2]_i_2_n_0\,
      I1 => ow_rst,
      I2 => \reg[state]\(0),
      I3 => \reg[state]\(1),
      I4 => \FSM_sequential_reg[state][2]_i_3_n_0\,
      I5 => \reg[state]\(2),
      O => \FSM_sequential_reg[state][2]_i_1_n_0\
    );
\FSM_sequential_reg[state][2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000004440000"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg_reg[id_bit_n_0_]\,
      I3 => \reg_reg[cmp_id_bit_n_0_]\,
      I4 => \reg[state]\(2),
      I5 => \reg[state]\(1),
      O => \FSM_sequential_reg[state][2]_i_2_n_0\
    );
\FSM_sequential_reg[state][2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBBFFBAFFBA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][2]_i_4__0_n_0\,
      I1 => \FSM_sequential_reg[state][2]_i_5_n_0\,
      I2 => if_rx_data_en,
      I3 => ow_rst,
      I4 => \FSM_sequential_reg[state][2]_i_6__0_n_0\,
      I5 => \FSM_sequential_reg[state][2]_i_7_n_0\,
      O => \FSM_sequential_reg[state][2]_i_3_n_0\
    );
\FSM_sequential_reg[state][2]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAEAAAAFEAEAAAAA"
    )
        port map (
      I0 => \FSM_sequential_reg[state][2]_i_8_n_0\,
      I1 => if_done,
      I2 => \reg[state]\(2),
      I3 => \reg[state]\(0),
      I4 => \reg[state]\(1),
      I5 => dcvr_start,
      O => \FSM_sequential_reg[state][2]_i_4__0_n_0\
    );
\FSM_sequential_reg[state][2]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      O => \FSM_sequential_reg[state][2]_i_5_n_0\
    );
\FSM_sequential_reg[state][2]_i_6__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(0),
      I2 => if_done,
      O => \FSM_sequential_reg[state][2]_i_6__0_n_0\
    );
\FSM_sequential_reg[state][2]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(0),
      I2 => \reg[state]\(2),
      O => \FSM_sequential_reg[state][2]_i_7_n_0\
    );
\FSM_sequential_reg[state][2]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00800080008C0080"
    )
        port map (
      I0 => if_rx_data_en,
      I1 => \reg[state]\(1),
      I2 => \reg[state]\(0),
      I3 => \reg[state]\(2),
      I4 => if_done,
      I5 => \reg[crc_reset]_i_2_n_0\,
      O => \FSM_sequential_reg[state][2]_i_8_n_0\
    );
\FSM_sequential_reg_reg[state][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => '1',
      D => \FSM_sequential_reg[state][0]_i_1__0_n_0\,
      Q => \reg[state]\(0),
      R => '0'
    );
\FSM_sequential_reg_reg[state][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => '1',
      D => \FSM_sequential_reg[state][1]_i_1__0_n_0\,
      Q => \reg[state]\(1),
      R => '0'
    );
\FSM_sequential_reg_reg[state][2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => '1',
      D => \FSM_sequential_reg[state][2]_i_1_n_0\,
      Q => \reg[state]\(2),
      R => '0'
    );
crc_inst: entity work.design_1_axi_onewire_0_0_onewire_crc
     port map (
      CO(0) => \nxt_reg[id]1_inferred__0/i__carry_n_0\,
      axi_aresetn_i => axi_aresetn_i,
      axi_clk_i => axi_clk_i,
      \crc_reg[2]_0\ => \^dcvr_bit_tx\,
      dcvr_bit_send => \^dcvr_bit_send\,
      dcvr_id(63 downto 0) => dcvr_id(63 downto 0),
      if_done => if_done,
      ow_reg_rst => ow_reg_rst,
      ow_rst => ow_rst,
      pristine_reg_0 => \reg_reg[crc_reset_n_0_]\,
      \reg[state]\(2 downto 0) => \reg[state]\(2 downto 0),
      \reg_reg[id][10]\ => crc_inst_n_54,
      \reg_reg[id][11]\ => crc_inst_n_53,
      \reg_reg[id][13]\ => crc_inst_n_51,
      \reg_reg[id][14]\ => crc_inst_n_50,
      \reg_reg[id][15]\ => crc_inst_n_49,
      \reg_reg[id][17]\ => crc_inst_n_47,
      \reg_reg[id][17]_0\ => \reg[id][56]_i_4_n_0\,
      \reg_reg[id][18]\ => crc_inst_n_46,
      \reg_reg[id][19]\ => crc_inst_n_45,
      \reg_reg[id][1]\ => crc_inst_n_63,
      \reg_reg[id][1]_0\ => \reg[marker][6]_i_4_n_0\,
      \reg_reg[id][1]_1\ => \FSM_sequential_reg[state][2]_i_5_n_0\,
      \reg_reg[id][1]_2\ => \reg[id][56]_i_3_n_0\,
      \reg_reg[id][1]_3\ => \reg[id][40]_i_3_n_0\,
      \reg_reg[id][1]_4\ => \reg[id][64]_i_9_n_0\,
      \reg_reg[id][1]_5\ => \reg[id][64]_i_10_n_0\,
      \reg_reg[id][21]\ => crc_inst_n_43,
      \reg_reg[id][22]\ => crc_inst_n_42,
      \reg_reg[id][23]\ => crc_inst_n_41,
      \reg_reg[id][25]\ => crc_inst_n_39,
      \reg_reg[id][25]_0\ => \reg[id][32]_i_3_n_0\,
      \reg_reg[id][26]\ => crc_inst_n_38,
      \reg_reg[id][27]\ => crc_inst_n_37,
      \reg_reg[id][29]\ => crc_inst_n_35,
      \reg_reg[id][2]\ => crc_inst_n_62,
      \reg_reg[id][30]\ => crc_inst_n_34,
      \reg_reg[id][31]\ => crc_inst_n_33,
      \reg_reg[id][33]\ => crc_inst_n_31,
      \reg_reg[id][34]\ => crc_inst_n_30,
      \reg_reg[id][35]\ => crc_inst_n_29,
      \reg_reg[id][37]\ => crc_inst_n_27,
      \reg_reg[id][38]\ => crc_inst_n_26,
      \reg_reg[id][39]\ => crc_inst_n_25,
      \reg_reg[id][3]\ => crc_inst_n_61,
      \reg_reg[id][41]\ => crc_inst_n_23,
      \reg_reg[id][42]\ => crc_inst_n_22,
      \reg_reg[id][43]\ => crc_inst_n_21,
      \reg_reg[id][45]\ => crc_inst_n_19,
      \reg_reg[id][46]\ => crc_inst_n_18,
      \reg_reg[id][47]\ => crc_inst_n_17,
      \reg_reg[id][49]\ => crc_inst_n_15,
      \reg_reg[id][50]\ => crc_inst_n_14,
      \reg_reg[id][51]\ => crc_inst_n_13,
      \reg_reg[id][53]\ => crc_inst_n_11,
      \reg_reg[id][54]\ => crc_inst_n_10,
      \reg_reg[id][55]\ => crc_inst_n_9,
      \reg_reg[id][57]\ => crc_inst_n_7,
      \reg_reg[id][57]_0\ => \reg[id][64]_i_8_n_0\,
      \reg_reg[id][57]_1\ => \reg[id][57]_i_2_n_0\,
      \reg_reg[id][58]\ => crc_inst_n_6,
      \reg_reg[id][58]_0\ => \reg[id][58]_i_2_n_0\,
      \reg_reg[id][59]\ => crc_inst_n_5,
      \reg_reg[id][59]_0\ => \reg[id][59]_i_2_n_0\,
      \reg_reg[id][5]\ => crc_inst_n_59,
      \reg_reg[id][61]\ => crc_inst_n_3,
      \reg_reg[id][61]_0\ => \reg[id][61]_i_2_n_0\,
      \reg_reg[id][62]\ => crc_inst_n_2,
      \reg_reg[id][62]_0\ => \reg[id][62]_i_2_n_0\,
      \reg_reg[id][63]\ => crc_inst_n_1,
      \reg_reg[id][63]_0\ => \reg[id][63]_i_2_n_0\,
      \reg_reg[id][64]\ => \reg[id][64]_i_2_n_0\,
      \reg_reg[id][64]_0\ => \reg_reg[id_bit_number_n_0_][2]\,
      \reg_reg[id][64]_1\ => \reg[id][64]_i_3_n_0\,
      \reg_reg[id][6]\ => crc_inst_n_58,
      \reg_reg[id][7]\ => crc_inst_n_57,
      \reg_reg[id][9]\ => crc_inst_n_55,
      \reg_reg[id][9]_0\ => \reg[id][48]_i_3_n_0\,
      \reg_reg[id_bit_number][2]\ => crc_inst_n_0,
      \reg_reg[id_bit_number][2]_0\ => crc_inst_n_4,
      \reg_reg[id_bit_number][2]_1\ => crc_inst_n_8,
      \reg_reg[id_bit_number][2]_10\ => crc_inst_n_44,
      \reg_reg[id_bit_number][2]_11\ => crc_inst_n_48,
      \reg_reg[id_bit_number][2]_12\ => crc_inst_n_52,
      \reg_reg[id_bit_number][2]_13\ => crc_inst_n_56,
      \reg_reg[id_bit_number][2]_14\ => crc_inst_n_60,
      \reg_reg[id_bit_number][2]_2\ => crc_inst_n_12,
      \reg_reg[id_bit_number][2]_3\ => crc_inst_n_16,
      \reg_reg[id_bit_number][2]_4\ => crc_inst_n_20,
      \reg_reg[id_bit_number][2]_5\ => crc_inst_n_24,
      \reg_reg[id_bit_number][2]_6\ => crc_inst_n_28,
      \reg_reg[id_bit_number][2]_7\ => crc_inst_n_32,
      \reg_reg[id_bit_number][2]_8\ => crc_inst_n_36,
      \reg_reg[id_bit_number][2]_9\ => crc_inst_n_40
    );
\i__carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][6]\,
      I1 => \reg[last_discrepancy]\(6),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][5]\,
      I1 => \reg[last_discrepancy]\(5),
      I2 => \reg_reg[id_bit_number_n_0_][4]\,
      I3 => \reg[last_discrepancy]\(4),
      O => \i__carry_i_2_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg[last_discrepancy]\(2),
      I2 => \reg[last_discrepancy]\(3),
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][0]\,
      I1 => \reg[last_discrepancy]\(0),
      I2 => \reg[last_discrepancy]\(1),
      I3 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \i__carry_i_4_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \reg[last_discrepancy]\(6),
      I1 => \reg_reg[id_bit_number_n_0_][6]\,
      O => \i__carry_i_5_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \reg[last_discrepancy]\(4),
      I1 => \reg_reg[id_bit_number_n_0_][4]\,
      I2 => \reg[last_discrepancy]\(5),
      I3 => \reg_reg[id_bit_number_n_0_][5]\,
      O => \i__carry_i_6_n_0\
    );
\i__carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg[last_discrepancy]\(2),
      I2 => \reg_reg[id_bit_number_n_0_][3]\,
      I3 => \reg[last_discrepancy]\(3),
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][0]\,
      I1 => \reg[last_discrepancy]\(0),
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg[last_discrepancy]\(1),
      O => \i__carry_i_8_n_0\
    );
mem_reg_0_63_0_2_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(0),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_bit_tx,
      O => wr_data_i(0)
    );
mem_reg_0_63_0_2_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(1),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(0),
      O => wr_data_i(1)
    );
mem_reg_0_63_0_2_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(2),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(1),
      O => wr_data_i(2)
    );
mem_reg_0_63_12_14_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(12),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(11),
      O => wr_data_i(12)
    );
mem_reg_0_63_12_14_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(13),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(12),
      O => wr_data_i(13)
    );
mem_reg_0_63_12_14_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(14),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(13),
      O => wr_data_i(14)
    );
mem_reg_0_63_15_17_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(15),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(14),
      O => wr_data_i(15)
    );
mem_reg_0_63_15_17_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(16),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(15),
      O => wr_data_i(16)
    );
mem_reg_0_63_15_17_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(17),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(16),
      O => wr_data_i(17)
    );
mem_reg_0_63_18_20_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(18),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(17),
      O => wr_data_i(18)
    );
mem_reg_0_63_18_20_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(19),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(18),
      O => wr_data_i(19)
    );
mem_reg_0_63_18_20_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(20),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(19),
      O => wr_data_i(20)
    );
mem_reg_0_63_21_23_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(21),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(20),
      O => wr_data_i(21)
    );
mem_reg_0_63_21_23_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(22),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(21),
      O => wr_data_i(22)
    );
mem_reg_0_63_21_23_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(23),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(22),
      O => wr_data_i(23)
    );
mem_reg_0_63_24_26_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(24),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(23),
      O => wr_data_i(24)
    );
mem_reg_0_63_24_26_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(25),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(24),
      O => wr_data_i(25)
    );
mem_reg_0_63_24_26_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(26),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(25),
      O => wr_data_i(26)
    );
mem_reg_0_63_27_29_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(27),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(26),
      O => wr_data_i(27)
    );
mem_reg_0_63_27_29_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(28),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(27),
      O => wr_data_i(28)
    );
mem_reg_0_63_27_29_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(29),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(28),
      O => wr_data_i(29)
    );
mem_reg_0_63_30_32_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(30),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(29),
      O => wr_data_i(30)
    );
mem_reg_0_63_30_32_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(31),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(30),
      O => wr_data_i(31)
    );
mem_reg_0_63_30_32_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(32),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(31),
      O => wr_data_i(32)
    );
mem_reg_0_63_33_35_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(33),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(32),
      O => wr_data_i(33)
    );
mem_reg_0_63_33_35_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(34),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(33),
      O => wr_data_i(34)
    );
mem_reg_0_63_33_35_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(35),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(34),
      O => wr_data_i(35)
    );
mem_reg_0_63_36_38_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(36),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(35),
      O => wr_data_i(36)
    );
mem_reg_0_63_36_38_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(37),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(36),
      O => wr_data_i(37)
    );
mem_reg_0_63_36_38_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(38),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(37),
      O => wr_data_i(38)
    );
mem_reg_0_63_39_41_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(39),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(38),
      O => wr_data_i(39)
    );
mem_reg_0_63_39_41_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(40),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(39),
      O => wr_data_i(40)
    );
mem_reg_0_63_39_41_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(41),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(40),
      O => wr_data_i(41)
    );
mem_reg_0_63_3_5_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(3),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(2),
      O => wr_data_i(3)
    );
mem_reg_0_63_3_5_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(4),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(3),
      O => wr_data_i(4)
    );
mem_reg_0_63_3_5_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(5),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(4),
      O => wr_data_i(5)
    );
mem_reg_0_63_42_44_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(42),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(41),
      O => wr_data_i(42)
    );
mem_reg_0_63_42_44_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(43),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(42),
      O => wr_data_i(43)
    );
mem_reg_0_63_42_44_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(44),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(43),
      O => wr_data_i(44)
    );
mem_reg_0_63_45_47_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(45),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(44),
      O => wr_data_i(45)
    );
mem_reg_0_63_45_47_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(46),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(45),
      O => wr_data_i(46)
    );
mem_reg_0_63_45_47_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(47),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(46),
      O => wr_data_i(47)
    );
mem_reg_0_63_48_50_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(48),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(47),
      O => wr_data_i(48)
    );
mem_reg_0_63_48_50_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(49),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(48),
      O => wr_data_i(49)
    );
mem_reg_0_63_48_50_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(50),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(49),
      O => wr_data_i(50)
    );
mem_reg_0_63_51_53_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(51),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(50),
      O => wr_data_i(51)
    );
mem_reg_0_63_51_53_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(52),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(51),
      O => wr_data_i(52)
    );
mem_reg_0_63_51_53_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(53),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(52),
      O => wr_data_i(53)
    );
mem_reg_0_63_54_56_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(54),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(53),
      O => wr_data_i(54)
    );
mem_reg_0_63_54_56_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(55),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(54),
      O => wr_data_i(55)
    );
mem_reg_0_63_54_56_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(56),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(55),
      O => wr_data_i(56)
    );
mem_reg_0_63_57_59_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(57),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(56),
      O => wr_data_i(57)
    );
mem_reg_0_63_57_59_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(58),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(57),
      O => wr_data_i(58)
    );
mem_reg_0_63_57_59_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(59),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(58),
      O => wr_data_i(59)
    );
mem_reg_0_63_60_62_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(60),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(59),
      O => wr_data_i(60)
    );
mem_reg_0_63_60_62_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(61),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(60),
      O => wr_data_i(61)
    );
mem_reg_0_63_60_62_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(62),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(61),
      O => wr_data_i(62)
    );
mem_reg_0_63_63_63_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(63),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(62),
      O => wr_data_i(63)
    );
mem_reg_0_63_6_8_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(6),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(5),
      O => wr_data_i(6)
    );
mem_reg_0_63_6_8_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(7),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(6),
      O => wr_data_i(7)
    );
mem_reg_0_63_6_8_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(8),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(7),
      O => wr_data_i(8)
    );
mem_reg_0_63_9_11_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(9),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(8),
      O => wr_data_i(9)
    );
mem_reg_0_63_9_11_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(10),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(9),
      O => wr_data_i(10)
    );
mem_reg_0_63_9_11_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => dcvr_id(11),
      I1 => \^dcvr_id_en\,
      I2 => ctrl_wr_data(10),
      O => wr_data_i(11)
    );
\nxt_reg[id]1_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \nxt_reg[id]1_inferred__0/i__carry_n_0\,
      CO(2) => \nxt_reg[id]1_inferred__0/i__carry_n_1\,
      CO(1) => \nxt_reg[id]1_inferred__0/i__carry_n_2\,
      CO(0) => \nxt_reg[id]1_inferred__0/i__carry_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry_i_1_n_0\,
      DI(2) => \i__carry_i_2_n_0\,
      DI(1) => \i__carry_i_3_n_0\,
      DI(0) => \i__carry_i_4_n_0\,
      O(3 downto 0) => \NLW_nxt_reg[id]1_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry_i_5_n_0\,
      S(2) => \i__carry_i_6_n_0\,
      S(1) => \i__carry_i_7_n_0\,
      S(0) => \i__carry_i_8_n_0\
    );
\reg[bit_recv]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \reg[bit_recv]_i_2_n_0\,
      I1 => ow_rst,
      I2 => \reg[state]\(2),
      I3 => \reg[state]\(1),
      I4 => \reg[state]\(0),
      I5 => if_rx_data_en,
      O => \reg[bit_recv]\
    );
\reg[bit_recv]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000040C00000"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][6]\,
      I1 => \reg[id_bit_number][3]_i_2_n_0\,
      I2 => if_done,
      I3 => \reg[state]\(2),
      I4 => \reg[state]\(1),
      I5 => \reg[crc_reset]_i_2_n_0\,
      O => \reg[bit_recv]_i_2_n_0\
    );
\reg[bit_send]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFAEAAAAAAAEAA"
    )
        port map (
      I0 => \reg[bit_send]_i_2_n_0\,
      I1 => \reg[bit_send]_i_3_n_0\,
      I2 => ow_rst,
      I3 => \nxt_reg[bit_send]19_out\,
      I4 => \reg[state]\(0),
      I5 => \FSM_sequential_reg[state][2]_i_2_n_0\,
      O => \reg[bit_send]\
    );
\reg[bit_send]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001000000"
    )
        port map (
      I0 => if_rx_data,
      I1 => \reg[state]\(2),
      I2 => ow_rst,
      I3 => if_done,
      I4 => \reg[state]\(0),
      I5 => \reg[state]\(1),
      O => \reg[bit_send]_i_2_n_0\
    );
\reg[bit_send]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      O => \reg[bit_send]_i_3_n_0\
    );
\reg[bus_rst]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888888F888888888"
    )
        port map (
      I0 => \FSM_sequential_reg[state][0]_i_3_n_0\,
      I1 => \reg[bus_rst]_i_2_n_0\,
      I2 => \reg[id_bit_number][3]_i_2_n_0\,
      I3 => \reg[state]\(2),
      I4 => \reg[state]\(1),
      I5 => dcvr_start,
      O => \reg[bus_rst]\
    );
\reg[bus_rst]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0200000000000000"
    )
        port map (
      I0 => if_done,
      I1 => \reg[state]\(0),
      I2 => ow_rst,
      I3 => \reg[state]\(2),
      I4 => \reg[state]\(1),
      I5 => \reg_reg[id_bit_number_n_0_][6]\,
      O => \reg[bus_rst]_i_2_n_0\
    );
\reg[cmd][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000B0000000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => if_rx_data,
      I2 => \reg[state]\(2),
      I3 => axi_aresetn_i,
      I4 => ow_reg_rst,
      I5 => in30(0),
      O => \reg[cmd][0]_i_1_n_0\
    );
\reg[cmd][1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000B0000000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => if_rx_data,
      I2 => \reg[state]\(2),
      I3 => axi_aresetn_i,
      I4 => ow_reg_rst,
      I5 => in30(1),
      O => \reg[cmd][1]_i_1_n_0\
    );
\reg[cmd][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000B0000000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => if_rx_data,
      I2 => \reg[state]\(2),
      I3 => axi_aresetn_i,
      I4 => ow_reg_rst,
      I5 => in30(2),
      O => \reg[cmd][2]_i_1_n_0\
    );
\reg[cmd][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000B0000000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => if_rx_data,
      I2 => \reg[state]\(2),
      I3 => axi_aresetn_i,
      I4 => ow_reg_rst,
      I5 => in30(3),
      O => \reg[cmd][3]_i_1_n_0\
    );
\reg[cmd][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3AFF3AFFFFFFFA"
    )
        port map (
      I0 => in30(4),
      I1 => \reg[state]\(0),
      I2 => \reg[state]\(2),
      I3 => ow_rst,
      I4 => if_rx_data,
      I5 => \reg[state]\(1),
      O => \reg[cmd][4]_i_1_n_0\
    );
\reg[cmd][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3AFF3AFFFFFFFA"
    )
        port map (
      I0 => in30(5),
      I1 => \reg[state]\(0),
      I2 => \reg[state]\(2),
      I3 => ow_rst,
      I4 => if_rx_data,
      I5 => \reg[state]\(1),
      O => \reg[cmd][5]_i_1_n_0\
    );
\reg[cmd][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3AFF3AFFFFFFFA"
    )
        port map (
      I0 => in30(6),
      I1 => \reg[state]\(0),
      I2 => \reg[state]\(2),
      I3 => ow_rst,
      I4 => if_rx_data,
      I5 => \reg[state]\(1),
      O => \reg[cmd][6]_i_1_n_0\
    );
\reg[cmd][7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAAAABAAA"
    )
        port map (
      I0 => ow_rst,
      I1 => \reg[state]\(2),
      I2 => if_done,
      I3 => \reg[state]\(0),
      I4 => \reg[state]\(1),
      I5 => \reg[cmd][7]_i_3_n_0\,
      O => \reg[cmd][7]_i_1_n_0\
    );
\reg[cmd][7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F5FFF4FFFFFFF4FF"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => if_rx_data,
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \reg[state]\(2),
      I5 => \reg[state]\(0),
      O => \reg[cmd][7]_i_2_n_0\
    );
\reg[cmd][7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0200"
    )
        port map (
      I0 => \nxt_reg[bit_send]19_out\,
      I1 => \reg[state]\(0),
      I2 => \reg[state]\(2),
      I3 => \reg[state]\(1),
      I4 => \reg[lfsr][3]_i_3_n_0\,
      O => \reg[cmd][7]_i_3_n_0\
    );
\reg[cmp_id_bit]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010FFFF00100000"
    )
        port map (
      I0 => ow_rst,
      I1 => \reg[state]\(0),
      I2 => if_rx_data,
      I3 => \reg[state]\(1),
      I4 => \reg[cmp_id_bit]_i_2_n_0\,
      I5 => \reg_reg[cmp_id_bit_n_0_]\,
      O => \reg[cmp_id_bit]_i_1_n_0\
    );
\reg[cmp_id_bit]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEAAAAAAA"
    )
        port map (
      I0 => \reg[id_bit_number][6]_i_3_n_0\,
      I1 => \reg[state]\(2),
      I2 => \reg[state]\(1),
      I3 => \reg_reg[id_bit_number_n_0_][6]\,
      I4 => if_done,
      I5 => \reg[cmp_id_bit]_i_3_n_0\,
      O => \reg[cmp_id_bit]_i_2_n_0\
    );
\reg[cmp_id_bit]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF0040FFFF"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => if_rx_data_en,
      I3 => \reg[state]\(0),
      I4 => axi_aresetn_i,
      I5 => ow_reg_rst,
      O => \reg[cmp_id_bit]_i_3_n_0\
    );
\reg[crc_reset]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000400"
    )
        port map (
      I0 => \reg[crc_reset]_i_2_n_0\,
      I1 => \reg[state]\(1),
      I2 => \reg[state]\(2),
      I3 => if_done,
      I4 => \reg[state]\(0),
      I5 => ow_rst,
      O => \reg[crc_reset]\
    );
\reg[crc_reset]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => lfsr(1),
      I1 => lfsr(0),
      I2 => lfsr(3),
      I3 => lfsr(2),
      O => \reg[crc_reset]_i_2_n_0\
    );
\reg[data]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^dcvr_bit_send\,
      I1 => ctrl_bit_send,
      O => \reg_reg[bit_send]_0\
    );
\reg[data]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^dcvr_bus_rst\,
      I1 => ctrl_bus_rst,
      O => \reg_reg[bus_rst]_0\
    );
\reg[done]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF00000400"
    )
        port map (
      I0 => \FSM_sequential_reg[state][0]_i_3_n_0\,
      I1 => if_done,
      I2 => \reg[state]\(0),
      I3 => \reg[done]_i_2__0_n_0\,
      I4 => ow_rst,
      I5 => \reg[id_bit_number][6]_i_3_n_0\,
      O => \reg[done]\
    );
\reg[done]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][6]\,
      I1 => \reg[state]\(1),
      I2 => \reg[state]\(2),
      O => \reg[done]_i_2__0_n_0\
    );
\reg[id][32]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFFABFFFFFFFFFD"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][5]\,
      I1 => \reg_reg[id_bit_number_n_0_][1]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][4]\,
      O => \reg[id][32]_i_3_n_0\
    );
\reg[id][40]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FAFAFAEB"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][4]\,
      I1 => \reg_reg[id_bit_number_n_0_][2]\,
      I2 => \reg_reg[id_bit_number_n_0_][3]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[id][40]_i_3_n_0\
    );
\reg[id][48]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFAFAFBD"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][4]\,
      I1 => \reg_reg[id_bit_number_n_0_][2]\,
      I2 => \reg_reg[id_bit_number_n_0_][3]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[id][48]_i_3_n_0\
    );
\reg[id][56]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000001FFFFFFFE"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][4]\,
      I1 => \reg_reg[id_bit_number_n_0_][2]\,
      I2 => \reg_reg[id_bit_number_n_0_][3]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => \reg_reg[id_bit_number_n_0_][5]\,
      O => \reg[id][56]_i_3_n_0\
    );
\reg[id][56]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF55FD57"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][4]\,
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      O => \reg[id][56]_i_4_n_0\
    );
\reg[id][57]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg_reg[id_bit_number_n_0_][1]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[id][57]_i_2_n_0\
    );
\reg[id][58]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg_reg[id_bit_number_n_0_][1]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[id][58]_i_2_n_0\
    );
\reg[id][59]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg_reg[id_bit_number_n_0_][1]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[id][59]_i_2_n_0\
    );
\reg[id][61]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg_reg[id_bit_number_n_0_][1]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[id][61]_i_2_n_0\
    );
\reg[id][62]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F7"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg_reg[id_bit_number_n_0_][1]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[id][62]_i_2_n_0\
    );
\reg[id][63]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg_reg[id_bit_number_n_0_][1]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[id][63]_i_2_n_0\
    );
\reg[id][64]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => if_rx_data,
      O => \reg[id][64]_i_10_n_0\
    );
\reg[id][64]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAEAA"
    )
        port map (
      I0 => \reg[id][64]_i_6_n_0\,
      I1 => if_done,
      I2 => \reg[state]\(0),
      I3 => axi_aresetn_i,
      I4 => ow_reg_rst,
      O => \reg[id][64]_i_2_n_0\
    );
\reg[id][64]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][1]\,
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[id][64]_i_3_n_0\
    );
\reg[id][64]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000300020"
    )
        port map (
      I0 => \reg[marker][6]_i_9_n_0\,
      I1 => \reg[state]\(1),
      I2 => \reg[state]\(2),
      I3 => \reg_reg[cmp_id_bit_n_0_]\,
      I4 => \reg_reg[id_bit_n_0_]\,
      I5 => ow_rst,
      O => \reg[id][64]_i_6_n_0\
    );
\reg[id][64]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"77FF77FF77FF7FFE"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][5]\,
      I1 => \reg_reg[id_bit_number_n_0_][4]\,
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][0]\,
      I5 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[id][64]_i_8_n_0\
    );
\reg[id][64]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8F808080"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][6]\,
      I1 => if_done,
      I2 => \reg[state]\(1),
      I3 => \reg_reg[id_bit_n_0_]\,
      I4 => \reg_reg[cmp_id_bit_n_0_]\,
      O => \reg[id][64]_i_9_n_0\
    );
\reg[id_bit]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0200FFFF02000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => ow_rst,
      I3 => if_rx_data,
      I4 => \reg[id_bit]_i_2_n_0\,
      I5 => \reg_reg[id_bit_n_0_]\,
      O => \reg[id_bit]_i_1_n_0\
    );
\reg[id_bit]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEFEEEEEEEEEEEEE"
    )
        port map (
      I0 => \reg[lfsr][3]_i_5_n_0\,
      I1 => \reg[lfsr][3]_i_3_n_0\,
      I2 => \reg[state]\(1),
      I3 => \reg[state]\(2),
      I4 => if_rx_data_en,
      I5 => \reg[state]\(0),
      O => \reg[id_bit]_i_2_n_0\
    );
\reg[id_bit_number][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF51FFFFFFFFFF"
    )
        port map (
      I0 => \reg[state]\(0),
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      I2 => \reg_reg[id_bit_number_n_0_][6]\,
      I3 => \reg[state]\(1),
      I4 => ow_reg_rst,
      I5 => axi_aresetn_i,
      O => \reg[id_bit_number][0]_i_1_n_0\
    );
\reg[id_bit_number][1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000600"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][1]\,
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      I2 => ow_reg_rst,
      I3 => axi_aresetn_i,
      I4 => \reg[state]\(0),
      I5 => \reg_reg[id_bit_number_n_0_][6]\,
      O => \reg[id_bit_number][1]_i_1_n_0\
    );
\reg[id_bit_number][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001010101000000"
    )
        port map (
      I0 => ow_rst,
      I1 => \reg[state]\(0),
      I2 => \reg_reg[id_bit_number_n_0_][6]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => \reg_reg[id_bit_number_n_0_][2]\,
      O => \reg[id_bit_number][2]_i_1_n_0\
    );
\reg[id_bit_number][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000078F00000"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][1]\,
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      I2 => \reg_reg[id_bit_number_n_0_][3]\,
      I3 => \reg_reg[id_bit_number_n_0_][2]\,
      I4 => \reg[id_bit_number][3]_i_2_n_0\,
      I5 => \reg_reg[id_bit_number_n_0_][6]\,
      O => \reg[id_bit_number][3]_i_1_n_0\
    );
\reg[id_bit_number][3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \reg[state]\(0),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => \reg[id_bit_number][3]_i_2_n_0\
    );
\reg[id_bit_number][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2AAAAAAA80000000"
    )
        port map (
      I0 => \FSM_sequential_reg[state][1]_i_2_n_0\,
      I1 => \reg_reg[id_bit_number_n_0_][1]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][4]\,
      O => \reg[id_bit_number][4]_i_1_n_0\
    );
\reg[id_bit_number][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFF000040000000"
    )
        port map (
      I0 => \reg[id_bit_number][6]_i_4_n_0\,
      I1 => \reg_reg[id_bit_number_n_0_][3]\,
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][4]\,
      I4 => \FSM_sequential_reg[state][1]_i_2_n_0\,
      I5 => \reg_reg[id_bit_number_n_0_][5]\,
      O => \reg[id_bit_number][5]_i_1_n_0\
    );
\reg[id_bit_number][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAAEAAAAA"
    )
        port map (
      I0 => ow_rst,
      I1 => if_done,
      I2 => \reg[state]\(2),
      I3 => \reg[state]\(0),
      I4 => \reg[state]\(1),
      I5 => \reg[id_bit_number][6]_i_3_n_0\,
      O => \reg[id_bit_number][6]_i_1_n_0\
    );
\reg[id_bit_number][6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][5]\,
      I1 => \FSM_sequential_reg[state][1]_i_2_n_0\,
      I2 => \reg_reg[id_bit_number_n_0_][4]\,
      I3 => \reg_reg[id_bit_number_n_0_][2]\,
      I4 => \reg_reg[id_bit_number_n_0_][3]\,
      I5 => \reg[id_bit_number][6]_i_4_n_0\,
      O => \reg[id_bit_number][6]_i_2_n_0\
    );
\reg[id_bit_number][6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0B08080800000000"
    )
        port map (
      I0 => \nxt_reg[state]125_out\,
      I1 => \reg[state]\(2),
      I2 => \reg[state]\(1),
      I3 => if_done,
      I4 => if_rx_data,
      I5 => \reg[state]\(0),
      O => \reg[id_bit_number][6]_i_3_n_0\
    );
\reg[id_bit_number][6]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][1]\,
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[id_bit_number][6]_i_4_n_0\
    );
\reg[id_bit_number][6]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \reg_reg[id_bit_n_0_]\,
      I1 => \reg_reg[cmp_id_bit_n_0_]\,
      O => \nxt_reg[state]125_out\
    );
\reg[id_en]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \reg[state]\(2),
      I1 => \reg[state]\(1),
      I2 => \reg_reg[id_bit_number_n_0_][6]\,
      I3 => if_done,
      I4 => \reg[state]\(0),
      I5 => ow_rst,
      O => \reg[id_en]\
    );
\reg[last_discrepancy][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(0),
      I3 => \reg_reg[marker_n_0_][0]\,
      O => \reg[last_discrepancy][0]_i_1_n_0\
    );
\reg[last_discrepancy][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(0),
      I3 => \reg_reg[marker_n_0_][1]\,
      O => \reg[last_discrepancy][1]_i_1_n_0\
    );
\reg[last_discrepancy][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(0),
      I3 => \reg_reg[marker_n_0_][2]\,
      O => \reg[last_discrepancy][2]_i_1_n_0\
    );
\reg[last_discrepancy][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(0),
      I3 => \reg_reg[marker_n_0_][3]\,
      O => \reg[last_discrepancy][3]_i_1_n_0\
    );
\reg[last_discrepancy][4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(0),
      I3 => \reg_reg[marker_n_0_][4]\,
      O => \reg[last_discrepancy][4]_i_1_n_0\
    );
\reg[last_discrepancy][5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(0),
      I3 => \reg_reg[marker_n_0_][5]\,
      O => \reg[last_discrepancy][5]_i_1_n_0\
    );
\reg[last_discrepancy][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEEEEEEEEEEEEEEE"
    )
        port map (
      I0 => \reg[id_bit_number][6]_i_3_n_0\,
      I1 => ow_rst,
      I2 => \reg[state]\(2),
      I3 => if_done,
      I4 => \reg_reg[id_bit_number_n_0_][6]\,
      I5 => \reg[last_discrepancy][6]_i_3_n_0\,
      O => \reg[last_discrepancy][6]_i_1_n_0\
    );
\reg[last_discrepancy][6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => ow_reg_rst,
      I1 => axi_aresetn_i,
      I2 => \reg[state]\(0),
      I3 => \reg_reg[marker_n_0_][6]\,
      O => \reg[last_discrepancy][6]_i_2_n_0\
    );
\reg[last_discrepancy][6]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(0),
      O => \reg[last_discrepancy][6]_i_3_n_0\
    );
\reg[lfsr][0]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF0FFF6F6F"
    )
        port map (
      I0 => lfsr(3),
      I1 => lfsr(2),
      I2 => \reg[state]\(1),
      I3 => \reg[state]\(0),
      I4 => \reg[state]\(2),
      I5 => ow_rst,
      O => \reg[lfsr][0]_i_1__0_n_0\
    );
\reg[lfsr][1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00200000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => axi_aresetn_i,
      I3 => ow_reg_rst,
      I4 => lfsr(0),
      O => \reg[lfsr][1]_i_1_n_0\
    );
\reg[lfsr][2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00200000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => axi_aresetn_i,
      I3 => ow_reg_rst,
      I4 => lfsr(1),
      O => \reg[lfsr][2]_i_1_n_0\
    );
\reg[lfsr][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAAAEAAAA"
    )
        port map (
      I0 => \reg[lfsr][3]_i_3_n_0\,
      I1 => \reg[state]\(1),
      I2 => \reg[state]\(2),
      I3 => \reg[state]\(0),
      I4 => \nxt_reg[bit_send]19_out\,
      I5 => \reg[lfsr][3]_i_5_n_0\,
      O => \reg[lfsr][3]_i_1_n_0\
    );
\reg[lfsr][3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00200000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => axi_aresetn_i,
      I3 => ow_reg_rst,
      I4 => lfsr(2),
      O => \reg[lfsr][3]_i_2_n_0\
    );
\reg[lfsr][3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F80008000800080"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][6]\,
      I1 => if_done,
      I2 => \FSM_sequential_reg[state][2]_i_5_n_0\,
      I3 => \FSM_sequential_reg[state][2]_i_7_n_0\,
      I4 => \reg_reg[cmp_id_bit_n_0_]\,
      I5 => \reg_reg[id_bit_n_0_]\,
      O => \reg[lfsr][3]_i_3_n_0\
    );
\reg[lfsr][3]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FBFF0000"
    )
        port map (
      I0 => lfsr(2),
      I1 => lfsr(3),
      I2 => lfsr(0),
      I3 => lfsr(1),
      I4 => if_done,
      O => \nxt_reg[bit_send]19_out\
    );
\reg[lfsr][3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF10000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => if_rx_data,
      I3 => if_done,
      I4 => \reg[state]\(0),
      I5 => ow_rst,
      O => \reg[lfsr][3]_i_5_n_0\
    );
\reg[marker][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000044400000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => \reg_reg[cmp_id_bit_n_0_]\,
      I3 => \reg_reg[id_bit_n_0_]\,
      I4 => ow_rst,
      I5 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[marker][0]_i_1_n_0\
    );
\reg[marker][1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000044400000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => \reg_reg[cmp_id_bit_n_0_]\,
      I3 => \reg_reg[id_bit_n_0_]\,
      I4 => ow_rst,
      I5 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[marker][1]_i_1_n_0\
    );
\reg[marker][2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000044400000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => \reg_reg[cmp_id_bit_n_0_]\,
      I3 => \reg_reg[id_bit_n_0_]\,
      I4 => ow_rst,
      I5 => \reg_reg[id_bit_number_n_0_][2]\,
      O => \reg[marker][2]_i_1_n_0\
    );
\reg[marker][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000044400000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => \reg_reg[cmp_id_bit_n_0_]\,
      I3 => \reg_reg[id_bit_n_0_]\,
      I4 => ow_rst,
      I5 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \reg[marker][3]_i_1_n_0\
    );
\reg[marker][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000044400000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => \reg_reg[cmp_id_bit_n_0_]\,
      I3 => \reg_reg[id_bit_n_0_]\,
      I4 => ow_rst,
      I5 => \reg_reg[id_bit_number_n_0_][4]\,
      O => \reg[marker][4]_i_1_n_0\
    );
\reg[marker][5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000044400000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => \reg_reg[cmp_id_bit_n_0_]\,
      I3 => \reg_reg[id_bit_n_0_]\,
      I4 => ow_rst,
      I5 => \reg_reg[id_bit_number_n_0_][5]\,
      O => \reg[marker][5]_i_1_n_0\
    );
\reg[marker][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAEAAAAAA"
    )
        port map (
      I0 => \reg[last_discrepancy][6]_i_1_n_0\,
      I1 => \reg[marker][6]_i_3_n_0\,
      I2 => \reg[state]\(1),
      I3 => \reg[state]\(2),
      I4 => \reg[state]\(0),
      I5 => \reg[marker][6]_i_4_n_0\,
      O => \reg[marker][6]_i_1_n_0\
    );
\reg[marker][6]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00080000"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][1]\,
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      I2 => \reg_reg[id_bit_number_n_0_][3]\,
      I3 => \reg_reg[id_bit_number_n_0_][2]\,
      I4 => dcvr_id(34),
      O => \reg[marker][6]_i_10_n_0\
    );
\reg[marker][6]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAABEAEBAAABAAA"
    )
        port map (
      I0 => \reg[marker][6]_i_28_n_0\,
      I1 => \reg_reg[id_bit_number_n_0_][3]\,
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg[marker][6]_i_29_n_0\,
      I4 => \reg[id][64]_i_3_n_0\,
      I5 => dcvr_id(39),
      O => \reg[marker][6]_i_11_n_0\
    );
\reg[marker][6]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF4F44"
    )
        port map (
      I0 => \reg[marker][6]_i_30_n_0\,
      I1 => dcvr_id(46),
      I2 => \reg[marker][6]_i_31_n_0\,
      I3 => dcvr_id(31),
      I4 => \reg[marker][6]_i_32_n_0\,
      I5 => \reg[marker][6]_i_33_n_0\,
      O => \reg[marker][6]_i_12_n_0\
    );
\reg[marker][6]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C0000000000A000"
    )
        port map (
      I0 => dcvr_id(43),
      I1 => dcvr_id(42),
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][0]\,
      I5 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[marker][6]_i_13_n_0\
    );
\reg[marker][6]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00080000"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][1]\,
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      I2 => \reg_reg[id_bit_number_n_0_][3]\,
      I3 => \reg_reg[id_bit_number_n_0_][2]\,
      I4 => dcvr_id(2),
      O => \reg[marker][6]_i_14_n_0\
    );
\reg[marker][6]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAABEAEBAAABAAA"
    )
        port map (
      I0 => \reg[marker][6]_i_34_n_0\,
      I1 => \reg_reg[id_bit_number_n_0_][3]\,
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg[marker][6]_i_35_n_0\,
      I4 => \reg[id][64]_i_3_n_0\,
      I5 => dcvr_id(7),
      O => \reg[marker][6]_i_15_n_0\
    );
\reg[marker][6]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF4F44"
    )
        port map (
      I0 => \reg[marker][6]_i_30_n_0\,
      I1 => dcvr_id(14),
      I2 => \reg[marker][6]_i_31_n_0\,
      I3 => dcvr_id(63),
      I4 => \reg[marker][6]_i_36_n_0\,
      I5 => \reg[marker][6]_i_37_n_0\,
      O => \reg[marker][6]_i_16_n_0\
    );
\reg[marker][6]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C0000000000A000"
    )
        port map (
      I0 => dcvr_id(11),
      I1 => dcvr_id(10),
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][0]\,
      I5 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[marker][6]_i_17_n_0\
    );
\reg[marker][6]_i_18\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00080000"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][1]\,
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      I2 => \reg_reg[id_bit_number_n_0_][3]\,
      I3 => \reg_reg[id_bit_number_n_0_][2]\,
      I4 => dcvr_id(50),
      O => \reg[marker][6]_i_18_n_0\
    );
\reg[marker][6]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAABEAEBAAABAAA"
    )
        port map (
      I0 => \reg[marker][6]_i_38_n_0\,
      I1 => \reg_reg[id_bit_number_n_0_][3]\,
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg[marker][6]_i_39_n_0\,
      I4 => \reg[id][64]_i_3_n_0\,
      I5 => dcvr_id(55),
      O => \reg[marker][6]_i_19_n_0\
    );
\reg[marker][6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000044400000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(2),
      I2 => \reg_reg[cmp_id_bit_n_0_]\,
      I3 => \reg_reg[id_bit_n_0_]\,
      I4 => ow_rst,
      I5 => \reg_reg[id_bit_number_n_0_][6]\,
      O => \reg[marker][6]_i_2_n_0\
    );
\reg[marker][6]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF4F44"
    )
        port map (
      I0 => \reg[marker][6]_i_30_n_0\,
      I1 => dcvr_id(62),
      I2 => \reg[marker][6]_i_31_n_0\,
      I3 => dcvr_id(47),
      I4 => \reg[marker][6]_i_40_n_0\,
      I5 => \reg[marker][6]_i_41_n_0\,
      O => \reg[marker][6]_i_20_n_0\
    );
\reg[marker][6]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C0000000000A000"
    )
        port map (
      I0 => dcvr_id(59),
      I1 => dcvr_id(58),
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][0]\,
      I5 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[marker][6]_i_21_n_0\
    );
\reg[marker][6]_i_22\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00080000"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][1]\,
      I1 => \reg_reg[id_bit_number_n_0_][0]\,
      I2 => \reg_reg[id_bit_number_n_0_][3]\,
      I3 => \reg_reg[id_bit_number_n_0_][2]\,
      I4 => dcvr_id(18),
      O => \reg[marker][6]_i_22_n_0\
    );
\reg[marker][6]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAABEAEBAAABAAA"
    )
        port map (
      I0 => \reg[marker][6]_i_42_n_0\,
      I1 => \reg_reg[id_bit_number_n_0_][3]\,
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg[marker][6]_i_43_n_0\,
      I4 => \reg[id][64]_i_3_n_0\,
      I5 => dcvr_id(23),
      O => \reg[marker][6]_i_23_n_0\
    );
\reg[marker][6]_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF4F44"
    )
        port map (
      I0 => \reg[marker][6]_i_30_n_0\,
      I1 => dcvr_id(30),
      I2 => \reg[marker][6]_i_31_n_0\,
      I3 => dcvr_id(15),
      I4 => \reg[marker][6]_i_44_n_0\,
      I5 => \reg[marker][6]_i_45_n_0\,
      O => \reg[marker][6]_i_24_n_0\
    );
\reg[marker][6]_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C0000000000A000"
    )
        port map (
      I0 => dcvr_id(27),
      I1 => dcvr_id(26),
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][0]\,
      I5 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[marker][6]_i_25_n_0\
    );
\reg[marker][6]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \reg[last_discrepancy]\(3),
      I1 => \reg_reg[id_bit_number_n_0_][3]\,
      I2 => \reg[last_discrepancy]\(0),
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg[last_discrepancy]\(2),
      O => \reg[marker][6]_i_26_n_0\
    );
\reg[marker][6]_i_27\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => \reg[last_discrepancy]\(4),
      I1 => \reg_reg[id_bit_number_n_0_][4]\,
      I2 => \reg[last_discrepancy]\(5),
      I3 => \reg_reg[id_bit_number_n_0_][5]\,
      O => \reg[marker][6]_i_27_n_0\
    );
\reg[marker][6]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000AC0"
    )
        port map (
      I0 => dcvr_id(32),
      I1 => dcvr_id(33),
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \reg[marker][6]_i_28_n_0\
    );
\reg[marker][6]_i_29\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFFAAF0CC00AAF0"
    )
        port map (
      I0 => dcvr_id(36),
      I1 => dcvr_id(38),
      I2 => dcvr_id(35),
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => dcvr_id(37),
      O => \reg[marker][6]_i_29_n_0\
    );
\reg[marker][6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAAB"
    )
        port map (
      I0 => \nxt_reg[id]1_inferred__0/i__carry_n_0\,
      I1 => \reg[marker][6]_i_5_n_0\,
      I2 => \reg[marker][6]_i_6_n_0\,
      I3 => \reg[marker][6]_i_7_n_0\,
      I4 => \reg[marker][6]_i_8_n_0\,
      O => \reg[marker][6]_i_3_n_0\
    );
\reg[marker][6]_i_30\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg_reg[id_bit_number_n_0_][3]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      I3 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[marker][6]_i_30_n_0\
    );
\reg[marker][6]_i_31\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][2]\,
      I1 => \reg_reg[id_bit_number_n_0_][3]\,
      I2 => \reg_reg[id_bit_number_n_0_][0]\,
      I3 => \reg_reg[id_bit_number_n_0_][1]\,
      O => \reg[marker][6]_i_31_n_0\
    );
\reg[marker][6]_i_32\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000A000C000000"
    )
        port map (
      I0 => dcvr_id(40),
      I1 => dcvr_id(41),
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[marker][6]_i_32_n_0\
    );
\reg[marker][6]_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0AC0000000000000"
    )
        port map (
      I0 => dcvr_id(44),
      I1 => dcvr_id(45),
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \reg[marker][6]_i_33_n_0\
    );
\reg[marker][6]_i_34\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000AC0"
    )
        port map (
      I0 => dcvr_id(0),
      I1 => dcvr_id(1),
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \reg[marker][6]_i_34_n_0\
    );
\reg[marker][6]_i_35\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFFAAF0CC00AAF0"
    )
        port map (
      I0 => dcvr_id(4),
      I1 => dcvr_id(6),
      I2 => dcvr_id(3),
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => dcvr_id(5),
      O => \reg[marker][6]_i_35_n_0\
    );
\reg[marker][6]_i_36\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000A000C000000"
    )
        port map (
      I0 => dcvr_id(8),
      I1 => dcvr_id(9),
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[marker][6]_i_36_n_0\
    );
\reg[marker][6]_i_37\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0AC0000000000000"
    )
        port map (
      I0 => dcvr_id(12),
      I1 => dcvr_id(13),
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \reg[marker][6]_i_37_n_0\
    );
\reg[marker][6]_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000AC0"
    )
        port map (
      I0 => dcvr_id(48),
      I1 => dcvr_id(49),
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \reg[marker][6]_i_38_n_0\
    );
\reg[marker][6]_i_39\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFFAAF0CC00AAF0"
    )
        port map (
      I0 => dcvr_id(52),
      I1 => dcvr_id(54),
      I2 => dcvr_id(51),
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => dcvr_id(53),
      O => \reg[marker][6]_i_39_n_0\
    );
\reg[marker][6]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \reg[marker][6]_i_9_n_0\,
      I1 => \reg_reg[cmp_id_bit_n_0_]\,
      I2 => \reg_reg[id_bit_n_0_]\,
      O => \reg[marker][6]_i_4_n_0\
    );
\reg[marker][6]_i_40\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000A000C000000"
    )
        port map (
      I0 => dcvr_id(56),
      I1 => dcvr_id(57),
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[marker][6]_i_40_n_0\
    );
\reg[marker][6]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0AC0000000000000"
    )
        port map (
      I0 => dcvr_id(60),
      I1 => dcvr_id(61),
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \reg[marker][6]_i_41_n_0\
    );
\reg[marker][6]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000AC0"
    )
        port map (
      I0 => dcvr_id(16),
      I1 => dcvr_id(17),
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \reg[marker][6]_i_42_n_0\
    );
\reg[marker][6]_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFFAAF0CC00AAF0"
    )
        port map (
      I0 => dcvr_id(20),
      I1 => dcvr_id(22),
      I2 => dcvr_id(19),
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => dcvr_id(21),
      O => \reg[marker][6]_i_43_n_0\
    );
\reg[marker][6]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000A000C000000"
    )
        port map (
      I0 => dcvr_id(24),
      I1 => dcvr_id(25),
      I2 => \reg_reg[id_bit_number_n_0_][2]\,
      I3 => \reg_reg[id_bit_number_n_0_][3]\,
      I4 => \reg_reg[id_bit_number_n_0_][1]\,
      I5 => \reg_reg[id_bit_number_n_0_][0]\,
      O => \reg[marker][6]_i_44_n_0\
    );
\reg[marker][6]_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0AC0000000000000"
    )
        port map (
      I0 => dcvr_id(28),
      I1 => dcvr_id(29),
      I2 => \reg_reg[id_bit_number_n_0_][1]\,
      I3 => \reg_reg[id_bit_number_n_0_][0]\,
      I4 => \reg_reg[id_bit_number_n_0_][2]\,
      I5 => \reg_reg[id_bit_number_n_0_][3]\,
      O => \reg[marker][6]_i_45_n_0\
    );
\reg[marker][6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444444444440"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][4]\,
      I1 => \reg_reg[id_bit_number_n_0_][5]\,
      I2 => \reg[marker][6]_i_10_n_0\,
      I3 => \reg[marker][6]_i_11_n_0\,
      I4 => \reg[marker][6]_i_12_n_0\,
      I5 => \reg[marker][6]_i_13_n_0\,
      O => \reg[marker][6]_i_5_n_0\
    );
\reg[marker][6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111111111111110"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][5]\,
      I1 => \reg_reg[id_bit_number_n_0_][4]\,
      I2 => \reg[marker][6]_i_14_n_0\,
      I3 => \reg[marker][6]_i_15_n_0\,
      I4 => \reg[marker][6]_i_16_n_0\,
      I5 => \reg[marker][6]_i_17_n_0\,
      O => \reg[marker][6]_i_6_n_0\
    );
\reg[marker][6]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888880"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][5]\,
      I1 => \reg_reg[id_bit_number_n_0_][4]\,
      I2 => \reg[marker][6]_i_18_n_0\,
      I3 => \reg[marker][6]_i_19_n_0\,
      I4 => \reg[marker][6]_i_20_n_0\,
      I5 => \reg[marker][6]_i_21_n_0\,
      O => \reg[marker][6]_i_7_n_0\
    );
\reg[marker][6]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444444444440"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][5]\,
      I1 => \reg_reg[id_bit_number_n_0_][4]\,
      I2 => \reg[marker][6]_i_22_n_0\,
      I3 => \reg[marker][6]_i_23_n_0\,
      I4 => \reg[marker][6]_i_24_n_0\,
      I5 => \reg[marker][6]_i_25_n_0\,
      O => \reg[marker][6]_i_8_n_0\
    );
\reg[marker][6]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000000000"
    )
        port map (
      I0 => \reg_reg[id_bit_number_n_0_][1]\,
      I1 => \reg[last_discrepancy]\(1),
      I2 => \reg_reg[id_bit_number_n_0_][6]\,
      I3 => \reg[last_discrepancy]\(6),
      I4 => \reg[marker][6]_i_26_n_0\,
      I5 => \reg[marker][6]_i_27_n_0\,
      O => \reg[marker][6]_i_9_n_0\
    );
\reg[search]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAAFFFFBAAA0000"
    )
        port map (
      I0 => \reg[search]_i_2_n_0\,
      I1 => \reg[marker][6]_i_3_n_0\,
      I2 => \FSM_sequential_reg[state][2]_i_2_n_0\,
      I3 => \nxt_reg[id]1__4\,
      I4 => \reg[search]_i_4_n_0\,
      I5 => \^dcvr_bit_tx\,
      O => \reg[search]_i_1_n_0\
    );
\reg[search]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAEAAAEAAAAAAAE"
    )
        port map (
      I0 => \reg[id][64]_i_6_n_0\,
      I1 => \reg_reg[cmd_n_0_][0]\,
      I2 => ow_rst,
      I3 => \reg[state]\(2),
      I4 => if_rx_data,
      I5 => \reg[state]\(1),
      O => \reg[search]_i_2_n_0\
    );
\reg[search]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \reg_reg[id_bit_n_0_]\,
      I1 => \reg_reg[cmp_id_bit_n_0_]\,
      O => \nxt_reg[id]1__4\
    );
\reg[search]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFF4F0F0FAF4F0"
    )
        port map (
      I0 => \reg[state]\(2),
      I1 => \nxt_reg[bit_send]19_out\,
      I2 => \reg[search]_i_5_n_0\,
      I3 => \reg[state]\(1),
      I4 => \reg[state]\(0),
      I5 => if_done,
      O => \reg[search]_i_4_n_0\
    );
\reg[search]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF20000000"
    )
        port map (
      I0 => \reg[state]\(1),
      I1 => \reg[state]\(0),
      I2 => \reg_reg[id_bit_number_n_0_][6]\,
      I3 => if_done,
      I4 => \reg[state]\(2),
      I5 => ow_rst,
      O => \reg[search]_i_5_n_0\
    );
\reg_reg[bit_recv]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[bit_recv]\,
      Q => dcvr_bit_recv,
      R => '0'
    );
\reg_reg[bit_send]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[bit_send]\,
      Q => \^dcvr_bit_send\,
      R => '0'
    );
\reg_reg[bus_rst]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[bus_rst]\,
      Q => \^dcvr_bus_rst\,
      R => '0'
    );
\reg_reg[cmd][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[cmd][7]_i_1_n_0\,
      D => \reg[cmd][0]_i_1_n_0\,
      Q => \reg_reg[cmd_n_0_][0]\,
      R => '0'
    );
\reg_reg[cmd][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[cmd][7]_i_1_n_0\,
      D => \reg[cmd][1]_i_1_n_0\,
      Q => in30(0),
      R => '0'
    );
\reg_reg[cmd][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[cmd][7]_i_1_n_0\,
      D => \reg[cmd][2]_i_1_n_0\,
      Q => in30(1),
      R => '0'
    );
\reg_reg[cmd][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[cmd][7]_i_1_n_0\,
      D => \reg[cmd][3]_i_1_n_0\,
      Q => in30(2),
      R => '0'
    );
\reg_reg[cmd][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[cmd][7]_i_1_n_0\,
      D => \reg[cmd][4]_i_1_n_0\,
      Q => in30(3),
      R => '0'
    );
\reg_reg[cmd][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[cmd][7]_i_1_n_0\,
      D => \reg[cmd][5]_i_1_n_0\,
      Q => in30(4),
      R => '0'
    );
\reg_reg[cmd][6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[cmd][7]_i_1_n_0\,
      D => \reg[cmd][6]_i_1_n_0\,
      Q => in30(5),
      R => '0'
    );
\reg_reg[cmd][7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[cmd][7]_i_1_n_0\,
      D => \reg[cmd][7]_i_2_n_0\,
      Q => in30(6),
      R => '0'
    );
\reg_reg[cmp_id_bit]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[cmp_id_bit]_i_1_n_0\,
      Q => \reg_reg[cmp_id_bit_n_0_]\,
      R => '0'
    );
\reg_reg[crc_reset]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[crc_reset]\,
      Q => \reg_reg[crc_reset_n_0_]\,
      R => '0'
    );
\reg_reg[done]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[done]\,
      Q => dcvr_done,
      R => '0'
    );
\reg_reg[id][10]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_54,
      Q => dcvr_id(9),
      R => '0'
    );
\reg_reg[id][11]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_53,
      Q => dcvr_id(10),
      R => '0'
    );
\reg_reg[id][12]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_52,
      Q => dcvr_id(11),
      R => '0'
    );
\reg_reg[id][13]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_51,
      Q => dcvr_id(12),
      R => '0'
    );
\reg_reg[id][14]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_50,
      Q => dcvr_id(13),
      R => '0'
    );
\reg_reg[id][15]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_49,
      Q => dcvr_id(14),
      R => '0'
    );
\reg_reg[id][16]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_48,
      Q => dcvr_id(15),
      R => '0'
    );
\reg_reg[id][17]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_47,
      Q => dcvr_id(16),
      R => '0'
    );
\reg_reg[id][18]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_46,
      Q => dcvr_id(17),
      R => '0'
    );
\reg_reg[id][19]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_45,
      Q => dcvr_id(18),
      R => '0'
    );
\reg_reg[id][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_63,
      Q => dcvr_id(0),
      R => '0'
    );
\reg_reg[id][20]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_44,
      Q => dcvr_id(19),
      R => '0'
    );
\reg_reg[id][21]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_43,
      Q => dcvr_id(20),
      R => '0'
    );
\reg_reg[id][22]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_42,
      Q => dcvr_id(21),
      R => '0'
    );
\reg_reg[id][23]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_41,
      Q => dcvr_id(22),
      R => '0'
    );
\reg_reg[id][24]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_40,
      Q => dcvr_id(23),
      R => '0'
    );
\reg_reg[id][25]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_39,
      Q => dcvr_id(24),
      R => '0'
    );
\reg_reg[id][26]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_38,
      Q => dcvr_id(25),
      R => '0'
    );
\reg_reg[id][27]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_37,
      Q => dcvr_id(26),
      R => '0'
    );
\reg_reg[id][28]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_36,
      Q => dcvr_id(27),
      R => '0'
    );
\reg_reg[id][29]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_35,
      Q => dcvr_id(28),
      R => '0'
    );
\reg_reg[id][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_62,
      Q => dcvr_id(1),
      R => '0'
    );
\reg_reg[id][30]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_34,
      Q => dcvr_id(29),
      R => '0'
    );
\reg_reg[id][31]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_33,
      Q => dcvr_id(30),
      R => '0'
    );
\reg_reg[id][32]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_32,
      Q => dcvr_id(31),
      R => '0'
    );
\reg_reg[id][33]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_31,
      Q => dcvr_id(32),
      R => '0'
    );
\reg_reg[id][34]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_30,
      Q => dcvr_id(33),
      R => '0'
    );
\reg_reg[id][35]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_29,
      Q => dcvr_id(34),
      R => '0'
    );
\reg_reg[id][36]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_28,
      Q => dcvr_id(35),
      R => '0'
    );
\reg_reg[id][37]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_27,
      Q => dcvr_id(36),
      R => '0'
    );
\reg_reg[id][38]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_26,
      Q => dcvr_id(37),
      R => '0'
    );
\reg_reg[id][39]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_25,
      Q => dcvr_id(38),
      R => '0'
    );
\reg_reg[id][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_61,
      Q => dcvr_id(2),
      R => '0'
    );
\reg_reg[id][40]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_24,
      Q => dcvr_id(39),
      R => '0'
    );
\reg_reg[id][41]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_23,
      Q => dcvr_id(40),
      R => '0'
    );
\reg_reg[id][42]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_22,
      Q => dcvr_id(41),
      R => '0'
    );
\reg_reg[id][43]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_21,
      Q => dcvr_id(42),
      R => '0'
    );
\reg_reg[id][44]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_20,
      Q => dcvr_id(43),
      R => '0'
    );
\reg_reg[id][45]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_19,
      Q => dcvr_id(44),
      R => '0'
    );
\reg_reg[id][46]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_18,
      Q => dcvr_id(45),
      R => '0'
    );
\reg_reg[id][47]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_17,
      Q => dcvr_id(46),
      R => '0'
    );
\reg_reg[id][48]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_16,
      Q => dcvr_id(47),
      R => '0'
    );
\reg_reg[id][49]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_15,
      Q => dcvr_id(48),
      R => '0'
    );
\reg_reg[id][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_60,
      Q => dcvr_id(3),
      R => '0'
    );
\reg_reg[id][50]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_14,
      Q => dcvr_id(49),
      R => '0'
    );
\reg_reg[id][51]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_13,
      Q => dcvr_id(50),
      R => '0'
    );
\reg_reg[id][52]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_12,
      Q => dcvr_id(51),
      R => '0'
    );
\reg_reg[id][53]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_11,
      Q => dcvr_id(52),
      R => '0'
    );
\reg_reg[id][54]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_10,
      Q => dcvr_id(53),
      R => '0'
    );
\reg_reg[id][55]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_9,
      Q => dcvr_id(54),
      R => '0'
    );
\reg_reg[id][56]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_8,
      Q => dcvr_id(55),
      R => '0'
    );
\reg_reg[id][57]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_7,
      Q => dcvr_id(56),
      R => '0'
    );
\reg_reg[id][58]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_6,
      Q => dcvr_id(57),
      R => '0'
    );
\reg_reg[id][59]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_5,
      Q => dcvr_id(58),
      R => '0'
    );
\reg_reg[id][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_59,
      Q => dcvr_id(4),
      R => '0'
    );
\reg_reg[id][60]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_4,
      Q => dcvr_id(59),
      R => '0'
    );
\reg_reg[id][61]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_3,
      Q => dcvr_id(60),
      R => '0'
    );
\reg_reg[id][62]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_2,
      Q => dcvr_id(61),
      R => '0'
    );
\reg_reg[id][63]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_1,
      Q => dcvr_id(62),
      R => '0'
    );
\reg_reg[id][64]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_0,
      Q => dcvr_id(63),
      R => '0'
    );
\reg_reg[id][6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_58,
      Q => dcvr_id(5),
      R => '0'
    );
\reg_reg[id][7]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_57,
      Q => dcvr_id(6),
      R => '0'
    );
\reg_reg[id][8]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_56,
      Q => dcvr_id(7),
      R => '0'
    );
\reg_reg[id][9]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => crc_inst_n_55,
      Q => dcvr_id(8),
      R => '0'
    );
\reg_reg[id_bit]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[id_bit]_i_1_n_0\,
      Q => \reg_reg[id_bit_n_0_]\,
      R => '0'
    );
\reg_reg[id_bit_number][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[id_bit_number][6]_i_1_n_0\,
      D => \reg[id_bit_number][0]_i_1_n_0\,
      Q => \reg_reg[id_bit_number_n_0_][0]\,
      R => '0'
    );
\reg_reg[id_bit_number][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[id_bit_number][6]_i_1_n_0\,
      D => \reg[id_bit_number][1]_i_1_n_0\,
      Q => \reg_reg[id_bit_number_n_0_][1]\,
      R => '0'
    );
\reg_reg[id_bit_number][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[id_bit_number][6]_i_1_n_0\,
      D => \reg[id_bit_number][2]_i_1_n_0\,
      Q => \reg_reg[id_bit_number_n_0_][2]\,
      R => '0'
    );
\reg_reg[id_bit_number][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[id_bit_number][6]_i_1_n_0\,
      D => \reg[id_bit_number][3]_i_1_n_0\,
      Q => \reg_reg[id_bit_number_n_0_][3]\,
      R => '0'
    );
\reg_reg[id_bit_number][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[id_bit_number][6]_i_1_n_0\,
      D => \reg[id_bit_number][4]_i_1_n_0\,
      Q => \reg_reg[id_bit_number_n_0_][4]\,
      R => '0'
    );
\reg_reg[id_bit_number][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[id_bit_number][6]_i_1_n_0\,
      D => \reg[id_bit_number][5]_i_1_n_0\,
      Q => \reg_reg[id_bit_number_n_0_][5]\,
      R => '0'
    );
\reg_reg[id_bit_number][6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[id_bit_number][6]_i_1_n_0\,
      D => \reg[id_bit_number][6]_i_2_n_0\,
      Q => \reg_reg[id_bit_number_n_0_][6]\,
      R => '0'
    );
\reg_reg[id_en]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[id_en]\,
      Q => \^dcvr_id_en\,
      R => '0'
    );
\reg_reg[last_discrepancy][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[last_discrepancy][6]_i_1_n_0\,
      D => \reg[last_discrepancy][0]_i_1_n_0\,
      Q => \reg[last_discrepancy]\(0),
      R => '0'
    );
\reg_reg[last_discrepancy][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[last_discrepancy][6]_i_1_n_0\,
      D => \reg[last_discrepancy][1]_i_1_n_0\,
      Q => \reg[last_discrepancy]\(1),
      R => '0'
    );
\reg_reg[last_discrepancy][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[last_discrepancy][6]_i_1_n_0\,
      D => \reg[last_discrepancy][2]_i_1_n_0\,
      Q => \reg[last_discrepancy]\(2),
      R => '0'
    );
\reg_reg[last_discrepancy][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[last_discrepancy][6]_i_1_n_0\,
      D => \reg[last_discrepancy][3]_i_1_n_0\,
      Q => \reg[last_discrepancy]\(3),
      R => '0'
    );
\reg_reg[last_discrepancy][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[last_discrepancy][6]_i_1_n_0\,
      D => \reg[last_discrepancy][4]_i_1_n_0\,
      Q => \reg[last_discrepancy]\(4),
      R => '0'
    );
\reg_reg[last_discrepancy][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[last_discrepancy][6]_i_1_n_0\,
      D => \reg[last_discrepancy][5]_i_1_n_0\,
      Q => \reg[last_discrepancy]\(5),
      R => '0'
    );
\reg_reg[last_discrepancy][6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[last_discrepancy][6]_i_1_n_0\,
      D => \reg[last_discrepancy][6]_i_2_n_0\,
      Q => \reg[last_discrepancy]\(6),
      R => '0'
    );
\reg_reg[lfsr][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][3]_i_1_n_0\,
      D => \reg[lfsr][0]_i_1__0_n_0\,
      Q => lfsr(0),
      R => '0'
    );
\reg_reg[lfsr][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][3]_i_1_n_0\,
      D => \reg[lfsr][1]_i_1_n_0\,
      Q => lfsr(1),
      R => '0'
    );
\reg_reg[lfsr][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][3]_i_1_n_0\,
      D => \reg[lfsr][2]_i_1_n_0\,
      Q => lfsr(2),
      R => '0'
    );
\reg_reg[lfsr][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[lfsr][3]_i_1_n_0\,
      D => \reg[lfsr][3]_i_2_n_0\,
      Q => lfsr(3),
      R => '0'
    );
\reg_reg[marker][0]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[marker][6]_i_1_n_0\,
      D => \reg[marker][0]_i_1_n_0\,
      Q => \reg_reg[marker_n_0_][0]\,
      R => '0'
    );
\reg_reg[marker][1]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[marker][6]_i_1_n_0\,
      D => \reg[marker][1]_i_1_n_0\,
      Q => \reg_reg[marker_n_0_][1]\,
      R => '0'
    );
\reg_reg[marker][2]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[marker][6]_i_1_n_0\,
      D => \reg[marker][2]_i_1_n_0\,
      Q => \reg_reg[marker_n_0_][2]\,
      R => '0'
    );
\reg_reg[marker][3]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[marker][6]_i_1_n_0\,
      D => \reg[marker][3]_i_1_n_0\,
      Q => \reg_reg[marker_n_0_][3]\,
      R => '0'
    );
\reg_reg[marker][4]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[marker][6]_i_1_n_0\,
      D => \reg[marker][4]_i_1_n_0\,
      Q => \reg_reg[marker_n_0_][4]\,
      R => '0'
    );
\reg_reg[marker][5]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[marker][6]_i_1_n_0\,
      D => \reg[marker][5]_i_1_n_0\,
      Q => \reg_reg[marker_n_0_][5]\,
      R => '0'
    );
\reg_reg[marker][6]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => \reg[marker][6]_i_1_n_0\,
      D => \reg[marker][6]_i_2_n_0\,
      Q => \reg_reg[marker_n_0_][6]\,
      R => '0'
    );
\reg_reg[search]\: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => \reg[search]_i_1_n_0\,
      Q => \^dcvr_bit_tx\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_onewire_idtemp is
  port (
    ADDRD : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ow_o : out STD_LOGIC;
    \reg_reg[busy]\ : out STD_LOGIC;
    \reg_reg[too_many]\ : out STD_LOGIC;
    ctrl_rd_en : out STD_LOGIC;
    rd_data_en_reg : out STD_LOGIC;
    ow_rd_data : out STD_LOGIC_VECTOR ( 63 downto 0 );
    \reg_reg[device_count][0]\ : out STD_LOGIC;
    \reg_reg[device_count][1]\ : out STD_LOGIC;
    \reg_reg[device_count][2]\ : out STD_LOGIC;
    \reg_reg[device_count][3]\ : out STD_LOGIC;
    \reg_reg[device_count][4]\ : out STD_LOGIC;
    ow_dev_count : out STD_LOGIC_VECTOR ( 0 to 0 );
    ow_pullup_o : out STD_LOGIC;
    \FSM_sequential_ow_state_reg[0]\ : out STD_LOGIC;
    \FSM_sequential_ow_state_reg[0]_0\ : out STD_LOGIC;
    ow_done_o_reg : out STD_LOGIC;
    \FSM_sequential_ow_state_reg[1]\ : out STD_LOGIC;
    \reg_reg[too_many]_0\ : out STD_LOGIC;
    \reg_reg[done]\ : out STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    ADDRA : in STD_LOGIC_VECTOR ( 5 downto 0 );
    rd_data_en : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    ow_reg_rst : in STD_LOGIC;
    ow_rst : in STD_LOGIC;
    ow_i : in STD_LOGIC;
    ow_temp_trig_reg : in STD_LOGIC;
    ow_disc_trig_reg : in STD_LOGIC;
    \ow_state__0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ow_trig : in STD_LOGIC;
    ow_done_o : in STD_LOGIC;
    ow_busy_o_reg : in STD_LOGIC;
    ow_busy_o : in STD_LOGIC;
    \rd_data_reg[63]\ : in STD_LOGIC
  );
end design_1_axi_onewire_0_0_onewire_idtemp;

architecture STRUCTURE of design_1_axi_onewire_0_0_onewire_idtemp is
  signal \^addrd\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal ctrl_bit_send : STD_LOGIC;
  signal ctrl_bit_tx : STD_LOGIC;
  signal ctrl_bus_rst : STD_LOGIC;
  signal ctrl_inst_n_84 : STD_LOGIC;
  signal ctrl_inst_n_85 : STD_LOGIC;
  signal ctrl_wr_data : STD_LOGIC_VECTOR ( 63 downto 1 );
  signal dcvr_bit_recv : STD_LOGIC;
  signal dcvr_bit_send : STD_LOGIC;
  signal dcvr_bit_tx : STD_LOGIC;
  signal dcvr_bus_rst : STD_LOGIC;
  signal dcvr_done : STD_LOGIC;
  signal dcvr_id_en : STD_LOGIC;
  signal dcvr_start : STD_LOGIC;
  signal discover_inst_n_6 : STD_LOGIC;
  signal discover_inst_n_7 : STD_LOGIC;
  signal if_done : STD_LOGIC;
  signal if_rx_data : STD_LOGIC;
  signal if_rx_data_en : STD_LOGIC;
  signal interface_inst_n_4 : STD_LOGIC;
  signal interface_inst_n_5 : STD_LOGIC;
  signal mem_wr_done : STD_LOGIC;
  signal \^ow_rd_data\ : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal \ram_gen.ram_inst_n_2\ : STD_LOGIC;
  signal \ram_gen.ram_inst_n_67\ : STD_LOGIC;
  signal \^rd_data_en_reg\ : STD_LOGIC;
  signal \reg[state]\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^reg_reg[busy]\ : STD_LOGIC;
  signal wr_data_i : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal wr_done : STD_LOGIC;
begin
  ADDRD(5 downto 0) <= \^addrd\(5 downto 0);
  ow_rd_data(63 downto 0) <= \^ow_rd_data\(63 downto 0);
  rd_data_en_reg <= \^rd_data_en_reg\;
  \reg_reg[busy]\ <= \^reg_reg[busy]\;
ctrl_inst: entity work.design_1_axi_onewire_0_0_onewire_control
     port map (
      \FSM_sequential_ow_state_reg[0]\ => \FSM_sequential_ow_state_reg[0]\,
      \FSM_sequential_ow_state_reg[0]_0\ => \FSM_sequential_ow_state_reg[0]_0\,
      \FSM_sequential_ow_state_reg[1]\ => \FSM_sequential_ow_state_reg[1]\,
      \FSM_sequential_reg[state][3]_i_6_0\ => \^rd_data_en_reg\,
      \FSM_sequential_reg_reg[state][2]_0\(1) => \reg[state]\(2),
      \FSM_sequential_reg_reg[state][2]_0\(0) => \reg[state]\(0),
      \FSM_sequential_reg_reg[state][2]_1\ => \ram_gen.ram_inst_n_2\,
      axi_aresetn_i => axi_aresetn_i,
      axi_clk_i => axi_clk_i,
      ctrl_bit_send => ctrl_bit_send,
      ctrl_bit_tx => ctrl_bit_tx,
      ctrl_bus_rst => ctrl_bus_rst,
      ctrl_rd_en => ctrl_rd_en,
      ctrl_wr_data(62 downto 0) => ctrl_wr_data(63 downto 1),
      dcvr_bit_recv => dcvr_bit_recv,
      dcvr_bit_send => dcvr_bit_send,
      dcvr_done => dcvr_done,
      dcvr_id_en => dcvr_id_en,
      dcvr_start => dcvr_start,
      if_done => if_done,
      if_rx_data => if_rx_data,
      if_rx_data_en => if_rx_data_en,
      mem_wr_done => mem_wr_done,
      ow_busy_o => ow_busy_o,
      ow_busy_o_reg => ow_busy_o_reg,
      ow_dev_count(0) => ow_dev_count(0),
      ow_disc_trig_reg => ow_disc_trig_reg,
      ow_done_o => ow_done_o,
      ow_done_o_reg => ow_done_o_reg,
      ow_pullup_o => ow_pullup_o,
      ow_rd_data(63 downto 0) => \^ow_rd_data\(63 downto 0),
      ow_reg_rst => ow_reg_rst,
      ow_rst => ow_rst,
      \ow_state__0\(1 downto 0) => \ow_state__0\(1 downto 0),
      ow_temp_trig_reg => ow_temp_trig_reg,
      ow_trig => ow_trig,
      \reg_reg[bit_recv]_0\ => ctrl_inst_n_84,
      \reg_reg[bit_recv]_1\ => ctrl_inst_n_85,
      \reg_reg[bit_send]_0\ => interface_inst_n_4,
      \reg_reg[busy]_0\ => \^reg_reg[busy]\,
      \reg_reg[data][30]_0\ => interface_inst_n_5,
      \reg_reg[device_count][0]_0\ => \reg_reg[device_count][0]\,
      \reg_reg[device_count][1]_0\ => \reg_reg[device_count][1]\,
      \reg_reg[device_count][2]_0\ => \reg_reg[device_count][2]\,
      \reg_reg[device_count][3]_0\ => \reg_reg[device_count][3]\,
      \reg_reg[device_count][4]_0\ => \reg_reg[device_count][4]\,
      \reg_reg[done]_0\ => \reg_reg[done]\,
      \reg_reg[mem_addr][0]_0\ => \^addrd\(0),
      \reg_reg[mem_addr][0]_1\ => \ram_gen.ram_inst_n_67\,
      \reg_reg[mem_addr][1]_0\ => \^addrd\(1),
      \reg_reg[mem_addr][2]_0\ => \^addrd\(2),
      \reg_reg[mem_addr][3]_0\ => \^addrd\(3),
      \reg_reg[mem_addr][4]_0\ => \^addrd\(4),
      \reg_reg[mem_addr][5]_0\ => \^addrd\(5),
      \reg_reg[too_many]_0\ => \reg_reg[too_many]\,
      \reg_reg[too_many]_1\ => \reg_reg[too_many]_0\,
      wr_done => wr_done
    );
discover_inst: entity work.design_1_axi_onewire_0_0_onewire_discover
     port map (
      axi_aresetn_i => axi_aresetn_i,
      axi_clk_i => axi_clk_i,
      ctrl_bit_send => ctrl_bit_send,
      ctrl_bit_tx => ctrl_bit_tx,
      ctrl_bus_rst => ctrl_bus_rst,
      ctrl_wr_data(62 downto 0) => ctrl_wr_data(63 downto 1),
      dcvr_bit_recv => dcvr_bit_recv,
      dcvr_bit_send => dcvr_bit_send,
      dcvr_bit_tx => dcvr_bit_tx,
      dcvr_bus_rst => dcvr_bus_rst,
      dcvr_done => dcvr_done,
      dcvr_id_en => dcvr_id_en,
      dcvr_start => dcvr_start,
      if_done => if_done,
      if_rx_data => if_rx_data,
      if_rx_data_en => if_rx_data_en,
      ow_reg_rst => ow_reg_rst,
      ow_rst => ow_rst,
      \reg_reg[bit_send]_0\ => discover_inst_n_7,
      \reg_reg[bus_rst]_0\ => discover_inst_n_6,
      wr_data_i(63 downto 0) => wr_data_i(63 downto 0)
    );
interface_inst: entity work.design_1_axi_onewire_0_0_onewire_interface
     port map (
      \FSM_sequential_reg_reg[state][0]_0\ => ctrl_inst_n_84,
      \FSM_sequential_reg_reg[state][1]_0\ => ctrl_inst_n_85,
      axi_aresetn_i => axi_aresetn_i,
      axi_clk_i => axi_clk_i,
      ctrl_bit_tx => ctrl_bit_tx,
      ctrl_bus_rst => ctrl_bus_rst,
      dcvr_bit_tx => dcvr_bit_tx,
      dcvr_bus_rst => dcvr_bus_rst,
      if_done => if_done,
      if_rx_data => if_rx_data,
      if_rx_data_en => if_rx_data_en,
      ow_i => ow_i,
      ow_o => ow_o,
      ow_reg_rst => ow_reg_rst,
      ow_rst => ow_rst,
      \reg_reg[bit_send]\(1) => \reg[state]\(2),
      \reg_reg[bit_send]\(0) => \reg[state]\(0),
      \reg_reg[data]_0\ => discover_inst_n_6,
      \reg_reg[data]_1\ => discover_inst_n_7,
      \reg_reg[done]_0\ => interface_inst_n_4,
      \reg_reg[done]_1\ => interface_inst_n_5
    );
\ram_gen.ram_inst\: entity work.design_1_axi_onewire_0_0_two_port_ram
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => \^addrd\(5 downto 0),
      axi_aresetn_i => axi_aresetn_i,
      axi_clk_i => axi_clk_i,
      mem_wr_done => mem_wr_done,
      ow_rd_data(63 downto 0) => \^ow_rd_data\(63 downto 0),
      ow_reg_rst => ow_reg_rst,
      rd_data_en => rd_data_en,
      rd_data_en_reg_0 => \^rd_data_en_reg\,
      rd_data_en_reg_1 => \ram_gen.ram_inst_n_67\,
      \rd_data_reg[3]_0\ => \ram_gen.ram_inst_n_2\,
      \rd_data_reg[63]_0\ => \rd_data_reg[63]\,
      \reg[mem_addr][5]_i_4\ => \^reg_reg[busy]\,
      wr_data_i(63 downto 0) => wr_data_i(63 downto 0),
      wr_done => wr_done
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_onewire is
  port (
    ctrl_rd_addr : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ow_o : out STD_LOGIC;
    ow_busy : out STD_LOGIC;
    ow_too_many : out STD_LOGIC;
    ctrl_rd_en : out STD_LOGIC;
    mem_rd_data_en : out STD_LOGIC;
    ow_busy_o : out STD_LOGIC;
    ow_done_o : out STD_LOGIC;
    ow_rd_data : out STD_LOGIC_VECTOR ( 63 downto 0 );
    ow_dev_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ow_pullup_o : out STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    ADDRA : in STD_LOGIC_VECTOR ( 5 downto 0 );
    rd_data_en : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    ow_reg_rst : in STD_LOGIC;
    ow_rst : in STD_LOGIC;
    ow_i : in STD_LOGIC;
    ow_trig : in STD_LOGIC;
    \rd_data_reg[63]\ : in STD_LOGIC
  );
end design_1_axi_onewire_0_0_onewire;

architecture STRUCTURE of design_1_axi_onewire_0_0_onewire is
  signal \^ow_busy_o\ : STD_LOGIC;
  signal ow_busy_o_i_2_n_0 : STD_LOGIC;
  signal ow_disc_trig_reg_n_0 : STD_LOGIC;
  signal \^ow_done_o\ : STD_LOGIC;
  signal \ow_state__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ow_temp_trig_reg_n_0 : STD_LOGIC;
  signal x_bus_master_n_82 : STD_LOGIC;
  signal x_bus_master_n_83 : STD_LOGIC;
  signal x_bus_master_n_84 : STD_LOGIC;
  signal x_bus_master_n_85 : STD_LOGIC;
  signal x_bus_master_n_86 : STD_LOGIC;
  signal x_bus_master_n_87 : STD_LOGIC;
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_ow_state_reg[0]\ : label is "discovery:01,temperature_read:10,idle:00";
  attribute FSM_ENCODED_STATES of \FSM_sequential_ow_state_reg[1]\ : label is "discovery:01,temperature_read:10,idle:00";
begin
  ow_busy_o <= \^ow_busy_o\;
  ow_done_o <= \^ow_done_o\;
\FSM_sequential_ow_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => '1',
      D => x_bus_master_n_87,
      Q => \ow_state__0\(0),
      R => '0'
    );
\FSM_sequential_ow_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => axi_clk_i,
      CE => '1',
      D => x_bus_master_n_86,
      Q => \ow_state__0\(1),
      R => '0'
    );
ow_busy_o_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \ow_state__0\(0),
      I1 => axi_aresetn_i,
      I2 => ow_reg_rst,
      O => ow_busy_o_i_2_n_0
    );
ow_busy_o_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => x_bus_master_n_85,
      Q => \^ow_busy_o\,
      R => '0'
    );
ow_disc_trig_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => x_bus_master_n_82,
      Q => ow_disc_trig_reg_n_0,
      R => '0'
    );
ow_done_o_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => x_bus_master_n_84,
      Q => \^ow_done_o\,
      R => '0'
    );
ow_temp_trig_reg: unisim.vcomponents.FDRE
     port map (
      C => axi_clk_i,
      CE => '1',
      D => x_bus_master_n_83,
      Q => ow_temp_trig_reg_n_0,
      R => '0'
    );
x_bus_master: entity work.design_1_axi_onewire_0_0_onewire_idtemp
     port map (
      ADDRA(5 downto 0) => ADDRA(5 downto 0),
      ADDRD(5 downto 0) => ctrl_rd_addr(5 downto 0),
      \FSM_sequential_ow_state_reg[0]\ => x_bus_master_n_82,
      \FSM_sequential_ow_state_reg[0]_0\ => x_bus_master_n_83,
      \FSM_sequential_ow_state_reg[1]\ => x_bus_master_n_85,
      axi_aresetn_i => axi_aresetn_i,
      axi_clk_i => axi_clk_i,
      ctrl_rd_en => ctrl_rd_en,
      ow_busy_o => \^ow_busy_o\,
      ow_busy_o_reg => ow_busy_o_i_2_n_0,
      ow_dev_count(0) => ow_dev_count(5),
      ow_disc_trig_reg => ow_disc_trig_reg_n_0,
      ow_done_o => \^ow_done_o\,
      ow_done_o_reg => x_bus_master_n_84,
      ow_i => ow_i,
      ow_o => ow_o,
      ow_pullup_o => ow_pullup_o,
      ow_rd_data(63 downto 0) => ow_rd_data(63 downto 0),
      ow_reg_rst => ow_reg_rst,
      ow_rst => ow_rst,
      \ow_state__0\(1 downto 0) => \ow_state__0\(1 downto 0),
      ow_temp_trig_reg => ow_temp_trig_reg_n_0,
      ow_trig => ow_trig,
      rd_data_en => rd_data_en,
      rd_data_en_reg => mem_rd_data_en,
      \rd_data_reg[63]\ => \rd_data_reg[63]\,
      \reg_reg[busy]\ => ow_busy,
      \reg_reg[device_count][0]\ => ow_dev_count(0),
      \reg_reg[device_count][1]\ => ow_dev_count(1),
      \reg_reg[device_count][2]\ => ow_dev_count(2),
      \reg_reg[device_count][3]\ => ow_dev_count(3),
      \reg_reg[device_count][4]\ => ow_dev_count(4),
      \reg_reg[done]\ => x_bus_master_n_87,
      \reg_reg[too_many]\ => ow_too_many,
      \reg_reg[too_many]_0\ => x_bus_master_n_86
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0_axi_onewire is
  port (
    ow_en_o : out STD_LOGIC;
    ow_ctrl_o : out STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_rdata_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_rvalid_o : out STD_LOGIC;
    axi_awready_o : out STD_LOGIC;
    axi_wready_o : out STD_LOGIC;
    axi_arready_o : out STD_LOGIC;
    ow_o : out STD_LOGIC;
    ow_pullup_o : out STD_LOGIC;
    axi_wdone_reg : out STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    axi_aresetn_i : in STD_LOGIC;
    axi_awaddr_i : in STD_LOGIC_VECTOR ( 8 downto 0 );
    axi_araddr_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    axi_wdata_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_awvalid_i : in STD_LOGIC;
    axi_wvalid_i : in STD_LOGIC;
    axi_arvalid_i : in STD_LOGIC;
    axi_rready_i : in STD_LOGIC;
    ow_i : in STD_LOGIC;
    axi_bready_i : in STD_LOGIC
  );
end design_1_axi_onewire_0_0_axi_onewire;

architecture STRUCTURE of design_1_axi_onewire_0_0_axi_onewire is
  signal axi_onewire_regs_n_5 : STD_LOGIC;
  signal ow_busy : STD_LOGIC;
  signal ow_busy_o : STD_LOGIC;
  signal ow_dev_count : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal ow_done_o : STD_LOGIC;
  signal ow_rd_data : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal ow_reg_rst : STD_LOGIC;
  signal ow_rst : STD_LOGIC;
  signal ow_too_many : STD_LOGIC;
  signal ow_trig : STD_LOGIC;
  signal \x_bus_master/ctrl_rd_addr\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \x_bus_master/ctrl_rd_en\ : STD_LOGIC;
  signal \x_bus_master/mem_rd_addr\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \x_bus_master/mem_rd_data_en\ : STD_LOGIC;
  signal \x_bus_master/ram_gen.ram_inst/rd_data_en\ : STD_LOGIC;
begin
axi_onewire_regs: entity work.design_1_axi_onewire_0_0_onewire_regs_axi
     port map (
      ADDRA(5 downto 0) => \x_bus_master/mem_rd_addr\(5 downto 0),
      axi_araddr_i(7 downto 0) => axi_araddr_i(7 downto 0),
      axi_aresetn_i => axi_aresetn_i,
      axi_arready_o => axi_arready_o,
      axi_arvalid_i => axi_arvalid_i,
      axi_awaddr_i(8 downto 0) => axi_awaddr_i(8 downto 0),
      axi_awready_o => axi_awready_o,
      axi_awvalid_i => axi_awvalid_i,
      axi_bready_i => axi_bready_i,
      axi_clk_i => axi_clk_i,
      axi_rdata_o(31 downto 0) => axi_rdata_o(31 downto 0),
      axi_rready_i => axi_rready_i,
      axi_rvalid_o => axi_rvalid_o,
      axi_wdata_i(3 downto 0) => axi_wdata_i(3 downto 0),
      axi_wdone_reg_0 => axi_wdone_reg,
      axi_wready_o => axi_wready_o,
      axi_wvalid_i => axi_wvalid_i,
      ctrl_rd_addr(5 downto 0) => \x_bus_master/ctrl_rd_addr\(5 downto 0),
      ctrl_rd_en => \x_bus_master/ctrl_rd_en\,
      mem_rd_data_en => \x_bus_master/mem_rd_data_en\,
      mem_readout_rt_reg_0 => axi_onewire_regs_n_5,
      ow_busy => ow_busy,
      ow_busy_o => ow_busy_o,
      ow_ctrl_o(2 downto 0) => ow_ctrl_o(2 downto 0),
      ow_dev_count(5 downto 0) => ow_dev_count(5 downto 0),
      ow_done_o => ow_done_o,
      ow_en_o => ow_en_o,
      ow_rd_data(63 downto 0) => ow_rd_data(63 downto 0),
      ow_reg_rst => ow_reg_rst,
      ow_rst => ow_rst,
      ow_too_many => ow_too_many,
      ow_trig => ow_trig,
      rd_data_en => \x_bus_master/ram_gen.ram_inst/rd_data_en\
    );
onewire_if: entity work.design_1_axi_onewire_0_0_onewire
     port map (
      ADDRA(5 downto 0) => \x_bus_master/mem_rd_addr\(5 downto 0),
      axi_aresetn_i => axi_aresetn_i,
      axi_clk_i => axi_clk_i,
      ctrl_rd_addr(5 downto 0) => \x_bus_master/ctrl_rd_addr\(5 downto 0),
      ctrl_rd_en => \x_bus_master/ctrl_rd_en\,
      mem_rd_data_en => \x_bus_master/mem_rd_data_en\,
      ow_busy => ow_busy,
      ow_busy_o => ow_busy_o,
      ow_dev_count(5 downto 0) => ow_dev_count(5 downto 0),
      ow_done_o => ow_done_o,
      ow_i => ow_i,
      ow_o => ow_o,
      ow_pullup_o => ow_pullup_o,
      ow_rd_data(63 downto 0) => ow_rd_data(63 downto 0),
      ow_reg_rst => ow_reg_rst,
      ow_rst => ow_rst,
      ow_too_many => ow_too_many,
      ow_trig => ow_trig,
      rd_data_en => \x_bus_master/ram_gen.ram_inst/rd_data_en\,
      \rd_data_reg[63]\ => axi_onewire_regs_n_5
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_axi_onewire_0_0 is
  port (
    axi_aresetn_i : in STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    axi_awvalid_i : in STD_LOGIC;
    axi_awready_o : out STD_LOGIC;
    axi_awaddr_i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_awprot_i : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_wvalid_i : in STD_LOGIC;
    axi_wready_o : out STD_LOGIC;
    axi_wdata_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_wstrb_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_bvalid_o : out STD_LOGIC;
    axi_bready_i : in STD_LOGIC;
    axi_bresp_o : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_arvalid_i : in STD_LOGIC;
    axi_arready_o : out STD_LOGIC;
    axi_araddr_i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_arprot_i : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_rvalid_o : out STD_LOGIC;
    axi_rready_i : in STD_LOGIC;
    axi_rdata_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_rresp_o : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ow_i : in STD_LOGIC;
    ow_o : out STD_LOGIC;
    ow_pullup_o : out STD_LOGIC;
    ow_ctrl_o : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ow_en_o : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_axi_onewire_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_axi_onewire_0_0 : entity is "design_1_axi_onewire_0_0,axi_onewire,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_axi_onewire_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_axi_onewire_0_0 : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of design_1_axi_onewire_0_0 : entity is "axi_onewire,Vivado 2022.2";
end design_1_axi_onewire_0_0;

architecture STRUCTURE of design_1_axi_onewire_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of axi_arready_o : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  attribute x_interface_info of axi_arvalid_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  attribute x_interface_info of axi_awready_o : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  attribute x_interface_info of axi_awvalid_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of axi_awvalid_i : signal is "XIL_INTERFACENAME S_AXI, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 11, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of axi_bready_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  attribute x_interface_info of axi_bvalid_o : signal is "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  attribute x_interface_info of axi_rready_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI RREADY";
  attribute x_interface_info of axi_rvalid_o : signal is "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  attribute x_interface_info of axi_wready_o : signal is "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  attribute x_interface_info of axi_wvalid_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  attribute x_interface_info of axi_araddr_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  attribute x_interface_info of axi_arprot_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARPROT";
  attribute x_interface_info of axi_awaddr_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  attribute x_interface_info of axi_awprot_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWPROT";
  attribute x_interface_info of axi_bresp_o : signal is "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  attribute x_interface_info of axi_rdata_o : signal is "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  attribute x_interface_info of axi_rresp_o : signal is "xilinx.com:interface:aximm:1.0 S_AXI RRESP";
  attribute x_interface_info of axi_wdata_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  attribute x_interface_info of axi_wstrb_i : signal is "xilinx.com:interface:aximm:1.0 S_AXI WSTRB";
begin
  axi_bresp_o(1) <= \<const0>\;
  axi_bresp_o(0) <= \<const0>\;
  axi_rresp_o(1) <= \<const0>\;
  axi_rresp_o(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.design_1_axi_onewire_0_0_axi_onewire
     port map (
      axi_araddr_i(7) => axi_araddr_i(10),
      axi_araddr_i(6 downto 0) => axi_araddr_i(8 downto 2),
      axi_aresetn_i => axi_aresetn_i,
      axi_arready_o => axi_arready_o,
      axi_arvalid_i => axi_arvalid_i,
      axi_awaddr_i(8 downto 0) => axi_awaddr_i(10 downto 2),
      axi_awready_o => axi_awready_o,
      axi_awvalid_i => axi_awvalid_i,
      axi_bready_i => axi_bready_i,
      axi_clk_i => axi_clk_i,
      axi_rdata_o(31 downto 0) => axi_rdata_o(31 downto 0),
      axi_rready_i => axi_rready_i,
      axi_rvalid_o => axi_rvalid_o,
      axi_wdata_i(3 downto 0) => axi_wdata_i(3 downto 0),
      axi_wdone_reg => axi_bvalid_o,
      axi_wready_o => axi_wready_o,
      axi_wvalid_i => axi_wvalid_i,
      ow_ctrl_o(2 downto 0) => ow_ctrl_o(2 downto 0),
      ow_en_o => ow_en_o,
      ow_i => ow_i,
      ow_o => ow_o,
      ow_pullup_o => ow_pullup_o
    );
end STRUCTURE;
