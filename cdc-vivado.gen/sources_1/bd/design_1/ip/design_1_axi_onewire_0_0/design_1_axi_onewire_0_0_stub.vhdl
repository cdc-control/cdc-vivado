-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Thu Mar  2 11:27:44 2023
-- Host        : PCSY219 running 64-bit unknown
-- Command     : write_vhdl -force -mode synth_stub -rename_top design_1_axi_onewire_0_0 -prefix
--               design_1_axi_onewire_0_0_ design_1_axi_onewire_0_0_stub.vhdl
-- Design      : design_1_axi_onewire_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z007sclg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_axi_onewire_0_0 is
  Port ( 
    axi_aresetn_i : in STD_LOGIC;
    axi_clk_i : in STD_LOGIC;
    axi_awvalid_i : in STD_LOGIC;
    axi_awready_o : out STD_LOGIC;
    axi_awaddr_i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_awprot_i : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_wvalid_i : in STD_LOGIC;
    axi_wready_o : out STD_LOGIC;
    axi_wdata_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_wstrb_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_bvalid_o : out STD_LOGIC;
    axi_bready_i : in STD_LOGIC;
    axi_bresp_o : out STD_LOGIC_VECTOR ( 1 downto 0 );
    axi_arvalid_i : in STD_LOGIC;
    axi_arready_o : out STD_LOGIC;
    axi_araddr_i : in STD_LOGIC_VECTOR ( 10 downto 0 );
    axi_arprot_i : in STD_LOGIC_VECTOR ( 2 downto 0 );
    axi_rvalid_o : out STD_LOGIC;
    axi_rready_i : in STD_LOGIC;
    axi_rdata_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_rresp_o : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ow_i : in STD_LOGIC;
    ow_o : out STD_LOGIC;
    ow_pullup_o : out STD_LOGIC;
    ow_ctrl_o : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ow_en_o : out STD_LOGIC
  );

end design_1_axi_onewire_0_0;

architecture stub of design_1_axi_onewire_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "axi_aresetn_i,axi_clk_i,axi_awvalid_i,axi_awready_o,axi_awaddr_i[10:0],axi_awprot_i[2:0],axi_wvalid_i,axi_wready_o,axi_wdata_i[31:0],axi_wstrb_i[3:0],axi_bvalid_o,axi_bready_i,axi_bresp_o[1:0],axi_arvalid_i,axi_arready_o,axi_araddr_i[10:0],axi_arprot_i[2:0],axi_rvalid_o,axi_rready_i,axi_rdata_o[31:0],axi_rresp_o[1:0],ow_i,ow_o,ow_pullup_o,ow_ctrl_o[2:0],ow_en_o";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "axi_onewire,Vivado 2022.2";
begin
end;
