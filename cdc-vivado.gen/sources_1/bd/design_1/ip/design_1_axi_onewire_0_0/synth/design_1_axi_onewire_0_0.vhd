-- (c) Copyright 1995-2023 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:module_ref:axi_onewire:1.0
-- IP Revision: 1

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_axi_onewire_0_0 IS
  PORT (
    axi_aresetn_i : IN STD_LOGIC;
    axi_clk_i : IN STD_LOGIC;
    axi_awvalid_i : IN STD_LOGIC;
    axi_awready_o : OUT STD_LOGIC;
    axi_awaddr_i : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
    axi_awprot_i : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    axi_wvalid_i : IN STD_LOGIC;
    axi_wready_o : OUT STD_LOGIC;
    axi_wdata_i : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    axi_wstrb_i : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    axi_bvalid_o : OUT STD_LOGIC;
    axi_bready_i : IN STD_LOGIC;
    axi_bresp_o : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    axi_arvalid_i : IN STD_LOGIC;
    axi_arready_o : OUT STD_LOGIC;
    axi_araddr_i : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
    axi_arprot_i : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    axi_rvalid_o : OUT STD_LOGIC;
    axi_rready_i : IN STD_LOGIC;
    axi_rdata_o : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    axi_rresp_o : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ow_i : IN STD_LOGIC;
    ow_o : OUT STD_LOGIC;
    ow_pullup_o : OUT STD_LOGIC;
    ow_ctrl_o : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ow_en_o : OUT STD_LOGIC
  );
END design_1_axi_onewire_0_0;

ARCHITECTURE design_1_axi_onewire_0_0_arch OF design_1_axi_onewire_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_axi_onewire_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT axi_onewire IS
    GENERIC (
      clk_frequency_g : INTEGER;
      max_devices_g : INTEGER;
      invert_tx_g : BOOLEAN;
      invert_rx_g : BOOLEAN;
      invert_pullup_g : BOOLEAN;
      use_tmr_bram_g : BOOLEAN
    );
    PORT (
      axi_aresetn_i : IN STD_LOGIC;
      axi_clk_i : IN STD_LOGIC;
      axi_awvalid_i : IN STD_LOGIC;
      axi_awready_o : OUT STD_LOGIC;
      axi_awaddr_i : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
      axi_awprot_i : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      axi_wvalid_i : IN STD_LOGIC;
      axi_wready_o : OUT STD_LOGIC;
      axi_wdata_i : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      axi_wstrb_i : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      axi_bvalid_o : OUT STD_LOGIC;
      axi_bready_i : IN STD_LOGIC;
      axi_bresp_o : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      axi_arvalid_i : IN STD_LOGIC;
      axi_arready_o : OUT STD_LOGIC;
      axi_araddr_i : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
      axi_arprot_i : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      axi_rvalid_o : OUT STD_LOGIC;
      axi_rready_i : IN STD_LOGIC;
      axi_rdata_o : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      axi_rresp_o : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      ow_i : IN STD_LOGIC;
      ow_o : OUT STD_LOGIC;
      ow_pullup_o : OUT STD_LOGIC;
      ow_ctrl_o : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      ow_en_o : OUT STD_LOGIC
    );
  END COMPONENT axi_onewire;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_axi_onewire_0_0_arch: ARCHITECTURE IS "axi_onewire,Vivado 2022.2";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_axi_onewire_0_0_arch : ARCHITECTURE IS "design_1_axi_onewire_0_0,axi_onewire,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF design_1_axi_onewire_0_0_arch: ARCHITECTURE IS "design_1_axi_onewire_0_0,axi_onewire,{x_ipProduct=Vivado 2022.2,x_ipVendor=xilinx.com,x_ipLibrary=module_ref,x_ipName=axi_onewire,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED,clk_frequency_g=100000000,max_devices_g=32,invert_tx_g=false,invert_rx_g=false,invert_pullup_g=true,use_tmr_bram_g=false}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF design_1_axi_onewire_0_0_arch: ARCHITECTURE IS "module_ref";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF axi_araddr_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF axi_arprot_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF axi_arready_o: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF axi_arvalid_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF axi_awaddr_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  ATTRIBUTE X_INTERFACE_INFO OF axi_awprot_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI AWPROT";
  ATTRIBUTE X_INTERFACE_INFO OF axi_awready_o: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  ATTRIBUTE X_INTERFACE_PARAMETER OF axi_awvalid_i: SIGNAL IS "XIL_INTERFACENAME S_AXI, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 11, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_B" & 
"ITS_PER_BYTE 0, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF axi_awvalid_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF axi_bready_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF axi_bresp_o: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF axi_bvalid_o: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF axi_rdata_o: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF axi_rready_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF axi_rresp_o: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF axi_rvalid_o: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF axi_wdata_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF axi_wready_o: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF axi_wstrb_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF axi_wvalid_i: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
BEGIN
  U0 : axi_onewire
    GENERIC MAP (
      clk_frequency_g => 100000000,
      max_devices_g => 32,
      invert_tx_g => false,
      invert_rx_g => false,
      invert_pullup_g => true,
      use_tmr_bram_g => false
    )
    PORT MAP (
      axi_aresetn_i => axi_aresetn_i,
      axi_clk_i => axi_clk_i,
      axi_awvalid_i => axi_awvalid_i,
      axi_awready_o => axi_awready_o,
      axi_awaddr_i => axi_awaddr_i,
      axi_awprot_i => axi_awprot_i,
      axi_wvalid_i => axi_wvalid_i,
      axi_wready_o => axi_wready_o,
      axi_wdata_i => axi_wdata_i,
      axi_wstrb_i => axi_wstrb_i,
      axi_bvalid_o => axi_bvalid_o,
      axi_bready_i => axi_bready_i,
      axi_bresp_o => axi_bresp_o,
      axi_arvalid_i => axi_arvalid_i,
      axi_arready_o => axi_arready_o,
      axi_araddr_i => axi_araddr_i,
      axi_arprot_i => axi_arprot_i,
      axi_rvalid_o => axi_rvalid_o,
      axi_rready_i => axi_rready_i,
      axi_rdata_o => axi_rdata_o,
      axi_rresp_o => axi_rresp_o,
      ow_i => ow_i,
      ow_o => ow_o,
      ow_pullup_o => ow_pullup_o,
      ow_ctrl_o => ow_ctrl_o,
      ow_en_o => ow_en_o
    );
END design_1_axi_onewire_0_0_arch;
