// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Thu Mar  2 11:27:44 2023
// Host        : PCSY219 running 64-bit unknown
// Command     : write_verilog -force -mode funcsim -rename_top design_1_axi_onewire_0_0 -prefix
//               design_1_axi_onewire_0_0_ design_1_axi_onewire_0_0_sim_netlist.v
// Design      : design_1_axi_onewire_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z007sclg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_axi_onewire_0_0_axi_onewire
   (ow_en_o,
    ow_ctrl_o,
    axi_rdata_o,
    axi_rvalid_o,
    axi_awready_o,
    axi_wready_o,
    axi_arready_o,
    ow_o,
    ow_pullup_o,
    axi_wdone_reg,
    axi_clk_i,
    axi_aresetn_i,
    axi_awaddr_i,
    axi_araddr_i,
    axi_wdata_i,
    axi_awvalid_i,
    axi_wvalid_i,
    axi_arvalid_i,
    axi_rready_i,
    ow_i,
    axi_bready_i);
  output ow_en_o;
  output [2:0]ow_ctrl_o;
  output [31:0]axi_rdata_o;
  output axi_rvalid_o;
  output axi_awready_o;
  output axi_wready_o;
  output axi_arready_o;
  output ow_o;
  output ow_pullup_o;
  output axi_wdone_reg;
  input axi_clk_i;
  input axi_aresetn_i;
  input [8:0]axi_awaddr_i;
  input [7:0]axi_araddr_i;
  input [3:0]axi_wdata_i;
  input axi_awvalid_i;
  input axi_wvalid_i;
  input axi_arvalid_i;
  input axi_rready_i;
  input ow_i;
  input axi_bready_i;

  wire [7:0]axi_araddr_i;
  wire axi_aresetn_i;
  wire axi_arready_o;
  wire axi_arvalid_i;
  wire [8:0]axi_awaddr_i;
  wire axi_awready_o;
  wire axi_awvalid_i;
  wire axi_bready_i;
  wire axi_clk_i;
  wire axi_onewire_regs_n_5;
  wire [31:0]axi_rdata_o;
  wire axi_rready_i;
  wire axi_rvalid_o;
  wire [3:0]axi_wdata_i;
  wire axi_wdone_reg;
  wire axi_wready_o;
  wire axi_wvalid_i;
  wire ow_busy;
  wire ow_busy_o;
  wire [2:0]ow_ctrl_o;
  wire [5:0]ow_dev_count;
  wire ow_done_o;
  wire ow_en_o;
  wire ow_i;
  wire ow_o;
  wire ow_pullup_o;
  wire [63:0]ow_rd_data;
  wire ow_reg_rst;
  wire ow_rst;
  wire ow_too_many;
  wire ow_trig;
  wire [5:0]\x_bus_master/ctrl_rd_addr ;
  wire \x_bus_master/ctrl_rd_en ;
  wire [5:0]\x_bus_master/mem_rd_addr ;
  wire \x_bus_master/mem_rd_data_en ;
  wire \x_bus_master/ram_gen.ram_inst/rd_data_en ;

  design_1_axi_onewire_0_0_onewire_regs_axi axi_onewire_regs
       (.ADDRA(\x_bus_master/mem_rd_addr ),
        .axi_araddr_i(axi_araddr_i),
        .axi_aresetn_i(axi_aresetn_i),
        .axi_arready_o(axi_arready_o),
        .axi_arvalid_i(axi_arvalid_i),
        .axi_awaddr_i(axi_awaddr_i),
        .axi_awready_o(axi_awready_o),
        .axi_awvalid_i(axi_awvalid_i),
        .axi_bready_i(axi_bready_i),
        .axi_clk_i(axi_clk_i),
        .axi_rdata_o(axi_rdata_o),
        .axi_rready_i(axi_rready_i),
        .axi_rvalid_o(axi_rvalid_o),
        .axi_wdata_i(axi_wdata_i),
        .axi_wdone_reg_0(axi_wdone_reg),
        .axi_wready_o(axi_wready_o),
        .axi_wvalid_i(axi_wvalid_i),
        .ctrl_rd_addr(\x_bus_master/ctrl_rd_addr ),
        .ctrl_rd_en(\x_bus_master/ctrl_rd_en ),
        .mem_rd_data_en(\x_bus_master/mem_rd_data_en ),
        .mem_readout_rt_reg_0(axi_onewire_regs_n_5),
        .ow_busy(ow_busy),
        .ow_busy_o(ow_busy_o),
        .ow_ctrl_o(ow_ctrl_o),
        .ow_dev_count(ow_dev_count),
        .ow_done_o(ow_done_o),
        .ow_en_o(ow_en_o),
        .ow_rd_data(ow_rd_data),
        .ow_reg_rst(ow_reg_rst),
        .ow_rst(ow_rst),
        .ow_too_many(ow_too_many),
        .ow_trig(ow_trig),
        .rd_data_en(\x_bus_master/ram_gen.ram_inst/rd_data_en ));
  design_1_axi_onewire_0_0_onewire onewire_if
       (.ADDRA(\x_bus_master/mem_rd_addr ),
        .axi_aresetn_i(axi_aresetn_i),
        .axi_clk_i(axi_clk_i),
        .ctrl_rd_addr(\x_bus_master/ctrl_rd_addr ),
        .ctrl_rd_en(\x_bus_master/ctrl_rd_en ),
        .mem_rd_data_en(\x_bus_master/mem_rd_data_en ),
        .ow_busy(ow_busy),
        .ow_busy_o(ow_busy_o),
        .ow_dev_count(ow_dev_count),
        .ow_done_o(ow_done_o),
        .ow_i(ow_i),
        .ow_o(ow_o),
        .ow_pullup_o(ow_pullup_o),
        .ow_rd_data(ow_rd_data),
        .ow_reg_rst(ow_reg_rst),
        .ow_rst(ow_rst),
        .ow_too_many(ow_too_many),
        .ow_trig(ow_trig),
        .rd_data_en(\x_bus_master/ram_gen.ram_inst/rd_data_en ),
        .\rd_data_reg[63] (axi_onewire_regs_n_5));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_axi_onewire_0_0,axi_onewire,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "axi_onewire,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module design_1_axi_onewire_0_0
   (axi_aresetn_i,
    axi_clk_i,
    axi_awvalid_i,
    axi_awready_o,
    axi_awaddr_i,
    axi_awprot_i,
    axi_wvalid_i,
    axi_wready_o,
    axi_wdata_i,
    axi_wstrb_i,
    axi_bvalid_o,
    axi_bready_i,
    axi_bresp_o,
    axi_arvalid_i,
    axi_arready_o,
    axi_araddr_i,
    axi_arprot_i,
    axi_rvalid_o,
    axi_rready_i,
    axi_rdata_o,
    axi_rresp_o,
    ow_i,
    ow_o,
    ow_pullup_o,
    ow_ctrl_o,
    ow_en_o);
  input axi_aresetn_i;
  input axi_clk_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME S_AXI, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 11, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input axi_awvalid_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWREADY" *) output axi_awready_o;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWADDR" *) input [10:0]axi_awaddr_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI AWPROT" *) input [2:0]axi_awprot_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WVALID" *) input axi_wvalid_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WREADY" *) output axi_wready_o;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WDATA" *) input [31:0]axi_wdata_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI WSTRB" *) input [3:0]axi_wstrb_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BVALID" *) output axi_bvalid_o;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BREADY" *) input axi_bready_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI BRESP" *) output [1:0]axi_bresp_o;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARVALID" *) input axi_arvalid_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARREADY" *) output axi_arready_o;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARADDR" *) input [10:0]axi_araddr_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI ARPROT" *) input [2:0]axi_arprot_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RVALID" *) output axi_rvalid_o;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RREADY" *) input axi_rready_i;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RDATA" *) output [31:0]axi_rdata_o;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S_AXI RRESP" *) output [1:0]axi_rresp_o;
  input ow_i;
  output ow_o;
  output ow_pullup_o;
  output [2:0]ow_ctrl_o;
  output ow_en_o;

  wire \<const0> ;
  wire [10:0]axi_araddr_i;
  wire axi_aresetn_i;
  wire axi_arready_o;
  wire axi_arvalid_i;
  wire [10:0]axi_awaddr_i;
  wire axi_awready_o;
  wire axi_awvalid_i;
  wire axi_bready_i;
  wire axi_bvalid_o;
  wire axi_clk_i;
  wire [31:0]axi_rdata_o;
  wire axi_rready_i;
  wire axi_rvalid_o;
  wire [31:0]axi_wdata_i;
  wire axi_wready_o;
  wire axi_wvalid_i;
  wire [2:0]ow_ctrl_o;
  wire ow_en_o;
  wire ow_i;
  wire ow_o;
  wire ow_pullup_o;

  assign axi_bresp_o[1] = \<const0> ;
  assign axi_bresp_o[0] = \<const0> ;
  assign axi_rresp_o[1] = \<const0> ;
  assign axi_rresp_o[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_axi_onewire_0_0_axi_onewire U0
       (.axi_araddr_i({axi_araddr_i[10],axi_araddr_i[8:2]}),
        .axi_aresetn_i(axi_aresetn_i),
        .axi_arready_o(axi_arready_o),
        .axi_arvalid_i(axi_arvalid_i),
        .axi_awaddr_i(axi_awaddr_i[10:2]),
        .axi_awready_o(axi_awready_o),
        .axi_awvalid_i(axi_awvalid_i),
        .axi_bready_i(axi_bready_i),
        .axi_clk_i(axi_clk_i),
        .axi_rdata_o(axi_rdata_o),
        .axi_rready_i(axi_rready_i),
        .axi_rvalid_o(axi_rvalid_o),
        .axi_wdata_i(axi_wdata_i[3:0]),
        .axi_wdone_reg(axi_bvalid_o),
        .axi_wready_o(axi_wready_o),
        .axi_wvalid_i(axi_wvalid_i),
        .ow_ctrl_o(ow_ctrl_o),
        .ow_en_o(ow_en_o),
        .ow_i(ow_i),
        .ow_o(ow_o),
        .ow_pullup_o(ow_pullup_o));
endmodule

module design_1_axi_onewire_0_0_onewire
   (ctrl_rd_addr,
    ow_o,
    ow_busy,
    ow_too_many,
    ctrl_rd_en,
    mem_rd_data_en,
    ow_busy_o,
    ow_done_o,
    ow_rd_data,
    ow_dev_count,
    ow_pullup_o,
    axi_clk_i,
    ADDRA,
    rd_data_en,
    axi_aresetn_i,
    ow_reg_rst,
    ow_rst,
    ow_i,
    ow_trig,
    \rd_data_reg[63] );
  output [5:0]ctrl_rd_addr;
  output ow_o;
  output ow_busy;
  output ow_too_many;
  output ctrl_rd_en;
  output mem_rd_data_en;
  output ow_busy_o;
  output ow_done_o;
  output [63:0]ow_rd_data;
  output [5:0]ow_dev_count;
  output ow_pullup_o;
  input axi_clk_i;
  input [5:0]ADDRA;
  input rd_data_en;
  input axi_aresetn_i;
  input ow_reg_rst;
  input ow_rst;
  input ow_i;
  input ow_trig;
  input \rd_data_reg[63] ;

  wire [5:0]ADDRA;
  wire axi_aresetn_i;
  wire axi_clk_i;
  wire [5:0]ctrl_rd_addr;
  wire ctrl_rd_en;
  wire mem_rd_data_en;
  wire ow_busy;
  wire ow_busy_o;
  wire ow_busy_o_i_2_n_0;
  wire [5:0]ow_dev_count;
  wire ow_disc_trig_reg_n_0;
  wire ow_done_o;
  wire ow_i;
  wire ow_o;
  wire ow_pullup_o;
  wire [63:0]ow_rd_data;
  wire ow_reg_rst;
  wire ow_rst;
  wire [1:0]ow_state__0;
  wire ow_temp_trig_reg_n_0;
  wire ow_too_many;
  wire ow_trig;
  wire rd_data_en;
  wire \rd_data_reg[63] ;
  wire x_bus_master_n_82;
  wire x_bus_master_n_83;
  wire x_bus_master_n_84;
  wire x_bus_master_n_85;
  wire x_bus_master_n_86;
  wire x_bus_master_n_87;

  (* FSM_ENCODED_STATES = "discovery:01,temperature_read:10,idle:00" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_ow_state_reg[0] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(x_bus_master_n_87),
        .Q(ow_state__0[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "discovery:01,temperature_read:10,idle:00" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_ow_state_reg[1] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(x_bus_master_n_86),
        .Q(ow_state__0[1]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h04)) 
    ow_busy_o_i_2
       (.I0(ow_state__0[0]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(ow_busy_o_i_2_n_0));
  FDRE ow_busy_o_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(x_bus_master_n_85),
        .Q(ow_busy_o),
        .R(1'b0));
  FDRE ow_disc_trig_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(x_bus_master_n_82),
        .Q(ow_disc_trig_reg_n_0),
        .R(1'b0));
  FDRE ow_done_o_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(x_bus_master_n_84),
        .Q(ow_done_o),
        .R(1'b0));
  FDRE ow_temp_trig_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(x_bus_master_n_83),
        .Q(ow_temp_trig_reg_n_0),
        .R(1'b0));
  design_1_axi_onewire_0_0_onewire_idtemp x_bus_master
       (.ADDRA(ADDRA),
        .ADDRD(ctrl_rd_addr),
        .\FSM_sequential_ow_state_reg[0] (x_bus_master_n_82),
        .\FSM_sequential_ow_state_reg[0]_0 (x_bus_master_n_83),
        .\FSM_sequential_ow_state_reg[1] (x_bus_master_n_85),
        .axi_aresetn_i(axi_aresetn_i),
        .axi_clk_i(axi_clk_i),
        .ctrl_rd_en(ctrl_rd_en),
        .ow_busy_o(ow_busy_o),
        .ow_busy_o_reg(ow_busy_o_i_2_n_0),
        .ow_dev_count(ow_dev_count[5]),
        .ow_disc_trig_reg(ow_disc_trig_reg_n_0),
        .ow_done_o(ow_done_o),
        .ow_done_o_reg(x_bus_master_n_84),
        .ow_i(ow_i),
        .ow_o(ow_o),
        .ow_pullup_o(ow_pullup_o),
        .ow_rd_data(ow_rd_data),
        .ow_reg_rst(ow_reg_rst),
        .ow_rst(ow_rst),
        .ow_state__0(ow_state__0),
        .ow_temp_trig_reg(ow_temp_trig_reg_n_0),
        .ow_trig(ow_trig),
        .rd_data_en(rd_data_en),
        .rd_data_en_reg(mem_rd_data_en),
        .\rd_data_reg[63] (\rd_data_reg[63] ),
        .\reg_reg[busy] (ow_busy),
        .\reg_reg[device_count][0] (ow_dev_count[0]),
        .\reg_reg[device_count][1] (ow_dev_count[1]),
        .\reg_reg[device_count][2] (ow_dev_count[2]),
        .\reg_reg[device_count][3] (ow_dev_count[3]),
        .\reg_reg[device_count][4] (ow_dev_count[4]),
        .\reg_reg[done] (x_bus_master_n_87),
        .\reg_reg[too_many] (ow_too_many),
        .\reg_reg[too_many]_0 (x_bus_master_n_86));
endmodule

module design_1_axi_onewire_0_0_onewire_control
   (\reg_reg[busy]_0 ,
    \reg_reg[too_many]_0 ,
    dcvr_start,
    ctrl_rd_en,
    ctrl_bus_rst,
    ctrl_bit_send,
    \FSM_sequential_reg_reg[state][2]_0 ,
    \reg_reg[mem_addr][0]_0 ,
    \reg_reg[mem_addr][1]_0 ,
    \reg_reg[mem_addr][2]_0 ,
    \reg_reg[mem_addr][3]_0 ,
    \reg_reg[mem_addr][4]_0 ,
    \reg_reg[mem_addr][5]_0 ,
    ctrl_wr_data,
    \reg_reg[device_count][0]_0 ,
    \reg_reg[device_count][1]_0 ,
    \reg_reg[device_count][2]_0 ,
    \reg_reg[device_count][3]_0 ,
    \reg_reg[device_count][4]_0 ,
    ow_dev_count,
    wr_done,
    \reg_reg[bit_recv]_0 ,
    \reg_reg[bit_recv]_1 ,
    ow_pullup_o,
    \FSM_sequential_ow_state_reg[0] ,
    \FSM_sequential_ow_state_reg[0]_0 ,
    ow_done_o_reg,
    \FSM_sequential_ow_state_reg[1] ,
    \reg_reg[too_many]_1 ,
    \reg_reg[done]_0 ,
    ctrl_bit_tx,
    axi_clk_i,
    ow_reg_rst,
    axi_aresetn_i,
    if_rx_data_en,
    if_rx_data,
    if_done,
    dcvr_id_en,
    dcvr_done,
    ow_temp_trig_reg,
    ow_disc_trig_reg,
    ow_rst,
    ow_rd_data,
    \FSM_sequential_reg_reg[state][2]_1 ,
    \reg_reg[bit_send]_0 ,
    mem_wr_done,
    ow_state__0,
    dcvr_bit_recv,
    dcvr_bit_send,
    \reg_reg[data][30]_0 ,
    \reg_reg[mem_addr][0]_1 ,
    \FSM_sequential_reg[state][3]_i_6_0 ,
    ow_trig,
    ow_done_o,
    ow_busy_o_reg,
    ow_busy_o);
  output \reg_reg[busy]_0 ;
  output \reg_reg[too_many]_0 ;
  output dcvr_start;
  output ctrl_rd_en;
  output ctrl_bus_rst;
  output ctrl_bit_send;
  output [1:0]\FSM_sequential_reg_reg[state][2]_0 ;
  output \reg_reg[mem_addr][0]_0 ;
  output \reg_reg[mem_addr][1]_0 ;
  output \reg_reg[mem_addr][2]_0 ;
  output \reg_reg[mem_addr][3]_0 ;
  output \reg_reg[mem_addr][4]_0 ;
  output \reg_reg[mem_addr][5]_0 ;
  output [62:0]ctrl_wr_data;
  output \reg_reg[device_count][0]_0 ;
  output \reg_reg[device_count][1]_0 ;
  output \reg_reg[device_count][2]_0 ;
  output \reg_reg[device_count][3]_0 ;
  output \reg_reg[device_count][4]_0 ;
  output [0:0]ow_dev_count;
  output wr_done;
  output \reg_reg[bit_recv]_0 ;
  output \reg_reg[bit_recv]_1 ;
  output ow_pullup_o;
  output \FSM_sequential_ow_state_reg[0] ;
  output \FSM_sequential_ow_state_reg[0]_0 ;
  output ow_done_o_reg;
  output \FSM_sequential_ow_state_reg[1] ;
  output \reg_reg[too_many]_1 ;
  output \reg_reg[done]_0 ;
  output ctrl_bit_tx;
  input axi_clk_i;
  input ow_reg_rst;
  input axi_aresetn_i;
  input if_rx_data_en;
  input if_rx_data;
  input if_done;
  input dcvr_id_en;
  input dcvr_done;
  input ow_temp_trig_reg;
  input ow_disc_trig_reg;
  input ow_rst;
  input [63:0]ow_rd_data;
  input \FSM_sequential_reg_reg[state][2]_1 ;
  input \reg_reg[bit_send]_0 ;
  input mem_wr_done;
  input [1:0]ow_state__0;
  input dcvr_bit_recv;
  input dcvr_bit_send;
  input \reg_reg[data][30]_0 ;
  input \reg_reg[mem_addr][0]_1 ;
  input \FSM_sequential_reg[state][3]_i_6_0 ;
  input ow_trig;
  input ow_done_o;
  input ow_busy_o_reg;
  input ow_busy_o;

  wire \FSM_sequential_ow_state[1]_i_2_n_0 ;
  wire \FSM_sequential_ow_state_reg[0] ;
  wire \FSM_sequential_ow_state_reg[0]_0 ;
  wire \FSM_sequential_ow_state_reg[1] ;
  wire \FSM_sequential_reg[state][0]_i_1__1_n_0 ;
  wire \FSM_sequential_reg[state][0]_i_2__1_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_1__1_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_2__0_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_4__0_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_5_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_1__0_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_2__0_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_3__0_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_4_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_6_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_7__0_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_10_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_11_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_12_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_13_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_14_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_15_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_16_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_17_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_18_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_19_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_1_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_20_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_21_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_22_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_23_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_2_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_3_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_4_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_5_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_6_0 ;
  wire \FSM_sequential_reg[state][3]_i_6_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_7_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_8_n_0 ;
  wire \FSM_sequential_reg[state][3]_i_9_n_0 ;
  wire [1:0]\FSM_sequential_reg_reg[state][2]_0 ;
  wire \FSM_sequential_reg_reg[state][2]_1 ;
  wire axi_aresetn_i;
  wire axi_clk_i;
  wire crc_inst_n_0;
  wire crc_inst_n_1;
  wire ctrl_bit_recv;
  wire ctrl_bit_send;
  wire ctrl_bit_tx;
  wire ctrl_bus_rst;
  wire ctrl_rd_en;
  wire [62:0]ctrl_wr_data;
  wire ctrl_wr_en;
  wire dcvr_bit_recv;
  wire dcvr_bit_send;
  wire dcvr_done;
  wire dcvr_id_en;
  wire dcvr_start;
  wire if_done;
  wire if_rx_data;
  wire if_rx_data_en;
  wire [26:1]lfsr_shift;
  wire [0:0]lfsr_shift__0;
  wire mem_wr_done;
  wire ow_busy_o;
  wire ow_busy_o_i_3_n_0;
  wire ow_busy_o_reg;
  wire [0:0]ow_dev_count;
  wire ow_disc_trig_reg;
  wire ow_done;
  wire ow_done_o;
  wire ow_done_o1_out;
  wire ow_done_o_reg;
  wire ow_pullup_o;
  wire [63:0]ow_rd_data;
  wire ow_reg_rst;
  wire ow_rst;
  wire [1:0]ow_state__0;
  wire ow_temp_trig_reg;
  wire ow_trig;
  wire p_0_in8_in;
  wire [26:0]p_1_in;
  wire \reg[bit_recv] ;
  wire \reg[bit_send] ;
  wire \reg[bit_send]_i_2__0_n_0 ;
  wire \reg[bit_send]_i_4_n_0 ;
  wire \reg[bit_send]_i_5_n_0 ;
  wire \reg[bit_send]_i_6_n_0 ;
  wire \reg[bit_send]_i_7_n_0 ;
  wire \reg[bus_rst] ;
  wire \reg[bus_rst]_i_10_n_0 ;
  wire \reg[bus_rst]_i_11_n_0 ;
  wire \reg[bus_rst]_i_12_n_0 ;
  wire \reg[bus_rst]_i_13_n_0 ;
  wire \reg[bus_rst]_i_2__0_n_0 ;
  wire \reg[bus_rst]_i_3_n_0 ;
  wire \reg[bus_rst]_i_4_n_0 ;
  wire \reg[bus_rst]_i_5_n_0 ;
  wire \reg[bus_rst]_i_6_n_0 ;
  wire \reg[bus_rst]_i_7_n_0 ;
  wire \reg[bus_rst]_i_8_n_0 ;
  wire \reg[bus_rst]_i_9_n_0 ;
  wire \reg[busy]_i_1_n_0 ;
  wire \reg[busy]_i_2_n_0 ;
  wire \reg[busy]_i_3_n_0 ;
  wire \reg[crc_reset] ;
  wire \reg[data][0]_i_1_n_0 ;
  wire \reg[data][0]_i_2_n_0 ;
  wire \reg[data][0]_i_3_n_0 ;
  wire \reg[data][10]_i_1_n_0 ;
  wire \reg[data][11]_i_1_n_0 ;
  wire \reg[data][12]_i_1_n_0 ;
  wire \reg[data][13]_i_1_n_0 ;
  wire \reg[data][14]_i_1_n_0 ;
  wire \reg[data][15]_i_1_n_0 ;
  wire \reg[data][16]_i_1_n_0 ;
  wire \reg[data][17]_i_1_n_0 ;
  wire \reg[data][18]_i_1_n_0 ;
  wire \reg[data][19]_i_1_n_0 ;
  wire \reg[data][1]_i_1_n_0 ;
  wire \reg[data][1]_i_2_n_0 ;
  wire \reg[data][20]_i_1_n_0 ;
  wire \reg[data][21]_i_1_n_0 ;
  wire \reg[data][22]_i_1_n_0 ;
  wire \reg[data][23]_i_1_n_0 ;
  wire \reg[data][24]_i_1_n_0 ;
  wire \reg[data][25]_i_1_n_0 ;
  wire \reg[data][26]_i_1_n_0 ;
  wire \reg[data][27]_i_1_n_0 ;
  wire \reg[data][28]_i_1_n_0 ;
  wire \reg[data][29]_i_1_n_0 ;
  wire \reg[data][2]_i_1_n_0 ;
  wire \reg[data][2]_i_2_n_0 ;
  wire \reg[data][2]_i_3_n_0 ;
  wire \reg[data][30]_i_1_n_0 ;
  wire \reg[data][31]_i_1_n_0 ;
  wire \reg[data][32]_i_1_n_0 ;
  wire \reg[data][33]_i_1_n_0 ;
  wire \reg[data][34]_i_1_n_0 ;
  wire \reg[data][35]_i_1_n_0 ;
  wire \reg[data][36]_i_1_n_0 ;
  wire \reg[data][37]_i_1_n_0 ;
  wire \reg[data][38]_i_1_n_0 ;
  wire \reg[data][39]_i_1_n_0 ;
  wire \reg[data][3]_i_1_n_0 ;
  wire \reg[data][40]_i_1_n_0 ;
  wire \reg[data][41]_i_1_n_0 ;
  wire \reg[data][42]_i_1_n_0 ;
  wire \reg[data][43]_i_1_n_0 ;
  wire \reg[data][44]_i_1_n_0 ;
  wire \reg[data][45]_i_1_n_0 ;
  wire \reg[data][46]_i_1_n_0 ;
  wire \reg[data][47]_i_1_n_0 ;
  wire \reg[data][48]_i_1_n_0 ;
  wire \reg[data][49]_i_1_n_0 ;
  wire \reg[data][4]_i_1_n_0 ;
  wire \reg[data][4]_i_2_n_0 ;
  wire \reg[data][4]_i_3_n_0 ;
  wire \reg[data][50]_i_1_n_0 ;
  wire \reg[data][51]_i_1_n_0 ;
  wire \reg[data][52]_i_1_n_0 ;
  wire \reg[data][53]_i_1_n_0 ;
  wire \reg[data][54]_i_1_n_0 ;
  wire \reg[data][55]_i_1_n_0 ;
  wire \reg[data][56]_i_1_n_0 ;
  wire \reg[data][57]_i_1_n_0 ;
  wire \reg[data][58]_i_1_n_0 ;
  wire \reg[data][59]_i_1_n_0 ;
  wire \reg[data][5]_i_1_n_0 ;
  wire \reg[data][5]_i_2_n_0 ;
  wire \reg[data][5]_i_3_n_0 ;
  wire \reg[data][5]_i_4_n_0 ;
  wire \reg[data][5]_i_5_n_0 ;
  wire \reg[data][5]_i_6_n_0 ;
  wire \reg[data][60]_i_1_n_0 ;
  wire \reg[data][61]_i_1_n_0 ;
  wire \reg[data][62]_i_1_n_0 ;
  wire \reg[data][62]_i_2_n_0 ;
  wire \reg[data][62]_i_3_n_0 ;
  wire \reg[data][63]_i_11_n_0 ;
  wire \reg[data][63]_i_2_n_0 ;
  wire \reg[data][63]_i_3_n_0 ;
  wire \reg[data][63]_i_4_n_0 ;
  wire \reg[data][63]_i_6_n_0 ;
  wire \reg[data][63]_i_7_n_0 ;
  wire \reg[data][63]_i_8_n_0 ;
  wire \reg[data][63]_i_9_n_0 ;
  wire \reg[data][6]_i_1_n_0 ;
  wire \reg[data][6]_i_2_n_0 ;
  wire \reg[data][6]_i_3_n_0 ;
  wire \reg[data][6]_i_4_n_0 ;
  wire \reg[data][6]_i_5_n_0 ;
  wire \reg[data][6]_i_6_n_0 ;
  wire \reg[data][6]_i_7_n_0 ;
  wire \reg[data][7]_i_10_n_0 ;
  wire \reg[data][7]_i_2_n_0 ;
  wire \reg[data][7]_i_3_n_0 ;
  wire \reg[data][7]_i_4_n_0 ;
  wire \reg[data][7]_i_5_n_0 ;
  wire \reg[data][7]_i_6_n_0 ;
  wire \reg[data][7]_i_7_n_0 ;
  wire \reg[data][7]_i_8_n_0 ;
  wire \reg[data][7]_i_9_n_0 ;
  wire \reg[data][8]_i_1_n_0 ;
  wire \reg[data][9]_i_1_n_0 ;
  wire \reg[device_count][0]_i_1_n_0 ;
  wire \reg[device_count][1]_i_1_n_0 ;
  wire \reg[device_count][2]_i_1_n_0 ;
  wire \reg[device_count][3]_i_1_n_0 ;
  wire \reg[device_count][4]_i_1_n_0 ;
  wire \reg[device_count][5]_i_1_n_0 ;
  wire \reg[device_count][5]_i_2_n_0 ;
  wire \reg[device_count][5]_i_3_n_0 ;
  wire \reg[discover] ;
  wire \reg[discover]_i_2_n_0 ;
  wire \reg[done] ;
  wire \reg[done]_i_2__1_n_0 ;
  wire \reg[lfsr][0]_i_10_n_0 ;
  wire \reg[lfsr][0]_i_2_n_0 ;
  wire \reg[lfsr][0]_i_3__0_n_0 ;
  wire \reg[lfsr][0]_i_4__0_n_0 ;
  wire \reg[lfsr][0]_i_5_n_0 ;
  wire \reg[lfsr][0]_i_6_n_0 ;
  wire \reg[lfsr][0]_i_7__0_n_0 ;
  wire \reg[lfsr][0]_i_9_n_0 ;
  wire \reg[lfsr][26]_i_10_n_0 ;
  wire \reg[lfsr][26]_i_11_n_0 ;
  wire \reg[lfsr][26]_i_12_n_0 ;
  wire \reg[lfsr][26]_i_13_n_0 ;
  wire \reg[lfsr][26]_i_14_n_0 ;
  wire \reg[lfsr][26]_i_15_n_0 ;
  wire \reg[lfsr][26]_i_16_n_0 ;
  wire \reg[lfsr][26]_i_17_n_0 ;
  wire \reg[lfsr][26]_i_18_n_0 ;
  wire \reg[lfsr][26]_i_19_n_0 ;
  wire \reg[lfsr][26]_i_1_n_0 ;
  wire \reg[lfsr][26]_i_20_n_0 ;
  wire \reg[lfsr][26]_i_21_n_0 ;
  wire \reg[lfsr][26]_i_22_n_0 ;
  wire \reg[lfsr][26]_i_23_n_0 ;
  wire \reg[lfsr][26]_i_24_n_0 ;
  wire \reg[lfsr][26]_i_25_n_0 ;
  wire \reg[lfsr][26]_i_26_n_0 ;
  wire \reg[lfsr][26]_i_27_n_0 ;
  wire \reg[lfsr][26]_i_3_n_0 ;
  wire \reg[lfsr][26]_i_4_n_0 ;
  wire \reg[lfsr][26]_i_5_n_0 ;
  wire \reg[lfsr][26]_i_6_n_0 ;
  wire \reg[lfsr][26]_i_7_n_0 ;
  wire \reg[lfsr][26]_i_8_n_0 ;
  wire \reg[lfsr][26]_i_9_n_0 ;
  wire \reg[mem_addr][0]_i_1_n_0 ;
  wire \reg[mem_addr][1]_i_1_n_0 ;
  wire \reg[mem_addr][1]_i_2_n_0 ;
  wire \reg[mem_addr][1]_i_3_n_0 ;
  wire \reg[mem_addr][2]_i_1_n_0 ;
  wire \reg[mem_addr][2]_i_2_n_0 ;
  wire \reg[mem_addr][2]_i_3_n_0 ;
  wire \reg[mem_addr][3]_i_1_n_0 ;
  wire \reg[mem_addr][3]_i_2_n_0 ;
  wire \reg[mem_addr][4]_i_1_n_0 ;
  wire \reg[mem_addr][4]_i_2_n_0 ;
  wire \reg[mem_addr][5]_i_11_n_0 ;
  wire \reg[mem_addr][5]_i_1_n_0 ;
  wire \reg[mem_addr][5]_i_2_n_0 ;
  wire \reg[mem_addr][5]_i_3_n_0 ;
  wire \reg[mem_addr][5]_i_4_n_0 ;
  wire \reg[mem_addr][5]_i_5_n_0 ;
  wire \reg[mem_addr][5]_i_6_n_0 ;
  wire \reg[mem_addr][5]_i_7_n_0 ;
  wire \reg[mem_addr][5]_i_8_n_0 ;
  wire \reg[mem_addr][5]_i_9_n_0 ;
  wire \reg[mem_rd_en] ;
  wire \reg[mem_rd_en]_i_2_n_0 ;
  wire \reg[mem_rd_en]_i_3_n_0 ;
  wire \reg[mem_rd_en]_i_4_n_0 ;
  wire \reg[mem_rd_en]_i_5_n_0 ;
  wire \reg[mem_rd_en]_i_6_n_0 ;
  wire \reg[mem_wr_en] ;
  wire \reg[mem_wr_en]_i_2_n_0 ;
  wire [3:1]\reg[state] ;
  wire \reg[strong_pullup]_i_10_n_0 ;
  wire \reg[strong_pullup]_i_11_n_0 ;
  wire \reg[strong_pullup]_i_1_n_0 ;
  wire \reg[strong_pullup]_i_2_n_0 ;
  wire \reg[strong_pullup]_i_3_n_0 ;
  wire \reg[strong_pullup]_i_4_n_0 ;
  wire \reg[strong_pullup]_i_5_n_0 ;
  wire \reg[strong_pullup]_i_6_n_0 ;
  wire \reg[strong_pullup]_i_7_n_0 ;
  wire \reg[strong_pullup]_i_8_n_0 ;
  wire \reg[strong_pullup]_i_9_n_0 ;
  wire \reg[too_many]_i_1_n_0 ;
  wire \reg[too_many]_i_2_n_0 ;
  wire \reg[too_many]_i_3_n_0 ;
  wire \reg_reg[bit_recv]_0 ;
  wire \reg_reg[bit_recv]_1 ;
  wire \reg_reg[bit_send]_0 ;
  wire \reg_reg[busy]_0 ;
  wire \reg_reg[crc_reset_n_0_] ;
  wire \reg_reg[data][30]_0 ;
  wire \reg_reg[device_count][0]_0 ;
  wire \reg_reg[device_count][1]_0 ;
  wire \reg_reg[device_count][2]_0 ;
  wire \reg_reg[device_count][3]_0 ;
  wire \reg_reg[device_count][4]_0 ;
  wire \reg_reg[done]_0 ;
  wire \reg_reg[lfsr_n_0_][26] ;
  wire \reg_reg[mem_addr][0]_0 ;
  wire \reg_reg[mem_addr][0]_1 ;
  wire \reg_reg[mem_addr][1]_0 ;
  wire \reg_reg[mem_addr][2]_0 ;
  wire \reg_reg[mem_addr][3]_0 ;
  wire \reg_reg[mem_addr][4]_0 ;
  wire \reg_reg[mem_addr][5]_0 ;
  wire \reg_reg[too_many]_0 ;
  wire \reg_reg[too_many]_1 ;
  wire strong_pullup;
  wire wr_done;

  LUT6 #(
    .INIT(64'h5151515100050000)) 
    \FSM_sequential_ow_state[0]_i_1 
       (.I0(ow_rst),
        .I1(ow_done),
        .I2(ow_state__0[1]),
        .I3(\reg_reg[busy]_0 ),
        .I4(ow_trig),
        .I5(ow_state__0[0]),
        .O(\reg_reg[done]_0 ));
  LUT6 #(
    .INIT(64'h3310331000000300)) 
    \FSM_sequential_ow_state[1]_i_1 
       (.I0(\reg_reg[too_many]_0 ),
        .I1(ow_rst),
        .I2(ow_done),
        .I3(ow_state__0[1]),
        .I4(\FSM_sequential_ow_state[1]_i_2_n_0 ),
        .I5(ow_state__0[0]),
        .O(\reg_reg[too_many]_1 ));
  LUT3 #(
    .INIT(8'h04)) 
    \FSM_sequential_ow_state[1]_i_2 
       (.I0(\reg_reg[busy]_0 ),
        .I1(ow_trig),
        .I2(ow_state__0[1]),
        .O(\FSM_sequential_ow_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FF005100)) 
    \FSM_sequential_reg[state][0]_i_1__1 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[state] [1]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\FSM_sequential_reg[state][0]_i_2__1_n_0 ),
        .I4(\reg[state] [3]),
        .I5(ow_rst),
        .O(\FSM_sequential_reg[state][0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h000E)) 
    \FSM_sequential_reg[state][0]_i_2 
       (.I0(ctrl_bit_recv),
        .I1(dcvr_bit_recv),
        .I2(ctrl_bit_send),
        .I3(dcvr_bit_send),
        .O(\reg_reg[bit_recv]_0 ));
  LUT6 #(
    .INIT(64'hCCFF00FF00F0AAFF)) 
    \FSM_sequential_reg[state][0]_i_2__1 
       (.I0(\FSM_sequential_reg_reg[state][2]_1 ),
        .I1(p_0_in8_in),
        .I2(\reg[state] [3]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(\reg[state] [1]),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\FSM_sequential_reg[state][0]_i_2__1_n_0 ));
  LUT4 #(
    .INIT(16'hFFF8)) 
    \FSM_sequential_reg[state][1]_i_1__1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(p_0_in8_in),
        .I2(\FSM_sequential_reg[state][1]_i_4__0_n_0 ),
        .I3(\FSM_sequential_reg[state][1]_i_5_n_0 ),
        .O(\FSM_sequential_reg[state][1]_i_1__1_n_0 ));
  LUT5 #(
    .INIT(32'h40000000)) 
    \FSM_sequential_reg[state][1]_i_2__0 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [3]),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\FSM_sequential_reg[state][1]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_reg[state][1]_i_2__1 
       (.I0(ctrl_bit_recv),
        .I1(dcvr_bit_recv),
        .I2(ctrl_bit_send),
        .I3(dcvr_bit_send),
        .O(\reg_reg[bit_recv]_1 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \FSM_sequential_reg[state][1]_i_3__1 
       (.I0(\reg_reg[mem_addr][0]_0 ),
        .I1(\reg_reg[mem_addr][5]_0 ),
        .I2(\reg_reg[mem_addr][3]_0 ),
        .I3(\reg_reg[mem_addr][4]_0 ),
        .I4(\reg_reg[mem_addr][1]_0 ),
        .I5(\reg_reg[mem_addr][2]_0 ),
        .O(p_0_in8_in));
  LUT6 #(
    .INIT(64'h0000000000FF0001)) 
    \FSM_sequential_reg[state][1]_i_4__0 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(ow_disc_trig_reg),
        .I2(\reg[state] [3]),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I5(ow_rst),
        .O(\FSM_sequential_reg[state][1]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'h000000000C000800)) 
    \FSM_sequential_reg[state][1]_i_5 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(\reg[state] [1]),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\reg[state] [3]),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\FSM_sequential_reg[state][1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hEAAAEAAAFFFFEAAA)) 
    \FSM_sequential_reg[state][2]_i_1__0 
       (.I0(\FSM_sequential_reg[state][2]_i_2__0_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_3__0_n_0 ),
        .I2(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I3(\FSM_sequential_reg_reg[state][2]_1 ),
        .I4(\FSM_sequential_reg[state][2]_i_6_n_0 ),
        .I5(\FSM_sequential_reg[state][2]_i_7__0_n_0 ),
        .O(\FSM_sequential_reg[state][2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h0C000A0A0A0A0A0A)) 
    \FSM_sequential_reg[state][2]_i_2__0 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(p_0_in8_in),
        .I2(ow_rst),
        .I3(\reg[state] [3]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I5(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][2]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_reg[state][2]_i_3__0 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][2]_i_3__0_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_sequential_reg[state][2]_i_4 
       (.I0(\reg[state] [3]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\FSM_sequential_reg[state][2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_sequential_reg[state][2]_i_6 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\FSM_sequential_reg[state][2]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_reg[state][2]_i_7__0 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][2]_i_7__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEEEEEEE)) 
    \FSM_sequential_reg[state][3]_i_1 
       (.I0(\FSM_sequential_reg[state][3]_i_3_n_0 ),
        .I1(\FSM_sequential_reg[state][3]_i_4_n_0 ),
        .I2(\reg[state] [3]),
        .I3(\FSM_sequential_reg[state][3]_i_5_n_0 ),
        .I4(\FSM_sequential_reg[state][3]_i_6_n_0 ),
        .I5(\FSM_sequential_reg[state][3]_i_7_n_0 ),
        .O(\FSM_sequential_reg[state][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \FSM_sequential_reg[state][3]_i_10 
       (.I0(\reg[state] [1]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\FSM_sequential_reg[state][3]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h00004000)) 
    \FSM_sequential_reg[state][3]_i_11 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[state] [1]),
        .I2(if_done),
        .I3(\FSM_sequential_reg[state][3]_i_15_n_0 ),
        .I4(\FSM_sequential_reg[state][3]_i_16_n_0 ),
        .O(\FSM_sequential_reg[state][3]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    \FSM_sequential_reg[state][3]_i_12 
       (.I0(\FSM_sequential_reg[state][3]_i_18_n_0 ),
        .I1(\FSM_sequential_reg[state][3]_i_19_n_0 ),
        .I2(lfsr_shift[17]),
        .I3(lfsr_shift[18]),
        .I4(\FSM_sequential_reg[state][3]_i_20_n_0 ),
        .I5(\FSM_sequential_reg[state][3]_i_21_n_0 ),
        .O(\FSM_sequential_reg[state][3]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'hF0F088F0)) 
    \FSM_sequential_reg[state][3]_i_13 
       (.I0(\FSM_sequential_reg[state][3]_i_6_0 ),
        .I1(\reg_reg[busy]_0 ),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][3]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \FSM_sequential_reg[state][3]_i_14 
       (.I0(\reg[state] [1]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\FSM_sequential_reg[state][3]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_reg[state][3]_i_15 
       (.I0(lfsr_shift[24]),
        .I1(\reg_reg[lfsr_n_0_][26] ),
        .I2(lfsr_shift[26]),
        .I3(lfsr_shift[23]),
        .O(\FSM_sequential_reg[state][3]_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \FSM_sequential_reg[state][3]_i_16 
       (.I0(lfsr_shift[26]),
        .I1(lfsr_shift[25]),
        .I2(\reg_reg[lfsr_n_0_][26] ),
        .O(\FSM_sequential_reg[state][3]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'h00AA00FC)) 
    \FSM_sequential_reg[state][3]_i_17 
       (.I0(dcvr_done),
        .I1(ow_temp_trig_reg),
        .I2(ow_disc_trig_reg),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][3]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h3100000000000000)) 
    \FSM_sequential_reg[state][3]_i_18 
       (.I0(lfsr_shift[22]),
        .I1(lfsr_shift[24]),
        .I2(lfsr_shift[23]),
        .I3(\FSM_sequential_reg[state][3]_i_22_n_0 ),
        .I4(\FSM_sequential_reg[state][3]_i_23_n_0 ),
        .I5(\reg[bus_rst]_i_11_n_0 ),
        .O(\FSM_sequential_reg[state][3]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \FSM_sequential_reg[state][3]_i_19 
       (.I0(lfsr_shift[8]),
        .I1(lfsr_shift[6]),
        .I2(lfsr_shift[5]),
        .O(\FSM_sequential_reg[state][3]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h006000C000C000C0)) 
    \FSM_sequential_reg[state][3]_i_2 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[state] [3]),
        .I2(axi_aresetn_i),
        .I3(ow_reg_rst),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    \FSM_sequential_reg[state][3]_i_20 
       (.I0(lfsr_shift[11]),
        .I1(lfsr_shift[15]),
        .I2(lfsr_shift[20]),
        .I3(lfsr_shift[21]),
        .I4(lfsr_shift[14]),
        .I5(lfsr_shift[12]),
        .O(\FSM_sequential_reg[state][3]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100010001)) 
    \FSM_sequential_reg[state][3]_i_21 
       (.I0(lfsr_shift[2]),
        .I1(lfsr_shift[3]),
        .I2(lfsr_shift[9]),
        .I3(lfsr_shift[15]),
        .I4(lfsr_shift[13]),
        .I5(lfsr_shift[14]),
        .O(\FSM_sequential_reg[state][3]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'h0000232300000023)) 
    \FSM_sequential_reg[state][3]_i_22 
       (.I0(lfsr_shift[17]),
        .I1(lfsr_shift[18]),
        .I2(lfsr_shift[16]),
        .I3(lfsr_shift[7]),
        .I4(lfsr_shift[9]),
        .I5(lfsr_shift[8]),
        .O(\FSM_sequential_reg[state][3]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'h0000003100003131)) 
    \FSM_sequential_reg[state][3]_i_23 
       (.I0(lfsr_shift[1]),
        .I1(lfsr_shift[3]),
        .I2(lfsr_shift[2]),
        .I3(lfsr_shift[19]),
        .I4(lfsr_shift[21]),
        .I5(lfsr_shift[20]),
        .O(\FSM_sequential_reg[state][3]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'h00000088000000F0)) 
    \FSM_sequential_reg[state][3]_i_3 
       (.I0(\reg[mem_rd_en]_i_2_n_0 ),
        .I1(if_done),
        .I2(\reg[bus_rst]_i_3_n_0 ),
        .I3(\reg[state] [3]),
        .I4(\FSM_sequential_reg[state][3]_i_8_n_0 ),
        .I5(\reg[mem_addr][5]_i_5_n_0 ),
        .O(\FSM_sequential_reg[state][3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAFAAAAAAAEEAAAA)) 
    \FSM_sequential_reg[state][3]_i_4 
       (.I0(ow_rst),
        .I1(if_done),
        .I2(\FSM_sequential_reg[state][3]_i_9_n_0 ),
        .I3(\reg[state] [3]),
        .I4(\FSM_sequential_reg[state][3]_i_8_n_0 ),
        .I5(\reg[mem_addr][5]_i_5_n_0 ),
        .O(\FSM_sequential_reg[state][3]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_reg[state][3]_i_5 
       (.I0(\reg[state] [1]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\FSM_sequential_reg[state][3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF808080)) 
    \FSM_sequential_reg[state][3]_i_6 
       (.I0(if_done),
        .I1(\FSM_sequential_reg[state][3]_i_10_n_0 ),
        .I2(\reg[mem_rd_en]_i_2_n_0 ),
        .I3(\FSM_sequential_reg[state][3]_i_11_n_0 ),
        .I4(\FSM_sequential_reg[state][3]_i_12_n_0 ),
        .I5(\FSM_sequential_reg[state][3]_i_13_n_0 ),
        .O(\FSM_sequential_reg[state][3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8080A28080808080)) 
    \FSM_sequential_reg[state][3]_i_7 
       (.I0(\reg[lfsr][26]_i_3_n_0 ),
        .I1(\FSM_sequential_reg[state][3]_i_14_n_0 ),
        .I2(\reg[mem_rd_en]_i_2_n_0 ),
        .I3(\FSM_sequential_reg[state][3]_i_15_n_0 ),
        .I4(\FSM_sequential_reg[state][3]_i_16_n_0 ),
        .I5(\FSM_sequential_reg[state][3]_i_12_n_0 ),
        .O(\FSM_sequential_reg[state][3]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \FSM_sequential_reg[state][3]_i_8 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[state] [1]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\FSM_sequential_reg[state][3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFC0C80008)) 
    \FSM_sequential_reg[state][3]_i_9 
       (.I0(mem_wr_done),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(\reg[state] [1]),
        .I3(p_0_in8_in),
        .I4(if_done),
        .I5(\FSM_sequential_reg[state][3]_i_17_n_0 ),
        .O(\FSM_sequential_reg[state][3]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "skip_command:0100,convert:0011,scratchpad_crc:1101,discover:0010,read_command:1011,scratchpad:1100,send_id:1010,erase:0001,idle:0000,get_id:1001,wait_reset:0111,match_command:1000,wait_conversion:0110,check_num:1111,save_data:1110,convert_command:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_reg_reg[state][0] 
       (.C(axi_clk_i),
        .CE(\FSM_sequential_reg[state][3]_i_1_n_0 ),
        .D(\FSM_sequential_reg[state][0]_i_1__1_n_0 ),
        .Q(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "skip_command:0100,convert:0011,scratchpad_crc:1101,discover:0010,read_command:1011,scratchpad:1100,send_id:1010,erase:0001,idle:0000,get_id:1001,wait_reset:0111,match_command:1000,wait_conversion:0110,check_num:1111,save_data:1110,convert_command:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_reg_reg[state][1] 
       (.C(axi_clk_i),
        .CE(\FSM_sequential_reg[state][3]_i_1_n_0 ),
        .D(\FSM_sequential_reg[state][1]_i_1__1_n_0 ),
        .Q(\reg[state] [1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "skip_command:0100,convert:0011,scratchpad_crc:1101,discover:0010,read_command:1011,scratchpad:1100,send_id:1010,erase:0001,idle:0000,get_id:1001,wait_reset:0111,match_command:1000,wait_conversion:0110,check_num:1111,save_data:1110,convert_command:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_reg_reg[state][2] 
       (.C(axi_clk_i),
        .CE(\FSM_sequential_reg[state][3]_i_1_n_0 ),
        .D(\FSM_sequential_reg[state][2]_i_1__0_n_0 ),
        .Q(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "skip_command:0100,convert:0011,scratchpad_crc:1101,discover:0010,read_command:1011,scratchpad:1100,send_id:1010,erase:0001,idle:0000,get_id:1001,wait_reset:0111,match_command:1000,wait_conversion:0110,check_num:1111,save_data:1110,convert_command:0101" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_reg_reg[state][3] 
       (.C(axi_clk_i),
        .CE(\FSM_sequential_reg[state][3]_i_1_n_0 ),
        .D(\FSM_sequential_reg[state][3]_i_2_n_0 ),
        .Q(\reg[state] [3]),
        .R(1'b0));
  design_1_axi_onewire_0_0_onewire_crc_0 crc_inst
       (.\FSM_sequential_reg_reg[state][0] (crc_inst_n_0),
        .\FSM_sequential_reg_reg[state][3] (crc_inst_n_1),
        .axi_aresetn_i(axi_aresetn_i),
        .axi_clk_i(axi_clk_i),
        .if_rx_data(if_rx_data),
        .if_rx_data_en(if_rx_data_en),
        .ow_reg_rst(ow_reg_rst),
        .pristine_reg_0(\reg_reg[crc_reset_n_0_] ),
        .\reg[data][63]_i_5_0 (\FSM_sequential_reg_reg[state][2]_0 [1]),
        .\reg[state] ({\reg[state] [3],\reg[state] [1]}),
        .\reg_reg[data][0] (\reg[data][7]_i_3_n_0 ),
        .\reg_reg[data][0]_0 (\reg[data][7]_i_4_n_0 ),
        .\reg_reg[data][0]_1 (\reg[data][63]_i_6_n_0 ),
        .\reg_reg[data][0]_2 (\FSM_sequential_reg_reg[state][2]_0 [0]),
        .\reg_reg[data][0]_3 (\reg[lfsr][26]_i_4_n_0 ),
        .\reg_reg[data][30] (\reg[data][63]_i_3_n_0 ),
        .\reg_reg[data][30]_0 (\reg[data][63]_i_4_n_0 ),
        .\reg_reg[data][30]_1 (\reg[lfsr][26]_i_6_n_0 ),
        .\reg_reg[data][30]_2 (\reg[data][63]_i_9_n_0 ),
        .\reg_reg[data][30]_3 (\reg[lfsr][26]_i_12_n_0 ),
        .\reg_reg[data][30]_4 (\reg[lfsr][26]_i_15_n_0 ),
        .\reg_reg[data][30]_5 (\reg_reg[data][30]_0 ),
        .\reg_reg[data][30]_6 (\reg[data][63]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'h3020)) 
    mem_reg_0_63_0_2_i_4
       (.I0(ctrl_wr_en),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(dcvr_id_en),
        .O(wr_done));
  LUT6 #(
    .INIT(64'h0400FFFF04000000)) 
    ow_busy_o_i_1
       (.I0(ow_state__0[1]),
        .I1(ow_trig),
        .I2(\reg_reg[busy]_0 ),
        .I3(ow_busy_o_reg),
        .I4(ow_busy_o_i_3_n_0),
        .I5(ow_busy_o),
        .O(\FSM_sequential_ow_state_reg[1] ));
  LUT6 #(
    .INIT(64'hCCECCCECFFFFFCCC)) 
    ow_busy_o_i_3
       (.I0(\reg_reg[too_many]_0 ),
        .I1(ow_rst),
        .I2(ow_done),
        .I3(ow_state__0[1]),
        .I4(\FSM_sequential_ow_state[1]_i_2_n_0 ),
        .I5(ow_state__0[0]),
        .O(ow_busy_o_i_3_n_0));
  LUT6 #(
    .INIT(64'h5151515100000100)) 
    ow_disc_trig_i_1
       (.I0(ow_rst),
        .I1(ow_state__0[0]),
        .I2(ow_state__0[1]),
        .I3(ow_trig),
        .I4(\reg_reg[busy]_0 ),
        .I5(ow_disc_trig_reg),
        .O(\FSM_sequential_ow_state_reg[0] ));
  LUT3 #(
    .INIT(8'hB8)) 
    ow_done_o_i_1
       (.I0(ow_done_o1_out),
        .I1(ow_busy_o_i_3_n_0),
        .I2(ow_done_o),
        .O(ow_done_o_reg));
  LUT6 #(
    .INIT(64'h000000002C200000)) 
    ow_done_o_i_2
       (.I0(ow_done),
        .I1(ow_state__0[0]),
        .I2(ow_state__0[1]),
        .I3(\reg_reg[too_many]_0 ),
        .I4(axi_aresetn_i),
        .I5(ow_reg_rst),
        .O(ow_done_o1_out));
  LUT1 #(
    .INIT(2'h1)) 
    ow_pullup_o_INST_0
       (.I0(strong_pullup),
        .O(ow_pullup_o));
  LUT6 #(
    .INIT(64'h4545454500000400)) 
    ow_temp_trig_i_1
       (.I0(ow_rst),
        .I1(ow_state__0[0]),
        .I2(ow_state__0[1]),
        .I3(ow_done),
        .I4(\reg_reg[too_many]_0 ),
        .I5(ow_temp_trig_reg),
        .O(\FSM_sequential_ow_state_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hAAEFAAAAAAAAAAAA)) 
    \reg[bit_recv]_i_1__0 
       (.I0(\reg[crc_reset] ),
        .I1(\reg[lfsr][26]_i_4_n_0 ),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(\FSM_sequential_reg[state][3]_i_5_n_0 ),
        .I4(if_rx_data_en),
        .I5(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .O(\reg[bit_recv] ));
  LUT6 #(
    .INIT(64'hFFFFFF80FF80FF80)) 
    \reg[bit_send]_i_1__0 
       (.I0(\reg[bit_send]_i_2__0_n_0 ),
        .I1(\reg_reg[bit_send]_0 ),
        .I2(\reg[lfsr][26]_i_4_n_0 ),
        .I3(\reg[bit_send]_i_4_n_0 ),
        .I4(\reg[bit_send]_i_5_n_0 ),
        .I5(\FSM_sequential_reg[state][2]_i_6_n_0 ),
        .O(\reg[bit_send] ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \reg[bit_send]_i_2__0 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [3]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[bit_send]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000020000020000)) 
    \reg[bit_send]_i_4 
       (.I0(if_done),
        .I1(ow_rst),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(\reg[state] [3]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(\reg[state] [1]),
        .O(\reg[bit_send]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFEFEAA)) 
    \reg[bit_send]_i_5 
       (.I0(\reg[bit_send]_i_6_n_0 ),
        .I1(\reg[data][63]_i_8_n_0 ),
        .I2(\reg[lfsr][26]_i_5_n_0 ),
        .I3(\reg[lfsr][26]_i_15_n_0 ),
        .I4(\reg[lfsr][26]_i_12_n_0 ),
        .I5(\reg[bit_send]_i_7_n_0 ),
        .O(\reg[bit_send]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000080000)) 
    \reg[bit_send]_i_6 
       (.I0(\FSM_sequential_reg[state][3]_i_6_0 ),
        .I1(\reg_reg[busy]_0 ),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\reg[state] [1]),
        .I4(\reg[state] [3]),
        .I5(\FSM_sequential_reg_reg[state][2]_1 ),
        .O(\reg[bit_send]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \reg[bit_send]_i_7 
       (.I0(\reg[state] [3]),
        .I1(if_done),
        .I2(\reg[state] [1]),
        .O(\reg[bit_send]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \reg[bus_rst]_i_10 
       (.I0(lfsr_shift[14]),
        .I1(lfsr_shift[13]),
        .I2(lfsr_shift[15]),
        .O(\reg[bus_rst]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h005D005D0000005D)) 
    \reg[bus_rst]_i_11 
       (.I0(lfsr_shift[12]),
        .I1(lfsr_shift[10]),
        .I2(lfsr_shift[11]),
        .I3(lfsr_shift[6]),
        .I4(lfsr_shift[4]),
        .I5(lfsr_shift[5]),
        .O(\reg[bus_rst]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \reg[bus_rst]_i_12 
       (.I0(lfsr_shift[12]),
        .I1(lfsr_shift[14]),
        .O(\reg[bus_rst]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \reg[bus_rst]_i_13 
       (.I0(lfsr_shift[11]),
        .I1(lfsr_shift[15]),
        .O(\reg[bus_rst]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEAAAEAAAEAAA)) 
    \reg[bus_rst]_i_1__0 
       (.I0(\reg[bus_rst]_i_2__0_n_0 ),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(p_0_in8_in),
        .I3(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I4(\reg[bus_rst]_i_3_n_0 ),
        .I5(\reg[bus_rst]_i_4_n_0 ),
        .O(\reg[bus_rst] ));
  LUT6 #(
    .INIT(64'h0000000000020000)) 
    \reg[bus_rst]_i_2__0 
       (.I0(\reg[discover]_i_2_n_0 ),
        .I1(\reg[state] [3]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(ow_rst),
        .I4(ow_temp_trig_reg),
        .I5(ow_disc_trig_reg),
        .O(\reg[bus_rst]_i_2__0_n_0 ));
  LUT5 #(
    .INIT(32'h80000000)) 
    \reg[bus_rst]_i_3 
       (.I0(\reg[bus_rst]_i_5_n_0 ),
        .I1(\reg[bus_rst]_i_6_n_0 ),
        .I2(\reg[bus_rst]_i_7_n_0 ),
        .I3(\reg[bus_rst]_i_8_n_0 ),
        .I4(\reg[bus_rst]_i_9_n_0 ),
        .O(\reg[bus_rst]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \reg[bus_rst]_i_4 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I5(\reg[state] [3]),
        .O(\reg[bus_rst]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000080000)) 
    \reg[bus_rst]_i_5 
       (.I0(lfsr_shift[24]),
        .I1(lfsr_shift[23]),
        .I2(lfsr_shift[22]),
        .I3(\reg_reg[lfsr_n_0_][26] ),
        .I4(lfsr_shift[25]),
        .I5(lfsr_shift[26]),
        .O(\reg[bus_rst]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000000000EA)) 
    \reg[bus_rst]_i_6 
       (.I0(lfsr_shift[18]),
        .I1(lfsr_shift[16]),
        .I2(lfsr_shift[17]),
        .I3(lfsr_shift[5]),
        .I4(lfsr_shift[6]),
        .I5(lfsr_shift[8]),
        .O(\reg[bus_rst]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000005D00005D5D)) 
    \reg[bus_rst]_i_7 
       (.I0(lfsr_shift[9]),
        .I1(lfsr_shift[7]),
        .I2(lfsr_shift[8]),
        .I3(lfsr_shift[19]),
        .I4(lfsr_shift[21]),
        .I5(lfsr_shift[20]),
        .O(\reg[bus_rst]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \reg[bus_rst]_i_8 
       (.I0(lfsr_shift[2]),
        .I1(lfsr_shift[1]),
        .I2(lfsr_shift[9]),
        .I3(lfsr_shift[3]),
        .I4(\reg[bus_rst]_i_10_n_0 ),
        .I5(\reg[bus_rst]_i_11_n_0 ),
        .O(\reg[bus_rst]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000040000)) 
    \reg[bus_rst]_i_9 
       (.I0(\reg[bus_rst]_i_12_n_0 ),
        .I1(lfsr_shift[20]),
        .I2(lfsr_shift[21]),
        .I3(\reg[bus_rst]_i_13_n_0 ),
        .I4(lfsr_shift[17]),
        .I5(lfsr_shift[18]),
        .O(\reg[bus_rst]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h1110FFFF11100000)) 
    \reg[busy]_i_1 
       (.I0(\reg[state] [1]),
        .I1(ow_rst),
        .I2(ow_disc_trig_reg),
        .I3(ow_temp_trig_reg),
        .I4(\reg[busy]_i_2_n_0 ),
        .I5(\reg_reg[busy]_0 ),
        .O(\reg[busy]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBAAAAAAAAAAAAAAA)) 
    \reg[busy]_i_2 
       (.I0(\reg[busy]_i_3_n_0 ),
        .I1(p_0_in8_in),
        .I2(\reg[state] [3]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(\reg[state] [1]),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[busy]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCCCCEECCCCCCFCCC)) 
    \reg[busy]_i_3 
       (.I0(dcvr_done),
        .I1(\reg[lfsr][26]_i_6_n_0 ),
        .I2(ow_temp_trig_reg),
        .I3(\reg[mem_addr][5]_i_9_n_0 ),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(\reg[state] [1]),
        .O(\reg[busy]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \reg[crc_reset]_i_1__0 
       (.I0(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I2(\reg[state] [1]),
        .I3(if_done),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I5(\reg[mem_rd_en]_i_2_n_0 ),
        .O(\reg[crc_reset] ));
  LUT6 #(
    .INIT(64'hFFFFEAAAEAAAEAAA)) 
    \reg[data][0]_i_1 
       (.I0(\reg[data][0]_i_2_n_0 ),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I2(ctrl_wr_data[0]),
        .I3(\reg[data][5]_i_3_n_0 ),
        .I4(\reg[data][0]_i_3_n_0 ),
        .I5(\reg[data][6]_i_6_n_0 ),
        .O(\reg[data][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFAAAEAFAAAAAEAAA)) 
    \reg[data][0]_i_2 
       (.I0(\reg[data][6]_i_2_n_0 ),
        .I1(ow_rd_data[0]),
        .I2(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(\reg[state] [1]),
        .I5(ctrl_wr_data[0]),
        .O(\reg[data][0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h04000000)) 
    \reg[data][0]_i_3 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(ctrl_wr_data[0]),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\reg[state] [1]),
        .O(\reg[data][0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][10]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[10]),
        .I3(ctrl_wr_data[10]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][11]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[11]),
        .I3(ctrl_wr_data[11]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][11]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][12]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[12]),
        .I3(ctrl_wr_data[12]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][13]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[13]),
        .I3(ctrl_wr_data[13]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][13]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][14]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[14]),
        .I3(ctrl_wr_data[14]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][14]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][15]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[15]),
        .I3(ctrl_wr_data[15]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][16]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[16]),
        .I3(ctrl_wr_data[16]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][16]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][17]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[17]),
        .I3(ctrl_wr_data[17]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][18]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[18]),
        .I3(ctrl_wr_data[18]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][18]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][19]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[19]),
        .I3(ctrl_wr_data[19]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFEFEFEFEEEFEEEEE)) 
    \reg[data][1]_i_1 
       (.I0(\reg[data][7]_i_6_n_0 ),
        .I1(\reg[data][1]_i_2_n_0 ),
        .I2(ctrl_wr_data[1]),
        .I3(\FSM_sequential_reg[state][3]_i_5_n_0 ),
        .I4(\reg[data][5]_i_3_n_0 ),
        .I5(\reg[data][5]_i_4_n_0 ),
        .O(\reg[data][1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \reg[data][1]_i_2 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(ow_rd_data[1]),
        .O(\reg[data][1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][20]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[20]),
        .I3(ctrl_wr_data[20]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][20]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][21]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[21]),
        .I3(ctrl_wr_data[21]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][21]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][22]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[22]),
        .I3(ctrl_wr_data[22]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][22]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][23]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[23]),
        .I3(ctrl_wr_data[23]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][24]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[24]),
        .I3(ctrl_wr_data[24]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][24]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][25]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[25]),
        .I3(ctrl_wr_data[25]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][25]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][26]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[26]),
        .I3(ctrl_wr_data[26]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][26]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][27]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[27]),
        .I3(ctrl_wr_data[27]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][27]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][28]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[28]),
        .I3(ctrl_wr_data[28]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][28]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][29]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[29]),
        .I3(ctrl_wr_data[29]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][29]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \reg[data][2]_i_1 
       (.I0(\reg[data][2]_i_2_n_0 ),
        .I1(\reg[data][6]_i_3_n_0 ),
        .I2(\reg[data][4]_i_2_n_0 ),
        .O(\reg[data][2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAC0C0EAEAC0C0)) 
    \reg[data][2]_i_2 
       (.I0(\reg[data][2]_i_3_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[2]),
        .I3(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I4(ctrl_wr_data[2]),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[data][2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h3030303030002020)) 
    \reg[data][2]_i_3 
       (.I0(\reg[state] [3]),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[data][2]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][30]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[30]),
        .I3(ctrl_wr_data[30]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][30]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][31]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[31]),
        .I3(ctrl_wr_data[31]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][32]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[32]),
        .I3(ctrl_wr_data[32]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][32]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][33]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[33]),
        .I3(ctrl_wr_data[33]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][33]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][34]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[34]),
        .I3(ctrl_wr_data[34]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][34]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][35]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[35]),
        .I3(ctrl_wr_data[35]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][35]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][36]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[36]),
        .I3(ctrl_wr_data[36]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][36]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][37]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[37]),
        .I3(ctrl_wr_data[37]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][37]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][38]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[38]),
        .I3(ctrl_wr_data[38]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][38]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][39]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[39]),
        .I3(ctrl_wr_data[39]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][39]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \reg[data][3]_i_1 
       (.I0(\reg[data][7]_i_5_n_0 ),
        .I1(\reg[data][7]_i_6_n_0 ),
        .I2(\reg[data][7]_i_7_n_0 ),
        .I3(ow_rd_data[3]),
        .I4(ctrl_wr_data[3]),
        .I5(\reg[data][7]_i_8_n_0 ),
        .O(\reg[data][3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][40]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[40]),
        .I3(ctrl_wr_data[40]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][40]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][41]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[41]),
        .I3(ctrl_wr_data[41]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][41]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][42]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[42]),
        .I3(ctrl_wr_data[42]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][42]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][43]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[43]),
        .I3(ctrl_wr_data[43]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][43]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][44]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[44]),
        .I3(ctrl_wr_data[44]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][44]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][45]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[45]),
        .I3(ctrl_wr_data[45]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][45]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][46]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[46]),
        .I3(ctrl_wr_data[46]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][46]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][47]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[47]),
        .I3(ctrl_wr_data[47]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][47]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][48]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[48]),
        .I3(ctrl_wr_data[48]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][48]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][49]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[49]),
        .I3(ctrl_wr_data[49]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][49]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFCC80)) 
    \reg[data][4]_i_1 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(ctrl_wr_data[4]),
        .I2(\reg[data][5]_i_3_n_0 ),
        .I3(\reg[data][5]_i_4_n_0 ),
        .I4(\reg[data][4]_i_2_n_0 ),
        .I5(\reg[data][4]_i_3_n_0 ),
        .O(\reg[data][4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0F00020000000000)) 
    \reg[data][4]_i_2 
       (.I0(\reg[lfsr][0]_i_9_n_0 ),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(\reg[state] [1]),
        .O(\reg[data][4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0008000000000000)) 
    \reg[data][4]_i_3 
       (.I0(ow_rd_data[4]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(\reg[state] [1]),
        .I3(ow_reg_rst),
        .I4(axi_aresetn_i),
        .I5(\reg[state] [3]),
        .O(\reg[data][4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][50]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[50]),
        .I3(ctrl_wr_data[50]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][50]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][51]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[51]),
        .I3(ctrl_wr_data[51]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][51]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][52]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[52]),
        .I3(ctrl_wr_data[52]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][52]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][53]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[53]),
        .I3(ctrl_wr_data[53]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][53]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][54]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[54]),
        .I3(ctrl_wr_data[54]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][54]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][55]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[55]),
        .I3(ctrl_wr_data[55]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][55]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][56]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[56]),
        .I3(ctrl_wr_data[56]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][56]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][57]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[57]),
        .I3(ctrl_wr_data[57]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][57]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][58]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[58]),
        .I3(ctrl_wr_data[58]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][58]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][59]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[59]),
        .I3(ctrl_wr_data[59]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][59]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFEFEFEFEEEFEEEEE)) 
    \reg[data][5]_i_1 
       (.I0(\reg[data][7]_i_6_n_0 ),
        .I1(\reg[data][5]_i_2_n_0 ),
        .I2(ctrl_wr_data[5]),
        .I3(\FSM_sequential_reg[state][3]_i_5_n_0 ),
        .I4(\reg[data][5]_i_3_n_0 ),
        .I5(\reg[data][5]_i_4_n_0 ),
        .O(\reg[data][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \reg[data][5]_i_2 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(ow_rd_data[5]),
        .O(\reg[data][5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF0000FFFE)) 
    \reg[data][5]_i_3 
       (.I0(\reg[lfsr][26]_i_12_n_0 ),
        .I1(\reg[data][5]_i_5_n_0 ),
        .I2(lfsr_shift[14]),
        .I3(lfsr_shift[4]),
        .I4(ow_rst),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[data][5]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h0D000400)) 
    \reg[data][5]_i_4 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[state] [1]),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\reg[state] [3]),
        .O(\reg[data][5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFBFF)) 
    \reg[data][5]_i_5 
       (.I0(lfsr_shift[25]),
        .I1(lfsr_shift[8]),
        .I2(lfsr_shift[20]),
        .I3(\reg[data][5]_i_6_n_0 ),
        .I4(\reg[lfsr][26]_i_14_n_0 ),
        .I5(lfsr_shift[24]),
        .O(\reg[data][5]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \reg[data][5]_i_6 
       (.I0(lfsr_shift[18]),
        .I1(lfsr_shift[17]),
        .O(\reg[data][5]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][60]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[60]),
        .I3(ctrl_wr_data[60]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][60]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][61]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[61]),
        .I3(ctrl_wr_data[61]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][61]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][62]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[62]),
        .I3(ctrl_wr_data[62]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][62]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \reg[data][62]_i_2 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I4(\reg[state] [1]),
        .O(\reg[data][62]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4444444444004440)) 
    \reg[data][62]_i_3 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [3]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I5(\reg[state] [1]),
        .O(\reg[data][62]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h10)) 
    \reg[data][63]_i_11 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(\reg[state] [1]),
        .I2(\reg[state] [3]),
        .O(\reg[data][63]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAA0088F0AA008800)) 
    \reg[data][63]_i_2 
       (.I0(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I1(if_rx_data),
        .I2(ow_rd_data[63]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I4(\reg[state] [1]),
        .I5(\FSM_sequential_reg[state][2]_i_6_n_0 ),
        .O(\reg[data][63]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'h08000000)) 
    \reg[data][63]_i_3 
       (.I0(\reg[state] [3]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\reg[state] [1]),
        .I4(if_done),
        .O(\reg[data][63]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888880)) 
    \reg[data][63]_i_4 
       (.I0(\reg[data][63]_i_7_n_0 ),
        .I1(\reg[data][63]_i_8_n_0 ),
        .I2(\reg[lfsr][26]_i_17_n_0 ),
        .I3(\reg[lfsr][26]_i_22_n_0 ),
        .I4(lfsr_shift[7]),
        .I5(\reg[lfsr][26]_i_23_n_0 ),
        .O(\reg[data][63]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \reg[data][63]_i_6 
       (.I0(\reg[state] [3]),
        .I1(if_done),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\reg[state] [1]),
        .O(\reg[data][63]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \reg[data][63]_i_7 
       (.I0(\reg[state] [3]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[data][63]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \reg[data][63]_i_8 
       (.I0(if_done),
        .I1(\reg[state] [1]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[data][63]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \reg[data][63]_i_9 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\FSM_sequential_reg[state][3]_i_6_0 ),
        .I2(\reg_reg[busy]_0 ),
        .I3(\FSM_sequential_reg_reg[state][2]_1 ),
        .O(\reg[data][63]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEFE)) 
    \reg[data][6]_i_1 
       (.I0(\reg[data][6]_i_2_n_0 ),
        .I1(\reg[data][6]_i_3_n_0 ),
        .I2(\reg[data][6]_i_4_n_0 ),
        .I3(\reg[data][6]_i_5_n_0 ),
        .I4(\reg[data][6]_i_6_n_0 ),
        .I5(\reg[data][6]_i_7_n_0 ),
        .O(\reg[data][6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \reg[data][6]_i_2 
       (.I0(\reg[state] [1]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I2(axi_aresetn_i),
        .I3(ow_reg_rst),
        .O(\reg[data][6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000FF0008)) 
    \reg[data][6]_i_3 
       (.I0(\reg[mem_rd_en]_i_2_n_0 ),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(\reg[state] [3]),
        .I4(\reg[state] [1]),
        .I5(ow_rst),
        .O(\reg[data][6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF200020002000)) 
    \reg[data][6]_i_4 
       (.I0(\reg[state] [3]),
        .I1(ow_rst),
        .I2(ctrl_wr_data[6]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I4(ow_rd_data[6]),
        .I5(\reg[data][62]_i_2_n_0 ),
        .O(\reg[data][6]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h04000000)) 
    \reg[data][6]_i_5 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(ctrl_wr_data[6]),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\reg[state] [1]),
        .O(\reg[data][6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \reg[data][6]_i_6 
       (.I0(\reg[lfsr][26]_i_23_n_0 ),
        .I1(lfsr_shift[2]),
        .I2(lfsr_shift[1]),
        .I3(lfsr_shift[3]),
        .I4(lfsr_shift[7]),
        .I5(\reg[lfsr][26]_i_17_n_0 ),
        .O(\reg[data][6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFC20000)) 
    \reg[data][6]_i_7 
       (.I0(\reg[state] [3]),
        .I1(\reg[state] [1]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I4(ctrl_wr_data[6]),
        .I5(ow_rst),
        .O(\reg[data][6]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \reg[data][7]_i_10 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I2(axi_aresetn_i),
        .I3(ow_reg_rst),
        .O(\reg[data][7]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFEEEFEEE)) 
    \reg[data][7]_i_2 
       (.I0(\reg[data][7]_i_5_n_0 ),
        .I1(\reg[data][7]_i_6_n_0 ),
        .I2(\reg[data][7]_i_7_n_0 ),
        .I3(ow_rd_data[7]),
        .I4(ctrl_wr_data[7]),
        .I5(\reg[data][7]_i_8_n_0 ),
        .O(\reg[data][7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \reg[data][7]_i_3 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(\reg[state] [1]),
        .I2(if_done),
        .I3(\reg[state] [3]),
        .O(\reg[data][7]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hFFFF2000)) 
    \reg[data][7]_i_4 
       (.I0(if_done),
        .I1(\reg[state] [3]),
        .I2(\reg[state] [1]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(\reg[lfsr][26]_i_6_n_0 ),
        .O(\reg[data][7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h01000000)) 
    \reg[data][7]_i_5 
       (.I0(\reg[state] [3]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\reg[state] [1]),
        .O(\reg[data][7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000F2002200)) 
    \reg[data][7]_i_6 
       (.I0(\reg[lfsr][0]_i_9_n_0 ),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\reg[state] [1]),
        .I4(\reg[state] [3]),
        .I5(ow_rst),
        .O(\reg[data][7]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h01000000)) 
    \reg[data][7]_i_7 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(\reg[state] [1]),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[data][7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFAEFFAEFFAEAEAE)) 
    \reg[data][7]_i_8 
       (.I0(\reg[data][7]_i_9_n_0 ),
        .I1(\reg[mem_addr][1]_i_3_n_0 ),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\reg[data][7]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_15_n_0 ),
        .I5(\reg[lfsr][26]_i_12_n_0 ),
        .O(\reg[data][7]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h003B000000380000)) 
    \reg[data][7]_i_9 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(\reg[state] [1]),
        .I3(ow_reg_rst),
        .I4(axi_aresetn_i),
        .I5(\reg[state] [3]),
        .O(\reg[data][7]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][8]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[8]),
        .I3(ctrl_wr_data[8]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFEAEAEA)) 
    \reg[data][9]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\reg[data][62]_i_2_n_0 ),
        .I2(ow_rd_data[9]),
        .I3(ctrl_wr_data[9]),
        .I4(\reg[data][62]_i_3_n_0 ),
        .O(\reg[data][9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \reg[device_count][0]_i_1 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [1]),
        .I3(\reg_reg[device_count][0]_0 ),
        .O(\reg[device_count][0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'h00080800)) 
    \reg[device_count][1]_i_1 
       (.I0(\reg[state] [1]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(\reg_reg[device_count][0]_0 ),
        .I4(\reg_reg[device_count][1]_0 ),
        .O(\reg[device_count][1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0008080808000000)) 
    \reg[device_count][2]_i_1 
       (.I0(\reg[state] [1]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(\reg_reg[device_count][0]_0 ),
        .I4(\reg_reg[device_count][1]_0 ),
        .I5(\reg_reg[device_count][2]_0 ),
        .O(\reg[device_count][2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h007F000000800000)) 
    \reg[device_count][3]_i_1 
       (.I0(\reg_reg[device_count][0]_0 ),
        .I1(\reg_reg[device_count][1]_0 ),
        .I2(\reg_reg[device_count][2]_0 ),
        .I3(ow_rst),
        .I4(\reg[state] [1]),
        .I5(\reg_reg[device_count][3]_0 ),
        .O(\reg[device_count][3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFF000080000000)) 
    \reg[device_count][4]_i_1 
       (.I0(\reg_reg[device_count][2]_0 ),
        .I1(\reg_reg[device_count][1]_0 ),
        .I2(\reg_reg[device_count][0]_0 ),
        .I3(\reg_reg[device_count][3]_0 ),
        .I4(\reg[mem_addr][1]_i_3_n_0 ),
        .I5(\reg_reg[device_count][4]_0 ),
        .O(\reg[device_count][4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000200)) 
    \reg[device_count][5]_i_1 
       (.I0(dcvr_id_en),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(\reg[state] [3]),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(\reg[lfsr][26]_i_6_n_0 ),
        .O(\reg[device_count][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0B00000004000000)) 
    \reg[device_count][5]_i_2 
       (.I0(\reg[device_count][5]_i_3_n_0 ),
        .I1(\reg_reg[device_count][4]_0 ),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\reg[state] [1]),
        .I5(ow_dev_count),
        .O(\reg[device_count][5]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \reg[device_count][5]_i_3 
       (.I0(\reg_reg[device_count][2]_0 ),
        .I1(\reg_reg[device_count][1]_0 ),
        .I2(\reg_reg[device_count][0]_0 ),
        .I3(\reg_reg[device_count][3]_0 ),
        .O(\reg[device_count][5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000040000000000)) 
    \reg[discover]_i_1 
       (.I0(p_0_in8_in),
        .I1(mem_wr_done),
        .I2(\reg[state] [3]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(ow_rst),
        .I5(\reg[discover]_i_2_n_0 ),
        .O(\reg[discover] ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \reg[discover]_i_2 
       (.I0(\reg[state] [1]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[discover]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0808FF0808080808)) 
    \reg[done]_i_1__1 
       (.I0(\FSM_sequential_reg[state][1]_i_2__0_n_0 ),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(p_0_in8_in),
        .I3(\reg[state] [1]),
        .I4(ow_rst),
        .I5(\reg[done]_i_2__1_n_0 ),
        .O(\reg[done] ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \reg[done]_i_2__1 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(dcvr_done),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(\reg[state] [3]),
        .O(\reg[done]_i_2__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \reg[lfsr][0]_i_10 
       (.I0(lfsr_shift[5]),
        .I1(\reg[mem_rd_en]_i_6_n_0 ),
        .I2(lfsr_shift[6]),
        .I3(lfsr_shift[8]),
        .I4(lfsr_shift[9]),
        .I5(lfsr_shift[7]),
        .O(\reg[lfsr][0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFEEFFEE)) 
    \reg[lfsr][0]_i_1__1 
       (.I0(\reg[lfsr][0]_i_2_n_0 ),
        .I1(\reg[lfsr][0]_i_3__0_n_0 ),
        .I2(\reg[lfsr][0]_i_4__0_n_0 ),
        .I3(\reg[lfsr][0]_i_5_n_0 ),
        .I4(\reg[lfsr][0]_i_6_n_0 ),
        .I5(\reg[lfsr][0]_i_7__0_n_0 ),
        .O(p_1_in[0]));
  LUT6 #(
    .INIT(64'hAAAAAAAAEEFEFEFE)) 
    \reg[lfsr][0]_i_2 
       (.I0(ow_rst),
        .I1(lfsr_shift__0),
        .I2(\reg[mem_rd_en]_i_2_n_0 ),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(\reg[state] [3]),
        .O(\reg[lfsr][0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hB0A03333)) 
    \reg[lfsr][0]_i_3__0 
       (.I0(lfsr_shift__0),
        .I1(\reg[state] [3]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[lfsr][0]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \reg[lfsr][0]_i_4__0 
       (.I0(\reg[state] [3]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[lfsr][0]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'h5151510051005100)) 
    \reg[lfsr][0]_i_5 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(\reg[state] [1]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(lfsr_shift__0),
        .I4(\reg[mem_rd_en]_i_2_n_0 ),
        .I5(\reg[state] [3]),
        .O(\reg[lfsr][0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \reg[lfsr][0]_i_6 
       (.I0(\reg[state] [3]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I2(\reg[state] [1]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[lfsr][0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'hBEEBEBBE)) 
    \reg[lfsr][0]_i_7__0 
       (.I0(\reg[lfsr][0]_i_9_n_0 ),
        .I1(lfsr_shift[22]),
        .I2(\reg_reg[lfsr_n_0_][26] ),
        .I3(lfsr_shift[26]),
        .I4(lfsr_shift[25]),
        .O(\reg[lfsr][0]_i_7__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \reg[lfsr][0]_i_8__0 
       (.I0(lfsr_shift[25]),
        .I1(lfsr_shift[26]),
        .I2(\reg_reg[lfsr_n_0_][26] ),
        .I3(lfsr_shift[22]),
        .O(lfsr_shift__0));
  LUT4 #(
    .INIT(16'h0100)) 
    \reg[lfsr][0]_i_9 
       (.I0(\reg[lfsr][26]_i_26_n_0 ),
        .I1(\reg[mem_rd_en]_i_3_n_0 ),
        .I2(lfsr_shift[17]),
        .I3(\reg[lfsr][0]_i_10_n_0 ),
        .O(\reg[lfsr][0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][10]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[10]),
        .O(p_1_in[10]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][11]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[11]),
        .O(p_1_in[11]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][12]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[12]),
        .O(p_1_in[12]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][13]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[13]),
        .O(p_1_in[13]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][14]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[14]),
        .O(p_1_in[14]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][15]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[15]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][16]_i_1__0 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[16]),
        .O(p_1_in[16]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][17]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[17]),
        .O(p_1_in[17]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][18]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[18]),
        .O(p_1_in[18]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][19]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[19]),
        .O(p_1_in[19]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][1]_i_1__0 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[1]),
        .O(p_1_in[1]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][20]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[20]),
        .O(p_1_in[20]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][21]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[21]),
        .O(p_1_in[21]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][22]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[22]),
        .O(p_1_in[22]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][23]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[23]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][24]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[24]),
        .O(p_1_in[24]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][25]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[25]),
        .O(p_1_in[25]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFA2)) 
    \reg[lfsr][26]_i_1 
       (.I0(\reg[lfsr][26]_i_3_n_0 ),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I2(\reg[lfsr][26]_i_4_n_0 ),
        .I3(\reg[lfsr][26]_i_5_n_0 ),
        .I4(\reg[lfsr][26]_i_6_n_0 ),
        .I5(\reg[lfsr][26]_i_7_n_0 ),
        .O(\reg[lfsr][26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    \reg[lfsr][26]_i_10 
       (.I0(\reg[lfsr][26]_i_20_n_0 ),
        .I1(\reg[lfsr][26]_i_15_n_0 ),
        .I2(\reg[lfsr][26]_i_21_n_0 ),
        .I3(lfsr_shift[5]),
        .I4(lfsr_shift[6]),
        .I5(\reg_reg[lfsr_n_0_][26] ),
        .O(\reg[lfsr][26]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888880)) 
    \reg[lfsr][26]_i_11 
       (.I0(\reg[lfsr][26]_i_19_n_0 ),
        .I1(\reg[bit_send]_i_2__0_n_0 ),
        .I2(\reg[lfsr][26]_i_17_n_0 ),
        .I3(\reg[lfsr][26]_i_22_n_0 ),
        .I4(lfsr_shift[7]),
        .I5(\reg[lfsr][26]_i_23_n_0 ),
        .O(\reg[lfsr][26]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \reg[lfsr][26]_i_12 
       (.I0(lfsr_shift[12]),
        .I1(\reg[mem_rd_en]_i_3_n_0 ),
        .I2(lfsr_shift[10]),
        .I3(\reg[lfsr][26]_i_24_n_0 ),
        .I4(\reg[lfsr][26]_i_25_n_0 ),
        .I5(\reg_reg[lfsr_n_0_][26] ),
        .O(\reg[lfsr][26]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \reg[lfsr][26]_i_13 
       (.I0(lfsr_shift[18]),
        .I1(lfsr_shift[17]),
        .I2(lfsr_shift[20]),
        .I3(lfsr_shift[8]),
        .I4(lfsr_shift[25]),
        .O(\reg[lfsr][26]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg[lfsr][26]_i_14 
       (.I0(lfsr_shift[22]),
        .I1(lfsr_shift[19]),
        .I2(lfsr_shift[21]),
        .I3(lfsr_shift[26]),
        .I4(lfsr_shift[23]),
        .O(\reg[lfsr][26]_i_14_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg[lfsr][26]_i_15 
       (.I0(lfsr_shift[4]),
        .I1(lfsr_shift[14]),
        .I2(lfsr_shift[24]),
        .I3(\reg[lfsr][26]_i_14_n_0 ),
        .I4(\reg[lfsr][26]_i_13_n_0 ),
        .O(\reg[lfsr][26]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h0004000000000000)) 
    \reg[lfsr][26]_i_16 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(\reg[state] [3]),
        .I4(\reg[state] [1]),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[lfsr][26]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \reg[lfsr][26]_i_17 
       (.I0(lfsr_shift[18]),
        .I1(lfsr_shift[17]),
        .I2(lfsr_shift[8]),
        .I3(lfsr_shift[6]),
        .I4(lfsr_shift[5]),
        .I5(\reg[lfsr][26]_i_26_n_0 ),
        .O(\reg[lfsr][26]_i_17_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg[lfsr][26]_i_18 
       (.I0(lfsr_shift[3]),
        .I1(lfsr_shift[1]),
        .I2(lfsr_shift[2]),
        .I3(lfsr_shift[7]),
        .I4(\reg[lfsr][26]_i_23_n_0 ),
        .O(\reg[lfsr][26]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \reg[lfsr][26]_i_19 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[state] [1]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[lfsr][26]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][26]_i_2 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[26]),
        .O(p_1_in[26]));
  LUT6 #(
    .INIT(64'h0000100010001000)) 
    \reg[lfsr][26]_i_20 
       (.I0(\reg[state] [3]),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I4(\reg[state] [1]),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[lfsr][26]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg[lfsr][26]_i_21 
       (.I0(lfsr_shift[7]),
        .I1(lfsr_shift[9]),
        .I2(lfsr_shift[10]),
        .I3(\reg[mem_rd_en]_i_3_n_0 ),
        .I4(lfsr_shift[12]),
        .O(\reg[lfsr][26]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \reg[lfsr][26]_i_22 
       (.I0(lfsr_shift[3]),
        .I1(lfsr_shift[1]),
        .I2(lfsr_shift[2]),
        .O(\reg[lfsr][26]_i_22_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg[lfsr][26]_i_23 
       (.I0(\reg[lfsr][26]_i_27_n_0 ),
        .I1(lfsr_shift[24]),
        .I2(\reg_reg[lfsr_n_0_][26] ),
        .I3(lfsr_shift[25]),
        .I4(lfsr_shift[9]),
        .O(\reg[lfsr][26]_i_23_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \reg[lfsr][26]_i_24 
       (.I0(lfsr_shift[7]),
        .I1(lfsr_shift[9]),
        .O(\reg[lfsr][26]_i_24_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \reg[lfsr][26]_i_25 
       (.I0(lfsr_shift[5]),
        .I1(lfsr_shift[6]),
        .O(\reg[lfsr][26]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFBFFFFFFF)) 
    \reg[lfsr][26]_i_26 
       (.I0(lfsr_shift[4]),
        .I1(lfsr_shift[20]),
        .I2(lfsr_shift[14]),
        .I3(lfsr_shift[12]),
        .I4(lfsr_shift[10]),
        .I5(\reg[lfsr][26]_i_14_n_0 ),
        .O(\reg[lfsr][26]_i_26_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \reg[lfsr][26]_i_27 
       (.I0(lfsr_shift[16]),
        .I1(lfsr_shift[13]),
        .I2(lfsr_shift[15]),
        .I3(lfsr_shift[11]),
        .O(\reg[lfsr][26]_i_27_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \reg[lfsr][26]_i_3 
       (.I0(if_rx_data_en),
        .I1(\reg[state] [3]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\reg[state] [1]),
        .O(\reg[lfsr][26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \reg[lfsr][26]_i_4 
       (.I0(\reg[lfsr][26]_i_12_n_0 ),
        .I1(\reg[lfsr][26]_i_13_n_0 ),
        .I2(\reg[lfsr][26]_i_14_n_0 ),
        .I3(lfsr_shift[24]),
        .I4(lfsr_shift[14]),
        .I5(lfsr_shift[4]),
        .O(\reg[lfsr][26]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \reg[lfsr][26]_i_5 
       (.I0(\reg[state] [3]),
        .I1(if_done),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[lfsr][26]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000002)) 
    \reg[lfsr][26]_i_6 
       (.I0(ow_disc_trig_reg),
        .I1(\reg[state] [3]),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I5(ow_rst),
        .O(\reg[lfsr][26]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'h1100C040)) 
    \reg[lfsr][26]_i_7 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[state] [3]),
        .I2(if_done),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .O(\reg[lfsr][26]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF0E000000)) 
    \reg[lfsr][26]_i_8 
       (.I0(\reg[lfsr][26]_i_12_n_0 ),
        .I1(\reg[lfsr][26]_i_15_n_0 ),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I5(\reg[lfsr][26]_i_16_n_0 ),
        .O(\reg[lfsr][26]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hF0EFF0EFF0EFF0E0)) 
    \reg[lfsr][26]_i_9 
       (.I0(\reg[lfsr][26]_i_17_n_0 ),
        .I1(\reg[lfsr][26]_i_18_n_0 ),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I3(\reg[lfsr][26]_i_19_n_0 ),
        .I4(\reg[lfsr][26]_i_15_n_0 ),
        .I5(\reg[lfsr][26]_i_12_n_0 ),
        .O(\reg[lfsr][26]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][2]_i_1__0 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[2]),
        .O(p_1_in[2]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][3]_i_1__0 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[3]),
        .O(p_1_in[3]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][4]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[4]),
        .O(p_1_in[4]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][5]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[5]),
        .O(p_1_in[5]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][6]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[6]),
        .O(p_1_in[6]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][7]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[7]),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][8]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[8]),
        .O(p_1_in[8]));
  LUT6 #(
    .INIT(64'hFFFFFFEA00000000)) 
    \reg[lfsr][9]_i_1 
       (.I0(\reg[lfsr][26]_i_8_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I2(\reg[lfsr][26]_i_9_n_0 ),
        .I3(\reg[lfsr][26]_i_10_n_0 ),
        .I4(\reg[lfsr][26]_i_11_n_0 ),
        .I5(lfsr_shift[9]),
        .O(p_1_in[9]));
  LUT6 #(
    .INIT(64'h000C000C0F020C00)) 
    \reg[mem_addr][0]_i_1 
       (.I0(p_0_in8_in),
        .I1(\reg[state] [1]),
        .I2(ow_rst),
        .I3(\reg[state] [3]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I5(\reg_reg[mem_addr][0]_0 ),
        .O(\reg[mem_addr][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000444400008F88)) 
    \reg[mem_addr][1]_i_1 
       (.I0(\reg_reg[mem_addr][0]_0 ),
        .I1(\reg[mem_addr][1]_i_2_n_0 ),
        .I2(\reg[state] [3]),
        .I3(\reg[mem_addr][1]_i_3_n_0 ),
        .I4(\reg[mem_addr][5]_i_8_n_0 ),
        .I5(\reg_reg[mem_addr][1]_0 ),
        .O(\reg[mem_addr][1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h0F000200)) 
    \reg[mem_addr][1]_i_2 
       (.I0(p_0_in8_in),
        .I1(\reg[state] [1]),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\reg[state] [3]),
        .O(\reg[mem_addr][1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \reg[mem_addr][1]_i_3 
       (.I0(\reg[state] [1]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\reg[mem_addr][1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hACA0ACA0ACA00000)) 
    \reg[mem_addr][2]_i_1 
       (.I0(\reg[mem_addr][2]_i_2_n_0 ),
        .I1(\reg[mem_addr][2]_i_3_n_0 ),
        .I2(\reg_reg[mem_addr][2]_0 ),
        .I3(\reg_reg[mem_addr][1]_0 ),
        .I4(\reg[state] [1]),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[mem_addr][2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000773300007770)) 
    \reg[mem_addr][2]_i_2 
       (.I0(\reg_reg[mem_addr][0]_0 ),
        .I1(\reg_reg[mem_addr][1]_0 ),
        .I2(p_0_in8_in),
        .I3(\reg[state] [3]),
        .I4(ow_rst),
        .I5(\reg[state] [1]),
        .O(\reg[mem_addr][2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AFA80000)) 
    \reg[mem_addr][2]_i_3 
       (.I0(\reg_reg[mem_addr][0]_0 ),
        .I1(p_0_in8_in),
        .I2(\reg[state] [3]),
        .I3(\reg[state] [1]),
        .I4(axi_aresetn_i),
        .I5(ow_reg_rst),
        .O(\reg[mem_addr][2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hAAA0CCC0)) 
    \reg[mem_addr][3]_i_1 
       (.I0(\reg[mem_addr][3]_i_2_n_0 ),
        .I1(\reg[mem_addr][5]_i_7_n_0 ),
        .I2(\reg[state] [1]),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(\reg_reg[mem_addr][3]_0 ),
        .O(\reg[mem_addr][3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0FFFFFFF00222222)) 
    \reg[mem_addr][3]_i_2 
       (.I0(\reg[mem_addr][1]_i_3_n_0 ),
        .I1(\reg[state] [3]),
        .I2(\reg_reg[mem_addr][0]_0 ),
        .I3(\reg_reg[mem_addr][2]_0 ),
        .I4(\reg_reg[mem_addr][1]_0 ),
        .I5(\reg[mem_addr][1]_i_2_n_0 ),
        .O(\reg[mem_addr][3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hACA0ACA0ACA00000)) 
    \reg[mem_addr][4]_i_1 
       (.I0(\reg[mem_addr][4]_i_2_n_0 ),
        .I1(\reg[mem_addr][5]_i_7_n_0 ),
        .I2(\reg_reg[mem_addr][4]_0 ),
        .I3(\reg_reg[mem_addr][3]_0 ),
        .I4(\reg[state] [1]),
        .I5(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[mem_addr][4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00550054)) 
    \reg[mem_addr][4]_i_2 
       (.I0(\reg_reg[mem_addr][3]_0 ),
        .I1(p_0_in8_in),
        .I2(\reg[state] [3]),
        .I3(ow_rst),
        .I4(\reg[state] [1]),
        .I5(\reg[mem_addr][3]_i_2_n_0 ),
        .O(\reg[mem_addr][4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFCFCFCFCFEFFFCFC)) 
    \reg[mem_addr][5]_i_1 
       (.I0(p_0_in8_in),
        .I1(\reg[mem_addr][5]_i_3_n_0 ),
        .I2(\reg[mem_addr][5]_i_4_n_0 ),
        .I3(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I4(\reg[state] [3]),
        .I5(\reg[mem_addr][5]_i_5_n_0 ),
        .O(\reg[mem_addr][5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h00F000E0)) 
    \reg[mem_addr][5]_i_11 
       (.I0(p_0_in8_in),
        .I1(\reg[state] [3]),
        .I2(axi_aresetn_i),
        .I3(ow_reg_rst),
        .I4(\reg[state] [1]),
        .O(\reg[mem_addr][5]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAEAAAAAA)) 
    \reg[mem_addr][5]_i_2 
       (.I0(\reg[mem_addr][5]_i_6_n_0 ),
        .I1(\reg[mem_addr][5]_i_7_n_0 ),
        .I2(\reg_reg[mem_addr][5]_0 ),
        .I3(\reg_reg[mem_addr][3]_0 ),
        .I4(\reg_reg[mem_addr][4]_0 ),
        .I5(\reg[mem_addr][5]_i_8_n_0 ),
        .O(\reg[mem_addr][5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCCCCEECCCCCCFCCC)) 
    \reg[mem_addr][5]_i_3 
       (.I0(dcvr_id_en),
        .I1(\reg[lfsr][26]_i_6_n_0 ),
        .I2(ow_temp_trig_reg),
        .I3(\reg[mem_addr][5]_i_9_n_0 ),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(\reg[state] [1]),
        .O(\reg[mem_addr][5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8880008000800080)) 
    \reg[mem_addr][5]_i_4 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[discover]_i_2_n_0 ),
        .I2(mem_wr_done),
        .I3(\reg[state] [3]),
        .I4(\reg_reg[mem_addr][0]_1 ),
        .I5(\FSM_sequential_reg_reg[state][2]_1 ),
        .O(\reg[mem_addr][5]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \reg[mem_addr][5]_i_5 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I1(\reg[state] [1]),
        .O(\reg[mem_addr][5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h4444040044444400)) 
    \reg[mem_addr][5]_i_6 
       (.I0(\reg[mem_addr][5]_i_8_n_0 ),
        .I1(\reg_reg[mem_addr][5]_0 ),
        .I2(\reg_reg[mem_addr][3]_0 ),
        .I3(\reg[mem_addr][5]_i_11_n_0 ),
        .I4(\reg[mem_addr][3]_i_2_n_0 ),
        .I5(\reg_reg[mem_addr][4]_0 ),
        .O(\reg[mem_addr][5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hF200000022000000)) 
    \reg[mem_addr][5]_i_7 
       (.I0(\reg[mem_addr][1]_i_3_n_0 ),
        .I1(\reg[state] [3]),
        .I2(\reg_reg[mem_addr][0]_0 ),
        .I3(\reg_reg[mem_addr][2]_0 ),
        .I4(\reg_reg[mem_addr][1]_0 ),
        .I5(\reg[mem_addr][1]_i_2_n_0 ),
        .O(\reg[mem_addr][5]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \reg[mem_addr][5]_i_8 
       (.I0(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I1(\reg[state] [1]),
        .O(\reg[mem_addr][5]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \reg[mem_addr][5]_i_9 
       (.I0(\reg[state] [3]),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .O(\reg[mem_addr][5]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    \reg[mem_rd_en]_i_1 
       (.I0(\FSM_sequential_reg[state][2]_i_4_n_0 ),
        .I1(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I2(if_done),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I5(\reg[mem_rd_en]_i_2_n_0 ),
        .O(\reg[mem_rd_en] ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \reg[mem_rd_en]_i_2 
       (.I0(\reg[mem_rd_en]_i_3_n_0 ),
        .I1(\reg[mem_rd_en]_i_4_n_0 ),
        .I2(lfsr_shift[4]),
        .I3(lfsr_shift[5]),
        .I4(lfsr_shift[6]),
        .I5(\reg[mem_rd_en]_i_5_n_0 ),
        .O(\reg[mem_rd_en]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg[mem_rd_en]_i_3 
       (.I0(lfsr_shift[11]),
        .I1(lfsr_shift[15]),
        .I2(lfsr_shift[13]),
        .I3(lfsr_shift[16]),
        .I4(\reg[lfsr][26]_i_22_n_0 ),
        .O(\reg[mem_rd_en]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \reg[mem_rd_en]_i_4 
       (.I0(lfsr_shift[10]),
        .I1(lfsr_shift[12]),
        .I2(lfsr_shift[14]),
        .I3(lfsr_shift[17]),
        .I4(lfsr_shift[9]),
        .I5(lfsr_shift[7]),
        .O(\reg[mem_rd_en]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \reg[mem_rd_en]_i_5 
       (.I0(lfsr_shift[20]),
        .I1(\reg[mem_rd_en]_i_6_n_0 ),
        .I2(lfsr_shift[8]),
        .I3(\reg[lfsr][26]_i_14_n_0 ),
        .O(\reg[mem_rd_en]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \reg[mem_rd_en]_i_6 
       (.I0(lfsr_shift[25]),
        .I1(\reg_reg[lfsr_n_0_][26] ),
        .I2(lfsr_shift[24]),
        .I3(lfsr_shift[18]),
        .O(\reg[mem_rd_en]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAEAAAAAA)) 
    \reg[mem_wr_en]_i_1 
       (.I0(\reg[mem_wr_en]_i_2_n_0 ),
        .I1(\reg[data][7]_i_7_n_0 ),
        .I2(\reg[state] [3]),
        .I3(mem_wr_done),
        .I4(p_0_in8_in),
        .O(\reg[mem_wr_en] ));
  LUT6 #(
    .INIT(64'h0300000000000002)) 
    \reg[mem_wr_en]_i_2 
       (.I0(ow_disc_trig_reg),
        .I1(ow_rst),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(\reg[state] [3]),
        .I4(\FSM_sequential_reg_reg[state][2]_0 [1]),
        .I5(\reg[state] [1]),
        .O(\reg[mem_wr_en]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h040404FF04040400)) 
    \reg[strong_pullup]_i_1 
       (.I0(ow_rst),
        .I1(if_done),
        .I2(\FSM_sequential_reg[state][3]_i_5_n_0 ),
        .I3(\reg[strong_pullup]_i_2_n_0 ),
        .I4(\reg[strong_pullup]_i_3_n_0 ),
        .I5(strong_pullup),
        .O(\reg[strong_pullup]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h3131003100310031)) 
    \reg[strong_pullup]_i_10 
       (.I0(lfsr_shift[19]),
        .I1(lfsr_shift[21]),
        .I2(lfsr_shift[20]),
        .I3(lfsr_shift[18]),
        .I4(lfsr_shift[16]),
        .I5(lfsr_shift[17]),
        .O(\reg[strong_pullup]_i_10_n_0 ));
  LUT3 #(
    .INIT(8'hF4)) 
    \reg[strong_pullup]_i_11 
       (.I0(lfsr_shift[5]),
        .I1(lfsr_shift[4]),
        .I2(lfsr_shift[6]),
        .O(\reg[strong_pullup]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00200000)) 
    \reg[strong_pullup]_i_2 
       (.I0(\reg[mem_rd_en]_i_2_n_0 ),
        .I1(\FSM_sequential_reg[state][3]_i_5_n_0 ),
        .I2(\FSM_sequential_reg_reg[state][2]_0 [0]),
        .I3(\reg[state] [3]),
        .I4(if_done),
        .I5(\reg[lfsr][26]_i_6_n_0 ),
        .O(\reg[strong_pullup]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \reg[strong_pullup]_i_3 
       (.I0(lfsr_shift[21]),
        .I1(lfsr_shift[20]),
        .I2(lfsr_shift[22]),
        .I3(lfsr_shift[23]),
        .I4(\reg[strong_pullup]_i_4_n_0 ),
        .I5(\reg[strong_pullup]_i_5_n_0 ),
        .O(\reg[strong_pullup]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    \reg[strong_pullup]_i_4 
       (.I0(\reg[mem_addr][5]_i_5_n_0 ),
        .I1(\reg[mem_addr][5]_i_9_n_0 ),
        .I2(lfsr_shift[24]),
        .I3(lfsr_shift[25]),
        .I4(lfsr_shift[26]),
        .I5(\reg_reg[lfsr_n_0_][26] ),
        .O(\reg[strong_pullup]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0008000000000000)) 
    \reg[strong_pullup]_i_5 
       (.I0(\reg[strong_pullup]_i_6_n_0 ),
        .I1(\reg[strong_pullup]_i_7_n_0 ),
        .I2(\reg[strong_pullup]_i_8_n_0 ),
        .I3(\FSM_sequential_reg[state][3]_i_19_n_0 ),
        .I4(\reg[strong_pullup]_i_9_n_0 ),
        .I5(\reg[strong_pullup]_i_10_n_0 ),
        .O(\reg[strong_pullup]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \reg[strong_pullup]_i_6 
       (.I0(lfsr_shift[11]),
        .I1(lfsr_shift[12]),
        .I2(lfsr_shift[15]),
        .I3(lfsr_shift[14]),
        .I4(lfsr_shift[18]),
        .I5(lfsr_shift[17]),
        .O(\reg[strong_pullup]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \reg[strong_pullup]_i_7 
       (.I0(\reg[strong_pullup]_i_11_n_0 ),
        .I1(lfsr_shift[3]),
        .I2(lfsr_shift[9]),
        .I3(lfsr_shift[1]),
        .I4(lfsr_shift[2]),
        .O(\reg[strong_pullup]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h4F)) 
    \reg[strong_pullup]_i_8 
       (.I0(lfsr_shift[8]),
        .I1(lfsr_shift[7]),
        .I2(lfsr_shift[9]),
        .O(\reg[strong_pullup]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00005D005D005D00)) 
    \reg[strong_pullup]_i_9 
       (.I0(lfsr_shift[15]),
        .I1(lfsr_shift[13]),
        .I2(lfsr_shift[14]),
        .I3(lfsr_shift[12]),
        .I4(lfsr_shift[11]),
        .I5(lfsr_shift[10]),
        .O(\reg[strong_pullup]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h404040FF40404000)) 
    \reg[too_many]_i_1 
       (.I0(ow_rst),
        .I1(\reg[state] [1]),
        .I2(dcvr_id_en),
        .I3(\reg[too_many]_i_2_n_0 ),
        .I4(\reg[lfsr][26]_i_6_n_0 ),
        .I5(\reg_reg[too_many]_0 ),
        .O(\reg[too_many]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000010000000000)) 
    \reg[too_many]_i_2 
       (.I0(\reg_reg[device_count][2]_0 ),
        .I1(\reg_reg[device_count][1]_0 ),
        .I2(\reg_reg[device_count][0]_0 ),
        .I3(\reg[too_many]_i_3_n_0 ),
        .I4(\FSM_sequential_reg[state][2]_i_7__0_n_0 ),
        .I5(\reg[mem_addr][5]_i_9_n_0 ),
        .O(\reg[too_many]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \reg[too_many]_i_3 
       (.I0(dcvr_id_en),
        .I1(ow_dev_count),
        .I2(\reg_reg[device_count][4]_0 ),
        .I3(\reg_reg[device_count][3]_0 ),
        .O(\reg[too_many]_i_3_n_0 ));
  FDRE \reg_reg[bit_recv] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[bit_recv] ),
        .Q(ctrl_bit_recv),
        .R(1'b0));
  FDRE \reg_reg[bit_send] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[bit_send] ),
        .Q(ctrl_bit_send),
        .R(1'b0));
  FDRE \reg_reg[bus_rst] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[bus_rst] ),
        .Q(ctrl_bus_rst),
        .R(1'b0));
  FDRE \reg_reg[busy] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[busy]_i_1_n_0 ),
        .Q(\reg_reg[busy]_0 ),
        .R(1'b0));
  FDRE \reg_reg[crc_reset] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[crc_reset] ),
        .Q(\reg_reg[crc_reset_n_0_] ),
        .R(1'b0));
  FDRE \reg_reg[data][0] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_0),
        .D(\reg[data][0]_i_1_n_0 ),
        .Q(ctrl_bit_tx),
        .R(1'b0));
  FDRE \reg_reg[data][10] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][10]_i_1_n_0 ),
        .Q(ctrl_wr_data[9]),
        .R(1'b0));
  FDRE \reg_reg[data][11] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][11]_i_1_n_0 ),
        .Q(ctrl_wr_data[10]),
        .R(1'b0));
  FDRE \reg_reg[data][12] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][12]_i_1_n_0 ),
        .Q(ctrl_wr_data[11]),
        .R(1'b0));
  FDRE \reg_reg[data][13] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][13]_i_1_n_0 ),
        .Q(ctrl_wr_data[12]),
        .R(1'b0));
  FDRE \reg_reg[data][14] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][14]_i_1_n_0 ),
        .Q(ctrl_wr_data[13]),
        .R(1'b0));
  FDRE \reg_reg[data][15] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][15]_i_1_n_0 ),
        .Q(ctrl_wr_data[14]),
        .R(1'b0));
  FDRE \reg_reg[data][16] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][16]_i_1_n_0 ),
        .Q(ctrl_wr_data[15]),
        .R(1'b0));
  FDRE \reg_reg[data][17] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][17]_i_1_n_0 ),
        .Q(ctrl_wr_data[16]),
        .R(1'b0));
  FDRE \reg_reg[data][18] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][18]_i_1_n_0 ),
        .Q(ctrl_wr_data[17]),
        .R(1'b0));
  FDRE \reg_reg[data][19] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][19]_i_1_n_0 ),
        .Q(ctrl_wr_data[18]),
        .R(1'b0));
  FDRE \reg_reg[data][1] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_0),
        .D(\reg[data][1]_i_1_n_0 ),
        .Q(ctrl_wr_data[0]),
        .R(1'b0));
  FDRE \reg_reg[data][20] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][20]_i_1_n_0 ),
        .Q(ctrl_wr_data[19]),
        .R(1'b0));
  FDRE \reg_reg[data][21] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][21]_i_1_n_0 ),
        .Q(ctrl_wr_data[20]),
        .R(1'b0));
  FDRE \reg_reg[data][22] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][22]_i_1_n_0 ),
        .Q(ctrl_wr_data[21]),
        .R(1'b0));
  FDRE \reg_reg[data][23] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][23]_i_1_n_0 ),
        .Q(ctrl_wr_data[22]),
        .R(1'b0));
  FDRE \reg_reg[data][24] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][24]_i_1_n_0 ),
        .Q(ctrl_wr_data[23]),
        .R(1'b0));
  FDRE \reg_reg[data][25] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][25]_i_1_n_0 ),
        .Q(ctrl_wr_data[24]),
        .R(1'b0));
  FDRE \reg_reg[data][26] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][26]_i_1_n_0 ),
        .Q(ctrl_wr_data[25]),
        .R(1'b0));
  FDRE \reg_reg[data][27] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][27]_i_1_n_0 ),
        .Q(ctrl_wr_data[26]),
        .R(1'b0));
  FDRE \reg_reg[data][28] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][28]_i_1_n_0 ),
        .Q(ctrl_wr_data[27]),
        .R(1'b0));
  FDRE \reg_reg[data][29] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][29]_i_1_n_0 ),
        .Q(ctrl_wr_data[28]),
        .R(1'b0));
  FDRE \reg_reg[data][2] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_0),
        .D(\reg[data][2]_i_1_n_0 ),
        .Q(ctrl_wr_data[1]),
        .R(1'b0));
  FDRE \reg_reg[data][30] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][30]_i_1_n_0 ),
        .Q(ctrl_wr_data[29]),
        .R(1'b0));
  FDRE \reg_reg[data][31] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][31]_i_1_n_0 ),
        .Q(ctrl_wr_data[30]),
        .R(1'b0));
  FDRE \reg_reg[data][32] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][32]_i_1_n_0 ),
        .Q(ctrl_wr_data[31]),
        .R(1'b0));
  FDRE \reg_reg[data][33] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][33]_i_1_n_0 ),
        .Q(ctrl_wr_data[32]),
        .R(1'b0));
  FDRE \reg_reg[data][34] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][34]_i_1_n_0 ),
        .Q(ctrl_wr_data[33]),
        .R(1'b0));
  FDRE \reg_reg[data][35] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][35]_i_1_n_0 ),
        .Q(ctrl_wr_data[34]),
        .R(1'b0));
  FDRE \reg_reg[data][36] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][36]_i_1_n_0 ),
        .Q(ctrl_wr_data[35]),
        .R(1'b0));
  FDRE \reg_reg[data][37] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][37]_i_1_n_0 ),
        .Q(ctrl_wr_data[36]),
        .R(1'b0));
  FDRE \reg_reg[data][38] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][38]_i_1_n_0 ),
        .Q(ctrl_wr_data[37]),
        .R(1'b0));
  FDRE \reg_reg[data][39] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][39]_i_1_n_0 ),
        .Q(ctrl_wr_data[38]),
        .R(1'b0));
  FDRE \reg_reg[data][3] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_0),
        .D(\reg[data][3]_i_1_n_0 ),
        .Q(ctrl_wr_data[2]),
        .R(1'b0));
  FDRE \reg_reg[data][40] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][40]_i_1_n_0 ),
        .Q(ctrl_wr_data[39]),
        .R(1'b0));
  FDRE \reg_reg[data][41] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][41]_i_1_n_0 ),
        .Q(ctrl_wr_data[40]),
        .R(1'b0));
  FDRE \reg_reg[data][42] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][42]_i_1_n_0 ),
        .Q(ctrl_wr_data[41]),
        .R(1'b0));
  FDRE \reg_reg[data][43] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][43]_i_1_n_0 ),
        .Q(ctrl_wr_data[42]),
        .R(1'b0));
  FDRE \reg_reg[data][44] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][44]_i_1_n_0 ),
        .Q(ctrl_wr_data[43]),
        .R(1'b0));
  FDRE \reg_reg[data][45] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][45]_i_1_n_0 ),
        .Q(ctrl_wr_data[44]),
        .R(1'b0));
  FDRE \reg_reg[data][46] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][46]_i_1_n_0 ),
        .Q(ctrl_wr_data[45]),
        .R(1'b0));
  FDRE \reg_reg[data][47] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][47]_i_1_n_0 ),
        .Q(ctrl_wr_data[46]),
        .R(1'b0));
  FDRE \reg_reg[data][48] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][48]_i_1_n_0 ),
        .Q(ctrl_wr_data[47]),
        .R(1'b0));
  FDRE \reg_reg[data][49] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][49]_i_1_n_0 ),
        .Q(ctrl_wr_data[48]),
        .R(1'b0));
  FDRE \reg_reg[data][4] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_0),
        .D(\reg[data][4]_i_1_n_0 ),
        .Q(ctrl_wr_data[3]),
        .R(1'b0));
  FDRE \reg_reg[data][50] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][50]_i_1_n_0 ),
        .Q(ctrl_wr_data[49]),
        .R(1'b0));
  FDRE \reg_reg[data][51] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][51]_i_1_n_0 ),
        .Q(ctrl_wr_data[50]),
        .R(1'b0));
  FDRE \reg_reg[data][52] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][52]_i_1_n_0 ),
        .Q(ctrl_wr_data[51]),
        .R(1'b0));
  FDRE \reg_reg[data][53] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][53]_i_1_n_0 ),
        .Q(ctrl_wr_data[52]),
        .R(1'b0));
  FDRE \reg_reg[data][54] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][54]_i_1_n_0 ),
        .Q(ctrl_wr_data[53]),
        .R(1'b0));
  FDRE \reg_reg[data][55] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][55]_i_1_n_0 ),
        .Q(ctrl_wr_data[54]),
        .R(1'b0));
  FDRE \reg_reg[data][56] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][56]_i_1_n_0 ),
        .Q(ctrl_wr_data[55]),
        .R(1'b0));
  FDRE \reg_reg[data][57] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][57]_i_1_n_0 ),
        .Q(ctrl_wr_data[56]),
        .R(1'b0));
  FDRE \reg_reg[data][58] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][58]_i_1_n_0 ),
        .Q(ctrl_wr_data[57]),
        .R(1'b0));
  FDRE \reg_reg[data][59] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][59]_i_1_n_0 ),
        .Q(ctrl_wr_data[58]),
        .R(1'b0));
  FDRE \reg_reg[data][5] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_0),
        .D(\reg[data][5]_i_1_n_0 ),
        .Q(ctrl_wr_data[4]),
        .R(1'b0));
  FDRE \reg_reg[data][60] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][60]_i_1_n_0 ),
        .Q(ctrl_wr_data[59]),
        .R(1'b0));
  FDRE \reg_reg[data][61] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][61]_i_1_n_0 ),
        .Q(ctrl_wr_data[60]),
        .R(1'b0));
  FDRE \reg_reg[data][62] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][62]_i_1_n_0 ),
        .Q(ctrl_wr_data[61]),
        .R(1'b0));
  FDRE \reg_reg[data][63] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][63]_i_2_n_0 ),
        .Q(ctrl_wr_data[62]),
        .R(1'b0));
  FDRE \reg_reg[data][6] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_0),
        .D(\reg[data][6]_i_1_n_0 ),
        .Q(ctrl_wr_data[5]),
        .R(1'b0));
  FDRE \reg_reg[data][7] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_0),
        .D(\reg[data][7]_i_2_n_0 ),
        .Q(ctrl_wr_data[6]),
        .R(1'b0));
  FDRE \reg_reg[data][8] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][8]_i_1_n_0 ),
        .Q(ctrl_wr_data[7]),
        .R(1'b0));
  FDRE \reg_reg[data][9] 
       (.C(axi_clk_i),
        .CE(crc_inst_n_1),
        .D(\reg[data][9]_i_1_n_0 ),
        .Q(ctrl_wr_data[8]),
        .R(1'b0));
  FDRE \reg_reg[device_count][0] 
       (.C(axi_clk_i),
        .CE(\reg[device_count][5]_i_1_n_0 ),
        .D(\reg[device_count][0]_i_1_n_0 ),
        .Q(\reg_reg[device_count][0]_0 ),
        .R(1'b0));
  FDRE \reg_reg[device_count][1] 
       (.C(axi_clk_i),
        .CE(\reg[device_count][5]_i_1_n_0 ),
        .D(\reg[device_count][1]_i_1_n_0 ),
        .Q(\reg_reg[device_count][1]_0 ),
        .R(1'b0));
  FDRE \reg_reg[device_count][2] 
       (.C(axi_clk_i),
        .CE(\reg[device_count][5]_i_1_n_0 ),
        .D(\reg[device_count][2]_i_1_n_0 ),
        .Q(\reg_reg[device_count][2]_0 ),
        .R(1'b0));
  FDRE \reg_reg[device_count][3] 
       (.C(axi_clk_i),
        .CE(\reg[device_count][5]_i_1_n_0 ),
        .D(\reg[device_count][3]_i_1_n_0 ),
        .Q(\reg_reg[device_count][3]_0 ),
        .R(1'b0));
  FDRE \reg_reg[device_count][4] 
       (.C(axi_clk_i),
        .CE(\reg[device_count][5]_i_1_n_0 ),
        .D(\reg[device_count][4]_i_1_n_0 ),
        .Q(\reg_reg[device_count][4]_0 ),
        .R(1'b0));
  FDRE \reg_reg[device_count][5] 
       (.C(axi_clk_i),
        .CE(\reg[device_count][5]_i_1_n_0 ),
        .D(\reg[device_count][5]_i_2_n_0 ),
        .Q(ow_dev_count),
        .R(1'b0));
  FDRE \reg_reg[discover] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[discover] ),
        .Q(dcvr_start),
        .R(1'b0));
  FDRE \reg_reg[done] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[done] ),
        .Q(ow_done),
        .R(1'b0));
  FDRE \reg_reg[lfsr][0] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[0]),
        .Q(lfsr_shift[1]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][10] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[10]),
        .Q(lfsr_shift[11]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][11] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[11]),
        .Q(lfsr_shift[12]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][12] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[12]),
        .Q(lfsr_shift[13]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][13] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[13]),
        .Q(lfsr_shift[14]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][14] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[14]),
        .Q(lfsr_shift[15]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][15] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[15]),
        .Q(lfsr_shift[16]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][16] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[16]),
        .Q(lfsr_shift[17]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][17] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[17]),
        .Q(lfsr_shift[18]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][18] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[18]),
        .Q(lfsr_shift[19]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][19] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[19]),
        .Q(lfsr_shift[20]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][1] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[1]),
        .Q(lfsr_shift[2]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][20] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[20]),
        .Q(lfsr_shift[21]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][21] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[21]),
        .Q(lfsr_shift[22]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][22] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[22]),
        .Q(lfsr_shift[23]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][23] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[23]),
        .Q(lfsr_shift[24]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][24] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[24]),
        .Q(lfsr_shift[25]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][25] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[25]),
        .Q(lfsr_shift[26]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][26] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[26]),
        .Q(\reg_reg[lfsr_n_0_][26] ),
        .R(1'b0));
  FDRE \reg_reg[lfsr][2] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(lfsr_shift[3]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][3] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(lfsr_shift[4]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][4] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(lfsr_shift[5]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][5] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(lfsr_shift[6]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][6] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[6]),
        .Q(lfsr_shift[7]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][7] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[7]),
        .Q(lfsr_shift[8]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][8] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[8]),
        .Q(lfsr_shift[9]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][9] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][26]_i_1_n_0 ),
        .D(p_1_in[9]),
        .Q(lfsr_shift[10]),
        .R(1'b0));
  FDRE \reg_reg[mem_addr][0] 
       (.C(axi_clk_i),
        .CE(\reg[mem_addr][5]_i_1_n_0 ),
        .D(\reg[mem_addr][0]_i_1_n_0 ),
        .Q(\reg_reg[mem_addr][0]_0 ),
        .R(1'b0));
  FDRE \reg_reg[mem_addr][1] 
       (.C(axi_clk_i),
        .CE(\reg[mem_addr][5]_i_1_n_0 ),
        .D(\reg[mem_addr][1]_i_1_n_0 ),
        .Q(\reg_reg[mem_addr][1]_0 ),
        .R(1'b0));
  FDRE \reg_reg[mem_addr][2] 
       (.C(axi_clk_i),
        .CE(\reg[mem_addr][5]_i_1_n_0 ),
        .D(\reg[mem_addr][2]_i_1_n_0 ),
        .Q(\reg_reg[mem_addr][2]_0 ),
        .R(1'b0));
  FDRE \reg_reg[mem_addr][3] 
       (.C(axi_clk_i),
        .CE(\reg[mem_addr][5]_i_1_n_0 ),
        .D(\reg[mem_addr][3]_i_1_n_0 ),
        .Q(\reg_reg[mem_addr][3]_0 ),
        .R(1'b0));
  FDRE \reg_reg[mem_addr][4] 
       (.C(axi_clk_i),
        .CE(\reg[mem_addr][5]_i_1_n_0 ),
        .D(\reg[mem_addr][4]_i_1_n_0 ),
        .Q(\reg_reg[mem_addr][4]_0 ),
        .R(1'b0));
  FDRE \reg_reg[mem_addr][5] 
       (.C(axi_clk_i),
        .CE(\reg[mem_addr][5]_i_1_n_0 ),
        .D(\reg[mem_addr][5]_i_2_n_0 ),
        .Q(\reg_reg[mem_addr][5]_0 ),
        .R(1'b0));
  FDRE \reg_reg[mem_rd_en] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[mem_rd_en] ),
        .Q(ctrl_rd_en),
        .R(1'b0));
  FDRE \reg_reg[mem_wr_en] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[mem_wr_en] ),
        .Q(ctrl_wr_en),
        .R(1'b0));
  FDRE \reg_reg[strong_pullup] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[strong_pullup]_i_1_n_0 ),
        .Q(strong_pullup),
        .R(1'b0));
  FDRE \reg_reg[too_many] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[too_many]_i_1_n_0 ),
        .Q(\reg_reg[too_many]_0 ),
        .R(1'b0));
endmodule

module design_1_axi_onewire_0_0_onewire_crc
   (\reg_reg[id_bit_number][2] ,
    \reg_reg[id][63] ,
    \reg_reg[id][62] ,
    \reg_reg[id][61] ,
    \reg_reg[id_bit_number][2]_0 ,
    \reg_reg[id][59] ,
    \reg_reg[id][58] ,
    \reg_reg[id][57] ,
    \reg_reg[id_bit_number][2]_1 ,
    \reg_reg[id][55] ,
    \reg_reg[id][54] ,
    \reg_reg[id][53] ,
    \reg_reg[id_bit_number][2]_2 ,
    \reg_reg[id][51] ,
    \reg_reg[id][50] ,
    \reg_reg[id][49] ,
    \reg_reg[id_bit_number][2]_3 ,
    \reg_reg[id][47] ,
    \reg_reg[id][46] ,
    \reg_reg[id][45] ,
    \reg_reg[id_bit_number][2]_4 ,
    \reg_reg[id][43] ,
    \reg_reg[id][42] ,
    \reg_reg[id][41] ,
    \reg_reg[id_bit_number][2]_5 ,
    \reg_reg[id][39] ,
    \reg_reg[id][38] ,
    \reg_reg[id][37] ,
    \reg_reg[id_bit_number][2]_6 ,
    \reg_reg[id][35] ,
    \reg_reg[id][34] ,
    \reg_reg[id][33] ,
    \reg_reg[id_bit_number][2]_7 ,
    \reg_reg[id][31] ,
    \reg_reg[id][30] ,
    \reg_reg[id][29] ,
    \reg_reg[id_bit_number][2]_8 ,
    \reg_reg[id][27] ,
    \reg_reg[id][26] ,
    \reg_reg[id][25] ,
    \reg_reg[id_bit_number][2]_9 ,
    \reg_reg[id][23] ,
    \reg_reg[id][22] ,
    \reg_reg[id][21] ,
    \reg_reg[id_bit_number][2]_10 ,
    \reg_reg[id][19] ,
    \reg_reg[id][18] ,
    \reg_reg[id][17] ,
    \reg_reg[id_bit_number][2]_11 ,
    \reg_reg[id][15] ,
    \reg_reg[id][14] ,
    \reg_reg[id][13] ,
    \reg_reg[id_bit_number][2]_12 ,
    \reg_reg[id][11] ,
    \reg_reg[id][10] ,
    \reg_reg[id][9] ,
    \reg_reg[id_bit_number][2]_13 ,
    \reg_reg[id][7] ,
    \reg_reg[id][6] ,
    \reg_reg[id][5] ,
    \reg_reg[id_bit_number][2]_14 ,
    \reg_reg[id][3] ,
    \reg_reg[id][2] ,
    \reg_reg[id][1] ,
    axi_clk_i,
    \reg_reg[id][1]_0 ,
    CO,
    \reg_reg[id][1]_1 ,
    \reg_reg[id][1]_2 ,
    \reg_reg[id][1]_3 ,
    \reg_reg[id][9]_0 ,
    \reg_reg[id][17]_0 ,
    \reg[state] ,
    \reg_reg[id][25]_0 ,
    \reg_reg[id][57]_0 ,
    ow_reg_rst,
    axi_aresetn_i,
    pristine_reg_0,
    dcvr_bit_send,
    \crc_reg[2]_0 ,
    \reg_reg[id][1]_4 ,
    if_done,
    \reg_reg[id][1]_5 ,
    ow_rst,
    \reg_reg[id][64] ,
    \reg_reg[id][64]_0 ,
    \reg_reg[id][64]_1 ,
    dcvr_id,
    \reg_reg[id][63]_0 ,
    \reg_reg[id][62]_0 ,
    \reg_reg[id][61]_0 ,
    \reg_reg[id][59]_0 ,
    \reg_reg[id][58]_0 ,
    \reg_reg[id][57]_1 );
  output \reg_reg[id_bit_number][2] ;
  output \reg_reg[id][63] ;
  output \reg_reg[id][62] ;
  output \reg_reg[id][61] ;
  output \reg_reg[id_bit_number][2]_0 ;
  output \reg_reg[id][59] ;
  output \reg_reg[id][58] ;
  output \reg_reg[id][57] ;
  output \reg_reg[id_bit_number][2]_1 ;
  output \reg_reg[id][55] ;
  output \reg_reg[id][54] ;
  output \reg_reg[id][53] ;
  output \reg_reg[id_bit_number][2]_2 ;
  output \reg_reg[id][51] ;
  output \reg_reg[id][50] ;
  output \reg_reg[id][49] ;
  output \reg_reg[id_bit_number][2]_3 ;
  output \reg_reg[id][47] ;
  output \reg_reg[id][46] ;
  output \reg_reg[id][45] ;
  output \reg_reg[id_bit_number][2]_4 ;
  output \reg_reg[id][43] ;
  output \reg_reg[id][42] ;
  output \reg_reg[id][41] ;
  output \reg_reg[id_bit_number][2]_5 ;
  output \reg_reg[id][39] ;
  output \reg_reg[id][38] ;
  output \reg_reg[id][37] ;
  output \reg_reg[id_bit_number][2]_6 ;
  output \reg_reg[id][35] ;
  output \reg_reg[id][34] ;
  output \reg_reg[id][33] ;
  output \reg_reg[id_bit_number][2]_7 ;
  output \reg_reg[id][31] ;
  output \reg_reg[id][30] ;
  output \reg_reg[id][29] ;
  output \reg_reg[id_bit_number][2]_8 ;
  output \reg_reg[id][27] ;
  output \reg_reg[id][26] ;
  output \reg_reg[id][25] ;
  output \reg_reg[id_bit_number][2]_9 ;
  output \reg_reg[id][23] ;
  output \reg_reg[id][22] ;
  output \reg_reg[id][21] ;
  output \reg_reg[id_bit_number][2]_10 ;
  output \reg_reg[id][19] ;
  output \reg_reg[id][18] ;
  output \reg_reg[id][17] ;
  output \reg_reg[id_bit_number][2]_11 ;
  output \reg_reg[id][15] ;
  output \reg_reg[id][14] ;
  output \reg_reg[id][13] ;
  output \reg_reg[id_bit_number][2]_12 ;
  output \reg_reg[id][11] ;
  output \reg_reg[id][10] ;
  output \reg_reg[id][9] ;
  output \reg_reg[id_bit_number][2]_13 ;
  output \reg_reg[id][7] ;
  output \reg_reg[id][6] ;
  output \reg_reg[id][5] ;
  output \reg_reg[id_bit_number][2]_14 ;
  output \reg_reg[id][3] ;
  output \reg_reg[id][2] ;
  output \reg_reg[id][1] ;
  input axi_clk_i;
  input \reg_reg[id][1]_0 ;
  input [0:0]CO;
  input \reg_reg[id][1]_1 ;
  input \reg_reg[id][1]_2 ;
  input \reg_reg[id][1]_3 ;
  input \reg_reg[id][9]_0 ;
  input \reg_reg[id][17]_0 ;
  input [2:0]\reg[state] ;
  input \reg_reg[id][25]_0 ;
  input \reg_reg[id][57]_0 ;
  input ow_reg_rst;
  input axi_aresetn_i;
  input pristine_reg_0;
  input dcvr_bit_send;
  input \crc_reg[2]_0 ;
  input \reg_reg[id][1]_4 ;
  input if_done;
  input \reg_reg[id][1]_5 ;
  input ow_rst;
  input \reg_reg[id][64] ;
  input \reg_reg[id][64]_0 ;
  input \reg_reg[id][64]_1 ;
  input [63:0]dcvr_id;
  input \reg_reg[id][63]_0 ;
  input \reg_reg[id][62]_0 ;
  input \reg_reg[id][61]_0 ;
  input \reg_reg[id][59]_0 ;
  input \reg_reg[id][58]_0 ;
  input \reg_reg[id][57]_1 ;

  wire [0:0]CO;
  wire axi_aresetn_i;
  wire axi_clk_i;
  wire \crc[0]_i_1_n_0 ;
  wire \crc[1]_i_1_n_0 ;
  wire \crc[2]_i_1_n_0 ;
  wire \crc[3]_i_1_n_0 ;
  wire \crc[4]_i_1_n_0 ;
  wire \crc[5]_i_1_n_0 ;
  wire \crc[6]_i_1_n_0 ;
  wire \crc[7]_i_1_n_0 ;
  wire \crc[7]_i_2_n_0 ;
  wire \crc_reg[2]_0 ;
  wire \crc_reg_n_0_[0] ;
  wire \crc_reg_n_0_[7] ;
  wire dcvr_bit_send;
  wire [63:0]dcvr_id;
  wire if_done;
  wire ow_reg_rst;
  wire ow_rst;
  wire p_1_in;
  wire p_2_in;
  wire p_3_in;
  wire p_4_in;
  wire p_5_in;
  wire p_6_in;
  wire pristine;
  wire pristine_i_1_n_0;
  wire pristine_reg_0;
  wire \reg[id][16]_i_2_n_0 ;
  wire \reg[id][24]_i_2_n_0 ;
  wire \reg[id][32]_i_2_n_0 ;
  wire \reg[id][40]_i_2_n_0 ;
  wire \reg[id][48]_i_2_n_0 ;
  wire \reg[id][56]_i_2_n_0 ;
  wire \reg[id][64]_i_11_n_0 ;
  wire \reg[id][64]_i_12_n_0 ;
  wire \reg[id][64]_i_4_n_0 ;
  wire \reg[id][64]_i_5_n_0 ;
  wire \reg[id][64]_i_7_n_0 ;
  wire \reg[id][8]_i_2_n_0 ;
  wire [2:0]\reg[state] ;
  wire \reg_reg[id][10] ;
  wire \reg_reg[id][11] ;
  wire \reg_reg[id][13] ;
  wire \reg_reg[id][14] ;
  wire \reg_reg[id][15] ;
  wire \reg_reg[id][17] ;
  wire \reg_reg[id][17]_0 ;
  wire \reg_reg[id][18] ;
  wire \reg_reg[id][19] ;
  wire \reg_reg[id][1] ;
  wire \reg_reg[id][1]_0 ;
  wire \reg_reg[id][1]_1 ;
  wire \reg_reg[id][1]_2 ;
  wire \reg_reg[id][1]_3 ;
  wire \reg_reg[id][1]_4 ;
  wire \reg_reg[id][1]_5 ;
  wire \reg_reg[id][21] ;
  wire \reg_reg[id][22] ;
  wire \reg_reg[id][23] ;
  wire \reg_reg[id][25] ;
  wire \reg_reg[id][25]_0 ;
  wire \reg_reg[id][26] ;
  wire \reg_reg[id][27] ;
  wire \reg_reg[id][29] ;
  wire \reg_reg[id][2] ;
  wire \reg_reg[id][30] ;
  wire \reg_reg[id][31] ;
  wire \reg_reg[id][33] ;
  wire \reg_reg[id][34] ;
  wire \reg_reg[id][35] ;
  wire \reg_reg[id][37] ;
  wire \reg_reg[id][38] ;
  wire \reg_reg[id][39] ;
  wire \reg_reg[id][3] ;
  wire \reg_reg[id][41] ;
  wire \reg_reg[id][42] ;
  wire \reg_reg[id][43] ;
  wire \reg_reg[id][45] ;
  wire \reg_reg[id][46] ;
  wire \reg_reg[id][47] ;
  wire \reg_reg[id][49] ;
  wire \reg_reg[id][50] ;
  wire \reg_reg[id][51] ;
  wire \reg_reg[id][53] ;
  wire \reg_reg[id][54] ;
  wire \reg_reg[id][55] ;
  wire \reg_reg[id][57] ;
  wire \reg_reg[id][57]_0 ;
  wire \reg_reg[id][57]_1 ;
  wire \reg_reg[id][58] ;
  wire \reg_reg[id][58]_0 ;
  wire \reg_reg[id][59] ;
  wire \reg_reg[id][59]_0 ;
  wire \reg_reg[id][5] ;
  wire \reg_reg[id][61] ;
  wire \reg_reg[id][61]_0 ;
  wire \reg_reg[id][62] ;
  wire \reg_reg[id][62]_0 ;
  wire \reg_reg[id][63] ;
  wire \reg_reg[id][63]_0 ;
  wire \reg_reg[id][64] ;
  wire \reg_reg[id][64]_0 ;
  wire \reg_reg[id][64]_1 ;
  wire \reg_reg[id][6] ;
  wire \reg_reg[id][7] ;
  wire \reg_reg[id][9] ;
  wire \reg_reg[id][9]_0 ;
  wire \reg_reg[id_bit_number][2] ;
  wire \reg_reg[id_bit_number][2]_0 ;
  wire \reg_reg[id_bit_number][2]_1 ;
  wire \reg_reg[id_bit_number][2]_10 ;
  wire \reg_reg[id_bit_number][2]_11 ;
  wire \reg_reg[id_bit_number][2]_12 ;
  wire \reg_reg[id_bit_number][2]_13 ;
  wire \reg_reg[id_bit_number][2]_14 ;
  wire \reg_reg[id_bit_number][2]_2 ;
  wire \reg_reg[id_bit_number][2]_3 ;
  wire \reg_reg[id_bit_number][2]_4 ;
  wire \reg_reg[id_bit_number][2]_5 ;
  wire \reg_reg[id_bit_number][2]_6 ;
  wire \reg_reg[id_bit_number][2]_7 ;
  wire \reg_reg[id_bit_number][2]_8 ;
  wire \reg_reg[id_bit_number][2]_9 ;

  LUT4 #(
    .INIT(16'h0020)) 
    \crc[0]_i_1 
       (.I0(p_3_in),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(\crc[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \crc[1]_i_1 
       (.I0(p_4_in),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(\crc[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0400000400040400)) 
    \crc[2]_i_1 
       (.I0(pristine_reg_0),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(p_1_in),
        .I4(\crc_reg[2]_0 ),
        .I5(\crc_reg_n_0_[0] ),
        .O(\crc[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0400000400040400)) 
    \crc[3]_i_1 
       (.I0(pristine_reg_0),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(p_2_in),
        .I4(\crc_reg[2]_0 ),
        .I5(\crc_reg_n_0_[0] ),
        .O(\crc[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \crc[4]_i_1 
       (.I0(p_5_in),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(\crc[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \crc[5]_i_1 
       (.I0(p_6_in),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(\crc[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \crc[6]_i_1 
       (.I0(\crc_reg_n_0_[7] ),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(\crc[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFFB)) 
    \crc[7]_i_1 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(pristine_reg_0),
        .I3(dcvr_bit_send),
        .O(\crc[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'h00000600)) 
    \crc[7]_i_2 
       (.I0(\crc_reg[2]_0 ),
        .I1(\crc_reg_n_0_[0] ),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(pristine_reg_0),
        .O(\crc[7]_i_2_n_0 ));
  FDRE \crc_reg[0] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1_n_0 ),
        .D(\crc[0]_i_1_n_0 ),
        .Q(\crc_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \crc_reg[1] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1_n_0 ),
        .D(\crc[1]_i_1_n_0 ),
        .Q(p_3_in),
        .R(1'b0));
  FDRE \crc_reg[2] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1_n_0 ),
        .D(\crc[2]_i_1_n_0 ),
        .Q(p_4_in),
        .R(1'b0));
  FDRE \crc_reg[3] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1_n_0 ),
        .D(\crc[3]_i_1_n_0 ),
        .Q(p_1_in),
        .R(1'b0));
  FDRE \crc_reg[4] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1_n_0 ),
        .D(\crc[4]_i_1_n_0 ),
        .Q(p_2_in),
        .R(1'b0));
  FDRE \crc_reg[5] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1_n_0 ),
        .D(\crc[5]_i_1_n_0 ),
        .Q(p_5_in),
        .R(1'b0));
  FDRE \crc_reg[6] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1_n_0 ),
        .D(\crc[6]_i_1_n_0 ),
        .Q(p_6_in),
        .R(1'b0));
  FDRE \crc_reg[7] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1_n_0 ),
        .D(\crc[7]_i_2_n_0 ),
        .Q(\crc_reg_n_0_[7] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    pristine_i_1
       (.I0(pristine_reg_0),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(pristine_i_1_n_0));
  FDRE pristine_reg
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1_n_0 ),
        .D(pristine_i_1_n_0),
        .Q(pristine),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][10]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][58]_0 ),
        .I2(\reg[id][16]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[9]),
        .O(\reg_reg[id][10] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][11]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][59]_0 ),
        .I2(\reg[id][16]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[10]),
        .O(\reg_reg[id][11] ));
  LUT6 #(
    .INIT(64'hAAAAFBFFAAAA0800)) 
    \reg[id][12]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][16]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[11]),
        .O(\reg_reg[id_bit_number][2]_12 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][13]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][61]_0 ),
        .I2(\reg[id][16]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[12]),
        .O(\reg_reg[id][13] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][14]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][62]_0 ),
        .I2(\reg[id][16]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[13]),
        .O(\reg_reg[id][14] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][15]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][63]_0 ),
        .I2(\reg[id][16]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[14]),
        .O(\reg_reg[id][15] ));
  LUT6 #(
    .INIT(64'hAAAAFEFFAAAA0200)) 
    \reg[id][16]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][16]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[15]),
        .O(\reg_reg[id_bit_number][2]_11 ));
  LUT6 #(
    .INIT(64'h0000000000E00000)) 
    \reg[id][16]_i_2 
       (.I0(\reg_reg[id][1]_0 ),
        .I1(CO),
        .I2(\reg[id][64]_i_7_n_0 ),
        .I3(\reg_reg[id][1]_1 ),
        .I4(\reg_reg[id][1]_2 ),
        .I5(\reg_reg[id][9]_0 ),
        .O(\reg[id][16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][17]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][57]_1 ),
        .I2(\reg[id][24]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[16]),
        .O(\reg_reg[id][17] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][18]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][58]_0 ),
        .I2(\reg[id][24]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[17]),
        .O(\reg_reg[id][18] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][19]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][59]_0 ),
        .I2(\reg[id][24]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[18]),
        .O(\reg_reg[id][19] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][1]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][57]_1 ),
        .I2(\reg[id][8]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[0]),
        .O(\reg_reg[id][1] ));
  LUT6 #(
    .INIT(64'hAAAAFBFFAAAA0800)) 
    \reg[id][20]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][24]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[19]),
        .O(\reg_reg[id_bit_number][2]_10 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][21]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][61]_0 ),
        .I2(\reg[id][24]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[20]),
        .O(\reg_reg[id][21] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][22]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][62]_0 ),
        .I2(\reg[id][24]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[21]),
        .O(\reg_reg[id][22] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][23]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][63]_0 ),
        .I2(\reg[id][24]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[22]),
        .O(\reg_reg[id][23] ));
  LUT6 #(
    .INIT(64'hAAAAFEFFAAAA0200)) 
    \reg[id][24]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][24]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[23]),
        .O(\reg_reg[id_bit_number][2]_9 ));
  LUT6 #(
    .INIT(64'h0000000000E00000)) 
    \reg[id][24]_i_2 
       (.I0(\reg_reg[id][1]_0 ),
        .I1(CO),
        .I2(\reg[id][64]_i_7_n_0 ),
        .I3(\reg_reg[id][1]_1 ),
        .I4(\reg_reg[id][1]_2 ),
        .I5(\reg_reg[id][17]_0 ),
        .O(\reg[id][24]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][25]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][57]_1 ),
        .I2(\reg[id][32]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[24]),
        .O(\reg_reg[id][25] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][26]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][58]_0 ),
        .I2(\reg[id][32]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[25]),
        .O(\reg_reg[id][26] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][27]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][59]_0 ),
        .I2(\reg[id][32]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[26]),
        .O(\reg_reg[id][27] ));
  LUT6 #(
    .INIT(64'hAAAAFBFFAAAA0800)) 
    \reg[id][28]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][32]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[27]),
        .O(\reg_reg[id_bit_number][2]_8 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][29]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][61]_0 ),
        .I2(\reg[id][32]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[28]),
        .O(\reg_reg[id][29] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][2]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][58]_0 ),
        .I2(\reg[id][8]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[1]),
        .O(\reg_reg[id][2] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][30]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][62]_0 ),
        .I2(\reg[id][32]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[29]),
        .O(\reg_reg[id][30] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][31]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][63]_0 ),
        .I2(\reg[id][32]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[30]),
        .O(\reg_reg[id][31] ));
  LUT6 #(
    .INIT(64'hAAAAFEFFAAAA0200)) 
    \reg[id][32]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][32]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[31]),
        .O(\reg_reg[id_bit_number][2]_7 ));
  LUT6 #(
    .INIT(64'h000000000000E000)) 
    \reg[id][32]_i_2 
       (.I0(\reg_reg[id][1]_0 ),
        .I1(CO),
        .I2(\reg[id][64]_i_7_n_0 ),
        .I3(\reg[state] [2]),
        .I4(\reg[state] [1]),
        .I5(\reg_reg[id][25]_0 ),
        .O(\reg[id][32]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][33]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][57]_1 ),
        .I2(\reg[id][40]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[32]),
        .O(\reg_reg[id][33] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][34]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][58]_0 ),
        .I2(\reg[id][40]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[33]),
        .O(\reg_reg[id][34] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][35]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][59]_0 ),
        .I2(\reg[id][40]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[34]),
        .O(\reg_reg[id][35] ));
  LUT6 #(
    .INIT(64'hAAAAFBFFAAAA0800)) 
    \reg[id][36]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][40]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[35]),
        .O(\reg_reg[id_bit_number][2]_6 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][37]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][61]_0 ),
        .I2(\reg[id][40]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[36]),
        .O(\reg_reg[id][37] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][38]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][62]_0 ),
        .I2(\reg[id][40]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[37]),
        .O(\reg_reg[id][38] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][39]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][63]_0 ),
        .I2(\reg[id][40]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[38]),
        .O(\reg_reg[id][39] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][3]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][59]_0 ),
        .I2(\reg[id][8]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[2]),
        .O(\reg_reg[id][3] ));
  LUT6 #(
    .INIT(64'hAAAAFEFFAAAA0200)) 
    \reg[id][40]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][40]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[39]),
        .O(\reg_reg[id_bit_number][2]_5 ));
  LUT6 #(
    .INIT(64'h00000000000000E0)) 
    \reg[id][40]_i_2 
       (.I0(\reg_reg[id][1]_0 ),
        .I1(CO),
        .I2(\reg[id][64]_i_7_n_0 ),
        .I3(\reg_reg[id][1]_1 ),
        .I4(\reg_reg[id][1]_2 ),
        .I5(\reg_reg[id][1]_3 ),
        .O(\reg[id][40]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][41]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][57]_1 ),
        .I2(\reg[id][48]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[40]),
        .O(\reg_reg[id][41] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][42]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][58]_0 ),
        .I2(\reg[id][48]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[41]),
        .O(\reg_reg[id][42] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][43]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][59]_0 ),
        .I2(\reg[id][48]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[42]),
        .O(\reg_reg[id][43] ));
  LUT6 #(
    .INIT(64'hAAAAFBFFAAAA0800)) 
    \reg[id][44]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][48]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[43]),
        .O(\reg_reg[id_bit_number][2]_4 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][45]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][61]_0 ),
        .I2(\reg[id][48]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[44]),
        .O(\reg_reg[id][45] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][46]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][62]_0 ),
        .I2(\reg[id][48]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[45]),
        .O(\reg_reg[id][46] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][47]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][63]_0 ),
        .I2(\reg[id][48]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[46]),
        .O(\reg_reg[id][47] ));
  LUT6 #(
    .INIT(64'hAAAAFEFFAAAA0200)) 
    \reg[id][48]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][48]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[47]),
        .O(\reg_reg[id_bit_number][2]_3 ));
  LUT6 #(
    .INIT(64'h00000000000000E0)) 
    \reg[id][48]_i_2 
       (.I0(\reg_reg[id][1]_0 ),
        .I1(CO),
        .I2(\reg[id][64]_i_7_n_0 ),
        .I3(\reg_reg[id][1]_1 ),
        .I4(\reg_reg[id][1]_2 ),
        .I5(\reg_reg[id][9]_0 ),
        .O(\reg[id][48]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][49]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][57]_1 ),
        .I2(\reg[id][56]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[48]),
        .O(\reg_reg[id][49] ));
  LUT6 #(
    .INIT(64'hAAAAFBFFAAAA0800)) 
    \reg[id][4]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][8]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[3]),
        .O(\reg_reg[id_bit_number][2]_14 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][50]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][58]_0 ),
        .I2(\reg[id][56]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[49]),
        .O(\reg_reg[id][50] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][51]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][59]_0 ),
        .I2(\reg[id][56]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[50]),
        .O(\reg_reg[id][51] ));
  LUT6 #(
    .INIT(64'hAAAAFBFFAAAA0800)) 
    \reg[id][52]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][56]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[51]),
        .O(\reg_reg[id_bit_number][2]_2 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][53]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][61]_0 ),
        .I2(\reg[id][56]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[52]),
        .O(\reg_reg[id][53] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][54]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][62]_0 ),
        .I2(\reg[id][56]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[53]),
        .O(\reg_reg[id][54] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][55]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][63]_0 ),
        .I2(\reg[id][56]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[54]),
        .O(\reg_reg[id][55] ));
  LUT6 #(
    .INIT(64'hAAAAFEFFAAAA0200)) 
    \reg[id][56]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][56]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[55]),
        .O(\reg_reg[id_bit_number][2]_1 ));
  LUT6 #(
    .INIT(64'h00000000000000E0)) 
    \reg[id][56]_i_2 
       (.I0(\reg_reg[id][1]_0 ),
        .I1(CO),
        .I2(\reg[id][64]_i_7_n_0 ),
        .I3(\reg_reg[id][1]_1 ),
        .I4(\reg_reg[id][1]_2 ),
        .I5(\reg_reg[id][17]_0 ),
        .O(\reg[id][56]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][57]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][57]_1 ),
        .I2(\reg[id][64]_i_4_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[56]),
        .O(\reg_reg[id][57] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][58]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][58]_0 ),
        .I2(\reg[id][64]_i_4_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[57]),
        .O(\reg_reg[id][58] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][59]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][59]_0 ),
        .I2(\reg[id][64]_i_4_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[58]),
        .O(\reg_reg[id][59] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][5]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][61]_0 ),
        .I2(\reg[id][8]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[4]),
        .O(\reg_reg[id][5] ));
  LUT6 #(
    .INIT(64'hAAAAFBFFAAAA0800)) 
    \reg[id][60]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][64]_i_4_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[59]),
        .O(\reg_reg[id_bit_number][2]_0 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][61]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][61]_0 ),
        .I2(\reg[id][64]_i_4_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[60]),
        .O(\reg_reg[id][61] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][62]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][62]_0 ),
        .I2(\reg[id][64]_i_4_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[61]),
        .O(\reg_reg[id][62] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][63]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][63]_0 ),
        .I2(\reg[id][64]_i_4_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[62]),
        .O(\reg_reg[id][63] ));
  LUT6 #(
    .INIT(64'hAAAAFEFFAAAA0200)) 
    \reg[id][64]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][64]_i_4_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[63]),
        .O(\reg_reg[id_bit_number][2] ));
  LUT5 #(
    .INIT(32'h0FF00EF0)) 
    \reg[id][64]_i_11 
       (.I0(p_6_in),
        .I1(p_2_in),
        .I2(\reg[state] [0]),
        .I3(\reg[state] [1]),
        .I4(\crc_reg_n_0_[7] ),
        .O(\reg[id][64]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0F000F000F000E00)) 
    \reg[id][64]_i_12 
       (.I0(p_1_in),
        .I1(\crc_reg_n_0_[0] ),
        .I2(\reg[state] [0]),
        .I3(\reg[state] [1]),
        .I4(p_5_in),
        .I5(p_4_in),
        .O(\reg[id][64]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000E000)) 
    \reg[id][64]_i_4 
       (.I0(\reg_reg[id][1]_0 ),
        .I1(CO),
        .I2(\reg[id][64]_i_7_n_0 ),
        .I3(\reg[state] [2]),
        .I4(\reg[state] [1]),
        .I5(\reg_reg[id][57]_0 ),
        .O(\reg[id][64]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF80808A80)) 
    \reg[id][64]_i_5 
       (.I0(\reg[id][64]_i_7_n_0 ),
        .I1(\reg_reg[id][1]_4 ),
        .I2(\reg[state] [2]),
        .I3(if_done),
        .I4(\reg_reg[id][1]_5 ),
        .I5(ow_rst),
        .O(\reg[id][64]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF00E0)) 
    \reg[id][64]_i_7 
       (.I0(pristine),
        .I1(p_3_in),
        .I2(\reg[state] [1]),
        .I3(\reg[state] [0]),
        .I4(\reg[id][64]_i_11_n_0 ),
        .I5(\reg[id][64]_i_12_n_0 ),
        .O(\reg[id][64]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][6]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][62]_0 ),
        .I2(\reg[id][8]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[5]),
        .O(\reg_reg[id][6] ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][7]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][63]_0 ),
        .I2(\reg[id][8]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[6]),
        .O(\reg_reg[id][7] ));
  LUT6 #(
    .INIT(64'hAAAAFEFFAAAA0200)) 
    \reg[id][8]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][64]_0 ),
        .I2(\reg_reg[id][64]_1 ),
        .I3(\reg[id][8]_i_2_n_0 ),
        .I4(\reg[id][64]_i_5_n_0 ),
        .I5(dcvr_id[7]),
        .O(\reg_reg[id_bit_number][2]_13 ));
  LUT6 #(
    .INIT(64'h0000000000E00000)) 
    \reg[id][8]_i_2 
       (.I0(\reg_reg[id][1]_0 ),
        .I1(CO),
        .I2(\reg[id][64]_i_7_n_0 ),
        .I3(\reg_reg[id][1]_1 ),
        .I4(\reg_reg[id][1]_2 ),
        .I5(\reg_reg[id][1]_3 ),
        .O(\reg[id][8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAEFAA20)) 
    \reg[id][9]_i_1 
       (.I0(\reg_reg[id][64] ),
        .I1(\reg_reg[id][57]_1 ),
        .I2(\reg[id][16]_i_2_n_0 ),
        .I3(\reg[id][64]_i_5_n_0 ),
        .I4(dcvr_id[8]),
        .O(\reg_reg[id][9] ));
endmodule

(* ORIG_REF_NAME = "onewire_crc" *) 
module design_1_axi_onewire_0_0_onewire_crc_0
   (\FSM_sequential_reg_reg[state][0] ,
    \FSM_sequential_reg_reg[state][3] ,
    axi_clk_i,
    ow_reg_rst,
    axi_aresetn_i,
    pristine_reg_0,
    if_rx_data_en,
    if_rx_data,
    \reg_reg[data][0] ,
    \reg_reg[data][0]_0 ,
    \reg_reg[data][0]_1 ,
    \reg_reg[data][0]_2 ,
    \reg_reg[data][0]_3 ,
    \reg_reg[data][30] ,
    \reg_reg[data][30]_0 ,
    \reg_reg[data][30]_1 ,
    \reg_reg[data][30]_2 ,
    \reg_reg[data][30]_3 ,
    \reg_reg[data][30]_4 ,
    \reg_reg[data][30]_5 ,
    \reg_reg[data][30]_6 ,
    \reg[data][63]_i_5_0 ,
    \reg[state] );
  output \FSM_sequential_reg_reg[state][0] ;
  output \FSM_sequential_reg_reg[state][3] ;
  input axi_clk_i;
  input ow_reg_rst;
  input axi_aresetn_i;
  input pristine_reg_0;
  input if_rx_data_en;
  input if_rx_data;
  input \reg_reg[data][0] ;
  input \reg_reg[data][0]_0 ;
  input \reg_reg[data][0]_1 ;
  input \reg_reg[data][0]_2 ;
  input \reg_reg[data][0]_3 ;
  input \reg_reg[data][30] ;
  input \reg_reg[data][30]_0 ;
  input \reg_reg[data][30]_1 ;
  input \reg_reg[data][30]_2 ;
  input \reg_reg[data][30]_3 ;
  input \reg_reg[data][30]_4 ;
  input \reg_reg[data][30]_5 ;
  input \reg_reg[data][30]_6 ;
  input \reg[data][63]_i_5_0 ;
  input [1:0]\reg[state] ;

  wire \FSM_sequential_reg_reg[state][0] ;
  wire \FSM_sequential_reg_reg[state][3] ;
  wire axi_aresetn_i;
  wire axi_clk_i;
  wire \crc[7]_i_1__0_n_0 ;
  wire \crc_reg_n_0_[0] ;
  wire \crc_reg_n_0_[7] ;
  wire if_rx_data;
  wire if_rx_data_en;
  wire ow_reg_rst;
  wire p_1_in;
  wire [7:0]p_1_in__0;
  wire p_2_in;
  wire p_3_in;
  wire p_4_in;
  wire p_5_in;
  wire p_6_in;
  wire pristine;
  wire pristine_i_1__0_n_0;
  wire pristine_reg_0;
  wire \reg[data][63]_i_12_n_0 ;
  wire \reg[data][63]_i_13_n_0 ;
  wire \reg[data][63]_i_14_n_0 ;
  wire \reg[data][63]_i_5_0 ;
  wire \reg[data][63]_i_5_n_0 ;
  wire [1:0]\reg[state] ;
  wire \reg_reg[data][0] ;
  wire \reg_reg[data][0]_0 ;
  wire \reg_reg[data][0]_1 ;
  wire \reg_reg[data][0]_2 ;
  wire \reg_reg[data][0]_3 ;
  wire \reg_reg[data][30] ;
  wire \reg_reg[data][30]_0 ;
  wire \reg_reg[data][30]_1 ;
  wire \reg_reg[data][30]_2 ;
  wire \reg_reg[data][30]_3 ;
  wire \reg_reg[data][30]_4 ;
  wire \reg_reg[data][30]_5 ;
  wire \reg_reg[data][30]_6 ;

  LUT4 #(
    .INIT(16'h0020)) 
    \crc[0]_i_1__0 
       (.I0(p_3_in),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(p_1_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \crc[1]_i_1__0 
       (.I0(p_4_in),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(p_1_in__0[1]));
  LUT6 #(
    .INIT(64'h0400000400040400)) 
    \crc[2]_i_1__0 
       (.I0(pristine_reg_0),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(p_1_in),
        .I4(if_rx_data),
        .I5(\crc_reg_n_0_[0] ),
        .O(p_1_in__0[2]));
  LUT6 #(
    .INIT(64'h0400000400040400)) 
    \crc[3]_i_1__0 
       (.I0(pristine_reg_0),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(p_2_in),
        .I4(if_rx_data),
        .I5(\crc_reg_n_0_[0] ),
        .O(p_1_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \crc[4]_i_1__0 
       (.I0(p_5_in),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(p_1_in__0[4]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \crc[5]_i_1__0 
       (.I0(p_6_in),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(p_1_in__0[5]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \crc[6]_i_1__0 
       (.I0(\crc_reg_n_0_[7] ),
        .I1(ow_reg_rst),
        .I2(axi_aresetn_i),
        .I3(pristine_reg_0),
        .O(p_1_in__0[6]));
  LUT4 #(
    .INIT(16'hFFFB)) 
    \crc[7]_i_1__0 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(pristine_reg_0),
        .I3(if_rx_data_en),
        .O(\crc[7]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h00000600)) 
    \crc[7]_i_2__0 
       (.I0(if_rx_data),
        .I1(\crc_reg_n_0_[0] ),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(pristine_reg_0),
        .O(p_1_in__0[7]));
  FDRE \crc_reg[0] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1__0_n_0 ),
        .D(p_1_in__0[0]),
        .Q(\crc_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \crc_reg[1] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1__0_n_0 ),
        .D(p_1_in__0[1]),
        .Q(p_3_in),
        .R(1'b0));
  FDRE \crc_reg[2] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1__0_n_0 ),
        .D(p_1_in__0[2]),
        .Q(p_4_in),
        .R(1'b0));
  FDRE \crc_reg[3] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1__0_n_0 ),
        .D(p_1_in__0[3]),
        .Q(p_1_in),
        .R(1'b0));
  FDRE \crc_reg[4] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1__0_n_0 ),
        .D(p_1_in__0[4]),
        .Q(p_2_in),
        .R(1'b0));
  FDRE \crc_reg[5] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1__0_n_0 ),
        .D(p_1_in__0[5]),
        .Q(p_5_in),
        .R(1'b0));
  FDRE \crc_reg[6] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1__0_n_0 ),
        .D(p_1_in__0[6]),
        .Q(p_6_in),
        .R(1'b0));
  FDRE \crc_reg[7] 
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1__0_n_0 ),
        .D(p_1_in__0[7]),
        .Q(\crc_reg_n_0_[7] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    pristine_i_1__0
       (.I0(pristine_reg_0),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(pristine_i_1__0_n_0));
  FDRE pristine_reg
       (.C(axi_clk_i),
        .CE(\crc[7]_i_1__0_n_0 ),
        .D(pristine_i_1__0_n_0),
        .Q(pristine),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFCFFFC)) 
    \reg[data][63]_i_1 
       (.I0(\reg_reg[data][30] ),
        .I1(\reg_reg[data][30]_0 ),
        .I2(\reg_reg[data][30]_1 ),
        .I3(\reg[data][63]_i_5_n_0 ),
        .I4(\reg_reg[data][0]_1 ),
        .I5(\reg_reg[data][0]_3 ),
        .O(\FSM_sequential_reg_reg[state][3] ));
  LUT6 #(
    .INIT(64'h0000C80800000000)) 
    \reg[data][63]_i_12 
       (.I0(if_rx_data_en),
        .I1(\reg[data][63]_i_5_0 ),
        .I2(\reg[state] [0]),
        .I3(\reg[data][63]_i_13_n_0 ),
        .I4(\reg_reg[data][0]_2 ),
        .I5(\reg[state] [1]),
        .O(\reg[data][63]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg[data][63]_i_13 
       (.I0(\reg[data][63]_i_14_n_0 ),
        .I1(p_5_in),
        .I2(p_2_in),
        .I3(p_1_in),
        .I4(p_4_in),
        .O(\reg[data][63]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \reg[data][63]_i_14 
       (.I0(pristine),
        .I1(p_6_in),
        .I2(\crc_reg_n_0_[7] ),
        .I3(\crc_reg_n_0_[0] ),
        .I4(p_3_in),
        .O(\reg[data][63]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEAA0000)) 
    \reg[data][63]_i_5 
       (.I0(\reg_reg[data][30]_2 ),
        .I1(\reg_reg[data][30]_3 ),
        .I2(\reg_reg[data][30]_4 ),
        .I3(\reg_reg[data][30]_5 ),
        .I4(\reg_reg[data][30]_6 ),
        .I5(\reg[data][63]_i_12_n_0 ),
        .O(\reg[data][63]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFEFCFCFFFE)) 
    \reg[data][7]_i_1 
       (.I0(\reg_reg[data][0] ),
        .I1(\reg_reg[data][0]_0 ),
        .I2(\reg[data][63]_i_5_n_0 ),
        .I3(\reg_reg[data][0]_1 ),
        .I4(\reg_reg[data][0]_2 ),
        .I5(\reg_reg[data][0]_3 ),
        .O(\FSM_sequential_reg_reg[state][0] ));
endmodule

module design_1_axi_onewire_0_0_onewire_discover
   (dcvr_id_en,
    dcvr_done,
    dcvr_bus_rst,
    dcvr_bit_send,
    dcvr_bit_tx,
    dcvr_bit_recv,
    \reg_reg[bus_rst]_0 ,
    \reg_reg[bit_send]_0 ,
    wr_data_i,
    axi_clk_i,
    if_done,
    axi_aresetn_i,
    ow_reg_rst,
    ow_rst,
    if_rx_data,
    if_rx_data_en,
    dcvr_start,
    ctrl_bus_rst,
    ctrl_bit_send,
    ctrl_wr_data,
    ctrl_bit_tx);
  output dcvr_id_en;
  output dcvr_done;
  output dcvr_bus_rst;
  output dcvr_bit_send;
  output dcvr_bit_tx;
  output dcvr_bit_recv;
  output \reg_reg[bus_rst]_0 ;
  output \reg_reg[bit_send]_0 ;
  output [63:0]wr_data_i;
  input axi_clk_i;
  input if_done;
  input axi_aresetn_i;
  input ow_reg_rst;
  input ow_rst;
  input if_rx_data;
  input if_rx_data_en;
  input dcvr_start;
  input ctrl_bus_rst;
  input ctrl_bit_send;
  input [62:0]ctrl_wr_data;
  input ctrl_bit_tx;

  wire \FSM_sequential_reg[state][0]_i_1__0_n_0 ;
  wire \FSM_sequential_reg[state][0]_i_2__0_n_0 ;
  wire \FSM_sequential_reg[state][0]_i_3_n_0 ;
  wire \FSM_sequential_reg[state][0]_i_4_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_1__0_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_2_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_3__0_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_1_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_2_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_3_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_4__0_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_5_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_6__0_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_7_n_0 ;
  wire \FSM_sequential_reg[state][2]_i_8_n_0 ;
  wire axi_aresetn_i;
  wire axi_clk_i;
  wire crc_inst_n_0;
  wire crc_inst_n_1;
  wire crc_inst_n_10;
  wire crc_inst_n_11;
  wire crc_inst_n_12;
  wire crc_inst_n_13;
  wire crc_inst_n_14;
  wire crc_inst_n_15;
  wire crc_inst_n_16;
  wire crc_inst_n_17;
  wire crc_inst_n_18;
  wire crc_inst_n_19;
  wire crc_inst_n_2;
  wire crc_inst_n_20;
  wire crc_inst_n_21;
  wire crc_inst_n_22;
  wire crc_inst_n_23;
  wire crc_inst_n_24;
  wire crc_inst_n_25;
  wire crc_inst_n_26;
  wire crc_inst_n_27;
  wire crc_inst_n_28;
  wire crc_inst_n_29;
  wire crc_inst_n_3;
  wire crc_inst_n_30;
  wire crc_inst_n_31;
  wire crc_inst_n_32;
  wire crc_inst_n_33;
  wire crc_inst_n_34;
  wire crc_inst_n_35;
  wire crc_inst_n_36;
  wire crc_inst_n_37;
  wire crc_inst_n_38;
  wire crc_inst_n_39;
  wire crc_inst_n_4;
  wire crc_inst_n_40;
  wire crc_inst_n_41;
  wire crc_inst_n_42;
  wire crc_inst_n_43;
  wire crc_inst_n_44;
  wire crc_inst_n_45;
  wire crc_inst_n_46;
  wire crc_inst_n_47;
  wire crc_inst_n_48;
  wire crc_inst_n_49;
  wire crc_inst_n_5;
  wire crc_inst_n_50;
  wire crc_inst_n_51;
  wire crc_inst_n_52;
  wire crc_inst_n_53;
  wire crc_inst_n_54;
  wire crc_inst_n_55;
  wire crc_inst_n_56;
  wire crc_inst_n_57;
  wire crc_inst_n_58;
  wire crc_inst_n_59;
  wire crc_inst_n_6;
  wire crc_inst_n_60;
  wire crc_inst_n_61;
  wire crc_inst_n_62;
  wire crc_inst_n_63;
  wire crc_inst_n_7;
  wire crc_inst_n_8;
  wire crc_inst_n_9;
  wire ctrl_bit_send;
  wire ctrl_bit_tx;
  wire ctrl_bus_rst;
  wire [62:0]ctrl_wr_data;
  wire dcvr_bit_recv;
  wire dcvr_bit_send;
  wire dcvr_bit_tx;
  wire dcvr_bus_rst;
  wire dcvr_done;
  wire [63:0]dcvr_id;
  wire dcvr_id_en;
  wire dcvr_start;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire if_done;
  wire if_rx_data;
  wire if_rx_data_en;
  wire [6:0]in30;
  wire [3:0]lfsr;
  wire \nxt_reg[bit_send]19_out ;
  wire \nxt_reg[id]1__4 ;
  wire \nxt_reg[id]1_inferred__0/i__carry_n_0 ;
  wire \nxt_reg[id]1_inferred__0/i__carry_n_1 ;
  wire \nxt_reg[id]1_inferred__0/i__carry_n_2 ;
  wire \nxt_reg[id]1_inferred__0/i__carry_n_3 ;
  wire \nxt_reg[state]125_out ;
  wire ow_reg_rst;
  wire ow_rst;
  wire \reg[bit_recv] ;
  wire \reg[bit_recv]_i_2_n_0 ;
  wire \reg[bit_send] ;
  wire \reg[bit_send]_i_2_n_0 ;
  wire \reg[bit_send]_i_3_n_0 ;
  wire \reg[bus_rst] ;
  wire \reg[bus_rst]_i_2_n_0 ;
  wire \reg[cmd][0]_i_1_n_0 ;
  wire \reg[cmd][1]_i_1_n_0 ;
  wire \reg[cmd][2]_i_1_n_0 ;
  wire \reg[cmd][3]_i_1_n_0 ;
  wire \reg[cmd][4]_i_1_n_0 ;
  wire \reg[cmd][5]_i_1_n_0 ;
  wire \reg[cmd][6]_i_1_n_0 ;
  wire \reg[cmd][7]_i_1_n_0 ;
  wire \reg[cmd][7]_i_2_n_0 ;
  wire \reg[cmd][7]_i_3_n_0 ;
  wire \reg[cmp_id_bit]_i_1_n_0 ;
  wire \reg[cmp_id_bit]_i_2_n_0 ;
  wire \reg[cmp_id_bit]_i_3_n_0 ;
  wire \reg[crc_reset] ;
  wire \reg[crc_reset]_i_2_n_0 ;
  wire \reg[done] ;
  wire \reg[done]_i_2__0_n_0 ;
  wire \reg[id][32]_i_3_n_0 ;
  wire \reg[id][40]_i_3_n_0 ;
  wire \reg[id][48]_i_3_n_0 ;
  wire \reg[id][56]_i_3_n_0 ;
  wire \reg[id][56]_i_4_n_0 ;
  wire \reg[id][57]_i_2_n_0 ;
  wire \reg[id][58]_i_2_n_0 ;
  wire \reg[id][59]_i_2_n_0 ;
  wire \reg[id][61]_i_2_n_0 ;
  wire \reg[id][62]_i_2_n_0 ;
  wire \reg[id][63]_i_2_n_0 ;
  wire \reg[id][64]_i_10_n_0 ;
  wire \reg[id][64]_i_2_n_0 ;
  wire \reg[id][64]_i_3_n_0 ;
  wire \reg[id][64]_i_6_n_0 ;
  wire \reg[id][64]_i_8_n_0 ;
  wire \reg[id][64]_i_9_n_0 ;
  wire \reg[id_bit]_i_1_n_0 ;
  wire \reg[id_bit]_i_2_n_0 ;
  wire \reg[id_bit_number][0]_i_1_n_0 ;
  wire \reg[id_bit_number][1]_i_1_n_0 ;
  wire \reg[id_bit_number][2]_i_1_n_0 ;
  wire \reg[id_bit_number][3]_i_1_n_0 ;
  wire \reg[id_bit_number][3]_i_2_n_0 ;
  wire \reg[id_bit_number][4]_i_1_n_0 ;
  wire \reg[id_bit_number][5]_i_1_n_0 ;
  wire \reg[id_bit_number][6]_i_1_n_0 ;
  wire \reg[id_bit_number][6]_i_2_n_0 ;
  wire \reg[id_bit_number][6]_i_3_n_0 ;
  wire \reg[id_bit_number][6]_i_4_n_0 ;
  wire \reg[id_en] ;
  wire [6:0]\reg[last_discrepancy] ;
  wire \reg[last_discrepancy][0]_i_1_n_0 ;
  wire \reg[last_discrepancy][1]_i_1_n_0 ;
  wire \reg[last_discrepancy][2]_i_1_n_0 ;
  wire \reg[last_discrepancy][3]_i_1_n_0 ;
  wire \reg[last_discrepancy][4]_i_1_n_0 ;
  wire \reg[last_discrepancy][5]_i_1_n_0 ;
  wire \reg[last_discrepancy][6]_i_1_n_0 ;
  wire \reg[last_discrepancy][6]_i_2_n_0 ;
  wire \reg[last_discrepancy][6]_i_3_n_0 ;
  wire \reg[lfsr][0]_i_1__0_n_0 ;
  wire \reg[lfsr][1]_i_1_n_0 ;
  wire \reg[lfsr][2]_i_1_n_0 ;
  wire \reg[lfsr][3]_i_1_n_0 ;
  wire \reg[lfsr][3]_i_2_n_0 ;
  wire \reg[lfsr][3]_i_3_n_0 ;
  wire \reg[lfsr][3]_i_5_n_0 ;
  wire \reg[marker][0]_i_1_n_0 ;
  wire \reg[marker][1]_i_1_n_0 ;
  wire \reg[marker][2]_i_1_n_0 ;
  wire \reg[marker][3]_i_1_n_0 ;
  wire \reg[marker][4]_i_1_n_0 ;
  wire \reg[marker][5]_i_1_n_0 ;
  wire \reg[marker][6]_i_10_n_0 ;
  wire \reg[marker][6]_i_11_n_0 ;
  wire \reg[marker][6]_i_12_n_0 ;
  wire \reg[marker][6]_i_13_n_0 ;
  wire \reg[marker][6]_i_14_n_0 ;
  wire \reg[marker][6]_i_15_n_0 ;
  wire \reg[marker][6]_i_16_n_0 ;
  wire \reg[marker][6]_i_17_n_0 ;
  wire \reg[marker][6]_i_18_n_0 ;
  wire \reg[marker][6]_i_19_n_0 ;
  wire \reg[marker][6]_i_1_n_0 ;
  wire \reg[marker][6]_i_20_n_0 ;
  wire \reg[marker][6]_i_21_n_0 ;
  wire \reg[marker][6]_i_22_n_0 ;
  wire \reg[marker][6]_i_23_n_0 ;
  wire \reg[marker][6]_i_24_n_0 ;
  wire \reg[marker][6]_i_25_n_0 ;
  wire \reg[marker][6]_i_26_n_0 ;
  wire \reg[marker][6]_i_27_n_0 ;
  wire \reg[marker][6]_i_28_n_0 ;
  wire \reg[marker][6]_i_29_n_0 ;
  wire \reg[marker][6]_i_2_n_0 ;
  wire \reg[marker][6]_i_30_n_0 ;
  wire \reg[marker][6]_i_31_n_0 ;
  wire \reg[marker][6]_i_32_n_0 ;
  wire \reg[marker][6]_i_33_n_0 ;
  wire \reg[marker][6]_i_34_n_0 ;
  wire \reg[marker][6]_i_35_n_0 ;
  wire \reg[marker][6]_i_36_n_0 ;
  wire \reg[marker][6]_i_37_n_0 ;
  wire \reg[marker][6]_i_38_n_0 ;
  wire \reg[marker][6]_i_39_n_0 ;
  wire \reg[marker][6]_i_3_n_0 ;
  wire \reg[marker][6]_i_40_n_0 ;
  wire \reg[marker][6]_i_41_n_0 ;
  wire \reg[marker][6]_i_42_n_0 ;
  wire \reg[marker][6]_i_43_n_0 ;
  wire \reg[marker][6]_i_44_n_0 ;
  wire \reg[marker][6]_i_45_n_0 ;
  wire \reg[marker][6]_i_4_n_0 ;
  wire \reg[marker][6]_i_5_n_0 ;
  wire \reg[marker][6]_i_6_n_0 ;
  wire \reg[marker][6]_i_7_n_0 ;
  wire \reg[marker][6]_i_8_n_0 ;
  wire \reg[marker][6]_i_9_n_0 ;
  wire \reg[search]_i_1_n_0 ;
  wire \reg[search]_i_2_n_0 ;
  wire \reg[search]_i_4_n_0 ;
  wire \reg[search]_i_5_n_0 ;
  wire [2:0]\reg[state] ;
  wire \reg_reg[bit_send]_0 ;
  wire \reg_reg[bus_rst]_0 ;
  wire \reg_reg[cmd_n_0_][0] ;
  wire \reg_reg[cmp_id_bit_n_0_] ;
  wire \reg_reg[crc_reset_n_0_] ;
  wire \reg_reg[id_bit_n_0_] ;
  wire \reg_reg[id_bit_number_n_0_][0] ;
  wire \reg_reg[id_bit_number_n_0_][1] ;
  wire \reg_reg[id_bit_number_n_0_][2] ;
  wire \reg_reg[id_bit_number_n_0_][3] ;
  wire \reg_reg[id_bit_number_n_0_][4] ;
  wire \reg_reg[id_bit_number_n_0_][5] ;
  wire \reg_reg[id_bit_number_n_0_][6] ;
  wire \reg_reg[marker_n_0_][0] ;
  wire \reg_reg[marker_n_0_][1] ;
  wire \reg_reg[marker_n_0_][2] ;
  wire \reg_reg[marker_n_0_][3] ;
  wire \reg_reg[marker_n_0_][4] ;
  wire \reg_reg[marker_n_0_][5] ;
  wire \reg_reg[marker_n_0_][6] ;
  wire [63:0]wr_data_i;
  wire [3:0]\NLW_nxt_reg[id]1_inferred__0/i__carry_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h0000FFFF0F0D0000)) 
    \FSM_sequential_reg[state][0]_i_1__0 
       (.I0(\reg_reg[id_bit_number_n_0_][6] ),
        .I1(\FSM_sequential_reg[state][0]_i_2__0_n_0 ),
        .I2(ow_rst),
        .I3(\FSM_sequential_reg[state][0]_i_3_n_0 ),
        .I4(\FSM_sequential_reg[state][2]_i_3_n_0 ),
        .I5(\reg[state] [0]),
        .O(\FSM_sequential_reg[state][0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \FSM_sequential_reg[state][0]_i_2__0 
       (.I0(\reg[state] [2]),
        .I1(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][0]_i_2__0_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_reg[state][0]_i_3 
       (.I0(\FSM_sequential_reg[state][0]_i_4_n_0 ),
        .I1(\reg_reg[marker_n_0_][5] ),
        .I2(\reg_reg[marker_n_0_][3] ),
        .I3(\reg_reg[marker_n_0_][2] ),
        .O(\FSM_sequential_reg[state][0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_reg[state][0]_i_4 
       (.I0(\reg_reg[marker_n_0_][4] ),
        .I1(\reg_reg[marker_n_0_][6] ),
        .I2(\reg_reg[marker_n_0_][0] ),
        .I3(\reg_reg[marker_n_0_][1] ),
        .O(\FSM_sequential_reg[state][0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF8FFFFFF880000)) 
    \FSM_sequential_reg[state][1]_i_1__0 
       (.I0(\FSM_sequential_reg[state][2]_i_2_n_0 ),
        .I1(\reg[state] [0]),
        .I2(\FSM_sequential_reg[state][1]_i_2_n_0 ),
        .I3(\FSM_sequential_reg[state][1]_i_3__0_n_0 ),
        .I4(\FSM_sequential_reg[state][2]_i_3_n_0 ),
        .I5(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \FSM_sequential_reg[state][1]_i_2 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [0]),
        .I3(\reg_reg[id_bit_number_n_0_][6] ),
        .O(\FSM_sequential_reg[state][1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000040000040400)) 
    \FSM_sequential_reg[state][1]_i_3__0 
       (.I0(\reg[state] [2]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .I3(\reg[state] [1]),
        .I4(\reg[state] [0]),
        .I5(if_rx_data),
        .O(\FSM_sequential_reg[state][1]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hBAABFFFFBAAA0000)) 
    \FSM_sequential_reg[state][2]_i_1 
       (.I0(\FSM_sequential_reg[state][2]_i_2_n_0 ),
        .I1(ow_rst),
        .I2(\reg[state] [0]),
        .I3(\reg[state] [1]),
        .I4(\FSM_sequential_reg[state][2]_i_3_n_0 ),
        .I5(\reg[state] [2]),
        .O(\FSM_sequential_reg[state][2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000004440000)) 
    \FSM_sequential_reg[state][2]_i_2 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg_reg[id_bit_n_0_] ),
        .I3(\reg_reg[cmp_id_bit_n_0_] ),
        .I4(\reg[state] [2]),
        .I5(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFBBFFBAFFBA)) 
    \FSM_sequential_reg[state][2]_i_3 
       (.I0(\FSM_sequential_reg[state][2]_i_4__0_n_0 ),
        .I1(\FSM_sequential_reg[state][2]_i_5_n_0 ),
        .I2(if_rx_data_en),
        .I3(ow_rst),
        .I4(\FSM_sequential_reg[state][2]_i_6__0_n_0 ),
        .I5(\FSM_sequential_reg[state][2]_i_7_n_0 ),
        .O(\FSM_sequential_reg[state][2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hEAEAAAAFEAEAAAAA)) 
    \FSM_sequential_reg[state][2]_i_4__0 
       (.I0(\FSM_sequential_reg[state][2]_i_8_n_0 ),
        .I1(if_done),
        .I2(\reg[state] [2]),
        .I3(\reg[state] [0]),
        .I4(\reg[state] [1]),
        .I5(dcvr_start),
        .O(\FSM_sequential_reg[state][2]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_sequential_reg[state][2]_i_5 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .O(\FSM_sequential_reg[state][2]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_sequential_reg[state][2]_i_6__0 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [0]),
        .I2(if_done),
        .O(\FSM_sequential_reg[state][2]_i_6__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \FSM_sequential_reg[state][2]_i_7 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [0]),
        .I2(\reg[state] [2]),
        .O(\FSM_sequential_reg[state][2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h00800080008C0080)) 
    \FSM_sequential_reg[state][2]_i_8 
       (.I0(if_rx_data_en),
        .I1(\reg[state] [1]),
        .I2(\reg[state] [0]),
        .I3(\reg[state] [2]),
        .I4(if_done),
        .I5(\reg[crc_reset]_i_2_n_0 ),
        .O(\FSM_sequential_reg[state][2]_i_8_n_0 ));
  (* FSM_ENCODED_STATES = "read_cmp_id_bit:100,compare:101,search_command:010,check:110,read_id_bit:011,reset_done:001,idle:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_reg_reg[state][0] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\FSM_sequential_reg[state][0]_i_1__0_n_0 ),
        .Q(\reg[state] [0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "read_cmp_id_bit:100,compare:101,search_command:010,check:110,read_id_bit:011,reset_done:001,idle:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_reg_reg[state][1] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\FSM_sequential_reg[state][1]_i_1__0_n_0 ),
        .Q(\reg[state] [1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "read_cmp_id_bit:100,compare:101,search_command:010,check:110,read_id_bit:011,reset_done:001,idle:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_reg_reg[state][2] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\FSM_sequential_reg[state][2]_i_1_n_0 ),
        .Q(\reg[state] [2]),
        .R(1'b0));
  design_1_axi_onewire_0_0_onewire_crc crc_inst
       (.CO(\nxt_reg[id]1_inferred__0/i__carry_n_0 ),
        .axi_aresetn_i(axi_aresetn_i),
        .axi_clk_i(axi_clk_i),
        .\crc_reg[2]_0 (dcvr_bit_tx),
        .dcvr_bit_send(dcvr_bit_send),
        .dcvr_id(dcvr_id),
        .if_done(if_done),
        .ow_reg_rst(ow_reg_rst),
        .ow_rst(ow_rst),
        .pristine_reg_0(\reg_reg[crc_reset_n_0_] ),
        .\reg[state] (\reg[state] ),
        .\reg_reg[id][10] (crc_inst_n_54),
        .\reg_reg[id][11] (crc_inst_n_53),
        .\reg_reg[id][13] (crc_inst_n_51),
        .\reg_reg[id][14] (crc_inst_n_50),
        .\reg_reg[id][15] (crc_inst_n_49),
        .\reg_reg[id][17] (crc_inst_n_47),
        .\reg_reg[id][17]_0 (\reg[id][56]_i_4_n_0 ),
        .\reg_reg[id][18] (crc_inst_n_46),
        .\reg_reg[id][19] (crc_inst_n_45),
        .\reg_reg[id][1] (crc_inst_n_63),
        .\reg_reg[id][1]_0 (\reg[marker][6]_i_4_n_0 ),
        .\reg_reg[id][1]_1 (\FSM_sequential_reg[state][2]_i_5_n_0 ),
        .\reg_reg[id][1]_2 (\reg[id][56]_i_3_n_0 ),
        .\reg_reg[id][1]_3 (\reg[id][40]_i_3_n_0 ),
        .\reg_reg[id][1]_4 (\reg[id][64]_i_9_n_0 ),
        .\reg_reg[id][1]_5 (\reg[id][64]_i_10_n_0 ),
        .\reg_reg[id][21] (crc_inst_n_43),
        .\reg_reg[id][22] (crc_inst_n_42),
        .\reg_reg[id][23] (crc_inst_n_41),
        .\reg_reg[id][25] (crc_inst_n_39),
        .\reg_reg[id][25]_0 (\reg[id][32]_i_3_n_0 ),
        .\reg_reg[id][26] (crc_inst_n_38),
        .\reg_reg[id][27] (crc_inst_n_37),
        .\reg_reg[id][29] (crc_inst_n_35),
        .\reg_reg[id][2] (crc_inst_n_62),
        .\reg_reg[id][30] (crc_inst_n_34),
        .\reg_reg[id][31] (crc_inst_n_33),
        .\reg_reg[id][33] (crc_inst_n_31),
        .\reg_reg[id][34] (crc_inst_n_30),
        .\reg_reg[id][35] (crc_inst_n_29),
        .\reg_reg[id][37] (crc_inst_n_27),
        .\reg_reg[id][38] (crc_inst_n_26),
        .\reg_reg[id][39] (crc_inst_n_25),
        .\reg_reg[id][3] (crc_inst_n_61),
        .\reg_reg[id][41] (crc_inst_n_23),
        .\reg_reg[id][42] (crc_inst_n_22),
        .\reg_reg[id][43] (crc_inst_n_21),
        .\reg_reg[id][45] (crc_inst_n_19),
        .\reg_reg[id][46] (crc_inst_n_18),
        .\reg_reg[id][47] (crc_inst_n_17),
        .\reg_reg[id][49] (crc_inst_n_15),
        .\reg_reg[id][50] (crc_inst_n_14),
        .\reg_reg[id][51] (crc_inst_n_13),
        .\reg_reg[id][53] (crc_inst_n_11),
        .\reg_reg[id][54] (crc_inst_n_10),
        .\reg_reg[id][55] (crc_inst_n_9),
        .\reg_reg[id][57] (crc_inst_n_7),
        .\reg_reg[id][57]_0 (\reg[id][64]_i_8_n_0 ),
        .\reg_reg[id][57]_1 (\reg[id][57]_i_2_n_0 ),
        .\reg_reg[id][58] (crc_inst_n_6),
        .\reg_reg[id][58]_0 (\reg[id][58]_i_2_n_0 ),
        .\reg_reg[id][59] (crc_inst_n_5),
        .\reg_reg[id][59]_0 (\reg[id][59]_i_2_n_0 ),
        .\reg_reg[id][5] (crc_inst_n_59),
        .\reg_reg[id][61] (crc_inst_n_3),
        .\reg_reg[id][61]_0 (\reg[id][61]_i_2_n_0 ),
        .\reg_reg[id][62] (crc_inst_n_2),
        .\reg_reg[id][62]_0 (\reg[id][62]_i_2_n_0 ),
        .\reg_reg[id][63] (crc_inst_n_1),
        .\reg_reg[id][63]_0 (\reg[id][63]_i_2_n_0 ),
        .\reg_reg[id][64] (\reg[id][64]_i_2_n_0 ),
        .\reg_reg[id][64]_0 (\reg_reg[id_bit_number_n_0_][2] ),
        .\reg_reg[id][64]_1 (\reg[id][64]_i_3_n_0 ),
        .\reg_reg[id][6] (crc_inst_n_58),
        .\reg_reg[id][7] (crc_inst_n_57),
        .\reg_reg[id][9] (crc_inst_n_55),
        .\reg_reg[id][9]_0 (\reg[id][48]_i_3_n_0 ),
        .\reg_reg[id_bit_number][2] (crc_inst_n_0),
        .\reg_reg[id_bit_number][2]_0 (crc_inst_n_4),
        .\reg_reg[id_bit_number][2]_1 (crc_inst_n_8),
        .\reg_reg[id_bit_number][2]_10 (crc_inst_n_44),
        .\reg_reg[id_bit_number][2]_11 (crc_inst_n_48),
        .\reg_reg[id_bit_number][2]_12 (crc_inst_n_52),
        .\reg_reg[id_bit_number][2]_13 (crc_inst_n_56),
        .\reg_reg[id_bit_number][2]_14 (crc_inst_n_60),
        .\reg_reg[id_bit_number][2]_2 (crc_inst_n_12),
        .\reg_reg[id_bit_number][2]_3 (crc_inst_n_16),
        .\reg_reg[id_bit_number][2]_4 (crc_inst_n_20),
        .\reg_reg[id_bit_number][2]_5 (crc_inst_n_24),
        .\reg_reg[id_bit_number][2]_6 (crc_inst_n_28),
        .\reg_reg[id_bit_number][2]_7 (crc_inst_n_32),
        .\reg_reg[id_bit_number][2]_8 (crc_inst_n_36),
        .\reg_reg[id_bit_number][2]_9 (crc_inst_n_40));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry_i_1
       (.I0(\reg_reg[id_bit_number_n_0_][6] ),
        .I1(\reg[last_discrepancy] [6]),
        .O(i__carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2
       (.I0(\reg_reg[id_bit_number_n_0_][5] ),
        .I1(\reg[last_discrepancy] [5]),
        .I2(\reg_reg[id_bit_number_n_0_][4] ),
        .I3(\reg[last_discrepancy] [4]),
        .O(i__carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_3
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg[last_discrepancy] [2]),
        .I2(\reg[last_discrepancy] [3]),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .O(i__carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_4
       (.I0(\reg_reg[id_bit_number_n_0_][0] ),
        .I1(\reg[last_discrepancy] [0]),
        .I2(\reg[last_discrepancy] [1]),
        .I3(\reg_reg[id_bit_number_n_0_][1] ),
        .O(i__carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry_i_5
       (.I0(\reg[last_discrepancy] [6]),
        .I1(\reg_reg[id_bit_number_n_0_][6] ),
        .O(i__carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(\reg[last_discrepancy] [4]),
        .I1(\reg_reg[id_bit_number_n_0_][4] ),
        .I2(\reg[last_discrepancy] [5]),
        .I3(\reg_reg[id_bit_number_n_0_][5] ),
        .O(i__carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg[last_discrepancy] [2]),
        .I2(\reg_reg[id_bit_number_n_0_][3] ),
        .I3(\reg[last_discrepancy] [3]),
        .O(i__carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8
       (.I0(\reg_reg[id_bit_number_n_0_][0] ),
        .I1(\reg[last_discrepancy] [0]),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg[last_discrepancy] [1]),
        .O(i__carry_i_8_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_0_2_i_1
       (.I0(dcvr_id[0]),
        .I1(dcvr_id_en),
        .I2(ctrl_bit_tx),
        .O(wr_data_i[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_0_2_i_2
       (.I0(dcvr_id[1]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[0]),
        .O(wr_data_i[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_0_2_i_3
       (.I0(dcvr_id[2]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[1]),
        .O(wr_data_i[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_12_14_i_1
       (.I0(dcvr_id[12]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[11]),
        .O(wr_data_i[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_12_14_i_2
       (.I0(dcvr_id[13]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[12]),
        .O(wr_data_i[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_12_14_i_3
       (.I0(dcvr_id[14]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[13]),
        .O(wr_data_i[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_15_17_i_1
       (.I0(dcvr_id[15]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[14]),
        .O(wr_data_i[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_15_17_i_2
       (.I0(dcvr_id[16]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[15]),
        .O(wr_data_i[16]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_15_17_i_3
       (.I0(dcvr_id[17]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[16]),
        .O(wr_data_i[17]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_18_20_i_1
       (.I0(dcvr_id[18]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[17]),
        .O(wr_data_i[18]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_18_20_i_2
       (.I0(dcvr_id[19]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[18]),
        .O(wr_data_i[19]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_18_20_i_3
       (.I0(dcvr_id[20]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[19]),
        .O(wr_data_i[20]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_21_23_i_1
       (.I0(dcvr_id[21]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[20]),
        .O(wr_data_i[21]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_21_23_i_2
       (.I0(dcvr_id[22]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[21]),
        .O(wr_data_i[22]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_21_23_i_3
       (.I0(dcvr_id[23]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[22]),
        .O(wr_data_i[23]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_24_26_i_1
       (.I0(dcvr_id[24]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[23]),
        .O(wr_data_i[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_24_26_i_2
       (.I0(dcvr_id[25]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[24]),
        .O(wr_data_i[25]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_24_26_i_3
       (.I0(dcvr_id[26]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[25]),
        .O(wr_data_i[26]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_27_29_i_1
       (.I0(dcvr_id[27]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[26]),
        .O(wr_data_i[27]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_27_29_i_2
       (.I0(dcvr_id[28]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[27]),
        .O(wr_data_i[28]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_27_29_i_3
       (.I0(dcvr_id[29]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[28]),
        .O(wr_data_i[29]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_30_32_i_1
       (.I0(dcvr_id[30]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[29]),
        .O(wr_data_i[30]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_30_32_i_2
       (.I0(dcvr_id[31]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[30]),
        .O(wr_data_i[31]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_30_32_i_3
       (.I0(dcvr_id[32]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[31]),
        .O(wr_data_i[32]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_33_35_i_1
       (.I0(dcvr_id[33]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[32]),
        .O(wr_data_i[33]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_33_35_i_2
       (.I0(dcvr_id[34]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[33]),
        .O(wr_data_i[34]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_33_35_i_3
       (.I0(dcvr_id[35]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[34]),
        .O(wr_data_i[35]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_36_38_i_1
       (.I0(dcvr_id[36]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[35]),
        .O(wr_data_i[36]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_36_38_i_2
       (.I0(dcvr_id[37]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[36]),
        .O(wr_data_i[37]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_36_38_i_3
       (.I0(dcvr_id[38]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[37]),
        .O(wr_data_i[38]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_39_41_i_1
       (.I0(dcvr_id[39]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[38]),
        .O(wr_data_i[39]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_39_41_i_2
       (.I0(dcvr_id[40]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[39]),
        .O(wr_data_i[40]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_39_41_i_3
       (.I0(dcvr_id[41]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[40]),
        .O(wr_data_i[41]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_3_5_i_1
       (.I0(dcvr_id[3]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[2]),
        .O(wr_data_i[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_3_5_i_2
       (.I0(dcvr_id[4]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[3]),
        .O(wr_data_i[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_3_5_i_3
       (.I0(dcvr_id[5]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[4]),
        .O(wr_data_i[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_42_44_i_1
       (.I0(dcvr_id[42]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[41]),
        .O(wr_data_i[42]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_42_44_i_2
       (.I0(dcvr_id[43]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[42]),
        .O(wr_data_i[43]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_42_44_i_3
       (.I0(dcvr_id[44]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[43]),
        .O(wr_data_i[44]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_45_47_i_1
       (.I0(dcvr_id[45]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[44]),
        .O(wr_data_i[45]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_45_47_i_2
       (.I0(dcvr_id[46]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[45]),
        .O(wr_data_i[46]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_45_47_i_3
       (.I0(dcvr_id[47]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[46]),
        .O(wr_data_i[47]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_48_50_i_1
       (.I0(dcvr_id[48]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[47]),
        .O(wr_data_i[48]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_48_50_i_2
       (.I0(dcvr_id[49]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[48]),
        .O(wr_data_i[49]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_48_50_i_3
       (.I0(dcvr_id[50]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[49]),
        .O(wr_data_i[50]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_51_53_i_1
       (.I0(dcvr_id[51]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[50]),
        .O(wr_data_i[51]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_51_53_i_2
       (.I0(dcvr_id[52]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[51]),
        .O(wr_data_i[52]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_51_53_i_3
       (.I0(dcvr_id[53]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[52]),
        .O(wr_data_i[53]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_54_56_i_1
       (.I0(dcvr_id[54]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[53]),
        .O(wr_data_i[54]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_54_56_i_2
       (.I0(dcvr_id[55]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[54]),
        .O(wr_data_i[55]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_54_56_i_3
       (.I0(dcvr_id[56]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[55]),
        .O(wr_data_i[56]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_57_59_i_1
       (.I0(dcvr_id[57]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[56]),
        .O(wr_data_i[57]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_57_59_i_2
       (.I0(dcvr_id[58]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[57]),
        .O(wr_data_i[58]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_57_59_i_3
       (.I0(dcvr_id[59]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[58]),
        .O(wr_data_i[59]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_60_62_i_1
       (.I0(dcvr_id[60]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[59]),
        .O(wr_data_i[60]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_60_62_i_2
       (.I0(dcvr_id[61]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[60]),
        .O(wr_data_i[61]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_60_62_i_3
       (.I0(dcvr_id[62]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[61]),
        .O(wr_data_i[62]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_63_63_i_1
       (.I0(dcvr_id[63]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[62]),
        .O(wr_data_i[63]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_6_8_i_1
       (.I0(dcvr_id[6]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[5]),
        .O(wr_data_i[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_6_8_i_2
       (.I0(dcvr_id[7]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[6]),
        .O(wr_data_i[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_6_8_i_3
       (.I0(dcvr_id[8]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[7]),
        .O(wr_data_i[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_9_11_i_1
       (.I0(dcvr_id[9]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[8]),
        .O(wr_data_i[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_9_11_i_2
       (.I0(dcvr_id[10]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[9]),
        .O(wr_data_i[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    mem_reg_0_63_9_11_i_3
       (.I0(dcvr_id[11]),
        .I1(dcvr_id_en),
        .I2(ctrl_wr_data[10]),
        .O(wr_data_i[11]));
  (* COMPARATOR_THRESHOLD = "11" *) 
  CARRY4 \nxt_reg[id]1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\nxt_reg[id]1_inferred__0/i__carry_n_0 ,\nxt_reg[id]1_inferred__0/i__carry_n_1 ,\nxt_reg[id]1_inferred__0/i__carry_n_2 ,\nxt_reg[id]1_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}),
        .O(\NLW_nxt_reg[id]1_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0}));
  LUT6 #(
    .INIT(64'hABAAAAAAAAAAAAAA)) 
    \reg[bit_recv]_i_1 
       (.I0(\reg[bit_recv]_i_2_n_0 ),
        .I1(ow_rst),
        .I2(\reg[state] [2]),
        .I3(\reg[state] [1]),
        .I4(\reg[state] [0]),
        .I5(if_rx_data_en),
        .O(\reg[bit_recv] ));
  LUT6 #(
    .INIT(64'h4000000040C00000)) 
    \reg[bit_recv]_i_2 
       (.I0(\reg_reg[id_bit_number_n_0_][6] ),
        .I1(\reg[id_bit_number][3]_i_2_n_0 ),
        .I2(if_done),
        .I3(\reg[state] [2]),
        .I4(\reg[state] [1]),
        .I5(\reg[crc_reset]_i_2_n_0 ),
        .O(\reg[bit_recv]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFAEAAAAAAAEAA)) 
    \reg[bit_send]_i_1 
       (.I0(\reg[bit_send]_i_2_n_0 ),
        .I1(\reg[bit_send]_i_3_n_0 ),
        .I2(ow_rst),
        .I3(\nxt_reg[bit_send]19_out ),
        .I4(\reg[state] [0]),
        .I5(\FSM_sequential_reg[state][2]_i_2_n_0 ),
        .O(\reg[bit_send] ));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    \reg[bit_send]_i_2 
       (.I0(if_rx_data),
        .I1(\reg[state] [2]),
        .I2(ow_rst),
        .I3(if_done),
        .I4(\reg[state] [0]),
        .I5(\reg[state] [1]),
        .O(\reg[bit_send]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \reg[bit_send]_i_3 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .O(\reg[bit_send]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h888888F888888888)) 
    \reg[bus_rst]_i_1 
       (.I0(\FSM_sequential_reg[state][0]_i_3_n_0 ),
        .I1(\reg[bus_rst]_i_2_n_0 ),
        .I2(\reg[id_bit_number][3]_i_2_n_0 ),
        .I3(\reg[state] [2]),
        .I4(\reg[state] [1]),
        .I5(dcvr_start),
        .O(\reg[bus_rst] ));
  LUT6 #(
    .INIT(64'h0200000000000000)) 
    \reg[bus_rst]_i_2 
       (.I0(if_done),
        .I1(\reg[state] [0]),
        .I2(ow_rst),
        .I3(\reg[state] [2]),
        .I4(\reg[state] [1]),
        .I5(\reg_reg[id_bit_number_n_0_][6] ),
        .O(\reg[bus_rst]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000B0000000000)) 
    \reg[cmd][0]_i_1 
       (.I0(\reg[state] [1]),
        .I1(if_rx_data),
        .I2(\reg[state] [2]),
        .I3(axi_aresetn_i),
        .I4(ow_reg_rst),
        .I5(in30[0]),
        .O(\reg[cmd][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000B0000000000)) 
    \reg[cmd][1]_i_1 
       (.I0(\reg[state] [1]),
        .I1(if_rx_data),
        .I2(\reg[state] [2]),
        .I3(axi_aresetn_i),
        .I4(ow_reg_rst),
        .I5(in30[1]),
        .O(\reg[cmd][1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000B0000000000)) 
    \reg[cmd][2]_i_1 
       (.I0(\reg[state] [1]),
        .I1(if_rx_data),
        .I2(\reg[state] [2]),
        .I3(axi_aresetn_i),
        .I4(ow_reg_rst),
        .I5(in30[2]),
        .O(\reg[cmd][2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000B0000000000)) 
    \reg[cmd][3]_i_1 
       (.I0(\reg[state] [1]),
        .I1(if_rx_data),
        .I2(\reg[state] [2]),
        .I3(axi_aresetn_i),
        .I4(ow_reg_rst),
        .I5(in30[3]),
        .O(\reg[cmd][3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF3AFF3AFFFFFFFA)) 
    \reg[cmd][4]_i_1 
       (.I0(in30[4]),
        .I1(\reg[state] [0]),
        .I2(\reg[state] [2]),
        .I3(ow_rst),
        .I4(if_rx_data),
        .I5(\reg[state] [1]),
        .O(\reg[cmd][4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF3AFF3AFFFFFFFA)) 
    \reg[cmd][5]_i_1 
       (.I0(in30[5]),
        .I1(\reg[state] [0]),
        .I2(\reg[state] [2]),
        .I3(ow_rst),
        .I4(if_rx_data),
        .I5(\reg[state] [1]),
        .O(\reg[cmd][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF3AFF3AFFFFFFFA)) 
    \reg[cmd][6]_i_1 
       (.I0(in30[6]),
        .I1(\reg[state] [0]),
        .I2(\reg[state] [2]),
        .I3(ow_rst),
        .I4(if_rx_data),
        .I5(\reg[state] [1]),
        .O(\reg[cmd][6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAAABAAA)) 
    \reg[cmd][7]_i_1 
       (.I0(ow_rst),
        .I1(\reg[state] [2]),
        .I2(if_done),
        .I3(\reg[state] [0]),
        .I4(\reg[state] [1]),
        .I5(\reg[cmd][7]_i_3_n_0 ),
        .O(\reg[cmd][7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF5FFF4FFFFFFF4FF)) 
    \reg[cmd][7]_i_2 
       (.I0(\reg[state] [1]),
        .I1(if_rx_data),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\reg[state] [2]),
        .I5(\reg[state] [0]),
        .O(\reg[cmd][7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT5 #(
    .INIT(32'hFFFF0200)) 
    \reg[cmd][7]_i_3 
       (.I0(\nxt_reg[bit_send]19_out ),
        .I1(\reg[state] [0]),
        .I2(\reg[state] [2]),
        .I3(\reg[state] [1]),
        .I4(\reg[lfsr][3]_i_3_n_0 ),
        .O(\reg[cmd][7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0010FFFF00100000)) 
    \reg[cmp_id_bit]_i_1 
       (.I0(ow_rst),
        .I1(\reg[state] [0]),
        .I2(if_rx_data),
        .I3(\reg[state] [1]),
        .I4(\reg[cmp_id_bit]_i_2_n_0 ),
        .I5(\reg_reg[cmp_id_bit_n_0_] ),
        .O(\reg[cmp_id_bit]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFEAAAAAAA)) 
    \reg[cmp_id_bit]_i_2 
       (.I0(\reg[id_bit_number][6]_i_3_n_0 ),
        .I1(\reg[state] [2]),
        .I2(\reg[state] [1]),
        .I3(\reg_reg[id_bit_number_n_0_][6] ),
        .I4(if_done),
        .I5(\reg[cmp_id_bit]_i_3_n_0 ),
        .O(\reg[cmp_id_bit]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF0040FFFF)) 
    \reg[cmp_id_bit]_i_3 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(if_rx_data_en),
        .I3(\reg[state] [0]),
        .I4(axi_aresetn_i),
        .I5(ow_reg_rst),
        .O(\reg[cmp_id_bit]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000400)) 
    \reg[crc_reset]_i_1 
       (.I0(\reg[crc_reset]_i_2_n_0 ),
        .I1(\reg[state] [1]),
        .I2(\reg[state] [2]),
        .I3(if_done),
        .I4(\reg[state] [0]),
        .I5(ow_rst),
        .O(\reg[crc_reset] ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    \reg[crc_reset]_i_2 
       (.I0(lfsr[1]),
        .I1(lfsr[0]),
        .I2(lfsr[3]),
        .I3(lfsr[2]),
        .O(\reg[crc_reset]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \reg[data]_i_10 
       (.I0(dcvr_bit_send),
        .I1(ctrl_bit_send),
        .O(\reg_reg[bit_send]_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \reg[data]_i_9 
       (.I0(dcvr_bus_rst),
        .I1(ctrl_bus_rst),
        .O(\reg_reg[bus_rst]_0 ));
  LUT6 #(
    .INIT(64'h0000FFFF00000400)) 
    \reg[done]_i_1__0 
       (.I0(\FSM_sequential_reg[state][0]_i_3_n_0 ),
        .I1(if_done),
        .I2(\reg[state] [0]),
        .I3(\reg[done]_i_2__0_n_0 ),
        .I4(ow_rst),
        .I5(\reg[id_bit_number][6]_i_3_n_0 ),
        .O(\reg[done] ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \reg[done]_i_2__0 
       (.I0(\reg_reg[id_bit_number_n_0_][6] ),
        .I1(\reg[state] [1]),
        .I2(\reg[state] [2]),
        .O(\reg[done]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'hAAFFABFFFFFFFFFD)) 
    \reg[id][32]_i_3 
       (.I0(\reg_reg[id_bit_number_n_0_][5] ),
        .I1(\reg_reg[id_bit_number_n_0_][1] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][4] ),
        .O(\reg[id][32]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'hFAFAFAEB)) 
    \reg[id][40]_i_3 
       (.I0(\reg_reg[id_bit_number_n_0_][4] ),
        .I1(\reg_reg[id_bit_number_n_0_][2] ),
        .I2(\reg_reg[id_bit_number_n_0_][3] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[id][40]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'hAFAFAFBD)) 
    \reg[id][48]_i_3 
       (.I0(\reg_reg[id_bit_number_n_0_][4] ),
        .I1(\reg_reg[id_bit_number_n_0_][2] ),
        .I2(\reg_reg[id_bit_number_n_0_][3] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[id][48]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \reg[id][56]_i_3 
       (.I0(\reg_reg[id_bit_number_n_0_][4] ),
        .I1(\reg_reg[id_bit_number_n_0_][2] ),
        .I2(\reg_reg[id_bit_number_n_0_][3] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(\reg_reg[id_bit_number_n_0_][5] ),
        .O(\reg[id][56]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT5 #(
    .INIT(32'hFF55FD57)) 
    \reg[id][56]_i_4 
       (.I0(\reg_reg[id_bit_number_n_0_][4] ),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .O(\reg[id][56]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \reg[id][57]_i_2 
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg_reg[id_bit_number_n_0_][1] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[id][57]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \reg[id][58]_i_2 
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg_reg[id_bit_number_n_0_][1] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[id][58]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \reg[id][59]_i_2 
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg_reg[id_bit_number_n_0_][1] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[id][59]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \reg[id][61]_i_2 
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg_reg[id_bit_number_n_0_][1] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[id][61]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hF7)) 
    \reg[id][62]_i_2 
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg_reg[id_bit_number_n_0_][1] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[id][62]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \reg[id][63]_i_2 
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg_reg[id_bit_number_n_0_][1] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[id][63]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \reg[id][64]_i_10 
       (.I0(\reg[state] [1]),
        .I1(if_rx_data),
        .O(\reg[id][64]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hAAAAAEAA)) 
    \reg[id][64]_i_2 
       (.I0(\reg[id][64]_i_6_n_0 ),
        .I1(if_done),
        .I2(\reg[state] [0]),
        .I3(axi_aresetn_i),
        .I4(ow_reg_rst),
        .O(\reg[id][64]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \reg[id][64]_i_3 
       (.I0(\reg_reg[id_bit_number_n_0_][1] ),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[id][64]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000300020)) 
    \reg[id][64]_i_6 
       (.I0(\reg[marker][6]_i_9_n_0 ),
        .I1(\reg[state] [1]),
        .I2(\reg[state] [2]),
        .I3(\reg_reg[cmp_id_bit_n_0_] ),
        .I4(\reg_reg[id_bit_n_0_] ),
        .I5(ow_rst),
        .O(\reg[id][64]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h77FF77FF77FF7FFE)) 
    \reg[id][64]_i_8 
       (.I0(\reg_reg[id_bit_number_n_0_][5] ),
        .I1(\reg_reg[id_bit_number_n_0_][4] ),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][0] ),
        .I5(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[id][64]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'h8F808080)) 
    \reg[id][64]_i_9 
       (.I0(\reg_reg[id_bit_number_n_0_][6] ),
        .I1(if_done),
        .I2(\reg[state] [1]),
        .I3(\reg_reg[id_bit_n_0_] ),
        .I4(\reg_reg[cmp_id_bit_n_0_] ),
        .O(\reg[id][64]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h0200FFFF02000000)) 
    \reg[id_bit]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(ow_rst),
        .I3(if_rx_data),
        .I4(\reg[id_bit]_i_2_n_0 ),
        .I5(\reg_reg[id_bit_n_0_] ),
        .O(\reg[id_bit]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEEFEEEEEEEEEEEEE)) 
    \reg[id_bit]_i_2 
       (.I0(\reg[lfsr][3]_i_5_n_0 ),
        .I1(\reg[lfsr][3]_i_3_n_0 ),
        .I2(\reg[state] [1]),
        .I3(\reg[state] [2]),
        .I4(if_rx_data_en),
        .I5(\reg[state] [0]),
        .O(\reg[id_bit]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF51FFFFFFFFFF)) 
    \reg[id_bit_number][0]_i_1 
       (.I0(\reg[state] [0]),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .I2(\reg_reg[id_bit_number_n_0_][6] ),
        .I3(\reg[state] [1]),
        .I4(ow_reg_rst),
        .I5(axi_aresetn_i),
        .O(\reg[id_bit_number][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000600)) 
    \reg[id_bit_number][1]_i_1 
       (.I0(\reg_reg[id_bit_number_n_0_][1] ),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .I2(ow_reg_rst),
        .I3(axi_aresetn_i),
        .I4(\reg[state] [0]),
        .I5(\reg_reg[id_bit_number_n_0_][6] ),
        .O(\reg[id_bit_number][1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0001010101000000)) 
    \reg[id_bit_number][2]_i_1 
       (.I0(ow_rst),
        .I1(\reg[state] [0]),
        .I2(\reg_reg[id_bit_number_n_0_][6] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(\reg_reg[id_bit_number_n_0_][2] ),
        .O(\reg[id_bit_number][2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000078F00000)) 
    \reg[id_bit_number][3]_i_1 
       (.I0(\reg_reg[id_bit_number_n_0_][1] ),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .I2(\reg_reg[id_bit_number_n_0_][3] ),
        .I3(\reg_reg[id_bit_number_n_0_][2] ),
        .I4(\reg[id_bit_number][3]_i_2_n_0 ),
        .I5(\reg_reg[id_bit_number_n_0_][6] ),
        .O(\reg[id_bit_number][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \reg[id_bit_number][3]_i_2 
       (.I0(\reg[state] [0]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\reg[id_bit_number][3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h2AAAAAAA80000000)) 
    \reg[id_bit_number][4]_i_1 
       (.I0(\FSM_sequential_reg[state][1]_i_2_n_0 ),
        .I1(\reg_reg[id_bit_number_n_0_][1] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][4] ),
        .O(\reg[id_bit_number][4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFFF000040000000)) 
    \reg[id_bit_number][5]_i_1 
       (.I0(\reg[id_bit_number][6]_i_4_n_0 ),
        .I1(\reg_reg[id_bit_number_n_0_][3] ),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][4] ),
        .I4(\FSM_sequential_reg[state][1]_i_2_n_0 ),
        .I5(\reg_reg[id_bit_number_n_0_][5] ),
        .O(\reg[id_bit_number][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAEAAAAA)) 
    \reg[id_bit_number][6]_i_1 
       (.I0(ow_rst),
        .I1(if_done),
        .I2(\reg[state] [2]),
        .I3(\reg[state] [0]),
        .I4(\reg[state] [1]),
        .I5(\reg[id_bit_number][6]_i_3_n_0 ),
        .O(\reg[id_bit_number][6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \reg[id_bit_number][6]_i_2 
       (.I0(\reg_reg[id_bit_number_n_0_][5] ),
        .I1(\FSM_sequential_reg[state][1]_i_2_n_0 ),
        .I2(\reg_reg[id_bit_number_n_0_][4] ),
        .I3(\reg_reg[id_bit_number_n_0_][2] ),
        .I4(\reg_reg[id_bit_number_n_0_][3] ),
        .I5(\reg[id_bit_number][6]_i_4_n_0 ),
        .O(\reg[id_bit_number][6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0B08080800000000)) 
    \reg[id_bit_number][6]_i_3 
       (.I0(\nxt_reg[state]125_out ),
        .I1(\reg[state] [2]),
        .I2(\reg[state] [1]),
        .I3(if_done),
        .I4(if_rx_data),
        .I5(\reg[state] [0]),
        .O(\reg[id_bit_number][6]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \reg[id_bit_number][6]_i_4 
       (.I0(\reg_reg[id_bit_number_n_0_][1] ),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[id_bit_number][6]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[id_bit_number][6]_i_5 
       (.I0(\reg_reg[id_bit_n_0_] ),
        .I1(\reg_reg[cmp_id_bit_n_0_] ),
        .O(\nxt_reg[state]125_out ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \reg[id_en]_i_1 
       (.I0(\reg[state] [2]),
        .I1(\reg[state] [1]),
        .I2(\reg_reg[id_bit_number_n_0_][6] ),
        .I3(if_done),
        .I4(\reg[state] [0]),
        .I5(ow_rst),
        .O(\reg[id_en] ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \reg[last_discrepancy][0]_i_1 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [0]),
        .I3(\reg_reg[marker_n_0_][0] ),
        .O(\reg[last_discrepancy][0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \reg[last_discrepancy][1]_i_1 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [0]),
        .I3(\reg_reg[marker_n_0_][1] ),
        .O(\reg[last_discrepancy][1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \reg[last_discrepancy][2]_i_1 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [0]),
        .I3(\reg_reg[marker_n_0_][2] ),
        .O(\reg[last_discrepancy][2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \reg[last_discrepancy][3]_i_1 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [0]),
        .I3(\reg_reg[marker_n_0_][3] ),
        .O(\reg[last_discrepancy][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \reg[last_discrepancy][4]_i_1 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [0]),
        .I3(\reg_reg[marker_n_0_][4] ),
        .O(\reg[last_discrepancy][4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \reg[last_discrepancy][5]_i_1 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [0]),
        .I3(\reg_reg[marker_n_0_][5] ),
        .O(\reg[last_discrepancy][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFEEEEEEEEEEEEEEE)) 
    \reg[last_discrepancy][6]_i_1 
       (.I0(\reg[id_bit_number][6]_i_3_n_0 ),
        .I1(ow_rst),
        .I2(\reg[state] [2]),
        .I3(if_done),
        .I4(\reg_reg[id_bit_number_n_0_][6] ),
        .I5(\reg[last_discrepancy][6]_i_3_n_0 ),
        .O(\reg[last_discrepancy][6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \reg[last_discrepancy][6]_i_2 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [0]),
        .I3(\reg_reg[marker_n_0_][6] ),
        .O(\reg[last_discrepancy][6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \reg[last_discrepancy][6]_i_3 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [0]),
        .O(\reg[last_discrepancy][6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF0FFF6F6F)) 
    \reg[lfsr][0]_i_1__0 
       (.I0(lfsr[3]),
        .I1(lfsr[2]),
        .I2(\reg[state] [1]),
        .I3(\reg[state] [0]),
        .I4(\reg[state] [2]),
        .I5(ow_rst),
        .O(\reg[lfsr][0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    \reg[lfsr][1]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(axi_aresetn_i),
        .I3(ow_reg_rst),
        .I4(lfsr[0]),
        .O(\reg[lfsr][1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    \reg[lfsr][2]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(axi_aresetn_i),
        .I3(ow_reg_rst),
        .I4(lfsr[1]),
        .O(\reg[lfsr][2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAAEAAAA)) 
    \reg[lfsr][3]_i_1 
       (.I0(\reg[lfsr][3]_i_3_n_0 ),
        .I1(\reg[state] [1]),
        .I2(\reg[state] [2]),
        .I3(\reg[state] [0]),
        .I4(\nxt_reg[bit_send]19_out ),
        .I5(\reg[lfsr][3]_i_5_n_0 ),
        .O(\reg[lfsr][3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h00200000)) 
    \reg[lfsr][3]_i_2 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(axi_aresetn_i),
        .I3(ow_reg_rst),
        .I4(lfsr[2]),
        .O(\reg[lfsr][3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0F80008000800080)) 
    \reg[lfsr][3]_i_3 
       (.I0(\reg_reg[id_bit_number_n_0_][6] ),
        .I1(if_done),
        .I2(\FSM_sequential_reg[state][2]_i_5_n_0 ),
        .I3(\FSM_sequential_reg[state][2]_i_7_n_0 ),
        .I4(\reg_reg[cmp_id_bit_n_0_] ),
        .I5(\reg_reg[id_bit_n_0_] ),
        .O(\reg[lfsr][3]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFBFF0000)) 
    \reg[lfsr][3]_i_4 
       (.I0(lfsr[2]),
        .I1(lfsr[3]),
        .I2(lfsr[0]),
        .I3(lfsr[1]),
        .I4(if_done),
        .O(\nxt_reg[bit_send]19_out ));
  LUT6 #(
    .INIT(64'hFFFFFFFF10000000)) 
    \reg[lfsr][3]_i_5 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(if_rx_data),
        .I3(if_done),
        .I4(\reg[state] [0]),
        .I5(ow_rst),
        .O(\reg[lfsr][3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000044400000000)) 
    \reg[marker][0]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(\reg_reg[cmp_id_bit_n_0_] ),
        .I3(\reg_reg[id_bit_n_0_] ),
        .I4(ow_rst),
        .I5(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[marker][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000044400000000)) 
    \reg[marker][1]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(\reg_reg[cmp_id_bit_n_0_] ),
        .I3(\reg_reg[id_bit_n_0_] ),
        .I4(ow_rst),
        .I5(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[marker][1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000044400000000)) 
    \reg[marker][2]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(\reg_reg[cmp_id_bit_n_0_] ),
        .I3(\reg_reg[id_bit_n_0_] ),
        .I4(ow_rst),
        .I5(\reg_reg[id_bit_number_n_0_][2] ),
        .O(\reg[marker][2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000044400000000)) 
    \reg[marker][3]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(\reg_reg[cmp_id_bit_n_0_] ),
        .I3(\reg_reg[id_bit_n_0_] ),
        .I4(ow_rst),
        .I5(\reg_reg[id_bit_number_n_0_][3] ),
        .O(\reg[marker][3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000044400000000)) 
    \reg[marker][4]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(\reg_reg[cmp_id_bit_n_0_] ),
        .I3(\reg_reg[id_bit_n_0_] ),
        .I4(ow_rst),
        .I5(\reg_reg[id_bit_number_n_0_][4] ),
        .O(\reg[marker][4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000044400000000)) 
    \reg[marker][5]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(\reg_reg[cmp_id_bit_n_0_] ),
        .I3(\reg_reg[id_bit_n_0_] ),
        .I4(ow_rst),
        .I5(\reg_reg[id_bit_number_n_0_][5] ),
        .O(\reg[marker][5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAEAAAAAA)) 
    \reg[marker][6]_i_1 
       (.I0(\reg[last_discrepancy][6]_i_1_n_0 ),
        .I1(\reg[marker][6]_i_3_n_0 ),
        .I2(\reg[state] [1]),
        .I3(\reg[state] [2]),
        .I4(\reg[state] [0]),
        .I5(\reg[marker][6]_i_4_n_0 ),
        .O(\reg[marker][6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT5 #(
    .INIT(32'h00080000)) 
    \reg[marker][6]_i_10 
       (.I0(\reg_reg[id_bit_number_n_0_][1] ),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .I2(\reg_reg[id_bit_number_n_0_][3] ),
        .I3(\reg_reg[id_bit_number_n_0_][2] ),
        .I4(dcvr_id[34]),
        .O(\reg[marker][6]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hBAAABEAEBAAABAAA)) 
    \reg[marker][6]_i_11 
       (.I0(\reg[marker][6]_i_28_n_0 ),
        .I1(\reg_reg[id_bit_number_n_0_][3] ),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg[marker][6]_i_29_n_0 ),
        .I4(\reg[id][64]_i_3_n_0 ),
        .I5(dcvr_id[39]),
        .O(\reg[marker][6]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF4F44)) 
    \reg[marker][6]_i_12 
       (.I0(\reg[marker][6]_i_30_n_0 ),
        .I1(dcvr_id[46]),
        .I2(\reg[marker][6]_i_31_n_0 ),
        .I3(dcvr_id[31]),
        .I4(\reg[marker][6]_i_32_n_0 ),
        .I5(\reg[marker][6]_i_33_n_0 ),
        .O(\reg[marker][6]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0C0000000000A000)) 
    \reg[marker][6]_i_13 
       (.I0(dcvr_id[43]),
        .I1(dcvr_id[42]),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][0] ),
        .I5(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[marker][6]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT5 #(
    .INIT(32'h00080000)) 
    \reg[marker][6]_i_14 
       (.I0(\reg_reg[id_bit_number_n_0_][1] ),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .I2(\reg_reg[id_bit_number_n_0_][3] ),
        .I3(\reg_reg[id_bit_number_n_0_][2] ),
        .I4(dcvr_id[2]),
        .O(\reg[marker][6]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hBAAABEAEBAAABAAA)) 
    \reg[marker][6]_i_15 
       (.I0(\reg[marker][6]_i_34_n_0 ),
        .I1(\reg_reg[id_bit_number_n_0_][3] ),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg[marker][6]_i_35_n_0 ),
        .I4(\reg[id][64]_i_3_n_0 ),
        .I5(dcvr_id[7]),
        .O(\reg[marker][6]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF4F44)) 
    \reg[marker][6]_i_16 
       (.I0(\reg[marker][6]_i_30_n_0 ),
        .I1(dcvr_id[14]),
        .I2(\reg[marker][6]_i_31_n_0 ),
        .I3(dcvr_id[63]),
        .I4(\reg[marker][6]_i_36_n_0 ),
        .I5(\reg[marker][6]_i_37_n_0 ),
        .O(\reg[marker][6]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h0C0000000000A000)) 
    \reg[marker][6]_i_17 
       (.I0(dcvr_id[11]),
        .I1(dcvr_id[10]),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][0] ),
        .I5(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[marker][6]_i_17_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT5 #(
    .INIT(32'h00080000)) 
    \reg[marker][6]_i_18 
       (.I0(\reg_reg[id_bit_number_n_0_][1] ),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .I2(\reg_reg[id_bit_number_n_0_][3] ),
        .I3(\reg_reg[id_bit_number_n_0_][2] ),
        .I4(dcvr_id[50]),
        .O(\reg[marker][6]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hBAAABEAEBAAABAAA)) 
    \reg[marker][6]_i_19 
       (.I0(\reg[marker][6]_i_38_n_0 ),
        .I1(\reg_reg[id_bit_number_n_0_][3] ),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg[marker][6]_i_39_n_0 ),
        .I4(\reg[id][64]_i_3_n_0 ),
        .I5(dcvr_id[55]),
        .O(\reg[marker][6]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h0000044400000000)) 
    \reg[marker][6]_i_2 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [2]),
        .I2(\reg_reg[cmp_id_bit_n_0_] ),
        .I3(\reg_reg[id_bit_n_0_] ),
        .I4(ow_rst),
        .I5(\reg_reg[id_bit_number_n_0_][6] ),
        .O(\reg[marker][6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF4F44)) 
    \reg[marker][6]_i_20 
       (.I0(\reg[marker][6]_i_30_n_0 ),
        .I1(dcvr_id[62]),
        .I2(\reg[marker][6]_i_31_n_0 ),
        .I3(dcvr_id[47]),
        .I4(\reg[marker][6]_i_40_n_0 ),
        .I5(\reg[marker][6]_i_41_n_0 ),
        .O(\reg[marker][6]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'h0C0000000000A000)) 
    \reg[marker][6]_i_21 
       (.I0(dcvr_id[59]),
        .I1(dcvr_id[58]),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][0] ),
        .I5(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[marker][6]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT5 #(
    .INIT(32'h00080000)) 
    \reg[marker][6]_i_22 
       (.I0(\reg_reg[id_bit_number_n_0_][1] ),
        .I1(\reg_reg[id_bit_number_n_0_][0] ),
        .I2(\reg_reg[id_bit_number_n_0_][3] ),
        .I3(\reg_reg[id_bit_number_n_0_][2] ),
        .I4(dcvr_id[18]),
        .O(\reg[marker][6]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'hBAAABEAEBAAABAAA)) 
    \reg[marker][6]_i_23 
       (.I0(\reg[marker][6]_i_42_n_0 ),
        .I1(\reg_reg[id_bit_number_n_0_][3] ),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg[marker][6]_i_43_n_0 ),
        .I4(\reg[id][64]_i_3_n_0 ),
        .I5(dcvr_id[23]),
        .O(\reg[marker][6]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF4F44)) 
    \reg[marker][6]_i_24 
       (.I0(\reg[marker][6]_i_30_n_0 ),
        .I1(dcvr_id[30]),
        .I2(\reg[marker][6]_i_31_n_0 ),
        .I3(dcvr_id[15]),
        .I4(\reg[marker][6]_i_44_n_0 ),
        .I5(\reg[marker][6]_i_45_n_0 ),
        .O(\reg[marker][6]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'h0C0000000000A000)) 
    \reg[marker][6]_i_25 
       (.I0(dcvr_id[27]),
        .I1(dcvr_id[26]),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][0] ),
        .I5(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[marker][6]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \reg[marker][6]_i_26 
       (.I0(\reg[last_discrepancy] [3]),
        .I1(\reg_reg[id_bit_number_n_0_][3] ),
        .I2(\reg[last_discrepancy] [0]),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg[last_discrepancy] [2]),
        .O(\reg[marker][6]_i_26_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \reg[marker][6]_i_27 
       (.I0(\reg[last_discrepancy] [4]),
        .I1(\reg_reg[id_bit_number_n_0_][4] ),
        .I2(\reg[last_discrepancy] [5]),
        .I3(\reg_reg[id_bit_number_n_0_][5] ),
        .O(\reg[marker][6]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000AC0)) 
    \reg[marker][6]_i_28 
       (.I0(dcvr_id[32]),
        .I1(dcvr_id[33]),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][3] ),
        .O(\reg[marker][6]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'hCCFFAAF0CC00AAF0)) 
    \reg[marker][6]_i_29 
       (.I0(dcvr_id[36]),
        .I1(dcvr_id[38]),
        .I2(dcvr_id[35]),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(dcvr_id[37]),
        .O(\reg[marker][6]_i_29_n_0 ));
  LUT5 #(
    .INIT(32'hAAAAAAAB)) 
    \reg[marker][6]_i_3 
       (.I0(\nxt_reg[id]1_inferred__0/i__carry_n_0 ),
        .I1(\reg[marker][6]_i_5_n_0 ),
        .I2(\reg[marker][6]_i_6_n_0 ),
        .I3(\reg[marker][6]_i_7_n_0 ),
        .I4(\reg[marker][6]_i_8_n_0 ),
        .O(\reg[marker][6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \reg[marker][6]_i_30 
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg_reg[id_bit_number_n_0_][3] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .I3(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[marker][6]_i_30_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \reg[marker][6]_i_31 
       (.I0(\reg_reg[id_bit_number_n_0_][2] ),
        .I1(\reg_reg[id_bit_number_n_0_][3] ),
        .I2(\reg_reg[id_bit_number_n_0_][0] ),
        .I3(\reg_reg[id_bit_number_n_0_][1] ),
        .O(\reg[marker][6]_i_31_n_0 ));
  LUT6 #(
    .INIT(64'h00000A000C000000)) 
    \reg[marker][6]_i_32 
       (.I0(dcvr_id[40]),
        .I1(dcvr_id[41]),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[marker][6]_i_32_n_0 ));
  LUT6 #(
    .INIT(64'h0AC0000000000000)) 
    \reg[marker][6]_i_33 
       (.I0(dcvr_id[44]),
        .I1(dcvr_id[45]),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][3] ),
        .O(\reg[marker][6]_i_33_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000AC0)) 
    \reg[marker][6]_i_34 
       (.I0(dcvr_id[0]),
        .I1(dcvr_id[1]),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][3] ),
        .O(\reg[marker][6]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hCCFFAAF0CC00AAF0)) 
    \reg[marker][6]_i_35 
       (.I0(dcvr_id[4]),
        .I1(dcvr_id[6]),
        .I2(dcvr_id[3]),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(dcvr_id[5]),
        .O(\reg[marker][6]_i_35_n_0 ));
  LUT6 #(
    .INIT(64'h00000A000C000000)) 
    \reg[marker][6]_i_36 
       (.I0(dcvr_id[8]),
        .I1(dcvr_id[9]),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[marker][6]_i_36_n_0 ));
  LUT6 #(
    .INIT(64'h0AC0000000000000)) 
    \reg[marker][6]_i_37 
       (.I0(dcvr_id[12]),
        .I1(dcvr_id[13]),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][3] ),
        .O(\reg[marker][6]_i_37_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000AC0)) 
    \reg[marker][6]_i_38 
       (.I0(dcvr_id[48]),
        .I1(dcvr_id[49]),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][3] ),
        .O(\reg[marker][6]_i_38_n_0 ));
  LUT6 #(
    .INIT(64'hCCFFAAF0CC00AAF0)) 
    \reg[marker][6]_i_39 
       (.I0(dcvr_id[52]),
        .I1(dcvr_id[54]),
        .I2(dcvr_id[51]),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(dcvr_id[53]),
        .O(\reg[marker][6]_i_39_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \reg[marker][6]_i_4 
       (.I0(\reg[marker][6]_i_9_n_0 ),
        .I1(\reg_reg[cmp_id_bit_n_0_] ),
        .I2(\reg_reg[id_bit_n_0_] ),
        .O(\reg[marker][6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00000A000C000000)) 
    \reg[marker][6]_i_40 
       (.I0(dcvr_id[56]),
        .I1(dcvr_id[57]),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[marker][6]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'h0AC0000000000000)) 
    \reg[marker][6]_i_41 
       (.I0(dcvr_id[60]),
        .I1(dcvr_id[61]),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][3] ),
        .O(\reg[marker][6]_i_41_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000AC0)) 
    \reg[marker][6]_i_42 
       (.I0(dcvr_id[16]),
        .I1(dcvr_id[17]),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][3] ),
        .O(\reg[marker][6]_i_42_n_0 ));
  LUT6 #(
    .INIT(64'hCCFFAAF0CC00AAF0)) 
    \reg[marker][6]_i_43 
       (.I0(dcvr_id[20]),
        .I1(dcvr_id[22]),
        .I2(dcvr_id[19]),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(dcvr_id[21]),
        .O(\reg[marker][6]_i_43_n_0 ));
  LUT6 #(
    .INIT(64'h00000A000C000000)) 
    \reg[marker][6]_i_44 
       (.I0(dcvr_id[24]),
        .I1(dcvr_id[25]),
        .I2(\reg_reg[id_bit_number_n_0_][2] ),
        .I3(\reg_reg[id_bit_number_n_0_][3] ),
        .I4(\reg_reg[id_bit_number_n_0_][1] ),
        .I5(\reg_reg[id_bit_number_n_0_][0] ),
        .O(\reg[marker][6]_i_44_n_0 ));
  LUT6 #(
    .INIT(64'h0AC0000000000000)) 
    \reg[marker][6]_i_45 
       (.I0(dcvr_id[28]),
        .I1(dcvr_id[29]),
        .I2(\reg_reg[id_bit_number_n_0_][1] ),
        .I3(\reg_reg[id_bit_number_n_0_][0] ),
        .I4(\reg_reg[id_bit_number_n_0_][2] ),
        .I5(\reg_reg[id_bit_number_n_0_][3] ),
        .O(\reg[marker][6]_i_45_n_0 ));
  LUT6 #(
    .INIT(64'h4444444444444440)) 
    \reg[marker][6]_i_5 
       (.I0(\reg_reg[id_bit_number_n_0_][4] ),
        .I1(\reg_reg[id_bit_number_n_0_][5] ),
        .I2(\reg[marker][6]_i_10_n_0 ),
        .I3(\reg[marker][6]_i_11_n_0 ),
        .I4(\reg[marker][6]_i_12_n_0 ),
        .I5(\reg[marker][6]_i_13_n_0 ),
        .O(\reg[marker][6]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h1111111111111110)) 
    \reg[marker][6]_i_6 
       (.I0(\reg_reg[id_bit_number_n_0_][5] ),
        .I1(\reg_reg[id_bit_number_n_0_][4] ),
        .I2(\reg[marker][6]_i_14_n_0 ),
        .I3(\reg[marker][6]_i_15_n_0 ),
        .I4(\reg[marker][6]_i_16_n_0 ),
        .I5(\reg[marker][6]_i_17_n_0 ),
        .O(\reg[marker][6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888880)) 
    \reg[marker][6]_i_7 
       (.I0(\reg_reg[id_bit_number_n_0_][5] ),
        .I1(\reg_reg[id_bit_number_n_0_][4] ),
        .I2(\reg[marker][6]_i_18_n_0 ),
        .I3(\reg[marker][6]_i_19_n_0 ),
        .I4(\reg[marker][6]_i_20_n_0 ),
        .I5(\reg[marker][6]_i_21_n_0 ),
        .O(\reg[marker][6]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h4444444444444440)) 
    \reg[marker][6]_i_8 
       (.I0(\reg_reg[id_bit_number_n_0_][5] ),
        .I1(\reg_reg[id_bit_number_n_0_][4] ),
        .I2(\reg[marker][6]_i_22_n_0 ),
        .I3(\reg[marker][6]_i_23_n_0 ),
        .I4(\reg[marker][6]_i_24_n_0 ),
        .I5(\reg[marker][6]_i_25_n_0 ),
        .O(\reg[marker][6]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h9009000000000000)) 
    \reg[marker][6]_i_9 
       (.I0(\reg_reg[id_bit_number_n_0_][1] ),
        .I1(\reg[last_discrepancy] [1]),
        .I2(\reg_reg[id_bit_number_n_0_][6] ),
        .I3(\reg[last_discrepancy] [6]),
        .I4(\reg[marker][6]_i_26_n_0 ),
        .I5(\reg[marker][6]_i_27_n_0 ),
        .O(\reg[marker][6]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hBAAAFFFFBAAA0000)) 
    \reg[search]_i_1 
       (.I0(\reg[search]_i_2_n_0 ),
        .I1(\reg[marker][6]_i_3_n_0 ),
        .I2(\FSM_sequential_reg[state][2]_i_2_n_0 ),
        .I3(\nxt_reg[id]1__4 ),
        .I4(\reg[search]_i_4_n_0 ),
        .I5(dcvr_bit_tx),
        .O(\reg[search]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAEAAAEAAAAAAAE)) 
    \reg[search]_i_2 
       (.I0(\reg[id][64]_i_6_n_0 ),
        .I1(\reg_reg[cmd_n_0_][0] ),
        .I2(ow_rst),
        .I3(\reg[state] [2]),
        .I4(if_rx_data),
        .I5(\reg[state] [1]),
        .O(\reg[search]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \reg[search]_i_3 
       (.I0(\reg_reg[id_bit_n_0_] ),
        .I1(\reg_reg[cmp_id_bit_n_0_] ),
        .O(\nxt_reg[id]1__4 ));
  LUT6 #(
    .INIT(64'hF0FFF4F0F0FAF4F0)) 
    \reg[search]_i_4 
       (.I0(\reg[state] [2]),
        .I1(\nxt_reg[bit_send]19_out ),
        .I2(\reg[search]_i_5_n_0 ),
        .I3(\reg[state] [1]),
        .I4(\reg[state] [0]),
        .I5(if_done),
        .O(\reg[search]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF20000000)) 
    \reg[search]_i_5 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [0]),
        .I2(\reg_reg[id_bit_number_n_0_][6] ),
        .I3(if_done),
        .I4(\reg[state] [2]),
        .I5(ow_rst),
        .O(\reg[search]_i_5_n_0 ));
  FDRE \reg_reg[bit_recv] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[bit_recv] ),
        .Q(dcvr_bit_recv),
        .R(1'b0));
  FDRE \reg_reg[bit_send] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[bit_send] ),
        .Q(dcvr_bit_send),
        .R(1'b0));
  FDRE \reg_reg[bus_rst] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[bus_rst] ),
        .Q(dcvr_bus_rst),
        .R(1'b0));
  FDRE \reg_reg[cmd][0] 
       (.C(axi_clk_i),
        .CE(\reg[cmd][7]_i_1_n_0 ),
        .D(\reg[cmd][0]_i_1_n_0 ),
        .Q(\reg_reg[cmd_n_0_][0] ),
        .R(1'b0));
  FDRE \reg_reg[cmd][1] 
       (.C(axi_clk_i),
        .CE(\reg[cmd][7]_i_1_n_0 ),
        .D(\reg[cmd][1]_i_1_n_0 ),
        .Q(in30[0]),
        .R(1'b0));
  FDRE \reg_reg[cmd][2] 
       (.C(axi_clk_i),
        .CE(\reg[cmd][7]_i_1_n_0 ),
        .D(\reg[cmd][2]_i_1_n_0 ),
        .Q(in30[1]),
        .R(1'b0));
  FDRE \reg_reg[cmd][3] 
       (.C(axi_clk_i),
        .CE(\reg[cmd][7]_i_1_n_0 ),
        .D(\reg[cmd][3]_i_1_n_0 ),
        .Q(in30[2]),
        .R(1'b0));
  FDRE \reg_reg[cmd][4] 
       (.C(axi_clk_i),
        .CE(\reg[cmd][7]_i_1_n_0 ),
        .D(\reg[cmd][4]_i_1_n_0 ),
        .Q(in30[3]),
        .R(1'b0));
  FDRE \reg_reg[cmd][5] 
       (.C(axi_clk_i),
        .CE(\reg[cmd][7]_i_1_n_0 ),
        .D(\reg[cmd][5]_i_1_n_0 ),
        .Q(in30[4]),
        .R(1'b0));
  FDRE \reg_reg[cmd][6] 
       (.C(axi_clk_i),
        .CE(\reg[cmd][7]_i_1_n_0 ),
        .D(\reg[cmd][6]_i_1_n_0 ),
        .Q(in30[5]),
        .R(1'b0));
  FDRE \reg_reg[cmd][7] 
       (.C(axi_clk_i),
        .CE(\reg[cmd][7]_i_1_n_0 ),
        .D(\reg[cmd][7]_i_2_n_0 ),
        .Q(in30[6]),
        .R(1'b0));
  FDRE \reg_reg[cmp_id_bit] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[cmp_id_bit]_i_1_n_0 ),
        .Q(\reg_reg[cmp_id_bit_n_0_] ),
        .R(1'b0));
  FDRE \reg_reg[crc_reset] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[crc_reset] ),
        .Q(\reg_reg[crc_reset_n_0_] ),
        .R(1'b0));
  FDRE \reg_reg[done] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[done] ),
        .Q(dcvr_done),
        .R(1'b0));
  FDRE \reg_reg[id][10] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_54),
        .Q(dcvr_id[9]),
        .R(1'b0));
  FDRE \reg_reg[id][11] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_53),
        .Q(dcvr_id[10]),
        .R(1'b0));
  FDRE \reg_reg[id][12] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_52),
        .Q(dcvr_id[11]),
        .R(1'b0));
  FDRE \reg_reg[id][13] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_51),
        .Q(dcvr_id[12]),
        .R(1'b0));
  FDRE \reg_reg[id][14] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_50),
        .Q(dcvr_id[13]),
        .R(1'b0));
  FDRE \reg_reg[id][15] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_49),
        .Q(dcvr_id[14]),
        .R(1'b0));
  FDRE \reg_reg[id][16] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_48),
        .Q(dcvr_id[15]),
        .R(1'b0));
  FDRE \reg_reg[id][17] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_47),
        .Q(dcvr_id[16]),
        .R(1'b0));
  FDRE \reg_reg[id][18] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_46),
        .Q(dcvr_id[17]),
        .R(1'b0));
  FDRE \reg_reg[id][19] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_45),
        .Q(dcvr_id[18]),
        .R(1'b0));
  FDRE \reg_reg[id][1] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_63),
        .Q(dcvr_id[0]),
        .R(1'b0));
  FDRE \reg_reg[id][20] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_44),
        .Q(dcvr_id[19]),
        .R(1'b0));
  FDRE \reg_reg[id][21] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_43),
        .Q(dcvr_id[20]),
        .R(1'b0));
  FDRE \reg_reg[id][22] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_42),
        .Q(dcvr_id[21]),
        .R(1'b0));
  FDRE \reg_reg[id][23] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_41),
        .Q(dcvr_id[22]),
        .R(1'b0));
  FDRE \reg_reg[id][24] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_40),
        .Q(dcvr_id[23]),
        .R(1'b0));
  FDRE \reg_reg[id][25] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_39),
        .Q(dcvr_id[24]),
        .R(1'b0));
  FDRE \reg_reg[id][26] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_38),
        .Q(dcvr_id[25]),
        .R(1'b0));
  FDRE \reg_reg[id][27] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_37),
        .Q(dcvr_id[26]),
        .R(1'b0));
  FDRE \reg_reg[id][28] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_36),
        .Q(dcvr_id[27]),
        .R(1'b0));
  FDRE \reg_reg[id][29] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_35),
        .Q(dcvr_id[28]),
        .R(1'b0));
  FDRE \reg_reg[id][2] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_62),
        .Q(dcvr_id[1]),
        .R(1'b0));
  FDRE \reg_reg[id][30] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_34),
        .Q(dcvr_id[29]),
        .R(1'b0));
  FDRE \reg_reg[id][31] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_33),
        .Q(dcvr_id[30]),
        .R(1'b0));
  FDRE \reg_reg[id][32] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_32),
        .Q(dcvr_id[31]),
        .R(1'b0));
  FDRE \reg_reg[id][33] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_31),
        .Q(dcvr_id[32]),
        .R(1'b0));
  FDRE \reg_reg[id][34] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_30),
        .Q(dcvr_id[33]),
        .R(1'b0));
  FDRE \reg_reg[id][35] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_29),
        .Q(dcvr_id[34]),
        .R(1'b0));
  FDRE \reg_reg[id][36] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_28),
        .Q(dcvr_id[35]),
        .R(1'b0));
  FDRE \reg_reg[id][37] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_27),
        .Q(dcvr_id[36]),
        .R(1'b0));
  FDRE \reg_reg[id][38] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_26),
        .Q(dcvr_id[37]),
        .R(1'b0));
  FDRE \reg_reg[id][39] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_25),
        .Q(dcvr_id[38]),
        .R(1'b0));
  FDRE \reg_reg[id][3] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_61),
        .Q(dcvr_id[2]),
        .R(1'b0));
  FDRE \reg_reg[id][40] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_24),
        .Q(dcvr_id[39]),
        .R(1'b0));
  FDRE \reg_reg[id][41] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_23),
        .Q(dcvr_id[40]),
        .R(1'b0));
  FDRE \reg_reg[id][42] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_22),
        .Q(dcvr_id[41]),
        .R(1'b0));
  FDRE \reg_reg[id][43] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_21),
        .Q(dcvr_id[42]),
        .R(1'b0));
  FDRE \reg_reg[id][44] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_20),
        .Q(dcvr_id[43]),
        .R(1'b0));
  FDRE \reg_reg[id][45] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_19),
        .Q(dcvr_id[44]),
        .R(1'b0));
  FDRE \reg_reg[id][46] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_18),
        .Q(dcvr_id[45]),
        .R(1'b0));
  FDRE \reg_reg[id][47] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_17),
        .Q(dcvr_id[46]),
        .R(1'b0));
  FDRE \reg_reg[id][48] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_16),
        .Q(dcvr_id[47]),
        .R(1'b0));
  FDRE \reg_reg[id][49] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_15),
        .Q(dcvr_id[48]),
        .R(1'b0));
  FDRE \reg_reg[id][4] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_60),
        .Q(dcvr_id[3]),
        .R(1'b0));
  FDRE \reg_reg[id][50] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_14),
        .Q(dcvr_id[49]),
        .R(1'b0));
  FDRE \reg_reg[id][51] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_13),
        .Q(dcvr_id[50]),
        .R(1'b0));
  FDRE \reg_reg[id][52] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_12),
        .Q(dcvr_id[51]),
        .R(1'b0));
  FDRE \reg_reg[id][53] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_11),
        .Q(dcvr_id[52]),
        .R(1'b0));
  FDRE \reg_reg[id][54] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_10),
        .Q(dcvr_id[53]),
        .R(1'b0));
  FDRE \reg_reg[id][55] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_9),
        .Q(dcvr_id[54]),
        .R(1'b0));
  FDRE \reg_reg[id][56] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_8),
        .Q(dcvr_id[55]),
        .R(1'b0));
  FDRE \reg_reg[id][57] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_7),
        .Q(dcvr_id[56]),
        .R(1'b0));
  FDRE \reg_reg[id][58] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_6),
        .Q(dcvr_id[57]),
        .R(1'b0));
  FDRE \reg_reg[id][59] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_5),
        .Q(dcvr_id[58]),
        .R(1'b0));
  FDRE \reg_reg[id][5] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_59),
        .Q(dcvr_id[4]),
        .R(1'b0));
  FDRE \reg_reg[id][60] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_4),
        .Q(dcvr_id[59]),
        .R(1'b0));
  FDRE \reg_reg[id][61] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_3),
        .Q(dcvr_id[60]),
        .R(1'b0));
  FDRE \reg_reg[id][62] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_2),
        .Q(dcvr_id[61]),
        .R(1'b0));
  FDRE \reg_reg[id][63] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_1),
        .Q(dcvr_id[62]),
        .R(1'b0));
  FDRE \reg_reg[id][64] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_0),
        .Q(dcvr_id[63]),
        .R(1'b0));
  FDRE \reg_reg[id][6] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_58),
        .Q(dcvr_id[5]),
        .R(1'b0));
  FDRE \reg_reg[id][7] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_57),
        .Q(dcvr_id[6]),
        .R(1'b0));
  FDRE \reg_reg[id][8] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_56),
        .Q(dcvr_id[7]),
        .R(1'b0));
  FDRE \reg_reg[id][9] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(crc_inst_n_55),
        .Q(dcvr_id[8]),
        .R(1'b0));
  FDRE \reg_reg[id_bit] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[id_bit]_i_1_n_0 ),
        .Q(\reg_reg[id_bit_n_0_] ),
        .R(1'b0));
  FDRE \reg_reg[id_bit_number][0] 
       (.C(axi_clk_i),
        .CE(\reg[id_bit_number][6]_i_1_n_0 ),
        .D(\reg[id_bit_number][0]_i_1_n_0 ),
        .Q(\reg_reg[id_bit_number_n_0_][0] ),
        .R(1'b0));
  FDRE \reg_reg[id_bit_number][1] 
       (.C(axi_clk_i),
        .CE(\reg[id_bit_number][6]_i_1_n_0 ),
        .D(\reg[id_bit_number][1]_i_1_n_0 ),
        .Q(\reg_reg[id_bit_number_n_0_][1] ),
        .R(1'b0));
  FDRE \reg_reg[id_bit_number][2] 
       (.C(axi_clk_i),
        .CE(\reg[id_bit_number][6]_i_1_n_0 ),
        .D(\reg[id_bit_number][2]_i_1_n_0 ),
        .Q(\reg_reg[id_bit_number_n_0_][2] ),
        .R(1'b0));
  FDRE \reg_reg[id_bit_number][3] 
       (.C(axi_clk_i),
        .CE(\reg[id_bit_number][6]_i_1_n_0 ),
        .D(\reg[id_bit_number][3]_i_1_n_0 ),
        .Q(\reg_reg[id_bit_number_n_0_][3] ),
        .R(1'b0));
  FDRE \reg_reg[id_bit_number][4] 
       (.C(axi_clk_i),
        .CE(\reg[id_bit_number][6]_i_1_n_0 ),
        .D(\reg[id_bit_number][4]_i_1_n_0 ),
        .Q(\reg_reg[id_bit_number_n_0_][4] ),
        .R(1'b0));
  FDRE \reg_reg[id_bit_number][5] 
       (.C(axi_clk_i),
        .CE(\reg[id_bit_number][6]_i_1_n_0 ),
        .D(\reg[id_bit_number][5]_i_1_n_0 ),
        .Q(\reg_reg[id_bit_number_n_0_][5] ),
        .R(1'b0));
  FDRE \reg_reg[id_bit_number][6] 
       (.C(axi_clk_i),
        .CE(\reg[id_bit_number][6]_i_1_n_0 ),
        .D(\reg[id_bit_number][6]_i_2_n_0 ),
        .Q(\reg_reg[id_bit_number_n_0_][6] ),
        .R(1'b0));
  FDRE \reg_reg[id_en] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[id_en] ),
        .Q(dcvr_id_en),
        .R(1'b0));
  FDRE \reg_reg[last_discrepancy][0] 
       (.C(axi_clk_i),
        .CE(\reg[last_discrepancy][6]_i_1_n_0 ),
        .D(\reg[last_discrepancy][0]_i_1_n_0 ),
        .Q(\reg[last_discrepancy] [0]),
        .R(1'b0));
  FDRE \reg_reg[last_discrepancy][1] 
       (.C(axi_clk_i),
        .CE(\reg[last_discrepancy][6]_i_1_n_0 ),
        .D(\reg[last_discrepancy][1]_i_1_n_0 ),
        .Q(\reg[last_discrepancy] [1]),
        .R(1'b0));
  FDRE \reg_reg[last_discrepancy][2] 
       (.C(axi_clk_i),
        .CE(\reg[last_discrepancy][6]_i_1_n_0 ),
        .D(\reg[last_discrepancy][2]_i_1_n_0 ),
        .Q(\reg[last_discrepancy] [2]),
        .R(1'b0));
  FDRE \reg_reg[last_discrepancy][3] 
       (.C(axi_clk_i),
        .CE(\reg[last_discrepancy][6]_i_1_n_0 ),
        .D(\reg[last_discrepancy][3]_i_1_n_0 ),
        .Q(\reg[last_discrepancy] [3]),
        .R(1'b0));
  FDRE \reg_reg[last_discrepancy][4] 
       (.C(axi_clk_i),
        .CE(\reg[last_discrepancy][6]_i_1_n_0 ),
        .D(\reg[last_discrepancy][4]_i_1_n_0 ),
        .Q(\reg[last_discrepancy] [4]),
        .R(1'b0));
  FDRE \reg_reg[last_discrepancy][5] 
       (.C(axi_clk_i),
        .CE(\reg[last_discrepancy][6]_i_1_n_0 ),
        .D(\reg[last_discrepancy][5]_i_1_n_0 ),
        .Q(\reg[last_discrepancy] [5]),
        .R(1'b0));
  FDRE \reg_reg[last_discrepancy][6] 
       (.C(axi_clk_i),
        .CE(\reg[last_discrepancy][6]_i_1_n_0 ),
        .D(\reg[last_discrepancy][6]_i_2_n_0 ),
        .Q(\reg[last_discrepancy] [6]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][0] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][3]_i_1_n_0 ),
        .D(\reg[lfsr][0]_i_1__0_n_0 ),
        .Q(lfsr[0]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][1] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][3]_i_1_n_0 ),
        .D(\reg[lfsr][1]_i_1_n_0 ),
        .Q(lfsr[1]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][2] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][3]_i_1_n_0 ),
        .D(\reg[lfsr][2]_i_1_n_0 ),
        .Q(lfsr[2]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][3] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][3]_i_1_n_0 ),
        .D(\reg[lfsr][3]_i_2_n_0 ),
        .Q(lfsr[3]),
        .R(1'b0));
  FDRE \reg_reg[marker][0] 
       (.C(axi_clk_i),
        .CE(\reg[marker][6]_i_1_n_0 ),
        .D(\reg[marker][0]_i_1_n_0 ),
        .Q(\reg_reg[marker_n_0_][0] ),
        .R(1'b0));
  FDRE \reg_reg[marker][1] 
       (.C(axi_clk_i),
        .CE(\reg[marker][6]_i_1_n_0 ),
        .D(\reg[marker][1]_i_1_n_0 ),
        .Q(\reg_reg[marker_n_0_][1] ),
        .R(1'b0));
  FDRE \reg_reg[marker][2] 
       (.C(axi_clk_i),
        .CE(\reg[marker][6]_i_1_n_0 ),
        .D(\reg[marker][2]_i_1_n_0 ),
        .Q(\reg_reg[marker_n_0_][2] ),
        .R(1'b0));
  FDRE \reg_reg[marker][3] 
       (.C(axi_clk_i),
        .CE(\reg[marker][6]_i_1_n_0 ),
        .D(\reg[marker][3]_i_1_n_0 ),
        .Q(\reg_reg[marker_n_0_][3] ),
        .R(1'b0));
  FDRE \reg_reg[marker][4] 
       (.C(axi_clk_i),
        .CE(\reg[marker][6]_i_1_n_0 ),
        .D(\reg[marker][4]_i_1_n_0 ),
        .Q(\reg_reg[marker_n_0_][4] ),
        .R(1'b0));
  FDRE \reg_reg[marker][5] 
       (.C(axi_clk_i),
        .CE(\reg[marker][6]_i_1_n_0 ),
        .D(\reg[marker][5]_i_1_n_0 ),
        .Q(\reg_reg[marker_n_0_][5] ),
        .R(1'b0));
  FDRE \reg_reg[marker][6] 
       (.C(axi_clk_i),
        .CE(\reg[marker][6]_i_1_n_0 ),
        .D(\reg[marker][6]_i_2_n_0 ),
        .Q(\reg_reg[marker_n_0_][6] ),
        .R(1'b0));
  FDRE \reg_reg[search] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[search]_i_1_n_0 ),
        .Q(dcvr_bit_tx),
        .R(1'b0));
endmodule

module design_1_axi_onewire_0_0_onewire_idtemp
   (ADDRD,
    ow_o,
    \reg_reg[busy] ,
    \reg_reg[too_many] ,
    ctrl_rd_en,
    rd_data_en_reg,
    ow_rd_data,
    \reg_reg[device_count][0] ,
    \reg_reg[device_count][1] ,
    \reg_reg[device_count][2] ,
    \reg_reg[device_count][3] ,
    \reg_reg[device_count][4] ,
    ow_dev_count,
    ow_pullup_o,
    \FSM_sequential_ow_state_reg[0] ,
    \FSM_sequential_ow_state_reg[0]_0 ,
    ow_done_o_reg,
    \FSM_sequential_ow_state_reg[1] ,
    \reg_reg[too_many]_0 ,
    \reg_reg[done] ,
    axi_clk_i,
    ADDRA,
    rd_data_en,
    axi_aresetn_i,
    ow_reg_rst,
    ow_rst,
    ow_i,
    ow_temp_trig_reg,
    ow_disc_trig_reg,
    ow_state__0,
    ow_trig,
    ow_done_o,
    ow_busy_o_reg,
    ow_busy_o,
    \rd_data_reg[63] );
  output [5:0]ADDRD;
  output ow_o;
  output \reg_reg[busy] ;
  output \reg_reg[too_many] ;
  output ctrl_rd_en;
  output rd_data_en_reg;
  output [63:0]ow_rd_data;
  output \reg_reg[device_count][0] ;
  output \reg_reg[device_count][1] ;
  output \reg_reg[device_count][2] ;
  output \reg_reg[device_count][3] ;
  output \reg_reg[device_count][4] ;
  output [0:0]ow_dev_count;
  output ow_pullup_o;
  output \FSM_sequential_ow_state_reg[0] ;
  output \FSM_sequential_ow_state_reg[0]_0 ;
  output ow_done_o_reg;
  output \FSM_sequential_ow_state_reg[1] ;
  output \reg_reg[too_many]_0 ;
  output \reg_reg[done] ;
  input axi_clk_i;
  input [5:0]ADDRA;
  input rd_data_en;
  input axi_aresetn_i;
  input ow_reg_rst;
  input ow_rst;
  input ow_i;
  input ow_temp_trig_reg;
  input ow_disc_trig_reg;
  input [1:0]ow_state__0;
  input ow_trig;
  input ow_done_o;
  input ow_busy_o_reg;
  input ow_busy_o;
  input \rd_data_reg[63] ;

  wire [5:0]ADDRA;
  wire [5:0]ADDRD;
  wire \FSM_sequential_ow_state_reg[0] ;
  wire \FSM_sequential_ow_state_reg[0]_0 ;
  wire \FSM_sequential_ow_state_reg[1] ;
  wire axi_aresetn_i;
  wire axi_clk_i;
  wire ctrl_bit_send;
  wire ctrl_bit_tx;
  wire ctrl_bus_rst;
  wire ctrl_inst_n_84;
  wire ctrl_inst_n_85;
  wire ctrl_rd_en;
  wire [63:1]ctrl_wr_data;
  wire dcvr_bit_recv;
  wire dcvr_bit_send;
  wire dcvr_bit_tx;
  wire dcvr_bus_rst;
  wire dcvr_done;
  wire dcvr_id_en;
  wire dcvr_start;
  wire discover_inst_n_6;
  wire discover_inst_n_7;
  wire if_done;
  wire if_rx_data;
  wire if_rx_data_en;
  wire interface_inst_n_4;
  wire interface_inst_n_5;
  wire mem_wr_done;
  wire ow_busy_o;
  wire ow_busy_o_reg;
  wire [0:0]ow_dev_count;
  wire ow_disc_trig_reg;
  wire ow_done_o;
  wire ow_done_o_reg;
  wire ow_i;
  wire ow_o;
  wire ow_pullup_o;
  wire [63:0]ow_rd_data;
  wire ow_reg_rst;
  wire ow_rst;
  wire [1:0]ow_state__0;
  wire ow_temp_trig_reg;
  wire ow_trig;
  wire \ram_gen.ram_inst_n_2 ;
  wire \ram_gen.ram_inst_n_67 ;
  wire rd_data_en;
  wire rd_data_en_reg;
  wire \rd_data_reg[63] ;
  wire [2:0]\reg[state] ;
  wire \reg_reg[busy] ;
  wire \reg_reg[device_count][0] ;
  wire \reg_reg[device_count][1] ;
  wire \reg_reg[device_count][2] ;
  wire \reg_reg[device_count][3] ;
  wire \reg_reg[device_count][4] ;
  wire \reg_reg[done] ;
  wire \reg_reg[too_many] ;
  wire \reg_reg[too_many]_0 ;
  wire [63:0]wr_data_i;
  wire wr_done;

  design_1_axi_onewire_0_0_onewire_control ctrl_inst
       (.\FSM_sequential_ow_state_reg[0] (\FSM_sequential_ow_state_reg[0] ),
        .\FSM_sequential_ow_state_reg[0]_0 (\FSM_sequential_ow_state_reg[0]_0 ),
        .\FSM_sequential_ow_state_reg[1] (\FSM_sequential_ow_state_reg[1] ),
        .\FSM_sequential_reg[state][3]_i_6_0 (rd_data_en_reg),
        .\FSM_sequential_reg_reg[state][2]_0 ({\reg[state] [2],\reg[state] [0]}),
        .\FSM_sequential_reg_reg[state][2]_1 (\ram_gen.ram_inst_n_2 ),
        .axi_aresetn_i(axi_aresetn_i),
        .axi_clk_i(axi_clk_i),
        .ctrl_bit_send(ctrl_bit_send),
        .ctrl_bit_tx(ctrl_bit_tx),
        .ctrl_bus_rst(ctrl_bus_rst),
        .ctrl_rd_en(ctrl_rd_en),
        .ctrl_wr_data(ctrl_wr_data),
        .dcvr_bit_recv(dcvr_bit_recv),
        .dcvr_bit_send(dcvr_bit_send),
        .dcvr_done(dcvr_done),
        .dcvr_id_en(dcvr_id_en),
        .dcvr_start(dcvr_start),
        .if_done(if_done),
        .if_rx_data(if_rx_data),
        .if_rx_data_en(if_rx_data_en),
        .mem_wr_done(mem_wr_done),
        .ow_busy_o(ow_busy_o),
        .ow_busy_o_reg(ow_busy_o_reg),
        .ow_dev_count(ow_dev_count),
        .ow_disc_trig_reg(ow_disc_trig_reg),
        .ow_done_o(ow_done_o),
        .ow_done_o_reg(ow_done_o_reg),
        .ow_pullup_o(ow_pullup_o),
        .ow_rd_data(ow_rd_data),
        .ow_reg_rst(ow_reg_rst),
        .ow_rst(ow_rst),
        .ow_state__0(ow_state__0),
        .ow_temp_trig_reg(ow_temp_trig_reg),
        .ow_trig(ow_trig),
        .\reg_reg[bit_recv]_0 (ctrl_inst_n_84),
        .\reg_reg[bit_recv]_1 (ctrl_inst_n_85),
        .\reg_reg[bit_send]_0 (interface_inst_n_4),
        .\reg_reg[busy]_0 (\reg_reg[busy] ),
        .\reg_reg[data][30]_0 (interface_inst_n_5),
        .\reg_reg[device_count][0]_0 (\reg_reg[device_count][0] ),
        .\reg_reg[device_count][1]_0 (\reg_reg[device_count][1] ),
        .\reg_reg[device_count][2]_0 (\reg_reg[device_count][2] ),
        .\reg_reg[device_count][3]_0 (\reg_reg[device_count][3] ),
        .\reg_reg[device_count][4]_0 (\reg_reg[device_count][4] ),
        .\reg_reg[done]_0 (\reg_reg[done] ),
        .\reg_reg[mem_addr][0]_0 (ADDRD[0]),
        .\reg_reg[mem_addr][0]_1 (\ram_gen.ram_inst_n_67 ),
        .\reg_reg[mem_addr][1]_0 (ADDRD[1]),
        .\reg_reg[mem_addr][2]_0 (ADDRD[2]),
        .\reg_reg[mem_addr][3]_0 (ADDRD[3]),
        .\reg_reg[mem_addr][4]_0 (ADDRD[4]),
        .\reg_reg[mem_addr][5]_0 (ADDRD[5]),
        .\reg_reg[too_many]_0 (\reg_reg[too_many] ),
        .\reg_reg[too_many]_1 (\reg_reg[too_many]_0 ),
        .wr_done(wr_done));
  design_1_axi_onewire_0_0_onewire_discover discover_inst
       (.axi_aresetn_i(axi_aresetn_i),
        .axi_clk_i(axi_clk_i),
        .ctrl_bit_send(ctrl_bit_send),
        .ctrl_bit_tx(ctrl_bit_tx),
        .ctrl_bus_rst(ctrl_bus_rst),
        .ctrl_wr_data(ctrl_wr_data),
        .dcvr_bit_recv(dcvr_bit_recv),
        .dcvr_bit_send(dcvr_bit_send),
        .dcvr_bit_tx(dcvr_bit_tx),
        .dcvr_bus_rst(dcvr_bus_rst),
        .dcvr_done(dcvr_done),
        .dcvr_id_en(dcvr_id_en),
        .dcvr_start(dcvr_start),
        .if_done(if_done),
        .if_rx_data(if_rx_data),
        .if_rx_data_en(if_rx_data_en),
        .ow_reg_rst(ow_reg_rst),
        .ow_rst(ow_rst),
        .\reg_reg[bit_send]_0 (discover_inst_n_7),
        .\reg_reg[bus_rst]_0 (discover_inst_n_6),
        .wr_data_i(wr_data_i));
  design_1_axi_onewire_0_0_onewire_interface interface_inst
       (.\FSM_sequential_reg_reg[state][0]_0 (ctrl_inst_n_84),
        .\FSM_sequential_reg_reg[state][1]_0 (ctrl_inst_n_85),
        .axi_aresetn_i(axi_aresetn_i),
        .axi_clk_i(axi_clk_i),
        .ctrl_bit_tx(ctrl_bit_tx),
        .ctrl_bus_rst(ctrl_bus_rst),
        .dcvr_bit_tx(dcvr_bit_tx),
        .dcvr_bus_rst(dcvr_bus_rst),
        .if_done(if_done),
        .if_rx_data(if_rx_data),
        .if_rx_data_en(if_rx_data_en),
        .ow_i(ow_i),
        .ow_o(ow_o),
        .ow_reg_rst(ow_reg_rst),
        .ow_rst(ow_rst),
        .\reg_reg[bit_send] ({\reg[state] [2],\reg[state] [0]}),
        .\reg_reg[data]_0 (discover_inst_n_6),
        .\reg_reg[data]_1 (discover_inst_n_7),
        .\reg_reg[done]_0 (interface_inst_n_4),
        .\reg_reg[done]_1 (interface_inst_n_5));
  design_1_axi_onewire_0_0_two_port_ram \ram_gen.ram_inst 
       (.ADDRA(ADDRA),
        .ADDRD(ADDRD),
        .axi_aresetn_i(axi_aresetn_i),
        .axi_clk_i(axi_clk_i),
        .mem_wr_done(mem_wr_done),
        .ow_rd_data(ow_rd_data),
        .ow_reg_rst(ow_reg_rst),
        .rd_data_en(rd_data_en),
        .rd_data_en_reg_0(rd_data_en_reg),
        .rd_data_en_reg_1(\ram_gen.ram_inst_n_67 ),
        .\rd_data_reg[3]_0 (\ram_gen.ram_inst_n_2 ),
        .\rd_data_reg[63]_0 (\rd_data_reg[63] ),
        .\reg[mem_addr][5]_i_4 (\reg_reg[busy] ),
        .wr_data_i(wr_data_i),
        .wr_done(wr_done));
endmodule

module design_1_axi_onewire_0_0_onewire_interface
   (if_rx_data,
    if_rx_data_en,
    if_done,
    ow_o,
    \reg_reg[done]_0 ,
    \reg_reg[done]_1 ,
    axi_clk_i,
    ctrl_bit_tx,
    dcvr_bit_tx,
    ow_rst,
    ow_i,
    axi_aresetn_i,
    ow_reg_rst,
    \FSM_sequential_reg_reg[state][1]_0 ,
    ctrl_bus_rst,
    dcvr_bus_rst,
    \reg_reg[data]_0 ,
    \reg_reg[data]_1 ,
    \reg_reg[bit_send] ,
    \FSM_sequential_reg_reg[state][0]_0 );
  output if_rx_data;
  output if_rx_data_en;
  output if_done;
  output ow_o;
  output \reg_reg[done]_0 ;
  output \reg_reg[done]_1 ;
  input axi_clk_i;
  input ctrl_bit_tx;
  input dcvr_bit_tx;
  input ow_rst;
  input ow_i;
  input axi_aresetn_i;
  input ow_reg_rst;
  input \FSM_sequential_reg_reg[state][1]_0 ;
  input ctrl_bus_rst;
  input dcvr_bus_rst;
  input \reg_reg[data]_0 ;
  input \reg_reg[data]_1 ;
  input [1:0]\reg_reg[bit_send] ;
  input \FSM_sequential_reg_reg[state][0]_0 ;

  wire \FSM_sequential_reg[state][0]_i_1_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_1_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_3_n_0 ;
  wire \FSM_sequential_reg[state][1]_i_4_n_0 ;
  wire \FSM_sequential_reg_reg[state][0]_0 ;
  wire \FSM_sequential_reg_reg[state][1]_0 ;
  wire axi_aresetn_i;
  wire axi_clk_i;
  wire ctrl_bit_tx;
  wire ctrl_bus_rst;
  wire dcvr_bit_tx;
  wire dcvr_bus_rst;
  wire if_done;
  wire if_rx_data;
  wire if_rx_data_en;
  wire [16:1]lfsr_shift;
  wire \nxt_reg[tx]__0 ;
  wire ow_i;
  wire ow_o;
  wire ow_reg_rst;
  wire ow_rst;
  wire [16:0]p_1_in;
  wire \reg[data] ;
  wire \reg[data]_i_11_n_0 ;
  wire \reg[data]_i_12_n_0 ;
  wire \reg[data]_i_13_n_0 ;
  wire \reg[data]_i_14_n_0 ;
  wire \reg[data]_i_15_n_0 ;
  wire \reg[data]_i_1_n_0 ;
  wire \reg[data]_i_3_n_0 ;
  wire \reg[data]_i_4_n_0 ;
  wire \reg[data]_i_5_n_0 ;
  wire \reg[data]_i_6_n_0 ;
  wire \reg[data]_i_7_n_0 ;
  wire \reg[data]_i_8_n_0 ;
  wire \reg[data_en] ;
  wire \reg[done] ;
  wire \reg[done]_i_2_n_0 ;
  wire \reg[lfsr][0]_i_3_n_0 ;
  wire \reg[lfsr][0]_i_4_n_0 ;
  wire \reg[lfsr][0]_i_5__0_n_0 ;
  wire \reg[lfsr][0]_i_6__0_n_0 ;
  wire \reg[lfsr][0]_i_7_n_0 ;
  wire \reg[lfsr][0]_i_8_n_0 ;
  wire \reg[lfsr][16]_i_1_n_0 ;
  wire \reg[lfsr][16]_i_3_n_0 ;
  wire \reg[lfsr][16]_i_4_n_0 ;
  wire \reg[lfsr][16]_i_5_n_0 ;
  wire \reg[lfsr][16]_i_6_n_0 ;
  wire \reg[lfsr][16]_i_7_n_0 ;
  wire [1:0]\reg[state] ;
  wire \reg[tx]_i_10_n_0 ;
  wire \reg[tx]_i_11_n_0 ;
  wire \reg[tx]_i_12_n_0 ;
  wire \reg[tx]_i_13_n_0 ;
  wire \reg[tx]_i_14_n_0 ;
  wire \reg[tx]_i_1_n_0 ;
  wire \reg[tx]_i_2_n_0 ;
  wire \reg[tx]_i_3_n_0 ;
  wire \reg[tx]_i_4_n_0 ;
  wire \reg[tx]_i_5_n_0 ;
  wire \reg[tx]_i_7_n_0 ;
  wire \reg[tx]_i_8_n_0 ;
  wire \reg[tx]_i_9_n_0 ;
  wire [1:0]\reg_reg[bit_send] ;
  wire \reg_reg[data]_0 ;
  wire \reg_reg[data]_1 ;
  wire \reg_reg[done]_0 ;
  wire \reg_reg[done]_1 ;
  wire \reg_reg[lfsr_n_0_][16] ;

  LUT6 #(
    .INIT(64'h3332FFFF33320000)) 
    \FSM_sequential_reg[state][0]_i_1 
       (.I0(\FSM_sequential_reg_reg[state][0]_0 ),
        .I1(\reg[lfsr][16]_i_1_n_0 ),
        .I2(dcvr_bus_rst),
        .I3(ctrl_bus_rst),
        .I4(\FSM_sequential_reg[state][1]_i_3_n_0 ),
        .I5(\reg[state] [0]),
        .O(\FSM_sequential_reg[state][0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0100FFFF01000000)) 
    \FSM_sequential_reg[state][1]_i_1 
       (.I0(dcvr_bus_rst),
        .I1(ctrl_bus_rst),
        .I2(\reg[lfsr][16]_i_1_n_0 ),
        .I3(\FSM_sequential_reg_reg[state][1]_0 ),
        .I4(\FSM_sequential_reg[state][1]_i_3_n_0 ),
        .I5(\reg[state] [1]),
        .O(\FSM_sequential_reg[state][1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT5 #(
    .INIT(32'hFFFFAEAA)) 
    \FSM_sequential_reg[state][1]_i_3 
       (.I0(\FSM_sequential_reg[state][1]_i_4_n_0 ),
        .I1(\reg[done]_i_2_n_0 ),
        .I2(\reg[state] [1]),
        .I3(\reg[state] [0]),
        .I4(\reg[data]_i_3_n_0 ),
        .O(\FSM_sequential_reg[state][1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF11111110)) 
    \FSM_sequential_reg[state][1]_i_4 
       (.I0(\reg[state] [0]),
        .I1(\reg[state] [1]),
        .I2(\FSM_sequential_reg_reg[state][1]_0 ),
        .I3(ctrl_bus_rst),
        .I4(dcvr_bus_rst),
        .I5(ow_rst),
        .O(\FSM_sequential_reg[state][1]_i_4_n_0 ));
  (* FSM_ENCODED_STATES = "reset:01,send:10,idle:00,receive:11" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_reg_reg[state][0] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\FSM_sequential_reg[state][0]_i_1_n_0 ),
        .Q(\reg[state] [0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "reset:01,send:10,idle:00,receive:11" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_reg_reg[state][1] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\FSM_sequential_reg[state][1]_i_1_n_0 ),
        .Q(\reg[state] [1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \reg[bit_send]_i_3__0 
       (.I0(if_done),
        .I1(\reg_reg[bit_send] [1]),
        .O(\reg_reg[done]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \reg[data][63]_i_10 
       (.I0(if_done),
        .I1(\reg_reg[bit_send] [0]),
        .O(\reg_reg[done]_1 ));
  LUT6 #(
    .INIT(64'hAAAAAAFBAAAAAA08)) 
    \reg[data]_i_1 
       (.I0(\reg[data] ),
        .I1(\reg[data]_i_3_n_0 ),
        .I2(\reg[state] [0]),
        .I3(\reg[data]_i_4_n_0 ),
        .I4(\reg[data]_i_5_n_0 ),
        .I5(if_rx_data),
        .O(\reg[data]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0444000000000000)) 
    \reg[data]_i_11 
       (.I0(\reg[lfsr][16]_i_7_n_0 ),
        .I1(\reg[data]_i_12_n_0 ),
        .I2(lfsr_shift[10]),
        .I3(lfsr_shift[11]),
        .I4(lfsr_shift[14]),
        .I5(\reg[data]_i_13_n_0 ),
        .O(\reg[data]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0040000000000000)) 
    \reg[data]_i_12 
       (.I0(lfsr_shift[1]),
        .I1(lfsr_shift[8]),
        .I2(\reg[data]_i_14_n_0 ),
        .I3(lfsr_shift[13]),
        .I4(lfsr_shift[16]),
        .I5(\reg[data]_i_15_n_0 ),
        .O(\reg[data]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000D5000000D5D5)) 
    \reg[data]_i_13 
       (.I0(lfsr_shift[6]),
        .I1(lfsr_shift[4]),
        .I2(lfsr_shift[3]),
        .I3(lfsr_shift[1]),
        .I4(lfsr_shift[2]),
        .I5(lfsr_shift[15]),
        .O(\reg[data]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \reg[data]_i_14 
       (.I0(\reg[state] [0]),
        .I1(\reg[state] [1]),
        .O(\reg[data]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001500)) 
    \reg[data]_i_15 
       (.I0(lfsr_shift[9]),
        .I1(lfsr_shift[7]),
        .I2(lfsr_shift[8]),
        .I3(lfsr_shift[4]),
        .I4(lfsr_shift[5]),
        .I5(lfsr_shift[12]),
        .O(\reg[data]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h00FF000000320032)) 
    \reg[data]_i_2 
       (.I0(ctrl_bit_tx),
        .I1(\reg[state] [1]),
        .I2(dcvr_bit_tx),
        .I3(ow_rst),
        .I4(ow_i),
        .I5(\reg[state] [0]),
        .O(\reg[data] ));
  LUT2 #(
    .INIT(4'h8)) 
    \reg[data]_i_3 
       (.I0(\reg[state] [1]),
        .I1(\reg[lfsr][0]_i_3_n_0 ),
        .O(\reg[data]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000888800000000)) 
    \reg[data]_i_4 
       (.I0(\reg[data]_i_6_n_0 ),
        .I1(\reg[data]_i_7_n_0 ),
        .I2(lfsr_shift[12]),
        .I3(lfsr_shift[14]),
        .I4(lfsr_shift[15]),
        .I5(\reg[data]_i_8_n_0 ),
        .O(\reg[data]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAABAAAA)) 
    \reg[data]_i_5 
       (.I0(ow_rst),
        .I1(\reg[state] [0]),
        .I2(\reg[state] [1]),
        .I3(\reg_reg[data]_0 ),
        .I4(\reg_reg[data]_1 ),
        .I5(\reg[data]_i_11_n_0 ),
        .O(\reg[data]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \reg[data]_i_6 
       (.I0(\reg_reg[lfsr_n_0_][16] ),
        .I1(lfsr_shift[14]),
        .I2(lfsr_shift[13]),
        .I3(lfsr_shift[15]),
        .I4(lfsr_shift[2]),
        .I5(lfsr_shift[9]),
        .O(\reg[data]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \reg[data]_i_7 
       (.I0(\reg[state] [0]),
        .I1(\reg[state] [1]),
        .I2(lfsr_shift[8]),
        .I3(lfsr_shift[7]),
        .I4(lfsr_shift[6]),
        .I5(lfsr_shift[16]),
        .O(\reg[data]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \reg[data]_i_8 
       (.I0(lfsr_shift[11]),
        .I1(lfsr_shift[10]),
        .I2(lfsr_shift[1]),
        .I3(lfsr_shift[5]),
        .I4(lfsr_shift[3]),
        .I5(lfsr_shift[4]),
        .O(\reg[data]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \reg[data_en]_i_1 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .I2(\reg[state] [0]),
        .I3(\reg[data]_i_3_n_0 ),
        .O(\reg[data_en] ));
  LUT6 #(
    .INIT(64'h00000000AEAA0000)) 
    \reg[done]_i_1 
       (.I0(\reg[data]_i_3_n_0 ),
        .I1(\reg[done]_i_2_n_0 ),
        .I2(\reg[state] [1]),
        .I3(\reg[state] [0]),
        .I4(axi_aresetn_i),
        .I5(ow_reg_rst),
        .O(\reg[done] ));
  LUT5 #(
    .INIT(32'h00000015)) 
    \reg[done]_i_2 
       (.I0(\reg[lfsr][16]_i_5_n_0 ),
        .I1(lfsr_shift[8]),
        .I2(lfsr_shift[7]),
        .I3(lfsr_shift[9]),
        .I4(\reg[lfsr][16]_i_4_n_0 ),
        .O(\reg[done]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFEFFEAFFA)) 
    \reg[lfsr][0]_i_1 
       (.I0(ow_rst),
        .I1(\reg[lfsr][0]_i_3_n_0 ),
        .I2(lfsr_shift[14]),
        .I3(\reg_reg[lfsr_n_0_][16] ),
        .I4(\reg[state] [1]),
        .I5(\reg[lfsr][0]_i_4_n_0 ),
        .O(p_1_in[0]));
  LUT6 #(
    .INIT(64'h0004000000000000)) 
    \reg[lfsr][0]_i_3 
       (.I0(\reg[lfsr][0]_i_5__0_n_0 ),
        .I1(\reg_reg[lfsr_n_0_][16] ),
        .I2(\reg[lfsr][0]_i_6__0_n_0 ),
        .I3(\reg[lfsr][0]_i_7_n_0 ),
        .I4(lfsr_shift[3]),
        .I5(lfsr_shift[4]),
        .O(\reg[lfsr][0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    \reg[lfsr][0]_i_4 
       (.I0(\reg[lfsr][0]_i_8_n_0 ),
        .I1(lfsr_shift[7]),
        .I2(\reg[state] [1]),
        .I3(lfsr_shift[15]),
        .I4(lfsr_shift[16]),
        .I5(\reg[lfsr][16]_i_4_n_0 ),
        .O(\reg[lfsr][0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \reg[lfsr][0]_i_5__0 
       (.I0(lfsr_shift[16]),
        .I1(lfsr_shift[6]),
        .I2(lfsr_shift[7]),
        .O(\reg[lfsr][0]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF7FF)) 
    \reg[lfsr][0]_i_6__0 
       (.I0(lfsr_shift[9]),
        .I1(lfsr_shift[2]),
        .I2(lfsr_shift[11]),
        .I3(lfsr_shift[10]),
        .I4(lfsr_shift[14]),
        .I5(lfsr_shift[12]),
        .O(\reg[lfsr][0]_i_6__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT5 #(
    .INIT(32'hFFFFBFFF)) 
    \reg[lfsr][0]_i_7 
       (.I0(lfsr_shift[1]),
        .I1(lfsr_shift[8]),
        .I2(lfsr_shift[13]),
        .I3(lfsr_shift[5]),
        .I4(lfsr_shift[15]),
        .O(\reg[lfsr][0]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \reg[lfsr][0]_i_8 
       (.I0(lfsr_shift[4]),
        .I1(lfsr_shift[3]),
        .O(\reg[lfsr][0]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][10]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[10]),
        .O(p_1_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][11]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[11]),
        .O(p_1_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][12]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[12]),
        .O(p_1_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][13]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[13]),
        .O(p_1_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][14]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[14]),
        .O(p_1_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][15]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[15]),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \reg[lfsr][16]_i_1 
       (.I0(\reg[state] [1]),
        .I1(\reg[state] [0]),
        .I2(axi_aresetn_i),
        .I3(ow_reg_rst),
        .O(\reg[lfsr][16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][16]_i_2 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[16]),
        .O(p_1_in[16]));
  LUT6 #(
    .INIT(64'h0000000000FEFFFE)) 
    \reg[lfsr][16]_i_3 
       (.I0(\reg[lfsr][16]_i_4_n_0 ),
        .I1(lfsr_shift[7]),
        .I2(\reg[lfsr][16]_i_5_n_0 ),
        .I3(\reg[state] [1]),
        .I4(\reg[lfsr][0]_i_3_n_0 ),
        .I5(ow_rst),
        .O(\reg[lfsr][16]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    \reg[lfsr][16]_i_4 
       (.I0(lfsr_shift[12]),
        .I1(lfsr_shift[10]),
        .I2(\reg[lfsr][16]_i_6_n_0 ),
        .I3(\reg[lfsr][16]_i_7_n_0 ),
        .O(\reg[lfsr][16]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \reg[lfsr][16]_i_5 
       (.I0(lfsr_shift[15]),
        .I1(lfsr_shift[16]),
        .I2(lfsr_shift[3]),
        .I3(lfsr_shift[4]),
        .O(\reg[lfsr][16]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \reg[lfsr][16]_i_6 
       (.I0(lfsr_shift[5]),
        .I1(lfsr_shift[13]),
        .I2(lfsr_shift[8]),
        .I3(lfsr_shift[1]),
        .O(\reg[lfsr][16]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFFFFFFFFFFF)) 
    \reg[lfsr][16]_i_7 
       (.I0(lfsr_shift[2]),
        .I1(\reg_reg[lfsr_n_0_][16] ),
        .I2(lfsr_shift[14]),
        .I3(lfsr_shift[9]),
        .I4(lfsr_shift[6]),
        .I5(lfsr_shift[11]),
        .O(\reg[lfsr][16]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][1]_i_1__1 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[1]),
        .O(p_1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][2]_i_1__1 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[2]),
        .O(p_1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][3]_i_1__1 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[3]),
        .O(p_1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][4]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[4]),
        .O(p_1_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][5]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[5]),
        .O(p_1_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][6]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[6]),
        .O(p_1_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][7]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[7]),
        .O(p_1_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][8]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[8]),
        .O(p_1_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \reg[lfsr][9]_i_1__0 
       (.I0(\reg[lfsr][16]_i_3_n_0 ),
        .I1(lfsr_shift[9]),
        .O(p_1_in[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \reg[tx]_i_1 
       (.I0(\reg[tx]_i_2_n_0 ),
        .I1(\reg[tx]_i_3_n_0 ),
        .I2(ow_o),
        .O(\reg[tx]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFBE)) 
    \reg[tx]_i_10 
       (.I0(lfsr_shift[7]),
        .I1(lfsr_shift[13]),
        .I2(lfsr_shift[12]),
        .I3(\reg[tx]_i_13_n_0 ),
        .I4(lfsr_shift[1]),
        .O(\reg[tx]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000020000)) 
    \reg[tx]_i_11 
       (.I0(\reg[tx]_i_14_n_0 ),
        .I1(lfsr_shift[4]),
        .I2(lfsr_shift[5]),
        .I3(lfsr_shift[8]),
        .I4(lfsr_shift[10]),
        .I5(lfsr_shift[12]),
        .O(\reg[tx]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \reg[tx]_i_12 
       (.I0(lfsr_shift[6]),
        .I1(lfsr_shift[7]),
        .I2(lfsr_shift[5]),
        .I3(lfsr_shift[13]),
        .O(\reg[tx]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'hF7)) 
    \reg[tx]_i_13 
       (.I0(lfsr_shift[11]),
        .I1(lfsr_shift[6]),
        .I2(lfsr_shift[9]),
        .O(\reg[tx]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000800000)) 
    \reg[tx]_i_14 
       (.I0(lfsr_shift[16]),
        .I1(lfsr_shift[15]),
        .I2(\reg[state] [0]),
        .I3(\reg[state] [1]),
        .I4(lfsr_shift[14]),
        .I5(\reg_reg[lfsr_n_0_][16] ),
        .O(\reg[tx]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFA8FFFFFFFF)) 
    \reg[tx]_i_2 
       (.I0(\reg[state] [1]),
        .I1(\reg[tx]_i_4_n_0 ),
        .I2(if_rx_data),
        .I3(\reg[state] [0]),
        .I4(ow_reg_rst),
        .I5(axi_aresetn_i),
        .O(\reg[tx]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCFFCCEFCCECCC)) 
    \reg[tx]_i_3 
       (.I0(\reg[lfsr][0]_i_3_n_0 ),
        .I1(\reg[tx]_i_5_n_0 ),
        .I2(\reg[state] [0]),
        .I3(\reg[state] [1]),
        .I4(\reg[tx]_i_4_n_0 ),
        .I5(\nxt_reg[tx]__0 ),
        .O(\reg[tx]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h000A800000008000)) 
    \reg[tx]_i_4 
       (.I0(\reg[tx]_i_7_n_0 ),
        .I1(\reg[tx]_i_8_n_0 ),
        .I2(lfsr_shift[4]),
        .I3(lfsr_shift[7]),
        .I4(lfsr_shift[6]),
        .I5(\reg[tx]_i_9_n_0 ),
        .O(\reg[tx]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF01400000)) 
    \reg[tx]_i_5 
       (.I0(\reg[tx]_i_10_n_0 ),
        .I1(lfsr_shift[3]),
        .I2(lfsr_shift[4]),
        .I3(lfsr_shift[2]),
        .I4(\reg[tx]_i_11_n_0 ),
        .I5(\FSM_sequential_reg[state][1]_i_4_n_0 ),
        .O(\reg[tx]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000040)) 
    \reg[tx]_i_6 
       (.I0(\reg[lfsr][16]_i_5_n_0 ),
        .I1(\reg[tx]_i_12_n_0 ),
        .I2(lfsr_shift[1]),
        .I3(lfsr_shift[8]),
        .I4(\reg_reg[lfsr_n_0_][16] ),
        .I5(\reg[lfsr][0]_i_6__0_n_0 ),
        .O(\nxt_reg[tx]__0 ));
  LUT6 #(
    .INIT(64'h0000000020000000)) 
    \reg[tx]_i_7 
       (.I0(lfsr_shift[10]),
        .I1(lfsr_shift[11]),
        .I2(lfsr_shift[2]),
        .I3(lfsr_shift[9]),
        .I4(lfsr_shift[3]),
        .I5(\reg[lfsr][0]_i_7_n_0 ),
        .O(\reg[tx]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \reg[tx]_i_8 
       (.I0(lfsr_shift[16]),
        .I1(\reg_reg[lfsr_n_0_][16] ),
        .I2(lfsr_shift[14]),
        .I3(lfsr_shift[12]),
        .O(\reg[tx]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \reg[tx]_i_9 
       (.I0(\reg_reg[lfsr_n_0_][16] ),
        .I1(lfsr_shift[16]),
        .I2(lfsr_shift[14]),
        .I3(lfsr_shift[12]),
        .O(\reg[tx]_i_9_n_0 ));
  FDRE \reg_reg[data] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[data]_i_1_n_0 ),
        .Q(if_rx_data),
        .R(1'b0));
  FDRE \reg_reg[data_en] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[data_en] ),
        .Q(if_rx_data_en),
        .R(1'b0));
  FDRE \reg_reg[done] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[done] ),
        .Q(if_done),
        .R(1'b0));
  FDRE \reg_reg[lfsr][0] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[0]),
        .Q(lfsr_shift[1]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][10] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[10]),
        .Q(lfsr_shift[11]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][11] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[11]),
        .Q(lfsr_shift[12]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][12] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[12]),
        .Q(lfsr_shift[13]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][13] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[13]),
        .Q(lfsr_shift[14]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][14] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[14]),
        .Q(lfsr_shift[15]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][15] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[15]),
        .Q(lfsr_shift[16]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][16] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[16]),
        .Q(\reg_reg[lfsr_n_0_][16] ),
        .R(1'b0));
  FDRE \reg_reg[lfsr][1] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[1]),
        .Q(lfsr_shift[2]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][2] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(lfsr_shift[3]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][3] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(lfsr_shift[4]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][4] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(lfsr_shift[5]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][5] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(lfsr_shift[6]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][6] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[6]),
        .Q(lfsr_shift[7]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][7] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[7]),
        .Q(lfsr_shift[8]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][8] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[8]),
        .Q(lfsr_shift[9]),
        .R(1'b0));
  FDRE \reg_reg[lfsr][9] 
       (.C(axi_clk_i),
        .CE(\reg[lfsr][16]_i_1_n_0 ),
        .D(p_1_in[9]),
        .Q(lfsr_shift[10]),
        .R(1'b0));
  FDRE \reg_reg[tx] 
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(\reg[tx]_i_1_n_0 ),
        .Q(ow_o),
        .R(1'b0));
endmodule

module design_1_axi_onewire_0_0_onewire_regs_axi
   (ow_en_o,
    axi_rvalid_o,
    axi_wdone_reg_0,
    ow_trig,
    ow_reg_rst,
    mem_readout_rt_reg_0,
    rd_data_en,
    ow_rst,
    ADDRA,
    axi_awready_o,
    axi_wready_o,
    axi_arready_o,
    ow_ctrl_o,
    axi_rdata_o,
    axi_clk_i,
    axi_awvalid_i,
    axi_wvalid_i,
    axi_aresetn_i,
    ow_busy,
    mem_rd_data_en,
    axi_arvalid_i,
    axi_rready_i,
    ctrl_rd_en,
    ctrl_rd_addr,
    ow_busy_o,
    ow_rd_data,
    ow_done_o,
    ow_dev_count,
    ow_too_many,
    axi_bready_i,
    axi_awaddr_i,
    axi_araddr_i,
    axi_wdata_i);
  output ow_en_o;
  output axi_rvalid_o;
  output axi_wdone_reg_0;
  output ow_trig;
  output ow_reg_rst;
  output mem_readout_rt_reg_0;
  output rd_data_en;
  output ow_rst;
  output [5:0]ADDRA;
  output axi_awready_o;
  output axi_wready_o;
  output axi_arready_o;
  output [2:0]ow_ctrl_o;
  output [31:0]axi_rdata_o;
  input axi_clk_i;
  input axi_awvalid_i;
  input axi_wvalid_i;
  input axi_aresetn_i;
  input ow_busy;
  input mem_rd_data_en;
  input axi_arvalid_i;
  input axi_rready_i;
  input ctrl_rd_en;
  input [5:0]ctrl_rd_addr;
  input ow_busy_o;
  input [63:0]ow_rd_data;
  input ow_done_o;
  input [5:0]ow_dev_count;
  input ow_too_many;
  input axi_bready_i;
  input [8:0]axi_awaddr_i;
  input [7:0]axi_araddr_i;
  input [3:0]axi_wdata_i;

  wire [5:0]ADDRA;
  wire [7:0]axi_araddr_i;
  wire axi_aresetn_i;
  wire axi_arready_o;
  wire axi_arset;
  wire axi_arset_i_1_n_0;
  wire axi_arvalid_i;
  wire [8:0]axi_awaddr_i;
  wire axi_awready_o;
  wire axi_awset;
  wire axi_awset_i_1_n_0;
  wire axi_awvalid_i;
  wire axi_bready_i;
  wire axi_clk_i;
  wire [31:0]axi_rdata_o;
  wire axi_rdone_i_1_n_0;
  wire axi_rready_i;
  wire axi_rvalid_o;
  wire [3:0]axi_wdata_i;
  wire axi_wdone_i_1_n_0;
  wire axi_wdone_i_2_n_0;
  wire axi_wdone_i_4_n_0;
  wire axi_wdone_reg_0;
  wire axi_wready_o;
  wire axi_wset_i_1_n_0;
  wire axi_wset_reg_n_0;
  wire axi_wvalid_i;
  wire control_wack;
  wire control_wreq;
  wire [5:0]ctrl_rd_addr;
  wire ctrl_rd_en;
  wire mem_rd_data_en;
  wire mem_readout_rr;
  wire mem_readout_rr0;
  wire mem_readout_rt;
  wire mem_readout_rt0;
  wire mem_readout_rt_reg_0;
  wire mem_readout_wr;
  wire mem_readout_wr0;
  wire mem_readout_wt0;
  wire mem_readout_wt_reg_n_0;
  wire mux_wack;
  wire mux_wreq;
  wire ow_busy;
  wire ow_busy_o;
  wire [2:0]ow_ctrl_o;
  wire [5:0]ow_dev_count;
  wire ow_done_o;
  wire ow_en_o;
  wire [63:0]ow_rd_data;
  wire ow_reg_rst;
  wire ow_rst;
  wire ow_too_many;
  wire ow_trig;
  wire p_0_in;
  wire [2:0]p_1_in;
  wire rd_ack;
  wire rd_ack_d0;
  wire rd_addr;
  wire \rd_addr_reg_n_0_[10] ;
  wire \rd_addr_reg_n_0_[2] ;
  wire \rd_addr_reg_n_0_[3] ;
  wire \rd_addr_reg_n_0_[4] ;
  wire \rd_addr_reg_n_0_[5] ;
  wire \rd_addr_reg_n_0_[6] ;
  wire \rd_addr_reg_n_0_[7] ;
  wire \rd_addr_reg_n_0_[8] ;
  wire [31:0]rd_dat_d0;
  wire [31:0]rd_data;
  wire \rd_data[0]_i_2_n_0 ;
  wire \rd_data[1]_i_2_n_0 ;
  wire \rd_data[2]_i_2_n_0 ;
  wire \rd_data[3]_i_2_n_0 ;
  wire \rd_data[8]_i_2_n_0 ;
  wire \rd_data[8]_i_3_n_0 ;
  wire \rd_data[8]_i_4_n_0 ;
  wire rd_data_en;
  wire rd_req;
  wire rd_req0;
  wire regs_control_rst_reg_i_1_n_0;
  wire regs_control_trig_reg_i_1_n_0;
  wire \regs_mux_ctrl_reg[2]_i_2_n_0 ;
  wire \regs_mux_ctrl_reg[2]_i_3_n_0 ;
  wire wr_ack;
  wire [10:2]wr_addr;
  wire wr_addr_0;
  wire \wr_adr_d0_reg_n_0_[2] ;
  wire \wr_adr_d0_reg_n_0_[3] ;
  wire \wr_adr_d0_reg_n_0_[4] ;
  wire \wr_adr_d0_reg_n_0_[5] ;
  wire \wr_adr_d0_reg_n_0_[6] ;
  wire \wr_adr_d0_reg_n_0_[7] ;
  wire \wr_adr_d0_reg_n_0_[8] ;
  wire \wr_adr_d0_reg_n_0_[9] ;
  wire \wr_dat_d0_reg_n_0_[0] ;
  wire [3:0]wr_data;
  wire wr_data_1;
  wire wr_req;
  wire wr_req_d0;
  wire wr_req_i_1_n_0;

  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT1 #(
    .INIT(2'h1)) 
    axi_arready_o_INST_0
       (.I0(axi_arset),
        .O(axi_arready_o));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00A8A8A8)) 
    axi_arset_i_1
       (.I0(axi_aresetn_i),
        .I1(axi_arvalid_i),
        .I2(axi_arset),
        .I3(axi_rready_i),
        .I4(axi_rvalid_o),
        .O(axi_arset_i_1_n_0));
  FDRE axi_arset_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(axi_arset_i_1_n_0),
        .Q(axi_arset),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_o_INST_0
       (.I0(axi_awset),
        .O(axi_awready_o));
  LUT5 #(
    .INIT(32'h00E0E0E0)) 
    axi_awset_i_1
       (.I0(axi_awset),
        .I1(axi_awvalid_i),
        .I2(axi_aresetn_i),
        .I3(axi_wdone_reg_0),
        .I4(axi_bready_i),
        .O(axi_awset_i_1_n_0));
  FDRE axi_awset_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(axi_awset_i_1_n_0),
        .Q(axi_awset),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hBA)) 
    axi_rdone_i_1
       (.I0(rd_ack),
        .I1(axi_rready_i),
        .I2(axi_rvalid_o),
        .O(axi_rdone_i_1_n_0));
  FDRE axi_rdone_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(axi_rdone_i_1_n_0),
        .Q(axi_rvalid_o),
        .R(axi_wdone_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_wdone_i_1
       (.I0(axi_aresetn_i),
        .O(axi_wdone_i_1_n_0));
  LUT3 #(
    .INIT(8'hF4)) 
    axi_wdone_i_2
       (.I0(axi_bready_i),
        .I1(axi_wdone_reg_0),
        .I2(wr_ack),
        .O(axi_wdone_i_2_n_0));
  LUT5 #(
    .INIT(32'h20FF2000)) 
    axi_wdone_i_3
       (.I0(mem_readout_wt_reg_n_0),
        .I1(ow_busy),
        .I2(mem_rd_data_en),
        .I3(p_0_in),
        .I4(axi_wdone_i_4_n_0),
        .O(wr_ack));
  LUT6 #(
    .INIT(64'hABA8ABABABA8A8A8)) 
    axi_wdone_i_4
       (.I0(wr_req_d0),
        .I1(\regs_mux_ctrl_reg[2]_i_3_n_0 ),
        .I2(\wr_adr_d0_reg_n_0_[3] ),
        .I3(control_wack),
        .I4(\wr_adr_d0_reg_n_0_[2] ),
        .I5(mux_wack),
        .O(axi_wdone_i_4_n_0));
  FDRE axi_wdone_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(axi_wdone_i_2_n_0),
        .Q(axi_wdone_reg_0),
        .R(axi_wdone_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT1 #(
    .INIT(2'h1)) 
    axi_wready_o_INST_0
       (.I0(axi_wset_reg_n_0),
        .O(axi_wready_o));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h00E0E0E0)) 
    axi_wset_i_1
       (.I0(axi_wset_reg_n_0),
        .I1(axi_wvalid_i),
        .I2(axi_aresetn_i),
        .I3(axi_wdone_reg_0),
        .I4(axi_bready_i),
        .O(axi_wset_i_1_n_0));
  FDRE axi_wset_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(axi_wset_i_1_n_0),
        .Q(axi_wset_reg_n_0),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    control_wack_i_1
       (.I0(\wr_adr_d0_reg_n_0_[2] ),
        .I1(\regs_mux_ctrl_reg[2]_i_2_n_0 ),
        .O(control_wreq));
  FDRE control_wack_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(control_wreq),
        .Q(control_wack),
        .R(axi_wdone_i_1_n_0));
  LUT6 #(
    .INIT(64'hF8F800F8F8F8F8F8)) 
    mem_readout_rr_i_1
       (.I0(rd_req),
        .I1(\rd_addr_reg_n_0_[10] ),
        .I2(mem_readout_rr),
        .I3(mem_rd_data_en),
        .I4(ow_busy),
        .I5(mem_readout_rt),
        .O(mem_readout_rr0));
  FDRE mem_readout_rr_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(mem_readout_rr0),
        .Q(mem_readout_rr),
        .R(axi_wdone_i_1_n_0));
  LUT6 #(
    .INIT(64'hBBBBBBBB000F0000)) 
    mem_readout_rt_i_1
       (.I0(ow_busy),
        .I1(mem_rd_data_en),
        .I2(mem_readout_wr),
        .I3(mem_readout_wt_reg_n_0),
        .I4(mem_readout_rr),
        .I5(mem_readout_rt),
        .O(mem_readout_rt0));
  FDRE mem_readout_rt_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(mem_readout_rt0),
        .Q(mem_readout_rt),
        .R(axi_wdone_i_1_n_0));
  LUT6 #(
    .INIT(64'hF8F800F8F8F8F8F8)) 
    mem_readout_wr_i_1
       (.I0(p_0_in),
        .I1(wr_req_d0),
        .I2(mem_readout_wr),
        .I3(mem_rd_data_en),
        .I4(ow_busy),
        .I5(mem_readout_wt_reg_n_0),
        .O(mem_readout_wr0));
  FDRE mem_readout_wr_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(mem_readout_wr0),
        .Q(mem_readout_wr),
        .R(axi_wdone_i_1_n_0));
  LUT5 #(
    .INIT(32'hB0B0BFB0)) 
    mem_readout_wt_i_1
       (.I0(ow_busy),
        .I1(mem_rd_data_en),
        .I2(mem_readout_wt_reg_n_0),
        .I3(mem_readout_wr),
        .I4(mem_readout_rt),
        .O(mem_readout_wt0));
  FDRE mem_readout_wt_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(mem_readout_wt0),
        .Q(mem_readout_wt_reg_n_0),
        .R(axi_wdone_i_1_n_0));
  LUT5 #(
    .INIT(32'hFF00E2E2)) 
    mem_reg_0_63_0_2_i_10
       (.I0(\rd_addr_reg_n_0_[3] ),
        .I1(mem_readout_wt_reg_n_0),
        .I2(\wr_adr_d0_reg_n_0_[3] ),
        .I3(ctrl_rd_addr[0]),
        .I4(ctrl_rd_en),
        .O(ADDRA[0]));
  LUT5 #(
    .INIT(32'hFF00E2E2)) 
    mem_reg_0_63_0_2_i_5
       (.I0(\rd_addr_reg_n_0_[8] ),
        .I1(mem_readout_wt_reg_n_0),
        .I2(\wr_adr_d0_reg_n_0_[8] ),
        .I3(ctrl_rd_addr[5]),
        .I4(ctrl_rd_en),
        .O(ADDRA[5]));
  LUT5 #(
    .INIT(32'hFF00E2E2)) 
    mem_reg_0_63_0_2_i_6
       (.I0(\rd_addr_reg_n_0_[7] ),
        .I1(mem_readout_wt_reg_n_0),
        .I2(\wr_adr_d0_reg_n_0_[7] ),
        .I3(ctrl_rd_addr[4]),
        .I4(ctrl_rd_en),
        .O(ADDRA[4]));
  LUT5 #(
    .INIT(32'hFF00E2E2)) 
    mem_reg_0_63_0_2_i_7
       (.I0(\rd_addr_reg_n_0_[6] ),
        .I1(mem_readout_wt_reg_n_0),
        .I2(\wr_adr_d0_reg_n_0_[6] ),
        .I3(ctrl_rd_addr[3]),
        .I4(ctrl_rd_en),
        .O(ADDRA[3]));
  LUT5 #(
    .INIT(32'hFF00E2E2)) 
    mem_reg_0_63_0_2_i_8
       (.I0(\rd_addr_reg_n_0_[5] ),
        .I1(mem_readout_wt_reg_n_0),
        .I2(\wr_adr_d0_reg_n_0_[5] ),
        .I3(ctrl_rd_addr[2]),
        .I4(ctrl_rd_en),
        .O(ADDRA[2]));
  LUT5 #(
    .INIT(32'hFF00E2E2)) 
    mem_reg_0_63_0_2_i_9
       (.I0(\rd_addr_reg_n_0_[4] ),
        .I1(mem_readout_wt_reg_n_0),
        .I2(\wr_adr_d0_reg_n_0_[4] ),
        .I3(ctrl_rd_addr[1]),
        .I4(ctrl_rd_en),
        .O(ADDRA[1]));
  FDRE mux_wack_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(mux_wreq),
        .Q(mux_wack),
        .R(axi_wdone_i_1_n_0));
  LUT5 #(
    .INIT(32'h20FF2000)) 
    rd_ack_i_1
       (.I0(mem_readout_rt),
        .I1(ow_busy),
        .I2(mem_rd_data_en),
        .I3(\rd_addr_reg_n_0_[10] ),
        .I4(rd_req),
        .O(rd_ack_d0));
  FDRE rd_ack_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(rd_ack_d0),
        .Q(rd_ack),
        .R(axi_wdone_i_1_n_0));
  LUT3 #(
    .INIT(8'h40)) 
    \rd_addr[10]_i_1 
       (.I0(axi_arset),
        .I1(axi_aresetn_i),
        .I2(axi_arvalid_i),
        .O(rd_addr));
  FDRE \rd_addr_reg[10] 
       (.C(axi_clk_i),
        .CE(rd_addr),
        .D(axi_araddr_i[7]),
        .Q(\rd_addr_reg_n_0_[10] ),
        .R(1'b0));
  FDRE \rd_addr_reg[2] 
       (.C(axi_clk_i),
        .CE(rd_addr),
        .D(axi_araddr_i[0]),
        .Q(\rd_addr_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \rd_addr_reg[3] 
       (.C(axi_clk_i),
        .CE(rd_addr),
        .D(axi_araddr_i[1]),
        .Q(\rd_addr_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \rd_addr_reg[4] 
       (.C(axi_clk_i),
        .CE(rd_addr),
        .D(axi_araddr_i[2]),
        .Q(\rd_addr_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \rd_addr_reg[5] 
       (.C(axi_clk_i),
        .CE(rd_addr),
        .D(axi_araddr_i[3]),
        .Q(\rd_addr_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \rd_addr_reg[6] 
       (.C(axi_clk_i),
        .CE(rd_addr),
        .D(axi_araddr_i[4]),
        .Q(\rd_addr_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \rd_addr_reg[7] 
       (.C(axi_clk_i),
        .CE(rd_addr),
        .D(axi_araddr_i[5]),
        .Q(\rd_addr_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \rd_addr_reg[8] 
       (.C(axi_clk_i),
        .CE(rd_addr),
        .D(axi_araddr_i[6]),
        .Q(\rd_addr_reg_n_0_[8] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hBBBAABAA)) 
    \rd_data[0]_i_1 
       (.I0(\rd_data[0]_i_2_n_0 ),
        .I1(\rd_addr_reg_n_0_[10] ),
        .I2(\rd_addr_reg_n_0_[3] ),
        .I3(ow_en_o),
        .I4(ow_busy_o),
        .O(rd_dat_d0[0]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[0]_i_2 
       (.I0(ow_rd_data[32]),
        .I1(ow_rd_data[0]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(\rd_data[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[10]_i_1 
       (.I0(ow_rd_data[42]),
        .I1(ow_rd_data[10]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[10]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[11]_i_1 
       (.I0(ow_rd_data[43]),
        .I1(ow_rd_data[11]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[11]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[12]_i_1 
       (.I0(ow_rd_data[44]),
        .I1(ow_rd_data[12]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[12]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[13]_i_1 
       (.I0(ow_rd_data[45]),
        .I1(ow_rd_data[13]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[13]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[14]_i_1 
       (.I0(ow_rd_data[46]),
        .I1(ow_rd_data[14]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[14]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[15]_i_1 
       (.I0(ow_rd_data[47]),
        .I1(ow_rd_data[15]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[15]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[16]_i_1 
       (.I0(ow_rd_data[48]),
        .I1(ow_rd_data[16]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[16]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[17]_i_1 
       (.I0(ow_rd_data[49]),
        .I1(ow_rd_data[17]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[17]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[18]_i_1 
       (.I0(ow_rd_data[50]),
        .I1(ow_rd_data[18]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[18]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[19]_i_1 
       (.I0(ow_rd_data[51]),
        .I1(ow_rd_data[19]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[19]));
  LUT5 #(
    .INIT(32'hBBBAABAA)) 
    \rd_data[1]_i_1 
       (.I0(\rd_data[1]_i_2_n_0 ),
        .I1(\rd_addr_reg_n_0_[10] ),
        .I2(\rd_addr_reg_n_0_[3] ),
        .I3(ow_ctrl_o[0]),
        .I4(ow_done_o),
        .O(rd_dat_d0[1]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[1]_i_2 
       (.I0(ow_rd_data[33]),
        .I1(ow_rd_data[1]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(\rd_data[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[20]_i_1 
       (.I0(ow_rd_data[52]),
        .I1(ow_rd_data[20]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[20]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[21]_i_1 
       (.I0(ow_rd_data[53]),
        .I1(ow_rd_data[21]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[21]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[22]_i_1 
       (.I0(ow_rd_data[54]),
        .I1(ow_rd_data[22]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[22]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[23]_i_1 
       (.I0(ow_rd_data[55]),
        .I1(ow_rd_data[23]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[23]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[24]_i_1 
       (.I0(ow_rd_data[56]),
        .I1(ow_rd_data[24]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[24]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[25]_i_1 
       (.I0(ow_rd_data[57]),
        .I1(ow_rd_data[25]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[25]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[26]_i_1 
       (.I0(ow_rd_data[58]),
        .I1(ow_rd_data[26]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[26]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[27]_i_1 
       (.I0(ow_rd_data[59]),
        .I1(ow_rd_data[27]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[27]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[28]_i_1 
       (.I0(ow_rd_data[60]),
        .I1(ow_rd_data[28]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[28]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[29]_i_1 
       (.I0(ow_rd_data[61]),
        .I1(ow_rd_data[29]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[29]));
  LUT5 #(
    .INIT(32'hBBBAABAA)) 
    \rd_data[2]_i_1 
       (.I0(\rd_data[2]_i_2_n_0 ),
        .I1(\rd_addr_reg_n_0_[10] ),
        .I2(\rd_addr_reg_n_0_[3] ),
        .I3(ow_ctrl_o[1]),
        .I4(ow_dev_count[0]),
        .O(rd_dat_d0[2]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[2]_i_2 
       (.I0(ow_rd_data[34]),
        .I1(ow_rd_data[2]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(\rd_data[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[30]_i_1 
       (.I0(ow_rd_data[62]),
        .I1(ow_rd_data[30]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[30]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[31]_i_1 
       (.I0(ow_rd_data[63]),
        .I1(ow_rd_data[31]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[31]));
  LUT5 #(
    .INIT(32'hBBBAABAA)) 
    \rd_data[3]_i_1 
       (.I0(\rd_data[3]_i_2_n_0 ),
        .I1(\rd_addr_reg_n_0_[10] ),
        .I2(\rd_addr_reg_n_0_[3] ),
        .I3(ow_ctrl_o[2]),
        .I4(ow_dev_count[1]),
        .O(rd_dat_d0[3]));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[3]_i_2 
       (.I0(ow_rd_data[35]),
        .I1(ow_rd_data[3]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(\rd_data[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rd_data[4]_i_1 
       (.I0(\rd_data[8]_i_2_n_0 ),
        .I1(ow_dev_count[2]),
        .I2(\rd_data[8]_i_3_n_0 ),
        .I3(ow_rd_data[4]),
        .I4(ow_rd_data[36]),
        .I5(\rd_data[8]_i_4_n_0 ),
        .O(rd_dat_d0[4]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rd_data[5]_i_1 
       (.I0(\rd_data[8]_i_2_n_0 ),
        .I1(ow_dev_count[3]),
        .I2(\rd_data[8]_i_3_n_0 ),
        .I3(ow_rd_data[5]),
        .I4(ow_rd_data[37]),
        .I5(\rd_data[8]_i_4_n_0 ),
        .O(rd_dat_d0[5]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \rd_data[63]_i_1 
       (.I0(mem_readout_rt),
        .I1(ctrl_rd_en),
        .I2(mem_readout_wt_reg_n_0),
        .I3(axi_aresetn_i),
        .I4(ow_reg_rst),
        .O(mem_readout_rt_reg_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rd_data[6]_i_1 
       (.I0(\rd_data[8]_i_2_n_0 ),
        .I1(ow_dev_count[4]),
        .I2(\rd_data[8]_i_3_n_0 ),
        .I3(ow_rd_data[6]),
        .I4(ow_rd_data[38]),
        .I5(\rd_data[8]_i_4_n_0 ),
        .O(rd_dat_d0[6]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rd_data[7]_i_1 
       (.I0(\rd_data[8]_i_2_n_0 ),
        .I1(ow_dev_count[5]),
        .I2(\rd_data[8]_i_3_n_0 ),
        .I3(ow_rd_data[7]),
        .I4(ow_rd_data[39]),
        .I5(\rd_data[8]_i_4_n_0 ),
        .O(rd_dat_d0[7]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \rd_data[8]_i_1 
       (.I0(\rd_data[8]_i_2_n_0 ),
        .I1(ow_too_many),
        .I2(\rd_data[8]_i_3_n_0 ),
        .I3(ow_rd_data[8]),
        .I4(ow_rd_data[40]),
        .I5(\rd_data[8]_i_4_n_0 ),
        .O(rd_dat_d0[8]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \rd_data[8]_i_2 
       (.I0(\rd_addr_reg_n_0_[3] ),
        .I1(\rd_addr_reg_n_0_[10] ),
        .O(\rd_data[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h02A2)) 
    \rd_data[8]_i_3 
       (.I0(\rd_addr_reg_n_0_[10] ),
        .I1(\rd_addr_reg_n_0_[2] ),
        .I2(mem_readout_wt_reg_n_0),
        .I3(\wr_adr_d0_reg_n_0_[2] ),
        .O(\rd_data[8]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hE200)) 
    \rd_data[8]_i_4 
       (.I0(\rd_addr_reg_n_0_[2] ),
        .I1(mem_readout_wt_reg_n_0),
        .I2(\wr_adr_d0_reg_n_0_[2] ),
        .I3(\rd_addr_reg_n_0_[10] ),
        .O(\rd_data[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hA0A0A0C0C0C0A0C0)) 
    \rd_data[9]_i_1 
       (.I0(ow_rd_data[41]),
        .I1(ow_rd_data[9]),
        .I2(\rd_addr_reg_n_0_[10] ),
        .I3(\rd_addr_reg_n_0_[2] ),
        .I4(mem_readout_wt_reg_n_0),
        .I5(\wr_adr_d0_reg_n_0_[2] ),
        .O(rd_dat_d0[9]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h0000FE00)) 
    rd_data_en_i_1
       (.I0(mem_readout_rt),
        .I1(ctrl_rd_en),
        .I2(mem_readout_wt_reg_n_0),
        .I3(axi_aresetn_i),
        .I4(ow_reg_rst),
        .O(rd_data_en));
  FDRE \rd_data_reg[0] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[0]),
        .Q(rd_data[0]),
        .R(1'b0));
  FDRE \rd_data_reg[10] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[10]),
        .Q(rd_data[10]),
        .R(1'b0));
  FDRE \rd_data_reg[11] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[11]),
        .Q(rd_data[11]),
        .R(1'b0));
  FDRE \rd_data_reg[12] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[12]),
        .Q(rd_data[12]),
        .R(1'b0));
  FDRE \rd_data_reg[13] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[13]),
        .Q(rd_data[13]),
        .R(1'b0));
  FDRE \rd_data_reg[14] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[14]),
        .Q(rd_data[14]),
        .R(1'b0));
  FDRE \rd_data_reg[15] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[15]),
        .Q(rd_data[15]),
        .R(1'b0));
  FDRE \rd_data_reg[16] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[16]),
        .Q(rd_data[16]),
        .R(1'b0));
  FDRE \rd_data_reg[17] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[17]),
        .Q(rd_data[17]),
        .R(1'b0));
  FDRE \rd_data_reg[18] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[18]),
        .Q(rd_data[18]),
        .R(1'b0));
  FDRE \rd_data_reg[19] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[19]),
        .Q(rd_data[19]),
        .R(1'b0));
  FDRE \rd_data_reg[1] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[1]),
        .Q(rd_data[1]),
        .R(1'b0));
  FDRE \rd_data_reg[20] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[20]),
        .Q(rd_data[20]),
        .R(1'b0));
  FDRE \rd_data_reg[21] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[21]),
        .Q(rd_data[21]),
        .R(1'b0));
  FDRE \rd_data_reg[22] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[22]),
        .Q(rd_data[22]),
        .R(1'b0));
  FDRE \rd_data_reg[23] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[23]),
        .Q(rd_data[23]),
        .R(1'b0));
  FDRE \rd_data_reg[24] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[24]),
        .Q(rd_data[24]),
        .R(1'b0));
  FDRE \rd_data_reg[25] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[25]),
        .Q(rd_data[25]),
        .R(1'b0));
  FDRE \rd_data_reg[26] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[26]),
        .Q(rd_data[26]),
        .R(1'b0));
  FDRE \rd_data_reg[27] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[27]),
        .Q(rd_data[27]),
        .R(1'b0));
  FDRE \rd_data_reg[28] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[28]),
        .Q(rd_data[28]),
        .R(1'b0));
  FDRE \rd_data_reg[29] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[29]),
        .Q(rd_data[29]),
        .R(1'b0));
  FDRE \rd_data_reg[2] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[2]),
        .Q(rd_data[2]),
        .R(1'b0));
  FDRE \rd_data_reg[30] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[30]),
        .Q(rd_data[30]),
        .R(1'b0));
  FDRE \rd_data_reg[31] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[31]),
        .Q(rd_data[31]),
        .R(1'b0));
  FDRE \rd_data_reg[3] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[3]),
        .Q(rd_data[3]),
        .R(1'b0));
  FDRE \rd_data_reg[4] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[4]),
        .Q(rd_data[4]),
        .R(1'b0));
  FDRE \rd_data_reg[5] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[5]),
        .Q(rd_data[5]),
        .R(1'b0));
  FDRE \rd_data_reg[6] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[6]),
        .Q(rd_data[6]),
        .R(1'b0));
  FDRE \rd_data_reg[7] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[7]),
        .Q(rd_data[7]),
        .R(1'b0));
  FDRE \rd_data_reg[8] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[8]),
        .Q(rd_data[8]),
        .R(1'b0));
  FDRE \rd_data_reg[9] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(rd_dat_d0[9]),
        .Q(rd_data[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    rd_req_i_1
       (.I0(axi_arvalid_i),
        .I1(axi_arset),
        .O(rd_req0));
  FDRE rd_req_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(rd_req0),
        .Q(rd_req),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[0] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[0]),
        .Q(axi_rdata_o[0]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[10] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[10]),
        .Q(axi_rdata_o[10]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[11] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[11]),
        .Q(axi_rdata_o[11]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[12] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[12]),
        .Q(axi_rdata_o[12]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[13] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[13]),
        .Q(axi_rdata_o[13]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[14] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[14]),
        .Q(axi_rdata_o[14]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[15] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[15]),
        .Q(axi_rdata_o[15]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[16] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[16]),
        .Q(axi_rdata_o[16]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[17] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[17]),
        .Q(axi_rdata_o[17]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[18] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[18]),
        .Q(axi_rdata_o[18]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[19] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[19]),
        .Q(axi_rdata_o[19]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[1] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[1]),
        .Q(axi_rdata_o[1]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[20] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[20]),
        .Q(axi_rdata_o[20]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[21] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[21]),
        .Q(axi_rdata_o[21]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[22] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[22]),
        .Q(axi_rdata_o[22]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[23] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[23]),
        .Q(axi_rdata_o[23]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[24] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[24]),
        .Q(axi_rdata_o[24]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[25] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[25]),
        .Q(axi_rdata_o[25]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[26] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[26]),
        .Q(axi_rdata_o[26]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[27] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[27]),
        .Q(axi_rdata_o[27]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[28] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[28]),
        .Q(axi_rdata_o[28]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[29] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[29]),
        .Q(axi_rdata_o[29]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[2] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[2]),
        .Q(axi_rdata_o[2]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[30] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[30]),
        .Q(axi_rdata_o[30]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[31] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[31]),
        .Q(axi_rdata_o[31]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[3] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[3]),
        .Q(axi_rdata_o[3]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[4] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[4]),
        .Q(axi_rdata_o[4]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[5] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[5]),
        .Q(axi_rdata_o[5]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[6] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[6]),
        .Q(axi_rdata_o[6]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[7] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[7]),
        .Q(axi_rdata_o[7]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[8] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[8]),
        .Q(axi_rdata_o[8]),
        .R(axi_wdone_i_1_n_0));
  FDRE \rdata_reg[9] 
       (.C(axi_clk_i),
        .CE(rd_ack),
        .D(rd_data[9]),
        .Q(axi_rdata_o[9]),
        .R(axi_wdone_i_1_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    \reg[lfsr][0]_i_2__0 
       (.I0(ow_reg_rst),
        .I1(axi_aresetn_i),
        .O(ow_rst));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    regs_control_rst_reg_i_1
       (.I0(p_1_in[0]),
        .I1(\regs_mux_ctrl_reg[2]_i_2_n_0 ),
        .I2(axi_aresetn_i),
        .I3(\wr_adr_d0_reg_n_0_[2] ),
        .O(regs_control_rst_reg_i_1_n_0));
  FDRE regs_control_rst_reg_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(regs_control_rst_reg_i_1_n_0),
        .Q(ow_reg_rst),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    regs_control_trig_reg_i_1
       (.I0(\wr_dat_d0_reg_n_0_[0] ),
        .I1(\regs_mux_ctrl_reg[2]_i_2_n_0 ),
        .I2(axi_aresetn_i),
        .I3(\wr_adr_d0_reg_n_0_[2] ),
        .O(regs_control_trig_reg_i_1_n_0));
  FDRE regs_control_trig_reg_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(regs_control_trig_reg_i_1_n_0),
        .Q(ow_trig),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h1)) 
    \regs_mux_ctrl_reg[2]_i_1 
       (.I0(\regs_mux_ctrl_reg[2]_i_2_n_0 ),
        .I1(\wr_adr_d0_reg_n_0_[2] ),
        .O(mux_wreq));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \regs_mux_ctrl_reg[2]_i_2 
       (.I0(wr_req_d0),
        .I1(p_0_in),
        .I2(\wr_adr_d0_reg_n_0_[3] ),
        .I3(\regs_mux_ctrl_reg[2]_i_3_n_0 ),
        .O(\regs_mux_ctrl_reg[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \regs_mux_ctrl_reg[2]_i_3 
       (.I0(\wr_adr_d0_reg_n_0_[9] ),
        .I1(\wr_adr_d0_reg_n_0_[8] ),
        .I2(\wr_adr_d0_reg_n_0_[7] ),
        .I3(\wr_adr_d0_reg_n_0_[6] ),
        .I4(\wr_adr_d0_reg_n_0_[4] ),
        .I5(\wr_adr_d0_reg_n_0_[5] ),
        .O(\regs_mux_ctrl_reg[2]_i_3_n_0 ));
  FDRE \regs_mux_ctrl_reg_reg[0] 
       (.C(axi_clk_i),
        .CE(mux_wreq),
        .D(p_1_in[0]),
        .Q(ow_ctrl_o[0]),
        .R(axi_wdone_i_1_n_0));
  FDRE \regs_mux_ctrl_reg_reg[1] 
       (.C(axi_clk_i),
        .CE(mux_wreq),
        .D(p_1_in[1]),
        .Q(ow_ctrl_o[1]),
        .R(axi_wdone_i_1_n_0));
  FDRE \regs_mux_ctrl_reg_reg[2] 
       (.C(axi_clk_i),
        .CE(mux_wreq),
        .D(p_1_in[2]),
        .Q(ow_ctrl_o[2]),
        .R(axi_wdone_i_1_n_0));
  FDRE regs_mux_en_reg_reg
       (.C(axi_clk_i),
        .CE(mux_wreq),
        .D(\wr_dat_d0_reg_n_0_[0] ),
        .Q(ow_en_o),
        .R(axi_wdone_i_1_n_0));
  LUT3 #(
    .INIT(8'h40)) 
    \wr_addr[10]_i_1 
       (.I0(axi_awset),
        .I1(axi_awvalid_i),
        .I2(axi_aresetn_i),
        .O(wr_addr_0));
  FDRE \wr_addr_reg[10] 
       (.C(axi_clk_i),
        .CE(wr_addr_0),
        .D(axi_awaddr_i[8]),
        .Q(wr_addr[10]),
        .R(1'b0));
  FDRE \wr_addr_reg[2] 
       (.C(axi_clk_i),
        .CE(wr_addr_0),
        .D(axi_awaddr_i[0]),
        .Q(wr_addr[2]),
        .R(1'b0));
  FDRE \wr_addr_reg[3] 
       (.C(axi_clk_i),
        .CE(wr_addr_0),
        .D(axi_awaddr_i[1]),
        .Q(wr_addr[3]),
        .R(1'b0));
  FDRE \wr_addr_reg[4] 
       (.C(axi_clk_i),
        .CE(wr_addr_0),
        .D(axi_awaddr_i[2]),
        .Q(wr_addr[4]),
        .R(1'b0));
  FDRE \wr_addr_reg[5] 
       (.C(axi_clk_i),
        .CE(wr_addr_0),
        .D(axi_awaddr_i[3]),
        .Q(wr_addr[5]),
        .R(1'b0));
  FDRE \wr_addr_reg[6] 
       (.C(axi_clk_i),
        .CE(wr_addr_0),
        .D(axi_awaddr_i[4]),
        .Q(wr_addr[6]),
        .R(1'b0));
  FDRE \wr_addr_reg[7] 
       (.C(axi_clk_i),
        .CE(wr_addr_0),
        .D(axi_awaddr_i[5]),
        .Q(wr_addr[7]),
        .R(1'b0));
  FDRE \wr_addr_reg[8] 
       (.C(axi_clk_i),
        .CE(wr_addr_0),
        .D(axi_awaddr_i[6]),
        .Q(wr_addr[8]),
        .R(1'b0));
  FDRE \wr_addr_reg[9] 
       (.C(axi_clk_i),
        .CE(wr_addr_0),
        .D(axi_awaddr_i[7]),
        .Q(wr_addr[9]),
        .R(1'b0));
  FDRE \wr_adr_d0_reg[10] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_addr[10]),
        .Q(p_0_in),
        .R(1'b0));
  FDRE \wr_adr_d0_reg[2] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_addr[2]),
        .Q(\wr_adr_d0_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \wr_adr_d0_reg[3] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_addr[3]),
        .Q(\wr_adr_d0_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \wr_adr_d0_reg[4] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_addr[4]),
        .Q(\wr_adr_d0_reg_n_0_[4] ),
        .R(1'b0));
  FDRE \wr_adr_d0_reg[5] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_addr[5]),
        .Q(\wr_adr_d0_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \wr_adr_d0_reg[6] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_addr[6]),
        .Q(\wr_adr_d0_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \wr_adr_d0_reg[7] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_addr[7]),
        .Q(\wr_adr_d0_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \wr_adr_d0_reg[8] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_addr[8]),
        .Q(\wr_adr_d0_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \wr_adr_d0_reg[9] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_addr[9]),
        .Q(\wr_adr_d0_reg_n_0_[9] ),
        .R(1'b0));
  FDRE \wr_dat_d0_reg[0] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_data[0]),
        .Q(\wr_dat_d0_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \wr_dat_d0_reg[1] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_data[1]),
        .Q(p_1_in[0]),
        .R(1'b0));
  FDRE \wr_dat_d0_reg[2] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_data[2]),
        .Q(p_1_in[1]),
        .R(1'b0));
  FDRE \wr_dat_d0_reg[3] 
       (.C(axi_clk_i),
        .CE(axi_aresetn_i),
        .D(wr_data[3]),
        .Q(p_1_in[2]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h40)) 
    \wr_data[3]_i_1 
       (.I0(axi_wset_reg_n_0),
        .I1(axi_aresetn_i),
        .I2(axi_wvalid_i),
        .O(wr_data_1));
  FDRE \wr_data_reg[0] 
       (.C(axi_clk_i),
        .CE(wr_data_1),
        .D(axi_wdata_i[0]),
        .Q(wr_data[0]),
        .R(1'b0));
  FDRE \wr_data_reg[1] 
       (.C(axi_clk_i),
        .CE(wr_data_1),
        .D(axi_wdata_i[1]),
        .Q(wr_data[1]),
        .R(1'b0));
  FDRE \wr_data_reg[2] 
       (.C(axi_clk_i),
        .CE(wr_data_1),
        .D(axi_wdata_i[2]),
        .Q(wr_data[2]),
        .R(1'b0));
  FDRE \wr_data_reg[3] 
       (.C(axi_clk_i),
        .CE(wr_data_1),
        .D(axi_wdata_i[3]),
        .Q(wr_data[3]),
        .R(1'b0));
  FDRE wr_req_d0_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(wr_req),
        .Q(wr_req_d0),
        .R(axi_wdone_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h2200E000)) 
    wr_req_i_1
       (.I0(axi_awvalid_i),
        .I1(axi_awset),
        .I2(axi_wvalid_i),
        .I3(axi_aresetn_i),
        .I4(axi_wset_reg_n_0),
        .O(wr_req_i_1_n_0));
  FDRE wr_req_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(wr_req_i_1_n_0),
        .Q(wr_req),
        .R(1'b0));
endmodule

module design_1_axi_onewire_0_0_two_port_ram
   (mem_wr_done,
    rd_data_en_reg_0,
    \rd_data_reg[3]_0 ,
    ow_rd_data,
    rd_data_en_reg_1,
    axi_clk_i,
    wr_data_i,
    wr_done,
    ADDRD,
    ADDRA,
    rd_data_en,
    axi_aresetn_i,
    ow_reg_rst,
    \reg[mem_addr][5]_i_4 ,
    \rd_data_reg[63]_0 );
  output mem_wr_done;
  output rd_data_en_reg_0;
  output \rd_data_reg[3]_0 ;
  output [63:0]ow_rd_data;
  output rd_data_en_reg_1;
  input axi_clk_i;
  input [63:0]wr_data_i;
  input wr_done;
  input [5:0]ADDRD;
  input [5:0]ADDRA;
  input rd_data_en;
  input axi_aresetn_i;
  input ow_reg_rst;
  input \reg[mem_addr][5]_i_4 ;
  input \rd_data_reg[63]_0 ;

  wire [5:0]ADDRA;
  wire [5:0]ADDRD;
  wire \FSM_sequential_reg[state][2]_i_8__0_n_0 ;
  wire axi_aresetn_i;
  wire axi_clk_i;
  wire mem_wr_done;
  wire [63:0]ow_rd_data;
  wire ow_reg_rst;
  wire [63:0]rd_data0;
  wire \rd_data[0]_i_1__0_n_0 ;
  wire \rd_data[10]_i_1__0_n_0 ;
  wire \rd_data[11]_i_1__0_n_0 ;
  wire \rd_data[12]_i_1__0_n_0 ;
  wire \rd_data[13]_i_1__0_n_0 ;
  wire \rd_data[14]_i_1__0_n_0 ;
  wire \rd_data[15]_i_1__0_n_0 ;
  wire \rd_data[16]_i_1__0_n_0 ;
  wire \rd_data[17]_i_1__0_n_0 ;
  wire \rd_data[18]_i_1__0_n_0 ;
  wire \rd_data[19]_i_1__0_n_0 ;
  wire \rd_data[1]_i_1__0_n_0 ;
  wire \rd_data[20]_i_1__0_n_0 ;
  wire \rd_data[21]_i_1__0_n_0 ;
  wire \rd_data[22]_i_1__0_n_0 ;
  wire \rd_data[23]_i_1__0_n_0 ;
  wire \rd_data[24]_i_1__0_n_0 ;
  wire \rd_data[25]_i_1__0_n_0 ;
  wire \rd_data[26]_i_1__0_n_0 ;
  wire \rd_data[27]_i_1__0_n_0 ;
  wire \rd_data[28]_i_1__0_n_0 ;
  wire \rd_data[29]_i_1__0_n_0 ;
  wire \rd_data[2]_i_1__0_n_0 ;
  wire \rd_data[30]_i_1__0_n_0 ;
  wire \rd_data[31]_i_1__0_n_0 ;
  wire \rd_data[32]_i_1_n_0 ;
  wire \rd_data[33]_i_1_n_0 ;
  wire \rd_data[34]_i_1_n_0 ;
  wire \rd_data[35]_i_1_n_0 ;
  wire \rd_data[36]_i_1_n_0 ;
  wire \rd_data[37]_i_1_n_0 ;
  wire \rd_data[38]_i_1_n_0 ;
  wire \rd_data[39]_i_1_n_0 ;
  wire \rd_data[3]_i_1__0_n_0 ;
  wire \rd_data[40]_i_1_n_0 ;
  wire \rd_data[41]_i_1_n_0 ;
  wire \rd_data[42]_i_1_n_0 ;
  wire \rd_data[43]_i_1_n_0 ;
  wire \rd_data[44]_i_1_n_0 ;
  wire \rd_data[45]_i_1_n_0 ;
  wire \rd_data[46]_i_1_n_0 ;
  wire \rd_data[47]_i_1_n_0 ;
  wire \rd_data[48]_i_1_n_0 ;
  wire \rd_data[49]_i_1_n_0 ;
  wire \rd_data[4]_i_1__0_n_0 ;
  wire \rd_data[50]_i_1_n_0 ;
  wire \rd_data[51]_i_1_n_0 ;
  wire \rd_data[52]_i_1_n_0 ;
  wire \rd_data[53]_i_1_n_0 ;
  wire \rd_data[54]_i_1_n_0 ;
  wire \rd_data[55]_i_1_n_0 ;
  wire \rd_data[56]_i_1_n_0 ;
  wire \rd_data[57]_i_1_n_0 ;
  wire \rd_data[58]_i_1_n_0 ;
  wire \rd_data[59]_i_1_n_0 ;
  wire \rd_data[5]_i_1__0_n_0 ;
  wire \rd_data[60]_i_1_n_0 ;
  wire \rd_data[61]_i_1_n_0 ;
  wire \rd_data[62]_i_1_n_0 ;
  wire \rd_data[63]_i_2_n_0 ;
  wire \rd_data[6]_i_1__0_n_0 ;
  wire \rd_data[7]_i_1__0_n_0 ;
  wire \rd_data[8]_i_1__0_n_0 ;
  wire \rd_data[9]_i_1__0_n_0 ;
  wire rd_data_en;
  wire rd_data_en_reg_0;
  wire rd_data_en_reg_1;
  wire \rd_data_reg[3]_0 ;
  wire \rd_data_reg[63]_0 ;
  wire \reg[mem_addr][5]_i_4 ;
  wire [63:0]wr_data_i;
  wire wr_done;
  wire NLW_mem_reg_0_63_0_2_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_12_14_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_15_17_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_18_20_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_21_23_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_24_26_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_27_29_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_30_32_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_33_35_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_36_38_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_39_41_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_3_5_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_42_44_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_45_47_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_48_50_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_51_53_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_54_56_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_57_59_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_60_62_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_63_63_SPO_UNCONNECTED;
  wire NLW_mem_reg_0_63_6_8_DOD_UNCONNECTED;
  wire NLW_mem_reg_0_63_9_11_DOD_UNCONNECTED;

  LUT5 #(
    .INIT(32'hFFFFFFF7)) 
    \FSM_sequential_reg[state][2]_i_5__0 
       (.I0(ow_rd_data[3]),
        .I1(ow_rd_data[5]),
        .I2(ow_rd_data[6]),
        .I3(ow_rd_data[4]),
        .I4(\FSM_sequential_reg[state][2]_i_8__0_n_0 ),
        .O(\rd_data_reg[3]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_reg[state][2]_i_8__0 
       (.I0(ow_rd_data[0]),
        .I1(ow_rd_data[1]),
        .I2(ow_rd_data[2]),
        .I3(ow_rd_data[7]),
        .O(\FSM_sequential_reg[state][2]_i_8__0_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_0_2" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "2" *) 
  RAM64M mem_reg_0_63_0_2
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[0]),
        .DIB(wr_data_i[1]),
        .DIC(wr_data_i[2]),
        .DID(1'b0),
        .DOA(rd_data0[0]),
        .DOB(rd_data0[1]),
        .DOC(rd_data0[2]),
        .DOD(NLW_mem_reg_0_63_0_2_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_12_14" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "14" *) 
  RAM64M mem_reg_0_63_12_14
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[12]),
        .DIB(wr_data_i[13]),
        .DIC(wr_data_i[14]),
        .DID(1'b0),
        .DOA(rd_data0[12]),
        .DOB(rd_data0[13]),
        .DOC(rd_data0[14]),
        .DOD(NLW_mem_reg_0_63_12_14_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_15_17" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "15" *) 
  (* ram_slice_end = "17" *) 
  RAM64M mem_reg_0_63_15_17
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[15]),
        .DIB(wr_data_i[16]),
        .DIC(wr_data_i[17]),
        .DID(1'b0),
        .DOA(rd_data0[15]),
        .DOB(rd_data0[16]),
        .DOC(rd_data0[17]),
        .DOD(NLW_mem_reg_0_63_15_17_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_18_20" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "20" *) 
  RAM64M mem_reg_0_63_18_20
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[18]),
        .DIB(wr_data_i[19]),
        .DIC(wr_data_i[20]),
        .DID(1'b0),
        .DOA(rd_data0[18]),
        .DOB(rd_data0[19]),
        .DOC(rd_data0[20]),
        .DOD(NLW_mem_reg_0_63_18_20_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_21_23" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "21" *) 
  (* ram_slice_end = "23" *) 
  RAM64M mem_reg_0_63_21_23
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[21]),
        .DIB(wr_data_i[22]),
        .DIC(wr_data_i[23]),
        .DID(1'b0),
        .DOA(rd_data0[21]),
        .DOB(rd_data0[22]),
        .DOC(rd_data0[23]),
        .DOD(NLW_mem_reg_0_63_21_23_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_24_26" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "26" *) 
  RAM64M mem_reg_0_63_24_26
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[24]),
        .DIB(wr_data_i[25]),
        .DIC(wr_data_i[26]),
        .DID(1'b0),
        .DOA(rd_data0[24]),
        .DOB(rd_data0[25]),
        .DOC(rd_data0[26]),
        .DOD(NLW_mem_reg_0_63_24_26_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_27_29" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "27" *) 
  (* ram_slice_end = "29" *) 
  RAM64M mem_reg_0_63_27_29
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[27]),
        .DIB(wr_data_i[28]),
        .DIC(wr_data_i[29]),
        .DID(1'b0),
        .DOA(rd_data0[27]),
        .DOB(rd_data0[28]),
        .DOC(rd_data0[29]),
        .DOD(NLW_mem_reg_0_63_27_29_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_30_32" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "32" *) 
  RAM64M mem_reg_0_63_30_32
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[30]),
        .DIB(wr_data_i[31]),
        .DIC(wr_data_i[32]),
        .DID(1'b0),
        .DOA(rd_data0[30]),
        .DOB(rd_data0[31]),
        .DOC(rd_data0[32]),
        .DOD(NLW_mem_reg_0_63_30_32_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_33_35" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "33" *) 
  (* ram_slice_end = "35" *) 
  RAM64M mem_reg_0_63_33_35
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[33]),
        .DIB(wr_data_i[34]),
        .DIC(wr_data_i[35]),
        .DID(1'b0),
        .DOA(rd_data0[33]),
        .DOB(rd_data0[34]),
        .DOC(rd_data0[35]),
        .DOD(NLW_mem_reg_0_63_33_35_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_36_38" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "36" *) 
  (* ram_slice_end = "38" *) 
  RAM64M mem_reg_0_63_36_38
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[36]),
        .DIB(wr_data_i[37]),
        .DIC(wr_data_i[38]),
        .DID(1'b0),
        .DOA(rd_data0[36]),
        .DOB(rd_data0[37]),
        .DOC(rd_data0[38]),
        .DOD(NLW_mem_reg_0_63_36_38_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_39_41" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "39" *) 
  (* ram_slice_end = "41" *) 
  RAM64M mem_reg_0_63_39_41
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[39]),
        .DIB(wr_data_i[40]),
        .DIC(wr_data_i[41]),
        .DID(1'b0),
        .DOA(rd_data0[39]),
        .DOB(rd_data0[40]),
        .DOC(rd_data0[41]),
        .DOD(NLW_mem_reg_0_63_39_41_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_3_5" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "3" *) 
  (* ram_slice_end = "5" *) 
  RAM64M mem_reg_0_63_3_5
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[3]),
        .DIB(wr_data_i[4]),
        .DIC(wr_data_i[5]),
        .DID(1'b0),
        .DOA(rd_data0[3]),
        .DOB(rd_data0[4]),
        .DOC(rd_data0[5]),
        .DOD(NLW_mem_reg_0_63_3_5_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_42_44" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "42" *) 
  (* ram_slice_end = "44" *) 
  RAM64M mem_reg_0_63_42_44
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[42]),
        .DIB(wr_data_i[43]),
        .DIC(wr_data_i[44]),
        .DID(1'b0),
        .DOA(rd_data0[42]),
        .DOB(rd_data0[43]),
        .DOC(rd_data0[44]),
        .DOD(NLW_mem_reg_0_63_42_44_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_45_47" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "45" *) 
  (* ram_slice_end = "47" *) 
  RAM64M mem_reg_0_63_45_47
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[45]),
        .DIB(wr_data_i[46]),
        .DIC(wr_data_i[47]),
        .DID(1'b0),
        .DOA(rd_data0[45]),
        .DOB(rd_data0[46]),
        .DOC(rd_data0[47]),
        .DOD(NLW_mem_reg_0_63_45_47_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_48_50" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "48" *) 
  (* ram_slice_end = "50" *) 
  RAM64M mem_reg_0_63_48_50
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[48]),
        .DIB(wr_data_i[49]),
        .DIC(wr_data_i[50]),
        .DID(1'b0),
        .DOA(rd_data0[48]),
        .DOB(rd_data0[49]),
        .DOC(rd_data0[50]),
        .DOD(NLW_mem_reg_0_63_48_50_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_51_53" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "51" *) 
  (* ram_slice_end = "53" *) 
  RAM64M mem_reg_0_63_51_53
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[51]),
        .DIB(wr_data_i[52]),
        .DIC(wr_data_i[53]),
        .DID(1'b0),
        .DOA(rd_data0[51]),
        .DOB(rd_data0[52]),
        .DOC(rd_data0[53]),
        .DOD(NLW_mem_reg_0_63_51_53_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_54_56" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "54" *) 
  (* ram_slice_end = "56" *) 
  RAM64M mem_reg_0_63_54_56
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[54]),
        .DIB(wr_data_i[55]),
        .DIC(wr_data_i[56]),
        .DID(1'b0),
        .DOA(rd_data0[54]),
        .DOB(rd_data0[55]),
        .DOC(rd_data0[56]),
        .DOD(NLW_mem_reg_0_63_54_56_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_57_59" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "57" *) 
  (* ram_slice_end = "59" *) 
  RAM64M mem_reg_0_63_57_59
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[57]),
        .DIB(wr_data_i[58]),
        .DIC(wr_data_i[59]),
        .DID(1'b0),
        .DOA(rd_data0[57]),
        .DOB(rd_data0[58]),
        .DOC(rd_data0[59]),
        .DOD(NLW_mem_reg_0_63_57_59_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_60_62" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "60" *) 
  (* ram_slice_end = "62" *) 
  RAM64M mem_reg_0_63_60_62
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[60]),
        .DIB(wr_data_i[61]),
        .DIC(wr_data_i[62]),
        .DID(1'b0),
        .DOA(rd_data0[60]),
        .DOB(rd_data0[61]),
        .DOC(rd_data0[62]),
        .DOD(NLW_mem_reg_0_63_60_62_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_63_63" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "63" *) 
  (* ram_slice_end = "63" *) 
  RAM64X1D mem_reg_0_63_63_63
       (.A0(ADDRD[0]),
        .A1(ADDRD[1]),
        .A2(ADDRD[2]),
        .A3(ADDRD[3]),
        .A4(ADDRD[4]),
        .A5(ADDRD[5]),
        .D(wr_data_i[63]),
        .DPO(rd_data0[63]),
        .DPRA0(ADDRA[0]),
        .DPRA1(ADDRA[1]),
        .DPRA2(ADDRA[2]),
        .DPRA3(ADDRA[3]),
        .DPRA4(ADDRA[4]),
        .DPRA5(ADDRA[5]),
        .SPO(NLW_mem_reg_0_63_63_63_SPO_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_6_8" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "8" *) 
  RAM64M mem_reg_0_63_6_8
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[6]),
        .DIB(wr_data_i[7]),
        .DIC(wr_data_i[8]),
        .DID(1'b0),
        .DOA(rd_data0[6]),
        .DOB(rd_data0[7]),
        .DOC(rd_data0[8]),
        .DOD(NLW_mem_reg_0_63_6_8_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* RTL_RAM_BITS = "4096" *) 
  (* RTL_RAM_NAME = "U0/onewire_if/x_bus_master/ram_gen.ram_inst/mem_reg_0_63_9_11" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "63" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "9" *) 
  (* ram_slice_end = "11" *) 
  RAM64M mem_reg_0_63_9_11
       (.ADDRA(ADDRA),
        .ADDRB(ADDRA),
        .ADDRC(ADDRA),
        .ADDRD(ADDRD),
        .DIA(wr_data_i[9]),
        .DIB(wr_data_i[10]),
        .DIC(wr_data_i[11]),
        .DID(1'b0),
        .DOA(rd_data0[9]),
        .DOB(rd_data0[10]),
        .DOC(rd_data0[11]),
        .DOD(NLW_mem_reg_0_63_9_11_DOD_UNCONNECTED),
        .WCLK(axi_clk_i),
        .WE(wr_done));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[0]_i_1__0 
       (.I0(rd_data0[0]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[10]_i_1__0 
       (.I0(rd_data0[10]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[10]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[11]_i_1__0 
       (.I0(rd_data0[11]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[11]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[12]_i_1__0 
       (.I0(rd_data0[12]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[12]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[13]_i_1__0 
       (.I0(rd_data0[13]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[13]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[14]_i_1__0 
       (.I0(rd_data0[14]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[14]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[15]_i_1__0 
       (.I0(rd_data0[15]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[15]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[16]_i_1__0 
       (.I0(rd_data0[16]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[16]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[17]_i_1__0 
       (.I0(rd_data0[17]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[17]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[18]_i_1__0 
       (.I0(rd_data0[18]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[18]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[19]_i_1__0 
       (.I0(rd_data0[19]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[19]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[1]_i_1__0 
       (.I0(rd_data0[1]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[20]_i_1__0 
       (.I0(rd_data0[20]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[20]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[21]_i_1__0 
       (.I0(rd_data0[21]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[21]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[22]_i_1__0 
       (.I0(rd_data0[22]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[22]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[23]_i_1__0 
       (.I0(rd_data0[23]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[23]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[24]_i_1__0 
       (.I0(rd_data0[24]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[24]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[25]_i_1__0 
       (.I0(rd_data0[25]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[25]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[26]_i_1__0 
       (.I0(rd_data0[26]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[26]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[27]_i_1__0 
       (.I0(rd_data0[27]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[27]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[28]_i_1__0 
       (.I0(rd_data0[28]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[28]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[29]_i_1__0 
       (.I0(rd_data0[29]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[29]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[2]_i_1__0 
       (.I0(rd_data0[2]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[30]_i_1__0 
       (.I0(rd_data0[30]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[30]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[31]_i_1__0 
       (.I0(rd_data0[31]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[31]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[32]_i_1 
       (.I0(rd_data0[32]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[32]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[33]_i_1 
       (.I0(rd_data0[33]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[33]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[34]_i_1 
       (.I0(rd_data0[34]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[34]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[35]_i_1 
       (.I0(rd_data0[35]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[35]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[36]_i_1 
       (.I0(rd_data0[36]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[36]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[37]_i_1 
       (.I0(rd_data0[37]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[37]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[38]_i_1 
       (.I0(rd_data0[38]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[38]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[39]_i_1 
       (.I0(rd_data0[39]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[39]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[3]_i_1__0 
       (.I0(rd_data0[3]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[3]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[40]_i_1 
       (.I0(rd_data0[40]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[40]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[41]_i_1 
       (.I0(rd_data0[41]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[41]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[42]_i_1 
       (.I0(rd_data0[42]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[42]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[43]_i_1 
       (.I0(rd_data0[43]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[43]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[44]_i_1 
       (.I0(rd_data0[44]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[44]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[45]_i_1 
       (.I0(rd_data0[45]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[45]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[46]_i_1 
       (.I0(rd_data0[46]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[46]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[47]_i_1 
       (.I0(rd_data0[47]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[47]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[48]_i_1 
       (.I0(rd_data0[48]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[48]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[49]_i_1 
       (.I0(rd_data0[49]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[49]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[4]_i_1__0 
       (.I0(rd_data0[4]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[4]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[50]_i_1 
       (.I0(rd_data0[50]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[50]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[51]_i_1 
       (.I0(rd_data0[51]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[51]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[52]_i_1 
       (.I0(rd_data0[52]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[52]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[53]_i_1 
       (.I0(rd_data0[53]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[53]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[54]_i_1 
       (.I0(rd_data0[54]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[54]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[55]_i_1 
       (.I0(rd_data0[55]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[55]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[56]_i_1 
       (.I0(rd_data0[56]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[56]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[57]_i_1 
       (.I0(rd_data0[57]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[57]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[58]_i_1 
       (.I0(rd_data0[58]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[58]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[59]_i_1 
       (.I0(rd_data0[59]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[59]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[5]_i_1__0 
       (.I0(rd_data0[5]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[5]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[60]_i_1 
       (.I0(rd_data0[60]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[60]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[61]_i_1 
       (.I0(rd_data0[61]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[61]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[62]_i_1 
       (.I0(rd_data0[62]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[62]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[63]_i_2 
       (.I0(rd_data0[63]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[63]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[6]_i_1__0 
       (.I0(rd_data0[6]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[6]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[7]_i_1__0 
       (.I0(rd_data0[7]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[7]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[8]_i_1__0 
       (.I0(rd_data0[8]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[8]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \rd_data[9]_i_1__0 
       (.I0(rd_data0[9]),
        .I1(axi_aresetn_i),
        .I2(ow_reg_rst),
        .O(\rd_data[9]_i_1__0_n_0 ));
  FDRE rd_data_en_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(rd_data_en),
        .Q(rd_data_en_reg_0),
        .R(1'b0));
  FDRE \rd_data_reg[0] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[0]_i_1__0_n_0 ),
        .Q(ow_rd_data[0]),
        .R(1'b0));
  FDRE \rd_data_reg[10] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[10]_i_1__0_n_0 ),
        .Q(ow_rd_data[10]),
        .R(1'b0));
  FDRE \rd_data_reg[11] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[11]_i_1__0_n_0 ),
        .Q(ow_rd_data[11]),
        .R(1'b0));
  FDRE \rd_data_reg[12] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[12]_i_1__0_n_0 ),
        .Q(ow_rd_data[12]),
        .R(1'b0));
  FDRE \rd_data_reg[13] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[13]_i_1__0_n_0 ),
        .Q(ow_rd_data[13]),
        .R(1'b0));
  FDRE \rd_data_reg[14] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[14]_i_1__0_n_0 ),
        .Q(ow_rd_data[14]),
        .R(1'b0));
  FDRE \rd_data_reg[15] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[15]_i_1__0_n_0 ),
        .Q(ow_rd_data[15]),
        .R(1'b0));
  FDRE \rd_data_reg[16] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[16]_i_1__0_n_0 ),
        .Q(ow_rd_data[16]),
        .R(1'b0));
  FDRE \rd_data_reg[17] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[17]_i_1__0_n_0 ),
        .Q(ow_rd_data[17]),
        .R(1'b0));
  FDRE \rd_data_reg[18] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[18]_i_1__0_n_0 ),
        .Q(ow_rd_data[18]),
        .R(1'b0));
  FDRE \rd_data_reg[19] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[19]_i_1__0_n_0 ),
        .Q(ow_rd_data[19]),
        .R(1'b0));
  FDRE \rd_data_reg[1] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[1]_i_1__0_n_0 ),
        .Q(ow_rd_data[1]),
        .R(1'b0));
  FDRE \rd_data_reg[20] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[20]_i_1__0_n_0 ),
        .Q(ow_rd_data[20]),
        .R(1'b0));
  FDRE \rd_data_reg[21] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[21]_i_1__0_n_0 ),
        .Q(ow_rd_data[21]),
        .R(1'b0));
  FDRE \rd_data_reg[22] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[22]_i_1__0_n_0 ),
        .Q(ow_rd_data[22]),
        .R(1'b0));
  FDRE \rd_data_reg[23] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[23]_i_1__0_n_0 ),
        .Q(ow_rd_data[23]),
        .R(1'b0));
  FDRE \rd_data_reg[24] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[24]_i_1__0_n_0 ),
        .Q(ow_rd_data[24]),
        .R(1'b0));
  FDRE \rd_data_reg[25] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[25]_i_1__0_n_0 ),
        .Q(ow_rd_data[25]),
        .R(1'b0));
  FDRE \rd_data_reg[26] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[26]_i_1__0_n_0 ),
        .Q(ow_rd_data[26]),
        .R(1'b0));
  FDRE \rd_data_reg[27] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[27]_i_1__0_n_0 ),
        .Q(ow_rd_data[27]),
        .R(1'b0));
  FDRE \rd_data_reg[28] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[28]_i_1__0_n_0 ),
        .Q(ow_rd_data[28]),
        .R(1'b0));
  FDRE \rd_data_reg[29] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[29]_i_1__0_n_0 ),
        .Q(ow_rd_data[29]),
        .R(1'b0));
  FDRE \rd_data_reg[2] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[2]_i_1__0_n_0 ),
        .Q(ow_rd_data[2]),
        .R(1'b0));
  FDRE \rd_data_reg[30] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[30]_i_1__0_n_0 ),
        .Q(ow_rd_data[30]),
        .R(1'b0));
  FDRE \rd_data_reg[31] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[31]_i_1__0_n_0 ),
        .Q(ow_rd_data[31]),
        .R(1'b0));
  FDRE \rd_data_reg[32] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[32]_i_1_n_0 ),
        .Q(ow_rd_data[32]),
        .R(1'b0));
  FDRE \rd_data_reg[33] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[33]_i_1_n_0 ),
        .Q(ow_rd_data[33]),
        .R(1'b0));
  FDRE \rd_data_reg[34] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[34]_i_1_n_0 ),
        .Q(ow_rd_data[34]),
        .R(1'b0));
  FDRE \rd_data_reg[35] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[35]_i_1_n_0 ),
        .Q(ow_rd_data[35]),
        .R(1'b0));
  FDRE \rd_data_reg[36] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[36]_i_1_n_0 ),
        .Q(ow_rd_data[36]),
        .R(1'b0));
  FDRE \rd_data_reg[37] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[37]_i_1_n_0 ),
        .Q(ow_rd_data[37]),
        .R(1'b0));
  FDRE \rd_data_reg[38] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[38]_i_1_n_0 ),
        .Q(ow_rd_data[38]),
        .R(1'b0));
  FDRE \rd_data_reg[39] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[39]_i_1_n_0 ),
        .Q(ow_rd_data[39]),
        .R(1'b0));
  FDRE \rd_data_reg[3] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[3]_i_1__0_n_0 ),
        .Q(ow_rd_data[3]),
        .R(1'b0));
  FDRE \rd_data_reg[40] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[40]_i_1_n_0 ),
        .Q(ow_rd_data[40]),
        .R(1'b0));
  FDRE \rd_data_reg[41] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[41]_i_1_n_0 ),
        .Q(ow_rd_data[41]),
        .R(1'b0));
  FDRE \rd_data_reg[42] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[42]_i_1_n_0 ),
        .Q(ow_rd_data[42]),
        .R(1'b0));
  FDRE \rd_data_reg[43] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[43]_i_1_n_0 ),
        .Q(ow_rd_data[43]),
        .R(1'b0));
  FDRE \rd_data_reg[44] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[44]_i_1_n_0 ),
        .Q(ow_rd_data[44]),
        .R(1'b0));
  FDRE \rd_data_reg[45] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[45]_i_1_n_0 ),
        .Q(ow_rd_data[45]),
        .R(1'b0));
  FDRE \rd_data_reg[46] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[46]_i_1_n_0 ),
        .Q(ow_rd_data[46]),
        .R(1'b0));
  FDRE \rd_data_reg[47] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[47]_i_1_n_0 ),
        .Q(ow_rd_data[47]),
        .R(1'b0));
  FDRE \rd_data_reg[48] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[48]_i_1_n_0 ),
        .Q(ow_rd_data[48]),
        .R(1'b0));
  FDRE \rd_data_reg[49] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[49]_i_1_n_0 ),
        .Q(ow_rd_data[49]),
        .R(1'b0));
  FDRE \rd_data_reg[4] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[4]_i_1__0_n_0 ),
        .Q(ow_rd_data[4]),
        .R(1'b0));
  FDRE \rd_data_reg[50] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[50]_i_1_n_0 ),
        .Q(ow_rd_data[50]),
        .R(1'b0));
  FDRE \rd_data_reg[51] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[51]_i_1_n_0 ),
        .Q(ow_rd_data[51]),
        .R(1'b0));
  FDRE \rd_data_reg[52] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[52]_i_1_n_0 ),
        .Q(ow_rd_data[52]),
        .R(1'b0));
  FDRE \rd_data_reg[53] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[53]_i_1_n_0 ),
        .Q(ow_rd_data[53]),
        .R(1'b0));
  FDRE \rd_data_reg[54] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[54]_i_1_n_0 ),
        .Q(ow_rd_data[54]),
        .R(1'b0));
  FDRE \rd_data_reg[55] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[55]_i_1_n_0 ),
        .Q(ow_rd_data[55]),
        .R(1'b0));
  FDRE \rd_data_reg[56] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[56]_i_1_n_0 ),
        .Q(ow_rd_data[56]),
        .R(1'b0));
  FDRE \rd_data_reg[57] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[57]_i_1_n_0 ),
        .Q(ow_rd_data[57]),
        .R(1'b0));
  FDRE \rd_data_reg[58] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[58]_i_1_n_0 ),
        .Q(ow_rd_data[58]),
        .R(1'b0));
  FDRE \rd_data_reg[59] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[59]_i_1_n_0 ),
        .Q(ow_rd_data[59]),
        .R(1'b0));
  FDRE \rd_data_reg[5] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[5]_i_1__0_n_0 ),
        .Q(ow_rd_data[5]),
        .R(1'b0));
  FDRE \rd_data_reg[60] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[60]_i_1_n_0 ),
        .Q(ow_rd_data[60]),
        .R(1'b0));
  FDRE \rd_data_reg[61] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[61]_i_1_n_0 ),
        .Q(ow_rd_data[61]),
        .R(1'b0));
  FDRE \rd_data_reg[62] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[62]_i_1_n_0 ),
        .Q(ow_rd_data[62]),
        .R(1'b0));
  FDRE \rd_data_reg[63] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[63]_i_2_n_0 ),
        .Q(ow_rd_data[63]),
        .R(1'b0));
  FDRE \rd_data_reg[6] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[6]_i_1__0_n_0 ),
        .Q(ow_rd_data[6]),
        .R(1'b0));
  FDRE \rd_data_reg[7] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[7]_i_1__0_n_0 ),
        .Q(ow_rd_data[7]),
        .R(1'b0));
  FDRE \rd_data_reg[8] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[8]_i_1__0_n_0 ),
        .Q(ow_rd_data[8]),
        .R(1'b0));
  FDRE \rd_data_reg[9] 
       (.C(axi_clk_i),
        .CE(\rd_data_reg[63]_0 ),
        .D(\rd_data[9]_i_1__0_n_0 ),
        .Q(ow_rd_data[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \reg[mem_addr][5]_i_10 
       (.I0(rd_data_en_reg_0),
        .I1(\reg[mem_addr][5]_i_4 ),
        .O(rd_data_en_reg_1));
  FDRE wr_done_reg
       (.C(axi_clk_i),
        .CE(1'b1),
        .D(wr_done),
        .Q(mem_wr_done),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
