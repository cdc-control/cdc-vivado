# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "clk_frequency_g" -parent ${Page_0}
  ipgui::add_param $IPINST -name "invert_pullup_g" -parent ${Page_0}
  ipgui::add_param $IPINST -name "invert_rx_g" -parent ${Page_0}
  ipgui::add_param $IPINST -name "invert_tx_g" -parent ${Page_0}
  ipgui::add_param $IPINST -name "max_devices_g" -parent ${Page_0}
  ipgui::add_param $IPINST -name "use_tmr_bram_g" -parent ${Page_0}


}

proc update_PARAM_VALUE.clk_frequency_g { PARAM_VALUE.clk_frequency_g } {
	# Procedure called to update clk_frequency_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.clk_frequency_g { PARAM_VALUE.clk_frequency_g } {
	# Procedure called to validate clk_frequency_g
	return true
}

proc update_PARAM_VALUE.invert_pullup_g { PARAM_VALUE.invert_pullup_g } {
	# Procedure called to update invert_pullup_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.invert_pullup_g { PARAM_VALUE.invert_pullup_g } {
	# Procedure called to validate invert_pullup_g
	return true
}

proc update_PARAM_VALUE.invert_rx_g { PARAM_VALUE.invert_rx_g } {
	# Procedure called to update invert_rx_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.invert_rx_g { PARAM_VALUE.invert_rx_g } {
	# Procedure called to validate invert_rx_g
	return true
}

proc update_PARAM_VALUE.invert_tx_g { PARAM_VALUE.invert_tx_g } {
	# Procedure called to update invert_tx_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.invert_tx_g { PARAM_VALUE.invert_tx_g } {
	# Procedure called to validate invert_tx_g
	return true
}

proc update_PARAM_VALUE.max_devices_g { PARAM_VALUE.max_devices_g } {
	# Procedure called to update max_devices_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.max_devices_g { PARAM_VALUE.max_devices_g } {
	# Procedure called to validate max_devices_g
	return true
}

proc update_PARAM_VALUE.use_tmr_bram_g { PARAM_VALUE.use_tmr_bram_g } {
	# Procedure called to update use_tmr_bram_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.use_tmr_bram_g { PARAM_VALUE.use_tmr_bram_g } {
	# Procedure called to validate use_tmr_bram_g
	return true
}


proc update_MODELPARAM_VALUE.clk_frequency_g { MODELPARAM_VALUE.clk_frequency_g PARAM_VALUE.clk_frequency_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.clk_frequency_g}] ${MODELPARAM_VALUE.clk_frequency_g}
}

proc update_MODELPARAM_VALUE.max_devices_g { MODELPARAM_VALUE.max_devices_g PARAM_VALUE.max_devices_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.max_devices_g}] ${MODELPARAM_VALUE.max_devices_g}
}

proc update_MODELPARAM_VALUE.invert_tx_g { MODELPARAM_VALUE.invert_tx_g PARAM_VALUE.invert_tx_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.invert_tx_g}] ${MODELPARAM_VALUE.invert_tx_g}
}

proc update_MODELPARAM_VALUE.invert_rx_g { MODELPARAM_VALUE.invert_rx_g PARAM_VALUE.invert_rx_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.invert_rx_g}] ${MODELPARAM_VALUE.invert_rx_g}
}

proc update_MODELPARAM_VALUE.invert_pullup_g { MODELPARAM_VALUE.invert_pullup_g PARAM_VALUE.invert_pullup_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.invert_pullup_g}] ${MODELPARAM_VALUE.invert_pullup_g}
}

proc update_MODELPARAM_VALUE.use_tmr_bram_g { MODELPARAM_VALUE.use_tmr_bram_g PARAM_VALUE.use_tmr_bram_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.use_tmr_bram_g}] ${MODELPARAM_VALUE.use_tmr_bram_g}
}

